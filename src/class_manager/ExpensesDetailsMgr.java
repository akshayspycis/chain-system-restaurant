package class_manager;

import data_manager.ExpensesDetails;
import data_manager.configuration.Config;

public class ExpensesDetailsMgr {
       //method to insert ExpensesDetails in database
    public boolean insExpensesDetails(ExpensesDetails expenses_details) {
        try { 
                   Config.sql = "insert into expenses_details ( "                   
                    + "type) "
                    + "values (?)";
            
                Config.pstmt = Config.conn.prepareStatement(Config.sql);

                Config.pstmt.setString(1, expenses_details.getType());
                
                int x = Config.pstmt.executeUpdate();

                if (x>0) {
                    Config.config_mgr.loadExpensesDetails();
                    return true;
                } else {
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to Update ExpensesDetails in database 
   public boolean updExpensesDetails(ExpensesDetails expenses_details) {
        try {
            Config.sql = "update expenses_details set"
                    + " type = ? "
                    + " where expenses_details_id = '"+expenses_details.getExpenses_details_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, expenses_details.getType());
   
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.config_mgr.loadExpensesDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
      //method to delete ExpensesDetails in database
   public boolean delExpensesDetails(String expenses_id) {
        try {
            Config.sql = "delete from expenses_details where expenses_details_id = '"+expenses_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadExpensesDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
