/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package class_manager;

import data_manager.TransportDetails;
import data_manager.configuration.Config;

/**
 *
 * @author akshay
 */
public class TransportDetailsMgr {
//method to insert TransportDetails in database
    public boolean insTransportDetails(TransportDetails transport_details) {
        try {          
            Config.sql = "insert into transport_details ("                   
                     + "bike_id,"
                    + "_date,"
                    + "_month,"
                    + "_year,"
                    + "str_reading,"
                    + "end_reading,"
                    + "diff_reading,"
                    + "message_id )"
                    + " values (?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, transport_details.getBike_id());
            Config.pstmt.setString(2, transport_details.getDate());
            Config.pstmt.setString(3, transport_details.getMonth());
            Config.pstmt.setString(4, transport_details.getYear());
            Config.pstmt.setString(5, transport_details.getStr_reading());
            Config.pstmt.setString(6, transport_details.getEnd_reading());
            Config.pstmt.setString(7, transport_details.getDiff_reading());
            Config.pstmt.setString(8, transport_details.getMessage_id());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to update TransportDetails in database 
   public boolean updTransportDetails(TransportDetails transport_details) {
        try {
            Config.sql = "update transport_details set"
                    + " bike_id = ?, "
                    + " _date = ?, "
                    + " _month = ?, "
                    + " _year = ?, "
                    + " str_reading = ?, "
                    + " end_reading = ?, "
                    + " diff_reading = ?, "
                    + " message_id = ? "
                    + " where transport_details_id = '"+transport_details.getTransport_details_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, transport_details.getBike_id());
            Config.pstmt.setString(2, transport_details.getDate());
            Config.pstmt.setString(3, transport_details.getMonth());
            Config.pstmt.setString(4, transport_details.getYear());
            Config.pstmt.setString(5, transport_details.getStr_reading());
            Config.pstmt.setString(6, transport_details.getEnd_reading());
            Config.pstmt.setString(7, transport_details.getDiff_reading());
            Config.pstmt.setString(8, transport_details.getMessage_id());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete TransportDetails in database
   public boolean deltbl_adv_report_model(String transport_details_id)
   {
        try {          
            Config.sql = "delete from transport_details where transport_details_id = '"+transport_details_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }        
}
