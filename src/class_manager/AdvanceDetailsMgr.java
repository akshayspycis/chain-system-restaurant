package class_manager;

import data_manager.AdvanceDetails;
import data_manager.configuration.Config;

public class AdvanceDetailsMgr {

    //method to insert AdvanceDetails in database
    public boolean insAdvanceDetails(AdvanceDetails advance_details) {
        try { 
             
            Config.sql = "insert into advance_details ("                   
                    + "employee_id,"
                    + "_date,"
                    + "_month,"
                    + "_year,"
                    + "amount,"
                    + "message_id)"
                    + " values ("
                    + "?,?,?,?,?,?"
                    + ")";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, advance_details.getEmployee_id());
            Config.pstmt.setString(2, advance_details.getDate());
            Config.pstmt.setString(3, advance_details.getMonth());
            Config.pstmt.setString(4, advance_details.getYear());
            Config.pstmt.setString(5, advance_details.getAmount());
            Config.pstmt.setString(6, advance_details.getMessage_id());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to update AdvanceDetails in database 
   public boolean updAdvanceDetails(AdvanceDetails advance_details) {
        try {
            Config.sql = "update advance_details set "
                    + "employee_id =?,"
                    + "_date = ?,"
                    + "_month = ?,"
                    + "_year = ?,"
                    + "amount = ?,"
                    + "message_id =? "
                    + " where advance_id = '"+advance_details.getAdvance_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, advance_details.getEmployee_id());
            Config.pstmt.setString(2, advance_details.getDate());
            Config.pstmt.setString(3, advance_details.getMonth());
            Config.pstmt.setString(4, advance_details.getYear());
            Config.pstmt.setString(5, advance_details.getAmount());
            Config.pstmt.setString(6, advance_details.getMessage_id());
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                return true;
            } else {
                return false;
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete AdvanceDetails in database
   public boolean delAdvanceDetails(String advance_id)
   {
        try { 
             Config.sql = "delete from advance_details where advance_id = '"+advance_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
            } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
