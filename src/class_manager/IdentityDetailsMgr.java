package class_manager;

import data_manager.IdentityDetails;
import data_manager.configuration.Config;

public class IdentityDetailsMgr {
       //method to insert IdentityDetails in database
    public boolean insIdentityDetails(IdentityDetails identity_details) {
        try {          
            Config.sql = "insert into identity_details ("                   
                    + "identity_type,"
                    + "identity_no )"
                    + " values (?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, identity_details.getIdentity_type());
            Config.pstmt.setString(2, identity_details.getIdentity_no());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadIdentityDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to IdentityDetails in database 
   public boolean updIdentityDetails(IdentityDetails identity_details) {
        try {
            Config.sql = "update identity_details set"
                    + " identity_type = ?, "
                    + " identity_no = ? "
                    + " where identity_id = '"+identity_details.getIdentity_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, identity_details.getIdentity_type());
            Config.pstmt.setString(2, identity_details.getIdentity_no());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.config_mgr.loadIdentityDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete IdentityDetails in database
   public boolean delIdentityDetails(String identity_id)
   {
        try {          
            Config.sql = "delete from identity_details where identity_id = '"+identity_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadIdentityDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
