package class_manager;

import data_manager.AttendenceDetails;
import data_manager.configuration.Config;
import java.util.ArrayList;

public class AttendenceDetailsMgr {
    //method to insert AttendenceDetails in database
    public boolean insAttendenceDetails(ArrayList<AttendenceDetails> attendence_details) {
        try {          
            Config.sql = "insert into attendence_details ("                   
                    + "employee_id,"
                    + "_date,"
                    + "_month,"
                    + "_year,"
                    + "status,"
                    + "time_in,"
                    + "time_out,"
                    + "message_id)"
                    + " values (?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            for(int i = 0; i < attendence_details.size(); i++) {
            Config.pstmt.setString(1, attendence_details.get(i).getEmployee_id());
            Config.pstmt.setString(2, attendence_details.get(i).getDate());
            Config.pstmt.setString(3, attendence_details.get(i).getMonth());
            Config.pstmt.setString(4, attendence_details.get(i).getYear());
            Config.pstmt.setString(5, attendence_details.get(i).getStatus());
            Config.pstmt.setString(6, attendence_details.get(i).getTime_in());
            Config.pstmt.setString(7, attendence_details.get(i).getTime_out());
            Config.pstmt.setString(8, attendence_details.get(i).getMessage_id());
            Config.pstmt.addBatch();
            }
            int[]arr=Config.pstmt.executeBatch();
            Config.pstmt.close();
            if(arr.length==attendence_details.size()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to AttendenceDetails in database 
   public boolean updAttendenceDetails(AttendenceDetails attendence_details) {
        try {
            Config.sql = "update attendence_details set"
                    + " employee_id = ?, "
                    + " _date = ?, "
                    + " _month = ?, "
                    + " _year = ?, "
                    + " status = ?, "
                    + " time_in = ?, "
                    + " time_out = ?, "
                    + " message_id = ? "
                    + " where attendence_id = '"+attendence_details.getAttendence_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, attendence_details.getEmployee_id());
            Config.pstmt.setString(2, attendence_details.getDate());
            Config.pstmt.setString(3, attendence_details.getMonth());
            Config.pstmt.setString(4, attendence_details.getYear());
            Config.pstmt.setString(5, attendence_details.getStatus());
            Config.pstmt.setString(6, attendence_details.getTime_in());
            Config.pstmt.setString(7, attendence_details.getTime_out());
            Config.pstmt.setString(8, attendence_details.getMessage_id());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete AttendenceDetails in database
   public boolean delAttendenceDetails(String attendence_id)
   {
        try {          
            Config.sql = "delete from attendence_details where attendence_id= '"+attendence_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
