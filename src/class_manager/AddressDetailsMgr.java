package class_manager;

import data_manager.AddressDetails;
import data_manager.configuration.Config;

public class AddressDetailsMgr {
    
    //method to insert address_details in database
    public boolean insAddressDetails(AddressDetails addressdetails) {
        try {          
            Config.sql = "insert into address_details ("                
                    + "address_1, "
                    + "address_2, "
                    + "city, "
                    + "pincode, "
                    + "state) "
                    + "values (?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, addressdetails.getAddress_1());
            Config.pstmt.setString(2, addressdetails.getAddress_2());
            Config.pstmt.setString(3, addressdetails.getCity());
            Config.pstmt.setString(4, addressdetails.getPincode());
            Config.pstmt.setString(5, addressdetails.getState());
            
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.config_mgr.loadAddressDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to Update AddressDetails in database 
   public boolean updAddressDetails(AddressDetails address_details) {
        try {
            Config.sql = "update address_details set"
                    + " address_1 = ?, "
                    + " address_2 = ?, "
                    + " city = ?, "
                    + " pincode = ?, "
                    + " state = ? "
                    + " where address_id = '"+address_details.getAddress_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, address_details.getAddress_1());
            Config.pstmt.setString(2, address_details.getAddress_2());
            Config.pstmt.setString(3, address_details.getCity());
            Config.pstmt.setString(4, address_details.getPincode());
            Config.pstmt.setString(5, address_details.getState());            
            
            int x = Config.pstmt.executeUpdate();            
            if (x>0) {
                Config.config_mgr.loadAddressDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
   
    //method to delete AddressDetails in database
    public boolean delAddressDetails(String address_id) {
        try {          
            Config.sql = "delete from address_details where address_id = '"+address_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadAddressDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
