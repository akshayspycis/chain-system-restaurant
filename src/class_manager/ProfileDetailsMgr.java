package class_manager;

import data_manager.ProfileDetails;
import data_manager.configuration.Config;

public class ProfileDetailsMgr {
         //method to insert ProfileDetail in database
    public boolean insProfileDetails(ProfileDetails profile_details) {
        try {          
            Config.sql = "insert into profile_details ("                   
                    + "name,"
                    + "dob,"
                    + "gender )"
                    + " values (?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, profile_details.getName());
            Config.pstmt.setString(2, profile_details.getDob());
            Config.pstmt.setString(3, profile_details.getGender());
           
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadProfileDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to ProfileDetails in database 
   public boolean updProfileDetails(ProfileDetails profile_details) {
        try {
            Config.sql = "update profile_details set"
                    + " name = ?, "
                    + " dob = ?, "
                    + " gender = ? "
                    + " where profile_id = '"+profile_details.getProfile_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, profile_details.getName());
            Config.pstmt.setString(2, profile_details.getDob());
            Config.pstmt.setString(3, profile_details.getGender());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                System.out.println("akkipppppp");
                Config.config_mgr.loadProfileDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete ProfileDetails in database
   public boolean delProfileDetails(String profile_id)
   {
        try {          
            Config.sql = "delete from profile_details where profile_id = '"+profile_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadProfileDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
