package class_manager;

import data_manager.ReturnStock;
import data_manager.configuration.Config;
import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class ReturnStockMgr {
         
    //method to insert ReturnStock in database
    public boolean insReturnStock(ArrayList<ReturnStock> return_stock) {
        try {          
            Config.sql = "insert into return_stock ("                   
                    + "item_id,"
                    + "item_list_id,"
                    + "_date,"
                    + "_month,"
                    + "_year,"
                    + "quantity,"
                    + "bill_type_id )"
                    + " values (?,?,?,?,?,?,?)";
            
    Config.pstmt = Config.conn.prepareStatement(Config.sql);
            for(int i = 0; i < return_stock.size(); i++) {
            ReturnStock rs = return_stock.get(i);
            Config.pstmt.setString(1, rs.getItem_id());
            Config.pstmt.setString(2, rs.getItem_list_id());
            Config.pstmt.setString(3, rs.getDate());
            Config.pstmt.setString(4, rs.getMonth());
            Config.pstmt.setString(5, rs.getYear());
            Config.pstmt.setString(6, rs.getQuantity());
            Config.pstmt.setString(7, rs.getBill_type_id());
            Config.pstmt.addBatch();
            }
            int[]arr=Config.pstmt.executeBatch();
            Config.pstmt.close();
            if(arr.length==return_stock.size()) {
                        try {
                            int i=0;
                            for (int j = 0; j < return_stock.size(); j++) {      
                            for ( i= 0; i < Config.config_stock_details.size(); i++) {
                            if(return_stock.get(j).getItem_id().equals(Config.config_stock_item.get(i).getItem_id())){ 
                            Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble(Config.config_stock_details.get(i).getTotal())-Double.parseDouble(return_stock.get(j).getQuantity()))+"' where item_id ='"+ Config.config_stock_details.get(i).getItem_id()+"'";
                            Config.stmt.addBatch(Config.sql);
                            break;
                            }
                            }
                            }
                            int[]a=Config.stmt.executeBatch();
                            if(a.length==return_stock.size()){
                                Config.config_mgr.loadStockDetails();
                                return true;
                            }else{
                                return false;    
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            return false;
                        
                        }    
            } else {
            return false;
            }
        }catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to ReturnStock in database 
   public boolean updReturnStock(ReturnStock return_stock,double quantity) {
             try {
            Config.sql = "update return_stock set"
                    + " quantity = ? "
                    + " where return_id = '"+return_stock.getReturn_id()+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, return_stock.getQuantity());
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                for (int i= 0; i < Config.config_stock_details.size(); i++) {
                if(return_stock.getItem_id().equals(Config.config_stock_item.get(i).getItem_id())){ 
                Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble( Config.config_stock_details.get(i).getTotal())-quantity)+"' where item_id ='"+ Config.config_stock_details.get(i).getItem_id()+"'";
                break;
                }
                }
                int y=Config.stmt.executeUpdate(Config.sql);
                if (y>0) {
                    Config.config_mgr.loadStockDetails();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete ReturnStock in database
   public boolean delReturnStock(String return_id)
   {
        try {   
            String quantity="";
            String item_id="";
                Config.sql ="SELECT * FROM return_stock where return_id="+return_id;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                if(Config.rs.next()) {
                  quantity=Config.rs.getString("quantity");
                  item_id=Config.rs.getString("item_id");
                }    
            Config.sql = "delete from return_stock where return_id = '"+return_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                for (int i= 0; i < Config.config_stock_details.size(); i++) {
                if(item_id.equals(Config.config_stock_item.get(i).getItem_id())){ 
                Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble(Config.config_stock_details.get(i).getTotal())+Double.parseDouble(quantity))+"' where item_id ='"+ item_id+"'";
                break;
                }
                }
                int y=Config.stmt.executeUpdate(Config.sql);
                if (y>0) {
                    Config.config_mgr.loadStockDetails();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
