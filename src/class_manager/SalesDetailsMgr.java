/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package class_manager;

import data_manager.SalesDetails;
import data_manager.configuration.Config;
import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class SalesDetailsMgr {
    //method to insert SalesDetails in database
    public boolean insSalesDetailsItem(ArrayList<SalesDetails> sales_details) {
        try {          
            Config.sql = "insert into sales_details ("                   
                    + "distribution_details_id,"
                    + "item_id,"
                    + "item_list_id,"
                    + "quantity,"
                    + "sale )"
                    + " values (?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            for(int i = 0; i < sales_details.size(); i++) {
            SalesDetails rs = sales_details.get(i);
            Config.pstmt.setString(1, rs.getDistribution_details_id());
            Config.pstmt.setString(2, rs.getItem_id());
            Config.pstmt.setString(3, rs.getItem_list_id());
            Config.pstmt.setString(4, rs.getQuantity());
            Config.pstmt.setString(5, rs.getSale());
            Config.pstmt.addBatch();
            }
            int[]arr=Config.pstmt.executeBatch();
            Config.pstmt.close();
            if(arr.length==sales_details.size()) {
            return true;
            } else {
            return false;
            }
        }catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//    //----------------------------------------------------------------------------------------------
//    
//   //method to SalesDetailsItem in database 
   public boolean updSalesDetailsItem(SalesDetails sales_details) {
        try {
            Config.sql = "update sales_details set"
                    + " quantity = ? "
                    + " where sales_details_id = '"+sales_details.getSales_details_id()+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, sales_details.getQuantity());
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                    return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//   //----------------------------------------------------------------------------------------------
//   
//   //method to delete SalesDetailsItem in database
   public boolean delSalesDetailsItem(String sales_details_id)
   {
        try {          
            Config.sql = "delete from sales_details where sales_details_id = '"+sales_details_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                return true;
            }else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }          
}
