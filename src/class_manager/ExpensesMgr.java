/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package class_manager;

import data_manager.AddressDetails;
import data_manager.Expenses;
import data_manager.configuration.Config;

/**
 *
 * @author infopark
 */
public class ExpensesMgr {
        //method to insert address_details in database
    public boolean insExpenses(Expenses expenses) {
        try {          
            Config.sql = "insert into expenses ("                
                    + "expenses_details_id, "
                    + "_date, "
                    + "_month, "
                    + "_year, "
                    + "amount, "
                    + "message_id) "
                    + "values (?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, expenses.getExpenses_details_id());
            Config.pstmt.setString(2, expenses.getDate());
            Config.pstmt.setString(3, expenses.getMonth());
            Config.pstmt.setString(4, expenses.getYear());
            Config.pstmt.setString(5, expenses.getAmount());
            Config.pstmt.setString(6, expenses.getMessage_id());
            
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to Update Expenses in database 
   public boolean updExpenses(Expenses expenses) {
        try {
            Config.sql = "update expenses set"
                    + " expenses_details_id = ?, "
                    + " _date = ?, "
                    + " _month = ?, "
                    + " _year = ?, "
                    + " amount = ? ,"
                    + " message_id = ? "
                    + " where expenses_id = '"+expenses.getExpenses_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, expenses.getExpenses_details_id());
            Config.pstmt.setString(2, expenses.getDate());
            Config.pstmt.setString(3, expenses.getMonth());
            Config.pstmt.setString(4, expenses.getYear());
            Config.pstmt.setString(5, expenses.getAmount());
            Config.pstmt.setString(6, expenses.getMessage_id());
            int x = Config.pstmt.executeUpdate();            
            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
   
    //method to delete Expenses in database
    public boolean delExpenses(String expenses_id) {
        try {          
            Config.sql = "delete from expenses where expenses_id = '"+expenses_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
