package class_manager;

import data_manager.DemageStock;
import data_manager.configuration.Config;

public class DemageStockMgr {
         
    //method to insert ReceiveStock in database
    public boolean insDemageStock(DemageStock demage_stock) {
        try {          
            Config.sql = "insert into demage_stock ("                   
                    + "item_id,"
                    + "item_list_id,"
                    + "_date,"
                    + "_month,"
                    + "_year,"
                    + "quantity,"
                    + "message_id )"
                    + " values (?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, demage_stock.getItem_id());
            Config.pstmt.setString(2, demage_stock.getItem_list_id());
            Config.pstmt.setString(3, demage_stock.getDate());
            Config.pstmt.setString(4, demage_stock.getMonth());
            Config.pstmt.setString(5, demage_stock.getYear());
            Config.pstmt.setString(6, demage_stock.getQuantity());
            Config.pstmt.setString(7, demage_stock.getMessage_id());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                   for (int i= 0; i < Config.config_stock_details.size(); i++) {
                            if(demage_stock.getItem_id().equals(Config.config_stock_item.get(i).getItem_id())){ 
                            Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble( Config.config_stock_details.get(i).getTotal())-Double.parseDouble(demage_stock.getQuantity()))+"' where item_id ='"+ Config.config_stock_details.get(i).getItem_id()+"'";
                            break;
                        }
                    }
                int y=Config.stmt.executeUpdate(Config.sql);
                if (y>0) {
                    Config.config_mgr.loadStockDetails();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to ReceiveStock in database 
   public boolean updDemageStock(DemageStock demage_stock,double quantity) {
        try {
            Config.sql = "update demage_stock set"
                    + " item_id = ?, "
                    + " item_list_id = ?, "
                    + " _date = ?, "
                    + " _month = ?, "
                    + " _year = ?, "
                    + " quantity = ?, "
                    + " message_id = ? "
                    + " where demage_stock_id = '"+demage_stock.getDemage_stock_id()+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, demage_stock.getItem_id());
            Config.pstmt.setString(2, demage_stock.getItem_list_id());
            Config.pstmt.setString(3, demage_stock.getDate());
            Config.pstmt.setString(4, demage_stock.getMonth());
            Config.pstmt.setString(5, demage_stock.getYear());
            Config.pstmt.setString(6, demage_stock.getQuantity());
            Config.pstmt.setString(7, demage_stock.getMessage_id());            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                for (int i= 0; i < Config.config_stock_details.size(); i++) {
                if(demage_stock.getItem_id().equals(Config.config_stock_item.get(i).getItem_id())){ 
                Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble( Config.config_stock_details.get(i).getTotal())+quantity)+"' where item_id ='"+ Config.config_stock_details.get(i).getItem_id()+"'";
                break;
                }
                }
                int y=Config.stmt.executeUpdate(Config.sql);
                if (y>0) {
                    Config.config_mgr.loadStockDetails();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete ReceiveStock in database
   public boolean delReceiveStock(String demage_stock_id)
   {
        try {          
            String quantity="";
            String item_id="";
            Config.sql ="SELECT * FROM demage_stock where demage_stock_id="+demage_stock_id;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if(Config.rs.next()) {
                  quantity=Config.rs.getString("quantity");
                  item_id=Config.rs.getString("item_id");
            }    
            Config.sql = "delete from demage_stock where demage_stock_id = '"+demage_stock_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                for (int i= 0; i < Config.config_stock_details.size(); i++) {
                if(item_id.equals(Config.config_stock_item.get(i).getItem_id())){ 
                Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble( Config.config_stock_details.get(i).getTotal())+Double.parseDouble(quantity))+"' where item_id ='"+ item_id+"'";
                break;
                }
                }
                int y=Config.stmt.executeUpdate(Config.sql);
                if (y>0) {
                    Config.config_mgr.loadStockDetails();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
