/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package class_manager;

import data_manager.MaintenanceType;
import data_manager.configuration.Config;

/**
 *
 * @author akshay
 */
public class MaintenanceTypeMgr {
    //method to insert MaintenanceType in database
    public boolean insMaintenanceType(MaintenanceType maintenance_type) {
        try { 
            Config.sql = "insert into maintenance_type ("                   
                    + "type,"
                    + "other )"
                    + " values"
                    + "(?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, maintenance_type.getType());
            Config.pstmt.setString(2, maintenance_type.getOther());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
            Config.config_mgr.loadMaintenanceType();
            return true;
            } else {
            return false;
            }
            } catch (Exception ex) {
            ex.printStackTrace();
            return false;
            }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to update MaintenanceType in database 
   public boolean updMaintenanceType(MaintenanceType maintenance_type) {
        try {
            Config.sql = "update maintenance_type set"
                    + " type = ?, "
                    + " other = ? "
                    + " where maintenance_type_id = '"+maintenance_type.getMaintenance_type_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, maintenance_type.getType());
            Config.pstmt.setString(2, maintenance_type.getOther());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
            Config.config_mgr.loadMaintenanceType();
            return true;
            } else {
            return false;
            }
            } catch (Exception ex) {
            ex.printStackTrace();
            return false;
            }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete MaintenanceType in database
   public boolean delMaintenanceType(String maintenance_type_id)
   {
        try { 
            Config.sql = "delete from maintenance_type where maintenance_type_id = '"+maintenance_type_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadMaintenanceType();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
