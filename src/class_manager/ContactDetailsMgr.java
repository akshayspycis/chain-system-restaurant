package class_manager;

import data_manager.ContactDetails;
import data_manager.configuration.Config;

public class ContactDetailsMgr {
   //method to insert ContactDetails in database
    public boolean insContactDetails(ContactDetails contact_details) {
        try {          
            Config.sql = "insert into contact_details ("
                    + "contact_no, "
                    + "alt_contact_no, "
                    + "email, "
                    + "other) "
                    + "values (?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, contact_details.getContact_no());
            Config.pstmt.setString(2, contact_details.getAlt_contact_no());
            Config.pstmt.setString(3, contact_details.getEmail());
            Config.pstmt.setString(4, contact_details.getOther());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadContactDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to ContactDetails in database 
   public boolean updContactDetails(ContactDetails contact_details) {
        try {
            Config.sql = "update contact_details set"
                    + " contact_no = ?, "
                    + " alt_contact_no = ?, "
                    + " email = ?, "
                    + " other = ? "
                    + " where contact_id = '"+contact_details.getContact_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, contact_details.getContact_no());
            Config.pstmt.setString(2, contact_details.getAlt_contact_no());
            Config.pstmt.setString(3, contact_details.getEmail());
            Config.pstmt.setString(4, contact_details.getOther());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.config_mgr.loadContactDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete ContactDetails in database
   public boolean delContactDetails(String contact_id) {
        try {          
            Config.sql = "delete from contact_details where contact_id = '"+contact_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadContactDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}