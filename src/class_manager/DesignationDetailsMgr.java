package class_manager;

import data_manager.configuration.Config;
import data_manager.configuration.DesignationProfile;

public class DesignationDetailsMgr {
    
    //method to insert ContactDetails in database
    public boolean insDesignationDetails(DesignationProfile designation_profile) {
        try { 
            
                if(Config.contact_details_mgr.insContactDetails(designation_profile.getContact_details())){ 
                if (Config.address_details_mgr.insAddressDetails(designation_profile.getAddress_details())){
                    Config.sql = "insert into designation_details ("                   
                            + "designation, "
                            + "contact_person, " 
                            +"contact_id, "
                            +"address_id) "
                            + "values ("
                            + "?, "
                            + "?, "
                            + "(select max(contact_id) from contact_details), "
                            + "(select max(address_id) from address_details) "
                            + ")";

                    Config.pstmt = Config.conn.prepareStatement(Config.sql);

                    Config.pstmt.setString(1, designation_profile.getDesignation_details().getDesignation());
                    Config.pstmt.setString(2, designation_profile.getDesignation_details().getContact_person());

                    int x = Config.pstmt.executeUpdate();

                    if (x>0) {
                        Config.config_mgr.loadDesignationDetails();
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;    
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    //method to DesignationDetails in database 
    public boolean updDesignationDetails(DesignationProfile designation_profile) {
        try {
                if(Config.contact_details_mgr.updContactDetails(designation_profile.getContact_details())){
                if(Config.address_details_mgr.updAddressDetails(designation_profile.getAddress_details())){
                    Config.sql = "update designation_details set "
                            + "designation = ?, "
                            + "contact_person = ?, "
                            + "contact_id = ?, "
                            + "address_id = ? "
                            + "where designation_id = '"+designation_profile.getDesignation_details().getDesignation_id()+"'";

                    Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
                    Config.pstmt.setString(1, designation_profile.getDesignation_details().getDesignation());
                    Config.pstmt.setString(2, designation_profile.getDesignation_details().getContact_person());
                    Config.pstmt.setString(3, designation_profile.getContact_details().getContact_id());
                    Config.pstmt.setString(4, designation_profile.getAddress_details().getAddress_id());
                    
                    int x = Config.pstmt.executeUpdate();

                    if (x>0) {
                        Config.config_mgr.loadDesignationDetails();
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;    
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
   
    //method to delete DesignationDetails in database
    public boolean delDesignationDetails(String designation_id) {
        try {    
            if(Config.contact_details_mgr.delContactDetails(Config.config_designation_details.get(Config.id_designation_details.indexOf(designation_id)).getContact_id())){
            if(Config.address_details_mgr.delAddressDetails(Config.config_designation_details.get(Config.id_designation_details.indexOf(designation_id)).getAddress_id())){

                Config.sql = "delete from designation_details where designation_id = '"+designation_id+"'";
                Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
                    int x = Config.pstmt.executeUpdate();

                    if (x>0) {
                        Config.config_mgr.loadDesignationDetails();
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;    
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
