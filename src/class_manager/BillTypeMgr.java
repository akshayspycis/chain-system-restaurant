package class_manager;

import data_manager.BillType;
import data_manager.configuration.Config;

public class BillTypeMgr {
     //method to insert BillType in database
    public boolean insBillType(BillType bill_type) {
        try {          
            Config.sql = "insert into bill_type ("                   
                    + "bill_no,"
                    + "chalan_no,"
                    + "loading_charges,"
                    + "other_charges,"
                    + "_date,"
                    + "_month,"
                    + "_year,"
                    + "message_id)"
                    + " values (?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, bill_type.getBill_no());
            Config.pstmt.setString(2, bill_type.getChalan_no());
            Config.pstmt.setString(3, bill_type.getLoading_charges());
            Config.pstmt.setString(4, bill_type.getOther_charges());
            Config.pstmt.setString(5, bill_type.getDate());
            Config.pstmt.setString(6, bill_type.getMonth());
            Config.pstmt.setString(7, bill_type.getYear());
            Config.pstmt.setString(8, bill_type.getMessage_id());
            
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to Update BillType in database 
   public boolean updBillType(BillType bill_type) {
        try {
            Config.sql = "update bill_type set"
                    + " bill_no = ?, "
                    + " chalan_no = ?, "
                    + " loading_charges = ?, "
                    + " other_charges = ?, "
                    + " _date = ?, "
                    + " _month = ?, "
                    + " _year = ?, "
                    + " message_id = ?"
                    + " where bill_type_id = '"+bill_type.getBill_type_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, bill_type.getBill_no());
            Config.pstmt.setString(2, bill_type.getChalan_no());
            Config.pstmt.setString(3, bill_type.getLoading_charges());
            Config.pstmt.setString(4, bill_type.getOther_charges());
            Config.pstmt.setString(5, bill_type.getDate());
            Config.pstmt.setString(6, bill_type.getMonth());
            Config.pstmt.setString(7, bill_type.getYear());
            Config.pstmt.setString(8, bill_type.getMessage_id());
                     
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete BillType in database
   public boolean delBillType(String bill_type_id)
   {
        try {          
            Config.sql = "delete from bill_type where bill_type_id = '"+bill_type_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
