/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package class_manager;

import data_manager.ReturnItem;
import data_manager.configuration.Config;
import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class ReturnItemMgr {
//method to insert ReturnItem in database
    public boolean insReturnItem(ReturnItem return_item) {
        try {          
            Config.sql = "insert into return_item ("                   
                    + "distribution_details_id,"
                    + "item_id,"
                    + "item_list_id,"
                    + "quantity )"
                    + " values (?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, return_item.getDistribution_details_id());
            Config.pstmt.setString(2, return_item.getItem_id());
            Config.pstmt.setString(3, return_item.getItem_list_id());
            Config.pstmt.setString(4, return_item.getQuantity());
            Config.pstmt.addBatch();

            int arr=Config.pstmt.executeUpdate();
            if(arr >0) {
                try {
                int i=0;
                for ( i= 0; i < Config.config_stock_details.size(); i++) {
                if(return_item.getItem_id().equals(Config.config_stock_item.get(i).getItem_id())){ 
                Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble(Config.config_stock_details.get(i).getTotal())+Double.parseDouble(return_item.getQuantity()))+"' where item_id ='"+ Config.config_stock_details.get(i).getItem_id()+"'";
                Config.stmt.addBatch(Config.sql);
                break;
                }
                }
                int[]a=Config.stmt.executeBatch();
                if(a.length>0){
                Config.config_mgr.loadStockDetails();
                return true;
                }else{
                return false;    
                }
                } catch (Exception e) {
                return false;
                }    
            } else {
            return false;
            }
        }catch (Exception ex) {
            return false;
        }
    }
//    //----------------------------------------------------------------------------------------------
//    
//   //method to ReturnItem in database 
   public boolean updReturnItem(ReturnItem return_item,double quantity) {
        try {
            Config.sql = "update return_item set"
                    + " quantity = ? "
                    + " where return_item_id = '"+return_item.getReturn_item_id()+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, return_item.getQuantity());
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                int i= 0;
                for ( i= 0; i < Config.config_stock_details.size(); i++) {
                if(return_item.getItem_id().equals(Config.config_stock_item.get(i).getItem_id())){ 
                Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble( Config.config_stock_details.get(i).getTotal())-quantity)+"' where item_id ='"+ Config.config_stock_details.get(i).getItem_id()+"'";
                break;
                }
                }
                int y=Config.stmt.executeUpdate(Config.sql);
                if (y>0) {
                    Config.config_mgr.loadStockDetails();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//   //----------------------------------------------------------------------------------------------
//   
//   //method to delete ReturnItem in database
   public boolean delReturnItem(String return_item_id)
   {
        try {          
            String quantity="";
            String item_id="";
                Config.sql ="SELECT * FROM return_item where return_item_id="+return_item_id;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                if(Config.rs.next()) {
                  quantity=Config.rs.getString("quantity");
                  item_id=Config.rs.getString("item_id");
                }    
            Config.sql = "delete from return_item where return_item_id = '"+return_item_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                for (int i= 0; i < Config.config_stock_details.size(); i++) {
                if(item_id.equals(Config.config_stock_item.get(i).getItem_id())){ 
                Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble( Config.config_stock_details.get(i).getTotal())-Double.parseDouble(quantity))+"' where item_id ='"+ item_id+"'";
                break;
                }
                }
                int y=Config.stmt.executeUpdate(Config.sql);
                if (y>0) {
                    Config.config_mgr.loadStockDetails();
                    return true;
                } else {
                    return false;
                }
 
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }          
}
