package class_manager;

import data_manager.ReceiveStock;
import data_manager.configuration.Config;
import java.util.ArrayList;

public class ReceiveStockMgr {
    //method to insert ReceiveStock in database
    public boolean insReceiveStock(ArrayList<ReceiveStock> receive_stock) {
        try {          
            Config.sql = "insert into receive_stock ("                   
                    + "item_id,"
                    + "item_list_id,"
                    + "_date,"
                    + "_month,"
                    + "_year,"
                    + "quantity,"
                    + "bill_type_id )"
                    + " values (?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            for(int i = 0; i < receive_stock.size(); i++) {
            ReceiveStock rs = receive_stock.get(i);
            Config.pstmt.setString(1, rs.getItem_id());
            Config.pstmt.setString(2, rs.getItem_list_id());
            Config.pstmt.setString(3, rs.getDate());
            Config.pstmt.setString(4, rs.getMonth());
            Config.pstmt.setString(5, rs.getYear());
            Config.pstmt.setString(6, rs.getQuantity());
            Config.pstmt.setString(7, rs.getBill_type_id());
            Config.pstmt.addBatch();
            }
            int[]arr=Config.pstmt.executeBatch();
            Config.pstmt.close();
            if(arr.length==receive_stock.size()) {
                        try {
                            int i=0;
                            for (int j = 0; j < receive_stock.size(); j++) {      
                            for ( i= 0; i < Config.config_stock_details.size(); i++) {
                            if(receive_stock.get(j).getItem_id().equals(Config.config_stock_item.get(i).getItem_id())){ 
                            Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble(Config.config_stock_details.get(i).getTotal())+Double.parseDouble(receive_stock.get(j).getQuantity()))+"' where item_id ='"+ Config.config_stock_details.get(i).getItem_id()+"'";
                            Config.stmt.addBatch(Config.sql);
                            break;
                            }
                            }
                            }
                            int[]a=Config.stmt.executeBatch();
                            if(a.length==receive_stock.size()){
                                Config.config_mgr.loadStockDetails();
                                return true;
                            }else{
                                return false;    
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            return false;
                        
                        }    
            } else {
            return false;
            }
        }catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//    //----------------------------------------------------------------------------------------------
//    
//   //method to ReceiveStock in database 
   public boolean updReceiveStock(ReceiveStock receive_stock,double quantity) {
        try {
            Config.sql = "update receive_stock set"
                    + " quantity = ? "
                    + " where receive_stock_id = '"+receive_stock.getReceive_stock_id()+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, receive_stock.getQuantity());
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                for (int i= 0; i < Config.config_stock_details.size(); i++) {
                if(receive_stock.getItem_id().equals(Config.config_stock_item.get(i).getItem_id())){ 
                Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble( Config.config_stock_details.get(i).getTotal())-quantity)+"' where item_id ='"+ Config.config_stock_details.get(i).getItem_id()+"'";
                break;
                }
                }
                int y=Config.stmt.executeUpdate(Config.sql);
                if (y>0) {
                    Config.config_mgr.loadStockDetails();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//   //----------------------------------------------------------------------------------------------
//   
//   //method to delete ReceiveStock in database
   public boolean delReceiveStock(String receive_stock_id)
   {
        try {          
            String quantity="";
            String item_id="";
                Config.sql ="SELECT * FROM receive_stock where receive_stock_id="+receive_stock_id;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                if(Config.rs.next()) {
                  quantity=Config.rs.getString("quantity");
                  item_id=Config.rs.getString("item_id");
                }    
            Config.sql = "delete from receive_stock where receive_stock_id = '"+receive_stock_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                for (int i= 0; i < Config.config_stock_details.size(); i++) {
                if(item_id.equals(Config.config_stock_item.get(i).getItem_id())){ 
                Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble( Config.config_stock_details.get(i).getTotal())-Double.parseDouble(quantity))+"' where item_id ='"+ item_id+"'";
                break;
                }
                }
                int y=Config.stmt.executeUpdate(Config.sql);
                if (y>0) {
                    Config.config_mgr.loadStockDetails();
                    return true;
                } else {
                    return false;
                }
 
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
