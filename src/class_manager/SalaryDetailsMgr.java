package class_manager;

import data_manager.SalaryDetails;
import data_manager.configuration.Config;

public class SalaryDetailsMgr {
   //method to insert SalaryDetails in database
    public boolean insSalaryDetails(SalaryDetails salary_details) {
        try {  
                      
            Config.sql = "insert into salary_details ("                   
                    + "employee_id,"
                    + "_date,"
                    + "_month,"
                    + "_year,"
                    + "salary,"
                    + "time_in,"
                    + "time_out )"
                    + " values ("
                   
                    + "?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, salary_details.getEmployee_id());
            Config.pstmt.setString(2, salary_details.getDate());
            Config.pstmt.setString(3, salary_details.getMonth());
            Config.pstmt.setString(4, salary_details.getYear());
            Config.pstmt.setString(5, salary_details.getSalary());
            Config.pstmt.setString(6, salary_details.getTime_in());
            Config.pstmt.setString(7, salary_details.getTime_out());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadSalaryDetails();
                return true;
            } else {
                return false;
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to update SalaryDetails in database 
   public boolean updSalaryDetails(SalaryDetails salary_details) {
        try {
           
            Config.sql = "update salary_details set"
                    + " employee_id = ?, "
                    + " _date = ?, "
                    + " _month = ?, "
                    + " _year = ?, "
                    + " salary = ?, "
                    + " time_in = ?, "
                    + " time_out = ? "
                    + " where salary_id = '"+salary_details.getSalary_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, salary_details.getEmployee_id());
            Config.pstmt.setString(2, salary_details.getDate());
            Config.pstmt.setString(3, salary_details.getMonth());
            Config.pstmt.setString(4, salary_details.getYear());
            Config.pstmt.setString(5, salary_details.getSalary());
            Config.pstmt.setString(6, salary_details.getTime_in());
            Config.pstmt.setString(7, salary_details.getTime_out());
            
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadSalaryDetails();
                return true;
            } else {
                return false;
            }
            } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
   //----------------------------------------------------------------------------------------------
   
   //method to delete SalaryDetails in database
   public boolean delSalaryDetails(String salary_id)
   {
        try {
            Config.sql = "delete from salary_details where salary_id = '"+salary_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadSalaryDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }   
}
