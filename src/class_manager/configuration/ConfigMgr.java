package class_manager.configuration;

import class_manager.AddressDetailsMgr;
import class_manager.AdvanceDetailsMgr;
import class_manager.AttendenceDetailsMgr;
import class_manager.BikeDetailsMgr;
import class_manager.BillTypeMgr;
import class_manager.ContactDetailsMgr;
import class_manager.CustomerDetailsMgr;
import class_manager.DemageItemMgr;
import class_manager.DemageStockMgr;
import class_manager.DesignationDetailsMgr;
import class_manager.DistributionDetailsMgr;
import class_manager.DistributionItemMgr;
import class_manager.EmployeeDetailsMgr;
import class_manager.ExpensesDetailsMgr;
import class_manager.ExpensesMgr;
import class_manager.IdentityDetailsMgr;
import class_manager.ItemListMgr;
import class_manager.MaintenanceDetailsMgr;
import class_manager.MaintenanceTypeMgr;
import class_manager.MessageDetailsMgr;
import class_manager.ProfileDetailsMgr;
import class_manager.ReceiveStockMgr;
import class_manager.ReturnItemMgr;
import class_manager.ReturnStockMgr;
import class_manager.SalaryDetailsMgr;
import class_manager.SalaryDistributionMgr;
import class_manager.SalesDetailsMgr;
import class_manager.StockDetailsMgr;
import class_manager.StockItemMgr;
import class_manager.SupplierDetailsMgr;
import class_manager.TransportDetailsMgr;
import data_manager.AddressDetails;
import data_manager.AdvanceDetails;
import data_manager.AttendenceDetails;
import data_manager.BikeDetails;
import data_manager.BillType;
import data_manager.ContactDetails;
import data_manager.CustomerDetails;
import data_manager.DesignationDetails;
import data_manager.DistributedStock;
import data_manager.EmployeeDetails;
import data_manager.ExpensesDetails;
import data_manager.IdentityDetails;
import data_manager.ItemList;
import data_manager.MaintenanceType;
import data_manager.MessageDetails;
import data_manager.ProfileDetails;
import data_manager.SalaryDetails;
import data_manager.SalaryDistribution;
import data_manager.StockDetails;
import data_manager.StockItem;
import data_manager.SupplierDetails;
import data_manager.configuration.Config;
import java.sql.DriverManager;
import java.util.ArrayList;
import modules.admin_management.AdminManagement;
import modules.admin_management.Expenses.Expensesdetails;
import modules.admin_management.Expenses.NewExpenses;
import modules.admin_management.Expenses.ViewExpenses;
import modules.admin_management.configproduct.NewBike;
import modules.admin_management.configproduct.NewDesignation;
import modules.admin_management.configproduct.NewMaintenance;
import modules.admin_management.configproduct.ViewBike;
import modules.admin_management.configproduct.ViewDesignation;
import modules.admin_management.configproduct.ViewMaintenance;
import modules.admin_management.configproduct.productconfiguration;
import modules.admin_management.itemlist.NewItem;
import modules.admin_management.itemlist.ViewItem;
import modules.admin_management.itemlist.itemlist;
import modules.admin_management.manufacture.ManufactureItem;
import modules.admin_management.manufacture.NewManufactureItem;
import modules.admin_management.manufacture.ViewManufactureItem;
import modules.admin_management.soda.NewSoda;
import modules.admin_management.soda.Soda;
import modules.admin_management.soda.ViewSoda;
import modules.admin_management.stock.NewStockItem;
import modules.admin_management.stock.ViewStockItem;
import modules.admin_management.stock.stockitem;
import modules.customer_management.CustomerManagement;
import modules.customer_management.NewCustomer;
import modules.customer_management.ViewCustomer;
import modules.dashboard.Home;
import modules.distribution_management.DistributionManagement;
import modules.distribution_management.demage.NewDemage;
import modules.distribution_management.demage.ViewDemage;
import modules.distribution_management.distribute.NewDistribution;
import modules.distribution_management.distribute.ViewDistribution;
import modules.distribution_management.distribute.ViewDistributionItem;
import modules.distribution_management.return_item.NewReturn;
import modules.distribution_management.return_item.ViewReturn;
import modules.distribution_management.search.DesignationSearch;
import modules.employee_management.EmployeeManagement;
import modules.employee_management.attendance.Attendance;
import modules.employee_management.attendance.ViewAttendance;
import modules.employee_management.employee.NewEmployee;
import modules.employee_management.employee.ViewEmployee;
import modules.employee_management.salary_information.AddAdvance;
import modules.employee_management.salary_information.AddSalaryInfo;
import modules.employee_management.salary_information.ViewAdvance;
import modules.employee_management.salary_information.ViewSalaryInfo;
import modules.employee_management.salary_management.NewSalary;
import modules.employee_management.salary_management.SalaryManagement;
import modules.employee_management.salary_management.ViewSalary;
import modules.employee_management.search.EmployeeSearch;
import modules.expenses_management.AddExpenses;
import modules.expenses_management.ExpensesManagement;
import modules.expenses_management.ViewAddExpenses;
import modules.expenses_management.search.ExpensesSearch;
import modules.message_management.NewMessage;
import modules.message_management.ViewMessage;
import modules.sales_management.NewSales;
import modules.sales_management.SaleManagement;
import modules.sales_management.ViewSaleItem;
import modules.sales_management.ViewSales;
import modules.stock_management.Stock.NewStock;
import modules.stock_management.Stock.StockManagement;
import modules.stock_management.Stock.ViewStock;
import modules.stock_management.demage_item.NewDemageItem;
import modules.stock_management.demage_item.ViewDemageItem;
import modules.stock_management.return_item.NewReturnItem;
import modules.stock_management.return_item.ViewReturnItem;
import modules.stock_management.search.ItemSearch;
import modules.stock_management.search.RateSearch;
import modules.supplier_management.NewSupplier;
import modules.supplier_management.SupplierManagement;
import modules.supplier_management.ViewSupplier;
import modules.transport_management.NewTransport;
import modules.transport_management.TransportManagement;
import modules.transport_management.ViewTransport;
import modules.transport_management.maintenance.AddMaintenance;
import modules.transport_management.maintenance.ViewAddMaintenance;
import modules.transport_management.search.MaintenaceSearch;
import modules.transport_management.search.TransportSearch;

public class ConfigMgr {
    
    public boolean loadDatabase() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            String userName = "root";
            String password = "root";
            String url = "jdbc:mysql://localhost:3307/albeik";
            Config.conn = DriverManager.getConnection(url, userName, password);
            Config.stmt = Config.conn.createStatement();            
            
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean loadProfileDetails() {
        try {
            Config.config_profile_details = new ArrayList<>();
            Config.id_profile_details = new ArrayList<>();
            Config.sql ="select * from profile_details";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ProfileDetails pd = new ProfileDetails();
                Config.id_profile_details.add(Config.rs.getString("profile_id"));
                pd.setProfile_id(Config.rs.getString("profile_id"));
                pd.setName(Config.rs.getString("name"));        
                pd.setDob(Config.rs.getString("dob"));        
                pd.setGender(Config.rs.getString("gender"));        
                Config.config_profile_details.add(pd);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    } 
    public boolean loadAddressDetails() {
        try {
            Config.id_address_details = new ArrayList<>();
            Config.config_address_details = new ArrayList<>();
            
            Config.sql ="select * from address_details";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            
            while (Config.rs.next()) {
                AddressDetails ad = new AddressDetails();
                Config.id_address_details.add(Config.rs.getString("address_id"));
                ad.setAddress_id(Config.rs.getString("address_id"));
                ad.setAddress_1(Config.rs.getString("address_1"));        
                ad.setAddress_2(Config.rs.getString("address_2"));        
                ad.setCity(Config.rs.getString("city"));        
                ad.setPincode(Config.rs.getString("pincode"));        
                ad.setState(Config.rs.getString("state"));
                
                Config.config_address_details.add(ad);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    public boolean loadContactDetails() {
        try {
            Config.id_contact_details = new ArrayList<>();
            Config.config_contact_details = new ArrayList<>();
            
            Config.sql ="select * from contact_details";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            
            while (Config.rs.next()) {
                ContactDetails cd = new ContactDetails();
                Config.id_contact_details.add(Config.rs.getString("contact_id"));
                cd.setContact_id(Config.rs.getString("contact_id"));
                cd.setContact_no(Config.rs.getString("contact_no"));        
                cd.setAlt_contact_no(Config.rs.getString("alt_contact_no"));        
                cd.setEmail(Config.rs.getString("email"));        
                cd.setOther(Config.rs.getString("other"));
                
                Config.config_contact_details.add(cd);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    public boolean loadIdentityDetails() {
        try {
            Config.config_identity_details = new ArrayList<>();
            Config.id_identity_details = new ArrayList<>();
            Config.sql ="select * from identity_details";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                IdentityDetails id = new IdentityDetails();
                Config.id_identity_details.add(Config.rs.getString("identity_id"));
                id.setIdentity_id(Config.rs.getString("identity_id"));
                id.setIdentity_type(Config.rs.getString("identity_type"));        
                id.setIdentity_no(Config.rs.getString("identity_no"));        
                Config.config_identity_details.add(id);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    public boolean loadDesignationDetails() {
        try {
            Config.id_designation_details = new ArrayList<>();
            Config.config_designation_details = new ArrayList<>();
            
            Config.sql ="select * from designation_details";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            
            while (Config.rs.next()) {                
                DesignationDetails dd = new DesignationDetails();                
                Config.id_designation_details.add(Config.rs.getString("designation_id"));
                dd.setDesignation_id(Config.rs.getString("designation_id"));
                dd.setDesignation(Config.rs.getString("designation"));        
                dd.setContact_person(Config.rs.getString("contact_person"));        
                dd.setContact_id(Config.rs.getString("contact_id"));
                dd.setAddress_id(Config.rs.getString("address_id"));
                
                Config.config_designation_details.add(dd);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    public boolean loadBikeDetails() {
        try {
            Config.id_bike_details= new ArrayList<>();
            Config.config_bike_details = new ArrayList<>();            
            
            Config.sql ="select * from bike_details";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            
            while (Config.rs.next()) {
                BikeDetails bd = new BikeDetails();
                Config.id_bike_details.add(Config.rs.getString("bike_id"));
                bd.setBike_id(Config.rs.getString("bike_id"));
                bd.setName(Config.rs.getString("name"));        
                bd.setOther(Config.rs.getString("other"));
                
                Config.config_bike_details.add(bd);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    public boolean loadExpensesDetails() {
        try {
            Config.config_expenses_details = new ArrayList<>();
            Config.id_expenses_details= new ArrayList<>();
            Config.sql ="select * from expenses_details";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ExpensesDetails ed = new ExpensesDetails();
                Config.id_expenses_details.add(Config.rs.getString("expenses_details_id"));
                ed.setExpenses_details_id(Config.rs.getString("expenses_details_id"));
                ed.setType(Config.rs.getString("type"));
                Config.config_expenses_details.add(ed);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    public boolean loadStockItem() {
        try {
            Config.config_stock_item = new ArrayList<>();
            Config.id_stock_item= new ArrayList<>();
            Config.sql ="select * from stock_item";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                StockItem si = new StockItem();
                Config.id_stock_item.add(Config.rs.getString("item_id"));
                si.setItem_id(Config.rs.getString("item_id"));
                si.setName(Config.rs.getString("name"));        
                si.setUnit(Config.rs.getString("unit"));        
                si.setIdentity(Config.rs.getString("identity"));        
                Config.config_stock_item.add(si);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }  
        public boolean loadItemList() {
        try {
            Config.config_item_list = new ArrayList<>();
            Config.id_item_list = new ArrayList<>();
            Config.sql ="SELECT * FROM  item_list a INNER JOIN (SELECT item_id, max(STR_TO_DATE(_date,'%d/%m/%Y')) max_date FROM  item_list GROUP BY item_id ) b where  a.item_id = b.item_id AND STR_TO_DATE(a._date,'%d/%m/%Y') = b.max_date order by a.item_id";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ItemList it = new ItemList();
                Config.id_item_list.add(Config.rs.getString("item_list_id"));
                it.setItem_list_id(Config.rs.getString("item_list_id"));
                it.setItem_id(Config.rs.getString("Item_id"));
                it.setSupplier_id(Config.rs.getString("supplier_id"));
                it.setQuantity(Config.rs.getString("quantity"));
                it.setPrise(Config.rs.getString("Prise"));
                it.setPack(Config.rs.getString("pack"));
                it.setDate(Config.rs.getString("_date"));
                it.setMonth(Config.rs.getString("_month"));
                it.setYear(Config.rs.getString("_year"));        
                Config.config_item_list.add(it);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    public boolean loadCustomerDetails() {
        try {
            Config.config_customer_details = new ArrayList<>();
            Config.id_customer_details = new ArrayList<>();
            Config.sql ="select * from customer_details";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                CustomerDetails cd = new CustomerDetails();
                Config.id_customer_details.add(Config.rs.getString("customer_id"));
                cd.setCustomer_id(Config.rs.getString("customer_id"));
                cd.setProfile_id(Config.rs.getString("profile_id"));        
                cd.setAddress_id(Config.rs.getString("address_id"));        
                cd.setContact_id(Config.rs.getString("contact_id"));        
                cd.setIdentity_id(Config.rs.getString("identity_id"));        
                cd.setAccount_no(Config.rs.getString("account_no"));        
                cd.setBank_name(Config.rs.getString("bank_name"));        
                Config.config_customer_details.add(cd);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    public boolean loadEmployeeDetails() {
        try {
            Config.id_employee_details = new ArrayList<>();
            Config.config_employee_details = new ArrayList<>();
            
            Config.sql ="select * from employee_details";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            
            while (Config.rs.next()) {
                EmployeeDetails ed = new EmployeeDetails();
                Config.id_employee_details.add(Config.rs.getString("employee_id"));
                ed.setEmployee_id(Config.rs.getString("employee_id"));
                ed.setProfile_id(Config.rs.getString("profile_id"));        
                ed.setAddress_id(Config.rs.getString("address_id"));        
                ed.setContact_id(Config.rs.getString("contact_id"));        
                ed.setIdentity_id(Config.rs.getString("identity_id"));        
                ed.setDesignation_id(Config.rs.getString("designation_id"));        
                ed.setAccount_no(Config.rs.getString("account_no"));        
                ed.setBank_name(Config.rs.getString("bank_name"));        
                ed.setJoining_date(Config.rs.getString("joining_date"));        
                ed.setLeaving_date(Config.rs.getString("leaving_date"));        
                ed.setMessage_id(Config.rs.getString("message_id"));        
                Config.config_employee_details.add(ed);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean loadSalaryDetails(){
        try {
            Config.config_salary_details = new ArrayList<>();
            Config.id_salary_details = new ArrayList<>();
            Config.sql ="SELECT * FROM salary_details a INNER JOIN (SELECT employee_id,max(STR_TO_DATE(_date,'%d/%m/%Y')) max_date FROM salary_details GROUP BY employee_id) b where a.employee_id = b.employee_id AND STR_TO_DATE(a._date,'%d/%m/%Y') = b.max_date order by a.employee_id";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
               SalaryDetails sd = new SalaryDetails();
                Config.id_salary_details.add(Config.rs.getString("salary_id"));
                sd.setSalary_id(Config.rs.getString("salary_id"));
                sd.setEmployee_id(Config.rs.getString("employee_id"));
                sd.setDate(Config.rs.getString("_date"));
                sd.setMonth(Config.rs.getString("_month"));
                sd.setYear(Config.rs.getString("_year"));
                sd.setSalary(Config.rs.getString("salary"));
                sd.setTime_in(Config.rs.getString("time_in"));
                sd.setTime_out(Config.rs.getString("time_out"));
                Config.config_salary_details.add(sd);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean loadSupplierDetails() {
        try {
            Config.config_supplier_details = new ArrayList<>();
            Config.id_supplier_details = new ArrayList<>();
            Config.sql ="select * from supplier_details";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                SupplierDetails sd = new SupplierDetails();
                Config.id_supplier_details.add(Config.rs.getString("supplier_id"));
                sd.setSupplier_id(Config.rs.getString("supplier_id"));
                sd.setProfile_id(Config.rs.getString("profile_id"));        
                sd.setAddress_id(Config.rs.getString("address_id"));        
                sd.setContact_id(Config.rs.getString("contact_id"));        
                sd.setIdentity_id(Config.rs.getString("identity_id"));        
                sd.setAccount_no(Config.rs.getString("account_no"));        
                sd.setBank_name(Config.rs.getString("bank_name"));        
                Config.config_supplier_details.add(sd);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
//    public boolean loadDistributedStock(){
//          try {
//            Config.config_distributed_stock= new ArrayList<>();
//            Config.id_distributed_stock = new ArrayList<>();
//            Config.sql ="select * from distributed_stock ";
//            Config.rs = Config.stmt.executeQuery(Config.sql);
//            while (Config.rs.next()) {
//               DistributedStock sd = new DistributedStock();
//                Config.id_distributed_stock.add(Config.rs.getString("dis_stock_id"));
//                sd.setDis_stock_id(Config.rs.getString("dis_stock_id"));
//                sd.setItem_id(Config.rs.getString("item_id"));
//                sd.setDate(Config.rs.getString("date"));
//                sd.setMonth(Config.rs.getString("month"));
//                sd.setYear(Config.rs.getString("year"));
//                sd.setQuantity(Config.rs.getString("quantity"));
//                sd.setBill_type_id(Config.rs.getString("Bill_type_id"));
//                sd.setDesignation_id(Config.rs.getString("Designation_id"));
//                sd.setMessage_id(Config.rs.getString("Message_id"));
//                Config.config_distributed_stock.add(sd);
//            }
//            return true;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return false;
//        }
//    } 
    
    public boolean loadStockDetails(){
          try {
            Config.config_stock_details= new ArrayList<>();
            Config.id_stock_details = new ArrayList<>();
            Config.sql ="select * from stock_details ";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
               StockDetails sd = new StockDetails();
                Config.id_stock_details.add(Config.rs.getString("stock_id"));
                sd.setStock_id(Config.rs.getString("stock_id"));
                sd.setItem_id(Config.rs.getString("item_id"));
                sd.setTotal(Config.rs.getString("total"));
                Config.config_stock_details.add(sd);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }          
    } 
    public boolean loadMaintenanceType(){
          try {
            Config.config_maintenance_type= new ArrayList<>();
            Config.id_maintenance_type = new ArrayList<>();
            Config.sql ="select * from maintenance_type ";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
               MaintenanceType sd = new MaintenanceType();
                Config.id_maintenance_type.add(Config.rs.getString("maintenance_type_id"));
                sd.setMaintenance_type_id(Config.rs.getString("maintenance_type_id"));
                sd.setType(Config.rs.getString("type"));
                sd.setOther(Config.rs.getString("other"));
                Config.config_maintenance_type.add(sd);
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }          
    } 
    
    public boolean loadClassManager() {
        try {
            Config.profile_details_mgr = new ProfileDetailsMgr();
            Config.address_details_mgr = new AddressDetailsMgr();
            Config.contact_details_mgr = new ContactDetailsMgr();
            Config.identity_details_mgr = new IdentityDetailsMgr();
            Config.designation_details_mgr = new DesignationDetailsMgr();
            Config.bike_details_mgr = new BikeDetailsMgr();
            Config.expenses_details_mgr = new ExpensesDetailsMgr();
            Config.stock_item_mgr = new StockItemMgr();
            Config.item_list_mgr = new ItemListMgr();
            Config.customer_details_mgr = new CustomerDetailsMgr();
            Config.employee_details_mgr = new EmployeeDetailsMgr();
            Config.advnace_details_mgr = new AdvanceDetailsMgr();
            Config.salary_distribution_mgr = new SalaryDistributionMgr();
            Config.demage_stock_mgr = new DemageStockMgr();
            Config.receive_stock_mgr = new ReceiveStockMgr();
            Config.return_stock_mgr = new ReturnStockMgr();
            Config.supplier_details_mgr = new SupplierDetailsMgr();
            Config.stock_details_mgr = new StockDetailsMgr();
            Config.message_details_mgr = new MessageDetailsMgr();
            Config.bill_type_mgr = new BillTypeMgr();
            Config.expenses_mgr = new ExpensesMgr();
            Config.salary_details_mgr = new SalaryDetailsMgr();
            Config.attendence_details_mgr = new AttendenceDetailsMgr();
            Config.maintenance_details_mgr = new MaintenanceDetailsMgr();
            Config.transport_details_mgr = new TransportDetailsMgr();
            Config.maintenance_type_mgr = new MaintenanceTypeMgr();
            Config.distribution_details_mgr = new DistributionDetailsMgr();
            Config.demage_item_mgr = new DemageItemMgr();
            Config.return_item_mgr = new ReturnItemMgr();
            Config.distribution_item_mgr = new DistributionItemMgr();
            Config.sales_details_mgr = new SalesDetailsMgr();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }    
    
    public boolean loadForms() {
        try {

            Config.home = new Home();
            Config.admin_management = new AdminManagement(null, true);
            
            Config.product_configuration = new productconfiguration();
            Config.new_designation =  new NewDesignation(null, true);
            Config.view_designation =  new ViewDesignation(null, true);
            Config.new_bike =  new NewBike(null, true);
            Config.view_bike =  new ViewBike(null, true);
            
            Config.expenses_details = new Expensesdetails();
            Config.new_expenses =  new NewExpenses(null, true);
            Config.view_expenses =  new ViewExpenses(null, true);
            
            Config.item_list = new itemlist();
            Config.new_item = new NewItem(null, true);
            Config.view_item = new ViewItem(null, true);
        
            Config.stock_item = new stockitem();
            Config.soda = new Soda();
            Config.manufactureitem = new ManufactureItem();
            Config.new_stock_item = new NewStockItem(null, true);
            Config.view_stock_item = new ViewStockItem(null, true);
            Config.new_soda =  new NewSoda(null, true);
            Config.view_soda =  new ViewSoda(null, true);  
            Config.new_manu_item =  new NewManufactureItem(null, true);
            Config.view_manu_item =  new ViewManufactureItem(null, true);
       
            
            Config.customer_management = new CustomerManagement(null, true);
            Config.new_customer = new NewCustomer(null, true);
            Config.view_customer = new ViewCustomer(null, true);
            
            Config.employee_management = new EmployeeManagement(null, true);
                    
            Config.new_employee = new NewEmployee(null, true);
            Config.view_employee = new ViewEmployee(null, true);
            Config.add_advance = new AddAdvance(null, true);
            Config.view_advance = new ViewAdvance(null, true);
            Config.add_salary_info = new AddSalaryInfo(null, true);
            Config.view_salary_info = new ViewSalaryInfo(null, true);
            Config.attendence = new Attendance(null, true);
            Config.view_attendence = new ViewAttendance(null, true);
            
            Config.salary_management = new SalaryManagement(null, true);
            Config.new_salary = new NewSalary(null, true);
            Config.view_salary = new ViewSalary(null, true);
            
            Config.supplier_management = new SupplierManagement(null, true);
            Config.new_supplier = new NewSupplier(null, true);
            Config.view_supplier = new ViewSupplier(null, true);
           
            Config.new_message = new NewMessage(null, true);
            Config.view_message = new ViewMessage(null, true);
            
            Config.stock_management = new StockManagement(null, true);
            Config.new_stock = new NewStock(null, true);
            Config.view_stock = new ViewStock(null, true);
            
            Config.new_demage_item = new NewDemageItem(null, true);
            Config.view_demage_Item = new ViewDemageItem(null, true);
            
            Config.employee_search = new EmployeeSearch(null, true);
            Config.item_search = new ItemSearch(null, true);
            Config.rate_search = new RateSearch(null, true);
            Config.new_return_item = new NewReturnItem(null, true);
            Config.view_return_Item = new ViewReturnItem(null, true);
            
            Config.add_expenses=new AddExpenses(null, true);
            Config.view_add_expenses=new ViewAddExpenses(null, true);
            Config.expenses_search=new ExpensesSearch(null, true);
            Config.expenses_management = new ExpensesManagement(null, true);
            
            Config.new_maintenance = new NewMaintenance(null, true);
            Config.view_Maintenance = new ViewMaintenance(null, true);
            
            Config.transport_management = new TransportManagement(null, true);
            Config.new_transport = new NewTransport(null, true);
            Config.view_transport = new ViewTransport(null, true);
            
            Config.add_maintenance = new AddMaintenance(null, true);
            Config.view_add_maintenance = new ViewAddMaintenance(null, true);
            
            Config.transport_search = new TransportSearch(null, true);
            Config.maintenance_search = new MaintenaceSearch(null, true);
            
            Config.distribution_management = new DistributionManagement(null, true);
            Config.new_distribution = new NewDistribution(null, true);
            Config.view_distribution = new ViewDistribution(null, true);
            Config.designation_search=new DesignationSearch(null, true);
            Config.new_demage = new NewDemage(null, true);
            Config.view_demage = new ViewDemage(null, true);
            Config.new_return = new NewReturn(null, true);
            Config.view_return = new ViewReturn(null, true);
            Config.view_distribution_item = new ViewDistributionItem(null, true);
            
            Config.sale_management = new SaleManagement(null, true);
            Config.new_sales = new NewSales(null, true);
            Config.view_sales = new ViewSales(null, true);
            Config.view_sales_item = new ViewSaleItem(null, true);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }        
    }
    
}

