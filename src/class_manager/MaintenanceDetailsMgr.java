/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package class_manager;

import data_manager.MaintenanceDetails;
import data_manager.configuration.Config;

/**
 *
 * @author akshay
 */
public class MaintenanceDetailsMgr {
//method to insert MaintenanceDetails in database
    public boolean insMaintenanceDetails(MaintenanceDetails maintenance_details) {
        try { 
            Config.sql = "insert into maintenance_details ("                   
                    + "maintenance_type_id,"
                    + "bike_id,"
                    + "_date,"
                    + "_month,"
                    + "_year,"
                    + "amount,"
                    + "message_id )"
                    + " values"
                    + "(?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, maintenance_details.getMaintenance_type_id());
            Config.pstmt.setString(2, maintenance_details.getBike_id());
            Config.pstmt.setString(3, maintenance_details.getDate());
            Config.pstmt.setString(4, maintenance_details.getMonth());
            Config.pstmt.setString(5, maintenance_details.getYear());
            Config.pstmt.setString(6, maintenance_details.getAmount());
            Config.pstmt.setString(7, maintenance_details.getMessage_id());
            

            int x = Config.pstmt.executeUpdate();

            if (x>0) {
            return true;
            } else {
            return false;
            }
            } catch (Exception ex) {
            ex.printStackTrace();
            return false;
            }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to update MaintenanceDetails in database 
   public boolean updMaintenanceDetails(MaintenanceDetails maintenance_details) {
        try {
            Config.sql = "update maintenance_details set"
                    + " maintenance_type_id = ?, "
                    + " bike_id = ?, "
                    + " _date = ?, "
                    + " _month = ?, "
                    + " _year = ?, "
                    + " amount = ?, "
                    + " message_id = ? "
                    + " where maintenance_details_id = '"+maintenance_details.getMaintenance_details_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, maintenance_details.getMaintenance_type_id());
            Config.pstmt.setString(2, maintenance_details.getBike_id());
            Config.pstmt.setString(3, maintenance_details.getDate());
            Config.pstmt.setString(4, maintenance_details.getMonth());
            Config.pstmt.setString(5, maintenance_details.getYear());
            Config.pstmt.setString(6, maintenance_details.getAmount());
            Config.pstmt.setString(7, maintenance_details.getMessage_id());

            int x = Config.pstmt.executeUpdate();

            if (x>0) {
            return true;
            } else {
            return false;
            }
            } catch (Exception ex) {
            ex.printStackTrace();
            return false;
            }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete MaintenanceDetails in database
   public boolean delMaintenanceDetails(String maintenance_details_id)
   {
        try { 
            Config.sql = "delete from maintenance_details where maintenance_details_id = '"+maintenance_details_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }      
}
