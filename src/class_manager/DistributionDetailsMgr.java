/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package class_manager;


import data_manager.DistributionDetails;
import data_manager.configuration.Config;

/**
 *
 * @author akshay
 */
public class DistributionDetailsMgr {
     //method to insert DistributionDetails in database
    public boolean insDistributionDetails(DistributionDetails distribution_details) {
        try {          
            Config.sql = "insert into distribution_details ("                   
                    + "designation_id,"
                    + "_date,"
                    + "_month,"
                    + "_year,"
                    + "message_id)"
                    + " values (?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, distribution_details.getDesignation_id());
            Config.pstmt.setString(2, distribution_details.getDate());
            Config.pstmt.setString(3, distribution_details.getMonth());
            Config.pstmt.setString(4, distribution_details.getYear());
            Config.pstmt.setString(5, distribution_details.getMessage_id());
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to Update DistributionDetails in database 
   public boolean updDistributionDetails(DistributionDetails distribution_details) {
        try {
            Config.sql = "update distribution_details set"
                    + " message_id = ?"
                    + " where distribution_details_id = '"+distribution_details.getDistribution_details_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, distribution_details.getMessage_id());         
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete DistributionDetails in database
   public boolean delDistributionDetails(String distribution_details_id)
   {
        try {          
            Config.sql = "delete from distribution_details where distribution_details_id = '"+distribution_details_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                return true;
            } else {
                return false;
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }      
}
