package class_manager;

import data_manager.SalaryDistribution;
import data_manager.configuration.Config;

public class SalaryDistributionMgr {
         //method to insert SalaryDistribution in database
    public boolean insSalaryDistribution(SalaryDistribution salary_distribution) {
        try {          
            Config.sql = "insert into salary_distribution ("                   
                     + "employee_id,"
                    + "_date,"
                    + "_month,"
                    + "_year,"
                    + "amount,"
                    + "message_id,"
                    + "salary_id)"
                    + " values ("
                    + "?,?,?,?,?,?,?"
                    + ")";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, salary_distribution.getEmployee_id());
            Config.pstmt.setString(2, salary_distribution.getDate());
            Config.pstmt.setString(3, salary_distribution.getMonth());
            Config.pstmt.setString(4, salary_distribution.getYear());
            Config.pstmt.setString(5, salary_distribution.getAmount());
            Config.pstmt.setString(6, salary_distribution.getMessage_id());
            Config.pstmt.setString(7, salary_distribution.getSalary_id());
                      
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to update SalaryDistribution in database 
   public boolean updSalaryDistribution(SalaryDistribution salary_distribution) {
        try {
            Config.sql = "update salary_distribution set"
                    + " employee_id = ?, "
                    + " _date = ?, "
                    + " _month = ?, "
                    + " _year = ?, "
                    + " amount = ?, "
                    + " message_id = ?, "
                    + " salary_id = ? "
                    + " where sal_dis_id = '"+salary_distribution.getSal_dis_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, salary_distribution.getEmployee_id());
            Config.pstmt.setString(2, salary_distribution.getDate());
            Config.pstmt.setString(3, salary_distribution.getMonth());
            Config.pstmt.setString(4, salary_distribution.getYear());
            Config.pstmt.setString(5, salary_distribution.getAmount());
            Config.pstmt.setString(6, salary_distribution.getMessage_id());
            Config.pstmt.setString(7, salary_distribution.getSalary_id());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete SalaryDistribution in database
   public boolean delSalaryDistribution(String sal_dis_id)
   {
        try { 
           
            Config.sql = "delete from salary_distribution where sal_dis_id = '"+sal_dis_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
