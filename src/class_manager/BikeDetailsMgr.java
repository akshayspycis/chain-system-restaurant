package class_manager;

import data_manager.BikeDetails;
import data_manager.configuration.Config;

public class BikeDetailsMgr {
    
    //method to insert AttendenceDetails in database
    public boolean insBikeDetails(BikeDetails bike_details) {
        try {          
            Config.sql = "insert into bike_details ("                   
                    + "name, "
                    + "other) "
                    + "values (?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, bike_details.getName());
            Config.pstmt.setString(2, bike_details.getOther());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadBikeDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    //method to update BikeDetails in database 
    public boolean updBikeDetails(BikeDetails bike_details) {
        try {
            Config.sql = "update bike_details set "
                    + "name = ?, "
                    + "other = ? "
                    + "where bike_id = '"+bike_details.getBike_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, bike_details.getName());
            Config.pstmt.setString(2, bike_details.getOther());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.config_mgr.loadBikeDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
   
    //method to delete BikeDetails in database
    public boolean delBikeDetails(String bike_id) {
        try {          
            Config.sql = "delete from bike_details where bike_id = '"+bike_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadBikeDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }  
}
