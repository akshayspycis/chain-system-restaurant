package class_manager;

import data_manager.StockItem;
import data_manager.configuration.Config;

public class StockItemMgr {
    
         //method to insert StockItem in database
    public boolean insStockItem(StockItem stock_item) {
        try {          
            Config.sql = "insert into stock_item ("                   
                    + "name,"
                    + "unit,"
                    + "identity )"
                    + " values (?,?,?)";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, stock_item.getName());
            Config.pstmt.setString(2, stock_item.getUnit());
            Config.pstmt.setString(3, stock_item.getIdentity());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.sql = "insert into stock_details ("                   
                    + "item_id,"
                    + "total )"
                    + " values ((select max(item_id) from stock_item),'0')";
                Config.pstmt = Config.conn.prepareStatement(Config.sql);
                int y = Config.pstmt.executeUpdate();
                if (y>0) {
                Config.config_mgr.loadStockItem();
                Config.config_mgr.loadStockDetails();
                return true;
                } else {
                return false;
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to update StockItem in database 
   public boolean updStockItem(StockItem stock_item) {
        try {
            Config.sql = "update stock_item set"
                    + " name = ?, "
                    + " unit = ?, "
                    + " identity = ? "
                    + " where item_id = '"+stock_item.getItem_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, stock_item.getName());
            Config.pstmt.setString(2, stock_item.getUnit());
            Config.pstmt.setString(3, stock_item.getIdentity());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.config_mgr.loadStockItem();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete StockItem in database
   public boolean delStockItem(String item_id)
   {
        try {          
            Config.sql = "delete from stock_item where item_id = '"+item_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadStockItem();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}