package class_manager;

import data_manager.configuration.Config;
import data_manager.configuration.SupplierProfile;

public class SupplierDetailsMgr {
      //method to insert SupplierDetails in database
      public boolean insSupplierDetails( SupplierProfile supplier_profile) {
        try {          
            
            if(Config.profile_details_mgr.insProfileDetails(supplier_profile.getProfile_details())){
            if(Config.address_details_mgr.insAddressDetails(supplier_profile.getAddress_details())){
            if(Config.contact_details_mgr.insContactDetails(supplier_profile.getContact_details())){
            if(Config.identity_details_mgr.insIdentityDetails(supplier_profile.getIdentity_details())){
            Config.sql = "insert into supplier_details ("                   
            + "profile_id,"
            + "address_id,"
            + "contact_id,"
            + "identity_id,"
            + "account_no,"
            + "bank_name )"
            + " values ("
            + "(select max(profile_id) from profile_details),"
            + "(select max(address_id) from address_details),"
            + "(select max(contact_id) from contact_details),"
            + "(select max(identity_id) from identity_details),"
            + "?,"
            + "?"
            + ")";

            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, supplier_profile.getSupplier_details().getAccount_no());
            Config.pstmt.setString(2, supplier_profile.getSupplier_details().getBank_name());

            int x = Config.pstmt.executeUpdate();

            if (x>0) {
            Config.config_mgr.loadSupplierDetails();
            return true;
            } else {
            return false;
            }
            }else{
            return false;
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            } catch (Exception ex) {
            ex.printStackTrace();
            return false;
            }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to update SupplierDetails in database 
   public boolean updSupplierDetails(SupplierProfile supplier_profile) {
        try {
            System.out.println(supplier_profile.getProfile_details().getProfile_id());
            if(Config.profile_details_mgr.updProfileDetails(supplier_profile.getProfile_details())){
            if(Config.address_details_mgr.updAddressDetails(supplier_profile.getAddress_details())){
            if(Config.contact_details_mgr.updContactDetails(supplier_profile.getContact_details())){
            if(Config.identity_details_mgr.updIdentityDetails(supplier_profile.getIdentity_details())){
            Config.sql = "update supplier_details set"
                     + " profile_id = ?, "
                    + " address_id = ?, "
                    + " contact_id = ?, "
                    + " identity_id = ?, "
                    + " account_no = ?, "
                    + " bank_name = ? "
                    + " where supplier_id = "+supplier_profile.getSupplier_details().getSupplier_id();
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, supplier_profile.getProfile_details().getProfile_id());
            Config.pstmt.setString(2, supplier_profile.getAddress_details().getAddress_id());
            Config.pstmt.setString(3, supplier_profile.getContact_details().getContact_id());
            Config.pstmt.setString(4, supplier_profile.getIdentity_details().getIdentity_id());
            Config.pstmt.setString(5, supplier_profile.getSupplier_details().getAccount_no());
            Config.pstmt.setString(6, supplier_profile.getSupplier_details().getBank_name());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                System.out.println("akki");
                Config.config_mgr.loadSupplierDetails();
                return true;
            } else {
                return false;
            }
            }else{
            return false;
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete SupplierDetails in database
   public boolean delSupplierDetails(String supplier_id)
   {
            try {
            if(Config.profile_details_mgr.delProfileDetails(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(supplier_id)).getProfile_id())){
            if(Config.address_details_mgr.delAddressDetails(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(supplier_id)).getAddress_id())){
            if(Config.contact_details_mgr.delContactDetails(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(supplier_id)).getContact_id())){
            if(Config.identity_details_mgr.delIdentityDetails(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(supplier_id)).getIdentity_id())){
            Config.sql = "delete from supplier_details where supplier_id = '"+supplier_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.config_mgr.loadSupplierDetails();
                return true;
            } else {
                return false;
            }
            }else{
            return false;
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
