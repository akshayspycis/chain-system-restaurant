package class_manager;

import data_manager.MessageDetails;
import data_manager.configuration.Config;
/**
 *
 * @author akshay
 */
public class MessageDetailsMgr {
       //method to insert MessageDetails in database
    public boolean insMessageDetails(MessageDetails message_details) {
        try {          
            Config.sql = "insert into message_details ("                   
                    + "message )"
                    + " values (?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, message_details.getMessage());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to update MessageDetails in database 
   public boolean updMessageDetails(MessageDetails message_details) {
        try {
            Config.sql = "update message_details set"
                    + " message = ? "
                    + " where message_id = '"+message_details.getMessage_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, message_details.getMessage());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete MessageDetails in database
   public boolean delMessageDetails(String message_id)
   {
        try {          
            Config.sql = "delete from message_details where message_id = '"+message_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }    
}
