/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package class_manager;

import data_manager.DistributionItem;
import data_manager.configuration.Config;
import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class DistributionItemMgr {
//method to insert Distribution in database
    public boolean insDistributionItem(ArrayList<DistributionItem> distribution_item) {
        try {          
            Config.sql = "insert into distribution_item ("                   
                    + "distribution_details_id,"
                    + "item_id,"
                    + "item_list_id,"
                    + "quantity )"
                    + " values (?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            for(int i = 0; i < distribution_item.size(); i++) {
            DistributionItem rs = distribution_item.get(i);
            Config.pstmt.setString(1, rs.getDistribution_details_id());
            Config.pstmt.setString(2, rs.getItem_id());
            Config.pstmt.setString(3, rs.getItem_list_id());
            Config.pstmt.setString(4, rs.getQuantity());
            Config.pstmt.addBatch();
            }
            int[]arr=Config.pstmt.executeBatch();
            Config.pstmt.close();
            if(arr.length==distribution_item.size()) {
                        try {
                            int i=0;
                            for (int j = 0; j < distribution_item.size(); j++) {      
                            for ( i= 0; i < Config.config_stock_details.size(); i++) {
                            if(distribution_item.get(j).getItem_id().equals(Config.config_stock_item.get(i).getItem_id())){ 
                            Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble(Config.config_stock_details.get(i).getTotal())-Double.parseDouble(distribution_item.get(j).getQuantity()))+"' where item_id ='"+ Config.config_stock_details.get(i).getItem_id()+"'";
                            Config.stmt.addBatch(Config.sql);
                            break;
                            }
                            }
                            }
                            int[]a=Config.stmt.executeBatch();
                            if(a.length==distribution_item.size()){
                                Config.config_mgr.loadStockDetails();
                                return true;
                            }else{
                                return false;    
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            return false;
                        
                        }    
            } else {
            return false;
            }
        }catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//    //----------------------------------------------------------------------------------------------
   
//   //method to DistributionItem in database 
   public boolean updDistributionItem(DistributionItem distribution_item,double quantity) {
        try {
            Config.sql = "update distribution_item set"
                    + " quantity = ? "
                    + " where distribution_item_id = '"+distribution_item.getDistribution_item_id()+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, distribution_item.getQuantity());
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                for (int i= 0; i < Config.config_stock_details.size(); i++) {
                if(distribution_item.getItem_id().equals(Config.config_stock_item.get(i).getItem_id())){ 
                Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble( Config.config_stock_details.get(i).getTotal())+quantity)+"' where item_id ='"+ Config.config_stock_details.get(i).getItem_id()+"'";
                break;
                }
                }
                int y=Config.stmt.executeUpdate(Config.sql);
                if (y>0) {
                    Config.config_mgr.loadStockDetails();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//   //----------------------------------------------------------------------------------------------
//   
//   //method to delete DistributionItem in database
   public boolean delDistributionItem(String distribution_item_id)
   {
        try {          
            String quantity="";
            String item_id="";
            String distribution_details_id="";
                Config.sql ="SELECT * FROM distribution_item where distribution_item_id="+distribution_item_id;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                if(Config.rs.next()) {
                  quantity=Config.rs.getString("quantity");
                  item_id=Config.rs.getString("item_id");
                  distribution_details_id=Config.rs.getString("distribution_details_id");
                }    
            Config.sql = "delete from distribution_item where distribution_item_id = '"+distribution_item_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                for (int i= 0; i < Config.config_stock_details.size(); i++) {
                if(item_id.equals(Config.config_stock_item.get(i).getItem_id())){ 
                Config.sql ="update stock_details set total='"+String.valueOf(Double.parseDouble( Config.config_stock_details.get(i).getTotal())+Double.parseDouble(quantity))+"' where item_id ='"+ item_id+"'";
                Config.stmt.addBatch(Config.sql);
                break;
                }
                }
                Config.sql ="delete from return_item where distribution_details_id = '"+distribution_details_id+"' and item_id='"+item_id+"'";
                System.out.println(Config.sql);
                Config.stmt.addBatch(Config.sql);
                Config.sql ="delete from demage_item where distribution_details_id = '"+distribution_details_id+"' and item_id='"+item_id+"'";
                Config.stmt.addBatch(Config.sql);
                System.out.println(Config.sql);
                int[]a=Config.stmt.executeBatch();
                System.out.println(a.length);
                if (a.length>0) {
                Config.config_mgr.loadStockDetails();
                return true;
                } else {
                    return false;
                }
            }else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }          
}
