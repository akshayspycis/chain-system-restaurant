package class_manager;

import data_manager.ItemList;
import data_manager.configuration.Config;

public class ItemListMgr {
      //method to insert ItemList in database
    public boolean insItemList(ItemList item_list) {
        try { 
            Config.sql = "insert into item_list ("                   
                    + "item_id,"
                    + "supplier_id,"
                    + "quantity,"
                    + "prise,"
                    + "pack,"
                    + "_date,"
                    + "_month,"
                    + "_year )"
                    + " values"
                    + "(?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, item_list.getItem_id());
            Config.pstmt.setString(2, item_list.getSupplier_id());
            Config.pstmt.setString(3, item_list.getQuantity());
            Config.pstmt.setString(4, item_list.getPrise());
            Config.pstmt.setString(5, item_list.getPack());
            Config.pstmt.setString(6, item_list.getDate());
            Config.pstmt.setString(7, item_list.getMonth());
            Config.pstmt.setString(8, item_list.getYear());

            int x = Config.pstmt.executeUpdate();

            if (x>0) {
            Config.config_mgr.loadItemList();
            return true;
            } else {
            return false;
            }
            } catch (Exception ex) {
            ex.printStackTrace();
            return false;
            }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to update ItemList in database 
   public boolean updItemList(ItemList item_list) {
        try {
            Config.sql = "update item_list set"
                    + " item_id = ?, "
                    + " supplier_id = ?, "
                    + " quantity = ?, "
                    + " prise = ?, "
                    + " pack = ?, "
                    + " _date = ?, "
                    + " _month = ?, "
                    + " _year = ? "
                    + " where item_list_id = '"+item_list.getItem_list_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, item_list.getItem_id());
            Config.pstmt.setString(2, item_list.getSupplier_id());
            Config.pstmt.setString(3, item_list.getQuantity());
            Config.pstmt.setString(4, item_list.getPrise());
            Config.pstmt.setString(5, item_list.getPack());
            Config.pstmt.setString(6, item_list.getDate());
            Config.pstmt.setString(7, item_list.getMonth());
            Config.pstmt.setString(8, item_list.getYear());

            int x = Config.pstmt.executeUpdate();

            if (x>0) {
            Config.config_mgr.loadItemList();
            return true;
            } else {
            return false;
            }
            } catch (Exception ex) {
            ex.printStackTrace();
            return false;
            }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete ItemList in database
   public boolean delItemList(String item_list_id)
   {
        try { 
            Config.sql = "delete from item_list where item_list_id = '"+item_list_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadItemList();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
