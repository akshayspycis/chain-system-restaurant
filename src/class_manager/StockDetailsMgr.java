package class_manager;

import data_manager.StockDetails;
import data_manager.configuration.Config;
/**
 *
 * @author akshay
 */
public class StockDetailsMgr {
         //method to insert StockDetails in database
    public boolean insStockDetails(StockDetails stock_details) {
        try {          
            Config.sql = "insert into stock_details ("                   
                    + "item_id,"
                    + "total )"
                    + " values (?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, stock_details.getItem_id());
            Config.pstmt.setString(2, stock_details.getTotal());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadStockDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to StockDetails in database 
   public boolean updStockDetails(StockDetails stock_details) {
        try {
            Config.sql = "update stock_details set"
                    + " item_id = ?, "
                    + " total = ? "
                    + " where stock_id = '"+stock_details.getStock_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, stock_details.getItem_id());
            Config.pstmt.setString(2, stock_details.getTotal());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.config_mgr.loadStockDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete StockDetails in database
   public boolean delStockDetails(String stock_id)
   {
        try {          
            Config.sql = "delete from stock_details where stock_id = '"+stock_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.config_mgr.loadStockDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
