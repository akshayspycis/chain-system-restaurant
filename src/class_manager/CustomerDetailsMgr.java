package class_manager;

import data_manager.configuration.Config;
import data_manager.configuration.CustomerProfile;

public class CustomerDetailsMgr {
     //method to insert CustomerDetails in database
    public boolean insCustomerDetails(CustomerProfile customer_profile) {
        try {          
            if(Config.profile_details_mgr.insProfileDetails(customer_profile.getProfile_details())){
            if(Config.address_details_mgr.insAddressDetails(customer_profile.getAddress_details())){
            if(Config.contact_details_mgr.insContactDetails(customer_profile.getContact_details())){
            if(Config.identity_details_mgr.insIdentityDetails(customer_profile.getIdentity_details())){
            Config.sql = "insert into customer_details ("                   
            + "profile_id,"
            + "address_id,"
            + "contact_id,"
            + "identity_id,"
            + "account_no,"
            + "bank_name )"
            + " values ("
            + "(select max(profile_id) from profile_details),"
            + "(select max(address_id) from address_details),"
            + "(select max(contact_id) from contact_details),"
            + "(select max(identity_id) from identity_details),"
            + "?,"
            + "?"
            + ")";

            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, customer_profile.getCustomer_details().getAccount_no());
            Config.pstmt.setString(2, customer_profile.getCustomer_details().getBank_name());

            int x = Config.pstmt.executeUpdate();

            if (x>0) {
            Config.config_mgr.loadCustomerDetails();
            return true;
            } else {
            return false;
            }
            }else{
            return false;
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            } catch (Exception ex) {
            ex.printStackTrace();
            return false;
            }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to update CustomerDetails  in database 
   public boolean updCustomerDetails(CustomerProfile customer_profile) {
        try {
            System.out.println(customer_profile.getProfile_details().getProfile_id());
            if(Config.profile_details_mgr.updProfileDetails(customer_profile.getProfile_details())){
            if(Config.address_details_mgr.updAddressDetails(customer_profile.getAddress_details())){
            if(Config.contact_details_mgr.updContactDetails(customer_profile.getContact_details())){
            if(Config.identity_details_mgr.updIdentityDetails(customer_profile.getIdentity_details())){
            Config.sql = "update customer_details set"
                     + " profile_id = ?, "
                    + " address_id = ?, "
                    + " contact_id = ?, "
                    + " identity_id = ?, "
                    + " account_no = ?, "
                    + " bank_name = ? "
                    + " where customer_id = "+customer_profile.getCustomer_details().getCustomer_id();
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, customer_profile.getProfile_details().getProfile_id());
            Config.pstmt.setString(2, customer_profile.getAddress_details().getAddress_id());
            Config.pstmt.setString(3, customer_profile.getContact_details().getContact_id());
            Config.pstmt.setString(4, customer_profile.getIdentity_details().getIdentity_id());
            Config.pstmt.setString(5, customer_profile.getCustomer_details().getAccount_no());
            Config.pstmt.setString(6, customer_profile.getCustomer_details().getBank_name());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                
                Config.config_mgr.loadCustomerDetails();
                return true;
            } else {
                return false;
            }
            }else{
            return false;
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete CustomerDetails in database
   public boolean delCustomerDetails(String customer_id)
   {
        try {          
           if(Config.profile_details_mgr.delProfileDetails(Config.config_customer_details.get(Config.id_customer_details.indexOf(customer_id)).getProfile_id())){
            if(Config.address_details_mgr.delAddressDetails(Config.config_customer_details.get(Config.id_customer_details.indexOf(customer_id)).getAddress_id())){
            if(Config.contact_details_mgr.delContactDetails(Config.config_customer_details.get(Config.id_customer_details.indexOf(customer_id)).getContact_id())){
            if(Config.identity_details_mgr.delIdentityDetails(Config.config_customer_details.get(Config.id_customer_details.indexOf(customer_id)).getIdentity_id())){
            Config.sql = "delete from customer_details where customer_id = '"+customer_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.config_mgr.loadCustomerDetails();
                return true;
            } else {
                return false;
            }
            }else{
            return false;
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }  
}
