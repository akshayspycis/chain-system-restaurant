package class_manager;

import data_manager.configuration.Config;
import data_manager.configuration.EmployeeProfile;

public class EmployeeDetailsMgr {
       //method to insert EmployeeDetails in database
    public boolean insEmployeeDetails(EmployeeProfile employee_profile) {
        try {          
            if(Config.profile_details_mgr.insProfileDetails(employee_profile.getProfile_details())){
            if(Config.address_details_mgr.insAddressDetails(employee_profile.getAddress_details())){
            if(Config.contact_details_mgr.insContactDetails(employee_profile.getContact_details())){
            if(Config.identity_details_mgr.insIdentityDetails(employee_profile.getIdentity_details())){
            Config.sql = "insert into employee_details ("                   
            + "profile_id,"
            + "address_id,"
            + "contact_id,"
            + "identity_id,"
            + "designation_id,"
            + "account_no,"
            + "bank_name ,"
            + "joining_date ,"
            + "leaving_date ,"
            + "message_id )"
            + " values ("
            + "(select max(profile_id) from profile_details),"
            + "(select max(address_id) from address_details),"
            + "(select max(contact_id) from contact_details),"
            + "(select max(identity_id) from identity_details),"
            + "?,"
            + "?,"
            + "?,"
            + "?,"
            + "?,"
            + "?"
            + ")";

            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, employee_profile.getEmployee_details().getDesignation_id());
            Config.pstmt.setString(2, employee_profile.getEmployee_details().getAccount_no());
            Config.pstmt.setString(3, employee_profile.getEmployee_details().getBank_name());
            Config.pstmt.setString(4, employee_profile.getEmployee_details().getJoining_date());
            Config.pstmt.setString(5, employee_profile.getEmployee_details().getLeaving_date());
            Config.pstmt.setString(6, employee_profile.getEmployee_details().getMessage_id());

            int x = Config.pstmt.executeUpdate();

            if (x>0) {
            Config.config_mgr.loadEmployeeDetails();
            return true;
            } else {
            return false;
            }
            }else{
            return false;
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            } catch (Exception ex) {
            ex.printStackTrace();
            return false;
            }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to update EmployeeDetails in database 
   public boolean updEmployeeDetails(EmployeeProfile employee_profile) {
        try {
            if(Config.profile_details_mgr.updProfileDetails(employee_profile.getProfile_details())){
            if(Config.address_details_mgr.updAddressDetails(employee_profile.getAddress_details())){
            if(Config.contact_details_mgr.updContactDetails(employee_profile.getContact_details())){
            if(Config.identity_details_mgr.updIdentityDetails(employee_profile.getIdentity_details())){
            Config.sql = "update employee_details set"
                    + " profile_id = ?, "
                    + " address_id = ?, "
                    + " contact_id = ?, "
                    + " identity_id = ?, "
                    + " designation_id = ?, "
                    + " account_no = ?, "
                    + " bank_name = ?, "
                    + " joining_date = ? ,"
                    + " leaving_date = ? ,"
                    + " message_id = ? "
                    + " where employee_id = "+employee_profile.getEmployee_details().getEmployee_id();
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, employee_profile.getProfile_details().getProfile_id());
            Config.pstmt.setString(2, employee_profile.getAddress_details().getAddress_id());
            Config.pstmt.setString(3, employee_profile.getContact_details().getContact_id());
            Config.pstmt.setString(4, employee_profile.getIdentity_details().getIdentity_id());
            Config.pstmt.setString(5, employee_profile.getEmployee_details().getDesignation_id());
            Config.pstmt.setString(6, employee_profile.getEmployee_details().getAccount_no());
            Config.pstmt.setString(7, employee_profile.getEmployee_details().getBank_name());
            Config.pstmt.setString(8, employee_profile.getEmployee_details().getJoining_date());
            Config.pstmt.setString(9, employee_profile.getEmployee_details().getLeaving_date());
            Config.pstmt.setString(10, employee_profile.getEmployee_details().getMessage_id());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                
                Config.config_mgr.loadEmployeeDetails();
                return true;
            } else {
                return false;
            }
            }else{
            return false;
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete EmployeeDetails in database
   public boolean delEmployeeDetails(String employee_id)
   {
       try {
            if(Config.profile_details_mgr.delProfileDetails(Config.config_employee_details.get(Config.id_employee_details.indexOf(employee_id)).getProfile_id())){
            if(Config.address_details_mgr.delAddressDetails(Config.config_employee_details.get(Config.id_employee_details.indexOf(employee_id)).getAddress_id())){
            if(Config.contact_details_mgr.delContactDetails(Config.config_employee_details.get(Config.id_employee_details.indexOf(employee_id)).getContact_id())){
            if(Config.identity_details_mgr.delIdentityDetails(Config.config_employee_details.get(Config.id_employee_details.indexOf(employee_id)).getIdentity_id())){
            Config.sql = "delete from employee_details where employee_id = '"+employee_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.config_mgr.loadEmployeeDetails();
                return true;
            } else {
                return false;
            }
            }else{
            return false;
            }
            }else{
            return false;
            }
            }else{
            return false;    
            }
            }else{
            return false;    
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
