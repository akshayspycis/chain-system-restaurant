/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package class_manager;

import data_manager.DemageItem;
import data_manager.configuration.Config;
import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class DemageItemMgr {
//method to insert DemageItem in database
    public boolean insDemageItem(DemageItem demage_item) {
        try {          
            Config.sql = "insert into demage_item ("                   
                    + "distribution_details_id,"
                    + "item_id,"
                    + "item_list_id,"
                    + "quantity )"
                    + " values (?,?,?,?)";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, demage_item.getDistribution_details_id());
            Config.pstmt.setString(2, demage_item.getItem_id());
            Config.pstmt.setString(3, demage_item.getItem_list_id());
            Config.pstmt.setString(4, demage_item.getQuantity());
            int arr=Config.pstmt.executeUpdate();
            if(arr>0) {
                return true;
            } else {
            return false;
            }
        }catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//    //----------------------------------------------------------------------------------------------
//    
//   //method to DemageItem in database 
   public boolean updDemageItem(DemageItem demage_item,double quantity) {
        try {
            Config.sql = "update demage_item set"
                    + " quantity = ? "
                    + " where demage_item_id = '"+demage_item.getDemage_item_id()+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, demage_item.getQuantity());
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                    return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//   //----------------------------------------------------------------------------------------------
//   
//   //method to delete DemageItem in database
   public boolean delDemageItem(String demage_item_id)
   {
        try {          
            Config.sql = "delete from demage_item where demage_item_id = '"+demage_item_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                    Config.config_mgr.loadStockDetails();
                    return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }      
}
