package data_manager;

/**
 *
 * @author akshay
 */
public class AdvanceDetails {
    String advance_id  = null;
    String employee_id  = null;
    String _date  = null;
    String _month  = null;
    String _year  = null;
    String amount  = null;
    String message_id  = null;

    public String getAdvance_id() {
        return advance_id;
    }

    public void setAdvance_id(String advance_id) {
        this.advance_id = advance_id;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }

    public String getMonth() {
        return _month;
    }

    public void setMonth(String _month) {
        this._month = _month;
    }

    public String getYear() {
        return _year;
    }

    public void setYear(String _year) {
        this._year = _year;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }
    
}
