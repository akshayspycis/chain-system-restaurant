/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data_manager;

/**
 *
 * @author akshay
 */
public class ReturnItem {
    String return_item_id =null;
    String distribution_details_id =null;
    String item_id =null;
    String item_list_id =null;
    String quantity =null;

    public String getReturn_item_id() {
        return return_item_id;
    }

    public void setReturn_item_id(String return_item_id) {
        this.return_item_id = return_item_id;
    }

    public String getDistribution_details_id() {
        return distribution_details_id;
    }

    public void setDistribution_details_id(String distribution_details_id) {
        this.distribution_details_id = distribution_details_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getItem_list_id() {
        return item_list_id;
    }

    public void setItem_list_id(String item_list_id) {
        this.item_list_id = item_list_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

        
}

