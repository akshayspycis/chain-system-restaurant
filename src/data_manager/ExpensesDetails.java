package data_manager;
public class ExpensesDetails {
    String expenses_details_id = null;
    String type = null;

    public String getExpenses_details_id() {
        return expenses_details_id;
    }

    public void setExpenses_details_id(String expenses_details_id) {
        this.expenses_details_id = expenses_details_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
   
    
}
