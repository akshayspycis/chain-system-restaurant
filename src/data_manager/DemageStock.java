package data_manager;

public class DemageStock {
    
    String demage_stock_id = null;    
    String item_id = null;
    String item_list_id = null;
    String _date = null;
    String _month = null;
    String _year = null;
    String quantity = null;
    String message_id = null;

    public String getDemage_stock_id() {
        return demage_stock_id;
    }

    public void setDemage_stock_id(String demage_stock_id) {
        this.demage_stock_id = demage_stock_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getItem_list_id() {
        return item_list_id;
    }

    public void setItem_list_id(String item_list_id) {
        this.item_list_id = item_list_id;
    }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }

    public String getMonth() {
        return _month;
    }

    public void setMonth(String _month) {
        this._month = _month;
    }

    public String getYear() {
        return _year;
    }

    public void setYear(String _year) {
        this._year = _year;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    
}
