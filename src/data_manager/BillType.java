package data_manager;

public class BillType {
    String bill_type_id =  null;
    String bill_no =  null;
    String chalan_no =  null;
    String loading_charges =  null;
    String other_charges =  null;
    String _date =  null;
    String _month =  null;
    String _year =  null;
    String message_id =  null;

    public String getBill_type_id() {
        return bill_type_id;
    }

    public void setBill_type_id(String bill_type_id) {
        this.bill_type_id = bill_type_id;
    }

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public String getChalan_no() {
        return chalan_no;
    }

    public void setChalan_no(String chalan_no) {
        this.chalan_no = chalan_no;
    }

    public String getLoading_charges() {
        return loading_charges;
    }

    public void setLoading_charges(String loading_charges) {
        this.loading_charges = loading_charges;
    }

    public String getOther_charges() {
        return other_charges;
    }

    public void setOther_charges(String other_charges) {
        this.other_charges = other_charges;
    }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }

    public String getMonth() {
        return _month;
    }

    public void setMonth(String _month) {
        this._month = _month;
    }

    public String getYear() {
        return _year;
    }

    public void setYear(String _year) {
        this._year = _year;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

        }
