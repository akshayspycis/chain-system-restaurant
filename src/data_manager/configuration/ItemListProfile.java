package data_manager.configuration;

import data_manager.ItemList;
import data_manager.StockItem;
/**
 *
 * @author akshay
 */
public class ItemListProfile {
    ItemList item_list = null;
    SupplierProfile supplier_profile = null;
    StockItem stock_item = null;

    public ItemList getItem_list() {
        return item_list;
    }

    public void setItem_list(ItemList item_list) {
        this.item_list = item_list;
    }

    public SupplierProfile getSupplier_profile() {
        return supplier_profile;
    }

    public void setSupplier_profile(SupplierProfile supplier_profile) {
        this.supplier_profile = supplier_profile;
    }

    public StockItem getStock_item() {
        return stock_item;
    }

    public void setStock_item(StockItem stock_item) {
        this.stock_item = stock_item;
    }

   
}
