package data_manager.configuration;

import data_manager.AddressDetails;
import data_manager.ContactDetails;
import data_manager.DesignationDetails;
import data_manager.EmployeeDetails;
import data_manager.IdentityDetails;
import data_manager.ProfileDetails;

public class EmployeeProfile {
    EmployeeDetails employee_details = null;
    ProfileDetails profile_details = null;
    AddressDetails address_details = null;
    ContactDetails contact_details = null;
    IdentityDetails identity_details = null;
    DesignationProfile designation_profile = null;

    public EmployeeDetails getEmployee_details() {
        return employee_details;
    }

    public void setEmployee_details(EmployeeDetails employee_details) {
        this.employee_details = employee_details;
    }

    public ProfileDetails getProfile_details() {
        return profile_details;
    }

    public void setProfile_details(ProfileDetails profile_details) {
        this.profile_details = profile_details;
    }

    public AddressDetails getAddress_details() {
        return address_details;
    }

    public void setAddress_details(AddressDetails address_details) {
        this.address_details = address_details;
    }

    public ContactDetails getContact_details() {
        return contact_details;
    }

    public void setContact_details(ContactDetails contact_details) {
        this.contact_details = contact_details;
    }

    public IdentityDetails getIdentity_details() {
        return identity_details;
    }

    public void setIdentity_details(IdentityDetails identity_details) {
        this.identity_details = identity_details;
    }

    public DesignationProfile getDesignation_profile() {
        return designation_profile;
    }

    public void setDesignation_profile(DesignationProfile designation_profile) {
        this.designation_profile = designation_profile;
    }
    
}
