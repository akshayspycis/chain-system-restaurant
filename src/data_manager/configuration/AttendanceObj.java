package data_manager.configuration;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AttendanceObj {
    JLabel sno=null;
    JTextField name =null;
    JFormattedTextField time_in =null;
    JFormattedTextField time_out =null;
    JCheckBox status = null;
    JButton message = null;
    JLabel message_id = null;

    public JLabel getSno() {
        return sno;
    }

    public void setSno(JLabel sno) {
        this.sno = sno;
    }

    public JTextField getName() {
        return name;
    }

    public void setName(JTextField name) {
        this.name = name;
    }

    public JFormattedTextField getTime_in() {
        return time_in;
    }

    public void setTime_in(JFormattedTextField time_in) {
        this.time_in = time_in;
    }

    public JFormattedTextField getTime_out() {
        return time_out;
    }

    public void setTime_out(JFormattedTextField time_out) {
        this.time_out = time_out;
    }

    public JCheckBox getStatus() {
        return status;
    }

    public void setStatus(JCheckBox status) {
        this.status = status;
    }

    public JButton getMessage() {
        return message;
    }

    public void setMessage(JButton message) {
        this.message = message;
    }

    public JLabel getMessage_id() {
        return message_id;
    }

    public void setMessage_id(JLabel message_id) {
        this.message_id = message_id;
    }
}
