package data_manager.configuration;

import class_manager.AddressDetailsMgr;
import class_manager.AdvanceDetailsMgr;
import class_manager.AttendenceDetailsMgr;
import class_manager.BikeDetailsMgr;
import class_manager.BillTypeMgr;
import class_manager.ContactDetailsMgr;
import class_manager.CustomerDetailsMgr;
import class_manager.DemageItemMgr;
import class_manager.DemageStockMgr;
import class_manager.DesignationDetailsMgr;
import class_manager.DistributedStockMgr;
import class_manager.DistributionDetailsMgr;
import class_manager.DistributionItemMgr;
import class_manager.EmployeeDetailsMgr;
import class_manager.ExpensesDetailsMgr;
import class_manager.ExpensesMgr;
import class_manager.IdentityDetailsMgr;
import class_manager.ItemListMgr;
import class_manager.MaintenanceDetailsMgr;
import class_manager.MaintenanceTypeMgr;
import class_manager.MessageDetailsMgr;
import class_manager.ProfileDetailsMgr;
import class_manager.ReceiveStockMgr;
import class_manager.ReturnItemMgr;
import class_manager.ReturnStockMgr;
import class_manager.SalaryDetailsMgr;
import class_manager.SalaryDistributionMgr;
import class_manager.SalesDetailsMgr;
import class_manager.StockDetailsMgr;
import class_manager.StockItemMgr;
import class_manager.SupplierDetailsMgr;
import class_manager.TransportDetailsMgr;
import class_manager.configuration.ConfigMgr;
import data_manager.AddressDetails;
import data_manager.BikeDetails;
import data_manager.BillType;
import data_manager.ContactDetails;
import data_manager.CustomerDetails;
import data_manager.DesignationDetails;
import data_manager.DistributedStock;
import data_manager.EmployeeDetails;
import data_manager.ExpensesDetails;
import data_manager.IdentityDetails;
import data_manager.ItemList;
import data_manager.MaintenanceType;
import data_manager.MessageDetails;
import data_manager.ProfileDetails;
import data_manager.SalaryDetails;
import data_manager.StockDetails;
import data_manager.StockItem;
import data_manager.SupplierDetails;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import modules.admin_management.AdminManagement;
import modules.admin_management.Expenses.Expensesdetails;
import modules.admin_management.Expenses.NewExpenses;
import modules.admin_management.Expenses.ViewExpenses;
import modules.admin_management.configproduct.NewBike;
import modules.admin_management.configproduct.NewDesignation;
import modules.admin_management.configproduct.NewMaintenance;
import modules.admin_management.configproduct.ViewBike;
import modules.admin_management.configproduct.ViewDesignation;
import modules.admin_management.configproduct.ViewMaintenance;
import modules.admin_management.configproduct.productconfiguration;
import modules.admin_management.itemlist.NewItem;
import modules.admin_management.itemlist.ViewItem;
import modules.admin_management.itemlist.itemlist;
import modules.admin_management.manufacture.ManufactureItem;
import modules.admin_management.manufacture.NewManufactureItem;
import modules.admin_management.manufacture.ViewManufactureItem;
import modules.admin_management.soda.NewSoda;
import modules.admin_management.soda.Soda;
import modules.admin_management.soda.ViewSoda;
import modules.admin_management.stock.NewStockItem;
import modules.admin_management.stock.ViewStockItem;
import modules.admin_management.stock.stockitem;
import modules.customer_management.CustomerManagement;
import modules.customer_management.NewCustomer;
import modules.customer_management.ViewCustomer;
import modules.dashboard.Home;
import modules.distribution_management.DistributionManagement;
import modules.distribution_management.demage.NewDemage;
import modules.distribution_management.demage.ViewDemage;
import modules.distribution_management.distribute.NewDistribution;
import modules.distribution_management.distribute.ViewDistribution;
import modules.distribution_management.distribute.ViewDistributionItem;
import modules.distribution_management.return_item.NewReturn;
import modules.distribution_management.return_item.ViewReturn;
import modules.distribution_management.search.DesignationSearch;
import modules.employee_management.EmployeeManagement;
import modules.employee_management.attendance.Attendance;
import modules.employee_management.attendance.ViewAttendance;
import modules.employee_management.employee.NewEmployee;
import modules.employee_management.employee.ViewEmployee;
import modules.employee_management.salary_information.AddAdvance;
import modules.employee_management.salary_information.AddSalaryInfo;
import modules.employee_management.salary_information.ViewAdvance;
import modules.employee_management.salary_information.ViewSalaryInfo;
import modules.employee_management.salary_management.NewSalary;
import modules.employee_management.salary_management.SalaryManagement;
import modules.employee_management.salary_management.ViewSalary;
import modules.employee_management.search.EmployeeSearch;
import modules.expenses_management.AddExpenses;
import modules.expenses_management.ExpensesManagement;
import modules.expenses_management.ViewAddExpenses;
import modules.expenses_management.search.ExpensesSearch;
import modules.message_management.NewMessage;
import modules.message_management.ViewMessage;
import modules.sales_management.NewSales;
import modules.sales_management.SaleManagement;
import modules.sales_management.ViewSaleItem;
import modules.sales_management.ViewSales;
import modules.stock_management.Stock.NewStock;
import modules.stock_management.Stock.StockManagement;
import modules.stock_management.Stock.ViewStock;
import modules.stock_management.demage_item.NewDemageItem;
import modules.stock_management.demage_item.ViewDemageItem;
import modules.stock_management.return_item.NewReturnItem;
import modules.stock_management.return_item.ViewReturnItem;
import modules.stock_management.search.ItemSearch;
import modules.stock_management.search.RateSearch;
import modules.supplier_management.NewSupplier;
import modules.supplier_management.SupplierManagement;
import modules.supplier_management.ViewSupplier;
import modules.transport_management.NewTransport;
import modules.transport_management.TransportManagement;
import modules.transport_management.ViewTransport;
import modules.transport_management.maintenance.AddMaintenance;
import modules.transport_management.maintenance.ViewAddMaintenance;
import modules.transport_management.search.MaintenaceSearch;
import modules.transport_management.search.TransportSearch;

public class Config {

    //database variables
    public static Connection conn = null;
    public static PreparedStatement pstmt = null;
    public static Statement stmt = null;
    public static ResultSet rs = null;
    public static String sql = null;    
    
    //product variables
    public static ArrayList<ProfileDetails> config_profile_details = null;
    public static ArrayList<String> id_profile_details = null;
    
    public static ArrayList<AddressDetails> config_address_details = null;
    public static ArrayList<String> id_address_details = null;
    
    public static ArrayList<ContactDetails> config_contact_details = null;
    public static ArrayList<String> id_contact_details = null;
    
    public static ArrayList<IdentityDetails> config_identity_details = null;
    public static ArrayList<String> id_identity_details = null;
    
    public static ArrayList<DesignationDetails> config_designation_details = null;
    public static ArrayList<String> id_designation_details = null;
    
    public static ArrayList<BikeDetails> config_bike_details = null;
    public static ArrayList<String> id_bike_details = null;
    
    public static ArrayList<ExpensesDetails> config_expenses_details = null;
    public static ArrayList<String> id_expenses_details = null;
    
    public static ArrayList<StockItem> config_stock_item = null;
    public static ArrayList<String> id_stock_item = null;
    
    public static ArrayList<ItemList> config_item_list = null;
    public static ArrayList<String> id_item_list = null;

    public static ArrayList<CustomerDetails> config_customer_details = null;
    public static ArrayList<String> id_customer_details = null;
    
    public static ArrayList<EmployeeDetails> config_employee_details = null;
    public static ArrayList<String> id_employee_details = null;
    
    public static ArrayList<SalaryDetails> config_salary_details = null;
    public static ArrayList<String> id_salary_details = null;
    
//class managers
    public static ConfigMgr config_mgr = null;
    public static ProfileDetailsMgr profile_details_mgr = null;
    public static AddressDetailsMgr address_details_mgr = null;
    public static ContactDetailsMgr contact_details_mgr = null;
    public static IdentityDetailsMgr identity_details_mgr = null;
    public static DesignationDetailsMgr designation_details_mgr = null;
    public static BikeDetailsMgr bike_details_mgr = null;
    public static ExpensesDetailsMgr expenses_details_mgr = null;
    public static StockItemMgr stock_item_mgr = null;
    public static ItemListMgr item_list_mgr = null;
    public static CustomerDetailsMgr customer_details_mgr = null;
    public static EmployeeDetailsMgr employee_details_mgr = null;
    public static AdvanceDetailsMgr advnace_details_mgr = null;
    public static SalaryDistributionMgr salary_distribution_mgr = null;
    public static SalaryDetailsMgr salary_details_mgr = null;
    public static AttendenceDetailsMgr attendence_details_mgr = null;
    public static BillTypeMgr bill_type_mgr = null;
    public static DistributedStockMgr distributed_stock_mgr = null;
    public static MessageDetailsMgr message_details_mgr = null;
    public static ReceiveStockMgr receive_stock_mgr = null;
    public static DemageStockMgr demage_stock_mgr = null;
    public static ReturnStockMgr return_stock_mgr = null;
    public static StockDetailsMgr stock_details_mgr = null;
    public static SupplierDetailsMgr supplier_details_mgr = null;
    public static ExpensesMgr expenses_mgr = null;
    public static MaintenanceTypeMgr maintenance_type_mgr = null;
    public static MaintenanceDetailsMgr maintenance_details_mgr = null;
    public static TransportDetailsMgr transport_details_mgr = null;
    public static DistributionDetailsMgr distribution_details_mgr = null;
    public static DistributionItemMgr distribution_item_mgr = null;
    public static DemageItemMgr demage_item_mgr = null;
    public static ReturnItemMgr return_item_mgr = null;
    public static SalesDetailsMgr sales_details_mgr = null;
    
    //module forms
    
    //dashboard
        public static Home home = null;    
        
    //admin management
        public static AdminManagement admin_management = null;    
        public static productconfiguration product_configuration = null;    
        public static ViewDesignation view_designation = null;    
        public static NewDesignation new_designation = null;
        public static NewBike new_bike = null;    
        public static ViewBike view_bike = null;
    
        public static Expensesdetails expenses_details = null;    
        public static NewExpenses new_expenses = null;    
        public static ViewExpenses view_expenses = null;

        public static itemlist item_list = null;    
        public static NewItem new_item = null;    
        public static ViewItem view_item = null; 
        
        public static stockitem stock_item = null; 
        public static Soda soda = null; 
        public static ManufactureItem manufactureitem = null; 
        public static NewStockItem new_stock_item = null; 
        public static ViewStockItem view_stock_item = null; 
        public static NewSoda new_soda = null; 
        public static ViewSoda view_soda = null; 
        public static NewManufactureItem new_manu_item = null; 
        public static ViewManufactureItem view_manu_item = null; 
       
       //customer managment
        public static CustomerManagement customer_management= null;    
        public static NewCustomer new_customer = null;    
        public static ViewCustomer view_customer = null; 
       
       //employee managment
        public static EmployeeManagement employee_management= null;   
        public static NewEmployee new_employee = null;    
        public static ViewEmployee view_employee = null;
        public static AddSalaryInfo add_salary_info = null;
        public static ViewSalaryInfo view_salary_info = null;
        public static AddAdvance add_advance = null;
        public static ViewAdvance view_advance = null;
        public static SalaryManagement salary_management= null;    
        public static NewSalary new_salary = null;    
        public static ViewSalary view_salary = null;
        public static Attendance attendence = null;
        public static ViewAttendance view_attendence = null;

        //supplier management  
        public static SupplierManagement supplier_management= null;    
        public static NewSupplier new_supplier = null;    
        public static ViewSupplier view_supplier = null;    
    
        //stock management
        public static StockManagement stock_management= null;    
        public static NewStock new_stock= null;    
        public static ViewStock view_stock= null;    

        public static NewDemageItem new_demage_item = null; 
        public static ViewDemageItem view_demage_Item = null; 

       // public static ReturnItem return_Item = null; 
        public static ViewReturnItem view_return_Item = null; 
        public static NewReturnItem new_return_item=null;
        public static ViewReturnItem view_return_item=null; 
        
        public static NewMessage new_message = null;    
        public static ViewMessage view_message = null; 

        public static AddExpenses add_expenses=null;
        public static ViewAddExpenses view_add_expenses=null;
        public static ExpensesManagement expenses_management=null;
        
        public static NewMaintenance new_maintenance=null;
        public static ViewMaintenance view_Maintenance=null;
        
        public static TransportManagement transport_management=null;
        public static NewTransport new_transport=null;
        public static ViewTransport view_transport=null;
        public static AddMaintenance add_maintenance=null;
        public static ViewAddMaintenance view_add_maintenance=null;
        
        public static DistributionManagement distribution_management=null;
        public static NewDistribution new_distribution=null;
        public static ViewDistribution view_distribution=null;
        public static ViewDistributionItem view_distribution_item=null;
        public static NewDemage new_demage=null;
        public static ViewDemage view_demage=null;
        public static NewReturn new_return=null;
        public static ViewReturn view_return=null;
        
        public static SaleManagement sale_management =null;
        public static NewSales new_sales=null;
        public static ViewSales view_sales=null;
        public static ViewSaleItem view_sales_item=null;
    
//.....................ArrayList.....................................
    
public static ArrayList<SupplierDetails> config_supplier_details = null;
public static ArrayList<String> id_supplier_details = null;
    
public static ArrayList<MessageDetails> config_message_details = null;
public static ArrayList<String> id_message_details = null;
    
public static ArrayList<BillType> config_bill_type = null;
public static ArrayList<String> id_bill_type = null;
    
public static ArrayList<DistributedStock> config_distributed_stock = null;
public static ArrayList<String> id_distributed_stock = null;
    
 public static ArrayList<StockDetails> config_stock_details = null;
 public static ArrayList<String> id_stock_details = null;
  //  public static ArrayList<DemageItem> config_demage_item = null;
 public static ArrayList<String> id_demage_item = null;
 
 public static ArrayList<String> id_receive_item = null;
 //   public static ArrayList<ReturnItem> config_return_item = null;
 public static ArrayList<String> id_return_item = null;
 
 public static ArrayList<MaintenanceType> config_maintenance_type = null;
 public static ArrayList<String> id_maintenance_type = null;       
 
//......................String list...................................
    
//...................search form...........................
public static EmployeeSearch employee_search = null;
public static ItemSearch item_search = null;
public static RateSearch rate_search = null;
public static ExpensesSearch expenses_search = null;
public static TransportSearch transport_search = null;
public static MaintenaceSearch maintenance_search =null;
public static DesignationSearch designation_search =null;
//.......................................................

}
