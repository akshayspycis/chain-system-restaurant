package data_manager.configuration;

import data_manager.BillType;
import data_manager.DemageStock;
import data_manager.StockItem;

public class DemageProfile {
    DemageStock demage_stock = null;
    BillType bill_type = null;
    StockItem stock_item = null; 
    public DemageStock getDemage_stock() {
        return demage_stock;
    }

    public void setDemage_stock(DemageStock demage_stock) {
        this.demage_stock = demage_stock;
    }

    public BillType getBill_type() {
        return bill_type;
    }

    public void setBill_type(BillType bill_type) {
        this.bill_type = bill_type;
    }

    public StockItem getStock_item() {
        return stock_item;
    }

    public void setStock_item(StockItem stock_item) {
        this.stock_item = stock_item;
    }
    
}
