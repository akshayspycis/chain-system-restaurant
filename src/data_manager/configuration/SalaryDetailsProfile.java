package data_manager.configuration;

import data_manager.SalaryDetails;

public class SalaryDetailsProfile {
    SalaryDetails selery_details= null;
    EmployeeProfile employee_profile = null;
   
    public SalaryDetails getSelery_details() {
        return selery_details;
    }

    public void setSelery_details(SalaryDetails selery_details) {
        this.selery_details = selery_details;
    }

    public EmployeeProfile getEmployee_profile() {
        return employee_profile;
    }

    public void setEmployee_profile(EmployeeProfile employee_profile) {
        this.employee_profile = employee_profile;
    }

}
