package data_manager.configuration;

import data_manager.BillType;
import data_manager.DesignationDetails;
import data_manager.DistributedStock;
import data_manager.MessageDetails;
import data_manager.StockItem;
/**
 *
 * @author akshay
 */
public class DistributedStockProfile {
    DistributedStock distributed_stock = null;
    StockItem stock_item = null;
    BillType bill_type = null;    
    DesignationDetails designation_details = null;
    MessageDetails message_details = null;         

    public DistributedStock getDistributed_stock() {
        return distributed_stock;
    }

    public void setDistributed_stock(DistributedStock distributed_stock) {
        this.distributed_stock = distributed_stock;
    }

    public StockItem getStock_item() {
        return stock_item;
    }

    public void setStock_item(StockItem stock_item) {
        this.stock_item = stock_item;
    }

   

    public BillType getBill_type() {
        return bill_type;
    }

    public void setBill_type(BillType bill_type) {
        this.bill_type = bill_type;
    }

    public DesignationDetails getDesignation_details() {
        return designation_details;
    }

    public void setDesignation_details(DesignationDetails designation_details) {
        this.designation_details = designation_details;
    }

    public MessageDetails getMessage_details() {
        return message_details;
    }

    public void setMessage_details(MessageDetails message_details) {
        this.message_details = message_details;
    }
}
