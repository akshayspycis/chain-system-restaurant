package data_manager.configuration;

import data_manager.AddressDetails;
import data_manager.ContactDetails;
import data_manager.DesignationDetails;

public class DesignationProfile {
    
    DesignationDetails designation_details = null;
    ContactDetails contact_details = null;
    AddressDetails address_details = null;

    public DesignationDetails getDesignation_details() {
        return designation_details;
    }

    public void setDesignation_details(DesignationDetails designation_details) {
        this.designation_details = designation_details;
    }
    
    public AddressDetails getAddress_details() {
        return address_details;
    }

    public void setAddress_details(AddressDetails address_details) {
        this.address_details = address_details;
    }

    public ContactDetails getContact_details() {
        return contact_details;
    }

    public void setContact_details(ContactDetails contact_details) {
        this.contact_details = contact_details;
    }
 
}
