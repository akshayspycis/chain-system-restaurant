/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data_manager;

/**
 *
 * @author akshay
 */
public class TransportDetails {
    String transport_details_id = null;
    String bike_id = null;
    String _date = null;
    String _month = null;
    String _year = null;
    String str_reading = null;
    String end_reading = null;
    String diff_reading = null;
    String message_id = null;

    public String getTransport_details_id() {
        return transport_details_id;
    }

    public void setTransport_details_id(String transport_details_id) {
        this.transport_details_id = transport_details_id;
    }

    public String getBike_id() {
        return bike_id;
    }

    public void setBike_id(String bike_id) {
        this.bike_id = bike_id;
    }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }

    public String getMonth() {
        return _month;
    }

    public void setMonth(String _month) {
        this._month = _month;
    }

    public String getYear() {
        return _year;
    }

    public void setYear(String _year) {
        this._year = _year;
    }

    public String getStr_reading() {
        return str_reading;
    }

    public void setStr_reading(String str_reading) {
        this.str_reading = str_reading;
    }

    public String getEnd_reading() {
        return end_reading;
    }

    public void setEnd_reading(String end_reading) {
        this.end_reading = end_reading;
    }

    public String getDiff_reading() {
        return diff_reading;
    }

    public void setDiff_reading(String diff_reading) {
        this.diff_reading = diff_reading;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }
    
}
