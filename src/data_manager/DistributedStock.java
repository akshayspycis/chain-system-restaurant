package data_manager;

public class DistributedStock {
    String dis_stock_id = null;
    String item_id = null;
    String _date = null;
    String _month = null;
    String _year = null;
    String quantity = null;
    String bill_type_id = null;
    String designation_id = null;
    String message_id = null;

    public String getDis_stock_id() {
        return dis_stock_id;
    }

    public void setDis_stock_id(String dis_stock_id) {
        this.dis_stock_id = dis_stock_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }

    public String getMonth() {
        return _month;
    }

    public void setMonth(String _month) {
        this._month = _month;
    }

    public String getYear() {
        return _year;
    }

    public void setYear(String _year) {
        this._year = _year;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getBill_type_id() {
        return bill_type_id;
    }

    public void setBill_type_id(String bill_type_id) {
        this.bill_type_id = bill_type_id;
    }

    public String getDesignation_id() {
        return designation_id;
    }

    public void setDesignation_id(String designation_id) {
        this.designation_id = designation_id;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }
}
