/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data_manager;

/**
 *
 * @author akshay
 */
public class MaintenanceDetails {
String maintenance_details_id =null;
    String maintenance_type_id =null;
    String bike_id =null;
    String _date =null;
    String _month =null;
    String _year =null;
    String amount =null;
    String message_id =null;

    public String getMaintenance_details_id() {
        return maintenance_details_id;
    }

    public void setMaintenance_details_id(String maintenance_details_id) {
        this.maintenance_details_id = maintenance_details_id;
    }

    public String getMaintenance_type_id() {
        return maintenance_type_id;
    }

    public void setMaintenance_type_id(String maintenance_type_id) {
        this.maintenance_type_id = maintenance_type_id;
    }

    public String getBike_id() {
        return bike_id;
    }

    public void setBike_id(String bike_id) {
        this.bike_id = bike_id;
    }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }

    public String getMonth() {
        return _month;
    }

    public void setMonth(String _month) {
        this._month = _month;
    }

    public String getYear() {
        return _year;
    }

    public void setYear(String _year) {
        this._year = _year;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

}
