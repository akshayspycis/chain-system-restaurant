/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data_manager;

/**
 *
 * @author infopark
 */
public class Expenses {

    String expenses_id=null;
    String expenses_details_id=null;
    String _date=null;
    String _month=null;
    String _year=null;
    String amount=null;
    String message_id=null;

    public String getExpenses_id() {
        return expenses_id;
    }

    public void setExpenses_id(String expenses_id) {
        this.expenses_id = expenses_id;
    }

    public String getExpenses_details_id() {
        return expenses_details_id;
    }

    public void setExpenses_details_id(String expenses_details_id) {
        this.expenses_details_id = expenses_details_id;
    }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }

    public String getMonth() {
        return _month;
    }

    public void setMonth(String _month) {
        this._month = _month;
    }

    public String getYear() {
        return _year;
    }

    public void setYear(String _year) {
        this._year = _year;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }
    
}
