/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data_manager;

/**
 *
 * @author akshay
 */
public class MaintenanceType {
   String maintenance_type_id =null; 
   String type =null;
   String other =null;

    public String getMaintenance_type_id() {
        return maintenance_type_id;
    }

    public void setMaintenance_type_id(String maintenance_type_id) {
        this.maintenance_type_id = maintenance_type_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

       
}
