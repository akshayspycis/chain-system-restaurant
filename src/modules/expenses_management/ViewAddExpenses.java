package modules.expenses_management;

import data_manager.Expenses;
import data_manager.configuration.Config;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;


public class ViewAddExpenses extends javax.swing.JDialog {
     String expenses_details_id="";
    String message_id="";
    
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
   
    public ViewAddExpenses(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        txt_expensestype.setBackground(new Color(240,240,240));
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_expensestype = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_amount = new javax.swing.JTextField();
        lbl_expenses_id = new javax.swing.JLabel();
        jdc_date = new com.toedter.calendar.JDateChooser();
        btn_save = new javax.swing.JButton();
        btn_reset = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Expenses", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Expense Type");

        txt_expensestype.setEditable(false);
        txt_expensestype.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_expensestypeKeyPressed(evt);
            }
        });

        jLabel2.setText("Date ");

        jLabel3.setText("Amount");

        txt_amount.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_amountFocusLost(evt);
            }
        });
        txt_amount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_amountKeyPressed(evt);
            }
        });

        lbl_expenses_id.setForeground(new java.awt.Color(255, 255, 255));
        lbl_expenses_id.setText("jLabel4");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(60, 60, 60)
                                .addComponent(lbl_expenses_id))
                            .addComponent(txt_expensestype, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lbl_expenses_id))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_expensestype, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        btn_save.setText("Update");
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });
        btn_save.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_saveKeyPressed(evt);
            }
        });

        btn_reset.setText("Massege");
        btn_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_resetActionPerformed(evt);
            }
        });
        btn_reset.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_resetKeyPressed(evt);
            }
        });

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });
        btn_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_cancelKeyPressed(evt);
            }
        });

        jPanel13.setBackground(new java.awt.Color(230, 79, 6));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("View Expense");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_reset)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_save)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancel)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_cancel)
                    .addComponent(btn_save)
                    .addComponent(btn_reset))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void txt_expensestypeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_expensestypeKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            jdc_date.requestFocus();
            jdc_date.setBackground(new Color(240,240,240));
            txt_expensestype.setBackground(Color.WHITE);
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            jdc_date.requestFocus();
            jdc_date.setBackground(new Color(240,240,240));
            txt_expensestype.setBackground(Color.WHITE);
        }
    }//GEN-LAST:event_txt_expensestypeKeyPressed

    private void txt_amountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_amountKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            btn_reset.requestFocus();
            txt_amount.setBackground(Color.WHITE);
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            btn_reset.requestFocus();
            txt_amount.setBackground(Color.WHITE);
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jdc_date.requestFocus();
            jdc_date.setBackground(new Color(240,240,240));
            txt_amount.setBackground(Color.WHITE);
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            jdc_date.requestFocus();
            jdc_date.setBackground(new Color(240,240,240));
            txt_amount.setBackground(Color.WHITE);
        }
    }//GEN-LAST:event_txt_amountKeyPressed

    private void btn_saveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_saveKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            btn_cancel.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            btn_cancel.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_reset.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            btn_reset.requestFocus();
        }
    }//GEN-LAST:event_btn_saveKeyPressed

    private void btn_resetKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_resetKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            btn_save.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            btn_save.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_amount.requestFocus();
            txt_amount.setBackground(new Color(240,240,240));
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            txt_amount.requestFocus();
            txt_amount.setBackground(new Color(240,240,240));
        }
    }//GEN-LAST:event_btn_resetKeyPressed

    private void btn_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_cancelKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_save.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            btn_save.requestFocus();
        }
    }//GEN-LAST:event_btn_cancelKeyPressed

    private void btn_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_resetActionPerformed
       if(message_id.equals("")){
           Config.new_message.onloadReset(7);
           Config.new_message.setVisible(true);
       }else{
           Config.view_message.onloadReset(7,message_id);
           Config.view_message.setVisible(true);
       }
    }//GEN-LAST:event_btn_resetActionPerformed

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        try {
        String str = checkValidity();
        if(str.equals("ok")){
                Expenses ed = new Expenses();
                ed.setExpenses_id(lbl_expenses_id.getText());
                ed.setExpenses_details_id(expenses_details_id);
                ed.setDate(sdf.format(jdc_date.getDate()));
                ed.setMonth(sdf_month.format(sdf.parse(sdf.format(jdc_date.getDate()))));
                ed.setYear(sdf_year.format(sdf.parse(sdf.format(jdc_date.getDate()))));
                ed.setAmount(txt_amount.getText().trim());
                ed.setMessage_id(message_id);
                System.out.println(message_id);
                if(Config.expenses_mgr.updExpenses(ed)){
                    Config.expenses_management.onloadReset();
                    JOptionPane.showMessageDialog(this, "Expenses Details updation successfully", "Success", JOptionPane.NO_OPTION);
                    dispose();
                }else{
                    JOptionPane.showMessageDialog(this, "Error in updation ", "Error", JOptionPane.ERROR_MESSAGE);   
                }
        }else{
            JOptionPane.showMessageDialog(this, str +"should not be blank", "Error", JOptionPane.ERROR_MESSAGE);
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_saveActionPerformed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void txt_amountFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_amountFocusLost
        if(!txt_amount.getText().trim().equals("")){
        try {
        Double.parseDouble(txt_amount.getText().trim());
        }catch (Exception e) {
        JOptionPane.showMessageDialog(this, "Only No.Value Required ", "Error", JOptionPane.ERROR_MESSAGE);   
        txt_amount.setText("");
        txt_amount.requestFocus();
        }
        }
    }//GEN-LAST:event_txt_amountFocusLost
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_reset;
    private javax.swing.JButton btn_save;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private com.toedter.calendar.JDateChooser jdc_date;
    private javax.swing.JLabel lbl_expenses_id;
    private javax.swing.JTextField txt_amount;
    private javax.swing.JTextField txt_expensestype;
    // End of variables declaration//GEN-END:variables

    public  void onloadReset(String expenses_id) {
        lbl_expenses_id.setText(expenses_id);
        try {
        Config.sql ="select * from expenses where expenses_id="+expenses_id;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if(Config.rs.next()) {
                expenses_details_id=Config.rs.getString("expenses_details_id");
                txt_expensestype.setText(Config.config_expenses_details.get(Config.id_expenses_details.indexOf(Config.rs.getString("expenses_details_id"))).getType());
                jdc_date.setDate(sdf.parse(Config.rs.getString("_date")));
                txt_amount.setText(Config.rs.getString("amount"));
                message_id=Config.rs.getString("message_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private String checkValidity() {
        if (Double.parseDouble(txt_amount.getText())==0.0) {
            return "Amount";
        }else{
            return "ok";
        }
    }
    public void setMessage(String message_id) {
        this.message_id = message_id;
    }
}