package modules.expenses_management;

import data_manager.configuration.Config;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

public class ExpensesManagement extends javax.swing.JDialog {
    boolean expenses_from = true;
    boolean expenses_to = true;
    SimpleDateFormat simple_date_formate = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_day = new SimpleDateFormat("dd");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
    
    DefaultTableModel tbl_expenses_model=null;
    DefaultTableModel tbl_expenses_report_model=null;

    public ExpensesManagement(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tbl_expenses_model=(DefaultTableModel) tbl_expenses.getModel();
        tbl_expenses_report_model=(DefaultTableModel) tbl_expenses_report.getModel();
        tbl_expenses_report.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        this.setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        pnl_employee = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txt_expenses_search = new javax.swing.JTextField();
        btn_expenses_search = new javax.swing.JButton();
        btn_expenses_add = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        expenses_check_to = new javax.swing.JCheckBox();
        expenses_check_from = new javax.swing.JCheckBox();
        jLabel15 = new javax.swing.JLabel();
        jdc_date_from = new com.toedter.calendar.JDateChooser();
        jdc_date_to = new com.toedter.calendar.JDateChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_expenses = new javax.swing.JTable();
        btn_expenses_cancel = new javax.swing.JButton();
        btn_expenses_view = new javax.swing.JButton();
        btn_expenses_print = new javax.swing.JButton();
        jPanel16 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        txt_expenses_message = new javax.swing.JTextPane();
        jPanel22 = new javax.swing.JPanel();
        pnl_adv_report1 = new javax.swing.JPanel();
        jPanel24 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        btn_expenses_report_search = new javax.swing.JButton();
        jmc_month_report = new com.toedter.calendar.JMonthChooser();
        jLabel24 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        jdc_date_report = new com.toedter.calendar.JDateChooser();
        jyc_year_report = new com.toedter.calendar.JYearChooser();
        jLabel5 = new javax.swing.JLabel();
        check_date_report = new javax.swing.JCheckBox();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane13 = new javax.swing.JScrollPane();
        tbl_expenses_report = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane2.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        pnl_employee.setBackground(new java.awt.Color(255, 255, 255));
        pnl_employee.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pnl_employeeKeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Search By :");

        txt_expenses_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_expenses_searchCaretUpdate(evt);
            }
        });
        txt_expenses_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_expenses_searchKeyPressed(evt);
            }
        });

        btn_expenses_search.setText("Search");
        btn_expenses_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_expenses_searchActionPerformed(evt);
            }
        });
        btn_expenses_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_expenses_searchKeyPressed(evt);
            }
        });

        btn_expenses_add.setText("Add");
        btn_expenses_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_expenses_addActionPerformed(evt);
            }
        });
        btn_expenses_add.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_expenses_addKeyPressed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        expenses_check_to.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        expenses_check_to.setText("To");
        expenses_check_to.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                expenses_check_toActionPerformed(evt);
            }
        });

        expenses_check_from.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        expenses_check_from.setText("From");
        expenses_check_from.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                expenses_check_fromActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel15.setText("Date :");

        jdc_date_from.setDateFormatString("dd/MM/yyyy");

        jdc_date_to.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_expenses_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(expenses_check_from)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_date_from, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(expenses_check_to)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_date_to, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_expenses_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_expenses_add)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator3)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jdc_date_from, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(expenses_check_to)
                            .addComponent(jdc_date_to, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txt_expenses_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_expenses_search)
                                .addComponent(btn_expenses_add)
                                .addComponent(expenses_check_from)
                                .addComponent(jLabel15)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_expenses_add, btn_expenses_search, expenses_check_from, expenses_check_to, jLabel15, jLabel2, jdc_date_from, jdc_date_to, txt_expenses_search});

        tbl_expenses.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "TYPE", "DATE", "AMOUNT", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_expenses.getTableHeader().setReorderingAllowed(false);
        tbl_expenses.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_expensesMouseClicked(evt);
            }
        });
        tbl_expenses.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_expensesKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbl_expensesKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_expenses);
        if (tbl_expenses.getColumnModel().getColumnCount() > 0) {
            tbl_expenses.getColumnModel().getColumn(0).setResizable(false);
            tbl_expenses.getColumnModel().getColumn(1).setResizable(false);
            tbl_expenses.getColumnModel().getColumn(2).setResizable(false);
            tbl_expenses.getColumnModel().getColumn(3).setResizable(false);
            tbl_expenses.getColumnModel().getColumn(4).setMinWidth(0);
            tbl_expenses.getColumnModel().getColumn(4).setPreferredWidth(0);
            tbl_expenses.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        btn_expenses_cancel.setText("Cancel");
        btn_expenses_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_expenses_cancelKeyPressed(evt);
            }
        });

        btn_expenses_view.setText("View");
        btn_expenses_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_expenses_viewActionPerformed(evt);
            }
        });
        btn_expenses_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_expenses_viewKeyPressed(evt);
            }
        });

        btn_expenses_print.setText("Print");
        btn_expenses_print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_expenses_printKeyPressed(evt);
            }
        });

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_expenses_message.setEditable(false);
        jScrollPane7.setViewportView(txt_expenses_message);

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7)
                .addContainerGap())
        );

        javax.swing.GroupLayout pnl_employeeLayout = new javax.swing.GroupLayout(pnl_employee);
        pnl_employee.setLayout(pnl_employeeLayout);
        pnl_employeeLayout.setHorizontalGroup(
            pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnl_employeeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 752, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addComponent(btn_expenses_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_expenses_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_expenses_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnl_employeeLayout.setVerticalGroup(
            pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_employeeLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 493, Short.MAX_VALUE)
                    .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_expenses_cancel)
                    .addComponent(btn_expenses_view)
                    .addComponent(btn_expenses_print))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Expenses", pnl_employee);

        jPanel22.setBackground(new java.awt.Color(255, 255, 255));

        pnl_adv_report1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel23.setText("Search By :");

        btn_expenses_report_search.setText("Search");
        btn_expenses_report_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_expenses_report_searchActionPerformed(evt);
            }
        });
        btn_expenses_report_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_expenses_report_searchKeyPressed(evt);
            }
        });

        jmc_month_report.setVerifyInputWhenFocusTarget(false);
        jmc_month_report.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_month_reportPropertyChange(evt);
            }
        });
        jmc_month_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_month_reportKeyPressed(evt);
            }
        });

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel24.setText("Month :");

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Date :");

        jdc_date_report.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdc_date_reportPropertyChange(evt);
            }
        });

        jyc_year_report.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_year_reportPropertyChange(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Year :");

        check_date_report.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                check_date_reportActionPerformed(evt);
            }
        });

        jButton1.setText("Print");

        jButton2.setText("Cancel");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(check_date_report)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_date_report, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jmc_month_report, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(jyc_year_report, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_expenses_report_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 301, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addContainerGap())
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel24Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator5)
                    .addComponent(btn_expenses_report_search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(check_date_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jdc_date_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1)
                        .addComponent(jButton2))
                    .addComponent(jyc_year_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jmc_month_report, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jScrollPane13.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tbl_expenses_report.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbl_expenses_report.getTableHeader().setReorderingAllowed(false);
        tbl_expenses_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_expenses_reportKeyPressed(evt);
            }
        });
        jScrollPane13.setViewportView(tbl_expenses_report);

        javax.swing.GroupLayout pnl_adv_report1Layout = new javax.swing.GroupLayout(pnl_adv_report1);
        pnl_adv_report1.setLayout(pnl_adv_report1Layout);
        pnl_adv_report1Layout.setHorizontalGroup(
            pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnl_adv_report1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane13, javax.swing.GroupLayout.DEFAULT_SIZE, 1057, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        pnl_adv_report1Layout.setVerticalGroup(
            pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report1Layout.createSequentialGroup()
                .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(550, Short.MAX_VALUE))
            .addGroup(pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_adv_report1Layout.createSequentialGroup()
                    .addContainerGap(55, Short.MAX_VALUE)
                    .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 529, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1077, Short.MAX_VALUE)
            .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnl_adv_report1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 595, Short.MAX_VALUE)
            .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnl_adv_report1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Reports", jPanel22);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );

        jPanel2.setBackground(new java.awt.Color(230, 79, 6));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Expenses Management");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void txt_expenses_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_expenses_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jdc_date_from.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            //            pnl_adv_report.requestFocus();
            //        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_expenses.requestFocus();
        }
    }//GEN-LAST:event_txt_expenses_searchKeyPressed

    private void btn_expenses_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_expenses_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_expenses_add.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jdc_date_to.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_expenses.requestFocus();
        }
    }//GEN-LAST:event_btn_expenses_searchKeyPressed

    private void btn_expenses_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_expenses_addActionPerformed
        Config.add_expenses.onloadReset();
        Config.add_expenses.setVisible(true);
    }//GEN-LAST:event_btn_expenses_addActionPerformed

    private void btn_expenses_addKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_expenses_addKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            tbl_expenses.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_expenses_search.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_expenses.requestFocus();
        }
    }//GEN-LAST:event_btn_expenses_addKeyPressed

    private void tbl_expensesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_expensesKeyPressed
//        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
//            btn_expenses_print.requestFocus();
//        }
//        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
//            btn_expenses_add.requestFocus();
//        }
        onloadExpensesMessage(tbl_expenses_model.getValueAt(tbl_expenses.getSelectedRow(), 4).toString());
    }//GEN-LAST:event_tbl_expensesKeyPressed

    private void btn_expenses_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_expenses_cancelKeyPressed
        //        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_expenses_view.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_expenses.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            tbl_employee.requestFocus();
            //           }
    }//GEN-LAST:event_btn_expenses_cancelKeyPressed

    private void btn_expenses_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_expenses_viewActionPerformed
               try{
                   Config.view_add_expenses.onloadReset(tbl_expenses_model.getValueAt(tbl_expenses.getSelectedRow(), 0).toString());
                   Config.view_add_expenses.setVisible(true);
               } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_expenses_viewActionPerformed

    private void btn_expenses_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_expenses_viewKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_expenses_cancel.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_expenses_print.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_expenses.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_expenses_cancel.requestFocus();
        }
    }//GEN-LAST:event_btn_expenses_viewKeyPressed

    private void btn_expenses_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_expenses_printKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_expenses_view.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            tbl_expenses.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_expenses.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_expenses_view.requestFocus();
        }
    }//GEN-LAST:event_btn_expenses_printKeyPressed

    private void pnl_employeeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pnl_employeeKeyPressed
//        System.out.println("sajjjj");
    }//GEN-LAST:event_pnl_employeeKeyPressed

    private void expenses_check_fromActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_expenses_check_fromActionPerformed
        if(expenses_check_from.isSelected()){
           expenses_from =true;
           expenses_to=false;
           jdc_date_from.setEnabled(true);
           jdc_date_to.setEnabled(false);
           expenses_check_to.setSelected(false);
           expenses_check_to.setEnabled(true);
        }else{
           expenses_from =false;
           expenses_to=false;
           jdc_date_from.setEnabled(false);
           jdc_date_to.setEnabled(false);
           expenses_check_to.setSelected(false);
           expenses_check_to.setEnabled(false);
        }
    }//GEN-LAST:event_expenses_check_fromActionPerformed

    private void expenses_check_toActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_expenses_check_toActionPerformed
        if(expenses_check_to.isSelected()){
           expenses_to=true;
           jdc_date_from.setEnabled(true);
        }else{
           expenses_to=false;
           jdc_date_to.setEnabled(false);
        }
    }//GEN-LAST:event_expenses_check_toActionPerformed

    private void btn_expenses_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_expenses_searchActionPerformed
        String sql = "";
            String text=txt_expenses_search.getText().trim().toUpperCase()+"%";
            if(expenses_from == true && expenses_to==true ){
                if (!txt_expenses_search.getText().equals("")) {
                    sql = "select e.* from expenses e,expenses_details ed where e.expenses_details_id=ed.expenses_details_id and STR_TO_DATE(e._date,'%d/%m/%Y') >STR_TO_DATE('"+simple_date_formate.format(jdc_date_from.getDate())+"','%d/%m/%Y') and STR_TO_DATE(e._date,'%d/%m/%Y')<=STR_TO_DATE('"+simple_date_formate.format(jdc_date_to.getDate())+"','%d/%m/%Y') and ed.type like '"+text+"' " ;
                } else {
                    sql = "select e.* from expenses e,expenses_details ed where e.expenses_details_id=ed.expenses_details_id and STR_TO_DATE(e._date,'%d/%m/%Y') >STR_TO_DATE('"+simple_date_formate.format(jdc_date_from.getDate())+"','%d/%m/%Y') and STR_TO_DATE(e._date,'%d/%m/%Y') <=STR_TO_DATE('"+simple_date_formate.format(jdc_date_to.getDate())+"','%d/%m/%Y') " ;
                }
            }else if(expenses_from == true && expenses_to==false ){
                if (!txt_expenses_search.getText().equals("")) {
                    sql = "select e.* from expenses e,expenses_details ed where e.expenses_details_id=ed.expenses_details_id and STR_TO_DATE(e._date,'%d/%m/%Y') =STR_TO_DATE('"+simple_date_formate.format(jdc_date_from.getDate())+"','%d/%m/%Y') and ed.type like '"+text+"'" ;
                } else {
                    sql = "select e.* from expenses e,expenses_details ed where e.expenses_details_id=ed.expenses_details_id and STR_TO_DATE(e._date,'%d/%m/%Y') =STR_TO_DATE('"+simple_date_formate.format(jdc_date_from.getDate())+"','%d/%m/%Y')" ;
                }
            }else{
                if (!txt_expenses_search.getText().equals("")) {
                    sql = "select e.* from expenses e,expenses_details ed where e.expenses_details_id=ed.expenses_details_id and ed.type like '"+text+"'" ;
                } else {
                    sql = "select * from expenses" ;
                }
            }
            view(sql);
    }//GEN-LAST:event_btn_expenses_searchActionPerformed

    private void txt_expenses_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_expenses_searchCaretUpdate
        btn_expenses_searchActionPerformed(null);
    }//GEN-LAST:event_txt_expenses_searchCaretUpdate

    private void tbl_expensesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_expensesMouseClicked
       try {
            if(evt.getClickCount()==1){
                onloadExpensesMessage(tbl_expenses_model.getValueAt(tbl_expenses.getSelectedRow(), 4).toString());
            }
            if(evt.getClickCount()==2){
                btn_expenses_viewActionPerformed(null);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_tbl_expensesMouseClicked

    private void btn_expenses_report_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_expenses_report_searchActionPerformed
        String sql="";
        if(!check_date_report.isSelected()){
            sql=" SELECT a._date,a.expenses_details_id,a.amount FROM expenses a where a._month='"+(jmc_month_report.getMonth()+1)+"' and a._year='"+jyc_year_report.getYear()+"' order by STR_TO_DATE(a._date,'%d/%m/%Y'),a.expenses_details_id;";
        }else{
            if(jdc_date_report.getDate()!=null){
            sql=" SELECT a._date,a.expenses_details_id,a.amount FROM expenses a where a._month='"+(jmc_month_report.getMonth()+1)+"' and a._year='"+jyc_year_report.getYear()+"' and STR_TO_DATE(a._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_date_report.getDate())+"','%d/%m/%Y') order by STR_TO_DATE(a._date,'%d/%m/%Y'),a.expenses_details_id;";
        }
        }
//        System.out.println(sql);
        loadReport(sql);
    }//GEN-LAST:event_btn_expenses_report_searchActionPerformed

    private void btn_expenses_report_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_expenses_report_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_expenses_report_searchKeyPressed

    private void jmc_month_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_month_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmc_month_reportKeyPressed

    private void tbl_expenses_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_expenses_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_expenses_reportKeyPressed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jmc_month_reportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_month_reportPropertyChange
        btn_expenses_report_searchActionPerformed(null);
    }//GEN-LAST:event_jmc_month_reportPropertyChange

    private void jyc_year_reportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_year_reportPropertyChange
        btn_expenses_report_searchActionPerformed(null);
    }//GEN-LAST:event_jyc_year_reportPropertyChange

    private void jdc_date_reportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_date_reportPropertyChange
        if(check_date_report.isSelected()){
            btn_expenses_report_searchActionPerformed(null);
        }
    }//GEN-LAST:event_jdc_date_reportPropertyChange

    private void check_date_reportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_check_date_reportActionPerformed
        btn_expenses_report_searchActionPerformed(null);
    }//GEN-LAST:event_check_date_reportActionPerformed

    private void tbl_expensesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_expensesKeyReleased
         onloadExpensesMessage(tbl_expenses_model.getValueAt(tbl_expenses.getSelectedRow(), 4).toString());
    }//GEN-LAST:event_tbl_expensesKeyReleased
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_expenses_add;
    private javax.swing.JButton btn_expenses_cancel;
    private javax.swing.JButton btn_expenses_print;
    private javax.swing.JButton btn_expenses_report_search;
    private javax.swing.JButton btn_expenses_search;
    private javax.swing.JButton btn_expenses_view;
    private javax.swing.JCheckBox check_date_report;
    private javax.swing.JCheckBox expenses_check_from;
    private javax.swing.JCheckBox expenses_check_to;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JTabbedPane jTabbedPane2;
    private com.toedter.calendar.JDateChooser jdc_date_from;
    private com.toedter.calendar.JDateChooser jdc_date_report;
    private com.toedter.calendar.JDateChooser jdc_date_to;
    private com.toedter.calendar.JMonthChooser jmc_month_report;
    private com.toedter.calendar.JYearChooser jyc_year_report;
    private javax.swing.JPanel pnl_adv_report1;
    private javax.swing.JPanel pnl_employee;
    private javax.swing.JTable tbl_expenses;
    private javax.swing.JTable tbl_expenses_report;
    private javax.swing.JTextPane txt_expenses_message;
    private javax.swing.JTextField txt_expenses_search;
    // End of variables declaration//GEN-END:variables

    public void onloadReset(){
        expenses_check_from.setSelected(true);
        expenses_check_to.setSelected(true);
        jdc_date_to.setDate(Calendar.getInstance().getTime());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -5);
        Date date=cal.getTime();
        jdc_date_from.setDate(date);
        btn_expenses_searchActionPerformed(null);
    }

    public void view(String sql) {
            try {
                tbl_expenses_model.setRowCount(0);
                Config.sql =sql;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                while (Config.rs.next()) {
                tbl_expenses_model.addRow(new Object[]{
                Config.rs.getString("expenses_id"),
                Config.config_expenses_details.get(Config.id_expenses_details.indexOf(Config.rs.getString("expenses_details_id"))).getType(),
                Config.rs.getString("_date"),
                Config.rs.getString("amount"),
                Config.rs.getString("message_id")
                });
                }
                
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void onloadExpensesMessage(String message_id) {
       try {
            if (!message_id.equals("")) {
                Config.sql ="select message from message_details where message_id="+message_id;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                if(Config.rs.next()) {
                    txt_expenses_message.setText(Config.rs.getString("message"));
                }
            } else {
                txt_expenses_message.setText("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

int col=0;

private void loadReport(String sql) {
    //.........................................................................................................................................
        int col_height=0;  
        int row=1;  
        int year = jyc_year_report.getYear();
        int month = jmc_month_report.getMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        int total = calendar.getActualMaximum(Calendar.DATE);
        int size= Config.config_expenses_details.size()+2;
        
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = new boolean [size];
        String []col = new String[size];
        Object [][]obj = new Object[total][size];
        
        
        //...................................................................SET FIRST COLUMN ON DATE.....................................................
        for (int i = 0; i < total; i++) {
         if(i<9){
             if(month<9){
                 obj[i][0]="0"+(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]="0"+(i+1)+"/"+(month+1)+"/"+year;
             }
         }else{
             if(month<9){
                 obj[i][0]=(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]=(i+1)+"/"+(month+1)+"/"+year;
             }
         }
        }
        //...................................................................SET COLUMN NAME.....................................................
        can[0]=false;
        col[0]="  DATE";
        
            for (int i = 0; i < Config.config_expenses_details.size(); i++) {
            can[row]=false;
            col[row]="  "+Config.config_expenses_details.get(i).getType();
            if(col_height<col[row].length()){
                col_height=col[row].length();
            }
            row++;
            }
        can[row]=false;
        col[row]="  TOTAL";
        
        //...................................................................SET DATE ON CELL.....................................................
        try {
            Config.sql =sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()) {
                int c=1;
                for (int i = 0; i < Config.config_expenses_details.size(); i++) {
                    if(Config.config_expenses_details.get(i).getExpenses_details_id().equals(Config.rs.getString("a.expenses_details_id"))){
                        Double advance=0.0;
                            try {
                            if(obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("a._date"))))-1][c]==null){
                                advance=Double.parseDouble(Config.rs.getString("a.amount").toString());
                                obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("a._date"))))-1][c]=advance;;
                            }else{
                                advance= Double.parseDouble(obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("a._date"))))-1][c].toString());
                                advance=advance+Double.parseDouble(Config.rs.getString("a.amount"));
                                obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("a._date").toString())))-1][c]=advance;;
                            }
                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                  }
                    c++;
             }
                
            }
        }catch (Exception e) {
        }
                
        //...................................................................SET TOTAL DATE WISE.....................................................
 for (int i = 0; i < total; i++) {
    Double amount=0.0;
    int j=0;
    for ( j = 1; j < size-1; j++) {
        try {
            amount=amount+Double.parseDouble(obj[i][j].toString());
        } catch (Exception e) {
        }
    }
    obj[i][j]=amount;
}
 
        //...................................................................SET TABLE..................................................... 
        tbl_expenses_report.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return can [columnIndex];
        }
    });
    
tbl_expenses_report.getTableHeader().getColumnModel().getColumn(0).setHeaderRenderer(new RotatedTableCellRenderer(270));
tbl_expenses_report.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
tbl_expenses_report.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(100);
tbl_expenses_report.getTableHeader().setPreferredSize(new Dimension(1000000,col_height*8));
for (int j = 1; j < size-1; j++) {
  tbl_expenses_report.getTableHeader().getColumnModel().getColumn(j).setHeaderRenderer(new RotatedTableCellRenderer(270));
  tbl_expenses_report.getTableHeader().getColumnModel().getColumn(j).setResizable(false);
  tbl_expenses_report.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(50);
}    
tbl_expenses_report.getTableHeader().getColumnModel().getColumn(row).setHeaderRenderer(new RotatedTableCellRenderer(270));
tbl_expenses_report.getTableHeader().getColumnModel().getColumn(row).setResizable(false);
tbl_expenses_report.getTableHeader().getColumnModel().getColumn(row).setPreferredWidth(100);
        //...................................................................SET DATE ON CELL.....................................................    

}
}
