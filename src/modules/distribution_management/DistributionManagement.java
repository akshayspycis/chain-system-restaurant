/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modules.distribution_management;

import data_manager.configuration.Config;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author akshay
 */
public class DistributionManagement extends javax.swing.JDialog {
    String item_id="";
    String designation_id="";
    SimpleDateFormat simple_date_formate = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_day = new SimpleDateFormat("dd");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");

    public DefaultTableModel tbl_dis_details_dis_mgmt_model=null;
    public DefaultTableModel tbl_dis_item_model=null;
    public DefaultTableModel tbl_ret_item_model=null;
    public DefaultTableModel tbl_dem_item_model=null;
    public DefaultTableModel tbl_report_model=null;
    
    public DistributionManagement(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        tbl_dis_details_dis_mgmt_model = (DefaultTableModel) tbl_dis_details_dis_mgmt.getModel();
        tbl_dis_item_model= (DefaultTableModel) tbl_dis_item.getModel();
        tbl_ret_item_model= (DefaultTableModel) tbl_ret_item.getModel();
        tbl_dem_item_model= (DefaultTableModel) tbl_dem_item.getModel();
        tbl_report_model= (DefaultTableModel) tbl_dem_item.getModel();
        tbl_report.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    @SuppressWarnings("unchecked")
 // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
 private void initComponents() {

  jPanel1 = new javax.swing.JPanel();
  jTabbedPane2 = new javax.swing.JTabbedPane();
  pnl_employee = new javax.swing.JPanel();
  btn_stock_mnt_cancel = new javax.swing.JButton();
  btn_stock_mnt_view = new javax.swing.JButton();
  btn_stock_mnt_print = new javax.swing.JButton();
  jPanel8 = new javax.swing.JPanel();
  jLabel2 = new javax.swing.JLabel();
  btn_search_dis_mgmt = new javax.swing.JButton();
  btn_new_dis_mgmt = new javax.swing.JButton();
  jSeparator3 = new javax.swing.JSeparator();
  cb_Item_type_dis_mgmt = new javax.swing.JComboBox();
  jLabel6 = new javax.swing.JLabel();
  check_date_dis_mgmt = new javax.swing.JCheckBox();
  jdc_date_dis_mgmt = new com.toedter.calendar.JDateChooser();
  jLabel3 = new javax.swing.JLabel();
  jmc_month_dis_mgmt = new com.toedter.calendar.JMonthChooser();
  jLabel4 = new javax.swing.JLabel();
  jyc_year_dis_mgmt = new com.toedter.calendar.JYearChooser();
  jLabel16 = new javax.swing.JLabel();
  txt_dd_dis_mgmt = new javax.swing.JTextField();
  jLabel17 = new javax.swing.JLabel();
  txt_designation = new javax.swing.JTextField();
  jPanel14 = new javax.swing.JPanel();
  jScrollPane1 = new javax.swing.JScrollPane();
  tbl_dis_item = new javax.swing.JTable();
  jPanel4 = new javax.swing.JPanel();
  jScrollPane11 = new javax.swing.JScrollPane();
  tbl_ret_item = new javax.swing.JTable();
  jPanel3 = new javax.swing.JPanel();
  jScrollPane2 = new javax.swing.JScrollPane();
  tbl_dis_details_dis_mgmt = new javax.swing.JTable();
  jPanel9 = new javax.swing.JPanel();
  jScrollPane17 = new javax.swing.JScrollPane();
  tbl_dem_item = new javax.swing.JTable();
  jButton10 = new javax.swing.JButton();
  jPanel15 = new javax.swing.JPanel();
  jScrollPane8 = new javax.swing.JScrollPane();
  txt_dis_details_message = new javax.swing.JTextPane();
  jPanel22 = new javax.swing.JPanel();
  jTabbedPane1 = new javax.swing.JTabbedPane();
  pnl_adv_report = new javax.swing.JPanel();
  pnl_adv_report1 = new javax.swing.JPanel();
  jPanel24 = new javax.swing.JPanel();
  jLabel23 = new javax.swing.JLabel();
  btn_search = new javax.swing.JButton();
  jmc_report = new com.toedter.calendar.JMonthChooser();
  jLabel26 = new javax.swing.JLabel();
  jSeparator5 = new javax.swing.JSeparator();
  jyc_report = new com.toedter.calendar.JYearChooser();
  jLabel7 = new javax.swing.JLabel();
  jButton4 = new javax.swing.JButton();
  jButton5 = new javax.swing.JButton();
  jLabel8 = new javax.swing.JLabel();
  cb_Item_type = new javax.swing.JComboBox();
  jLabel9 = new javax.swing.JLabel();
  txt_name = new javax.swing.JTextField();
  jScrollPane13 = new javax.swing.JScrollPane();
  tbl_report = new javax.swing.JTable();
  jPanel2 = new javax.swing.JPanel();
  jLabel1 = new javax.swing.JLabel();

  setUndecorated(true);
  addWindowListener(new java.awt.event.WindowAdapter() {
   public void windowClosing(java.awt.event.WindowEvent evt) {
    closeDialog(evt);
   }
  });

  jPanel1.setBackground(new java.awt.Color(255, 255, 255));

  jTabbedPane2.setBackground(new java.awt.Color(255, 255, 255));
  jTabbedPane2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

  pnl_employee.setBackground(new java.awt.Color(255, 255, 255));
  pnl_employee.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    pnl_employeeKeyPressed(evt);
   }
  });

  btn_stock_mnt_cancel.setText("Cancel");
  btn_stock_mnt_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    btn_stock_mnt_cancelKeyPressed(evt);
   }
  });

  btn_stock_mnt_view.setText("View");
  btn_stock_mnt_view.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    btn_stock_mnt_viewActionPerformed(evt);
   }
  });
  btn_stock_mnt_view.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    btn_stock_mnt_viewKeyPressed(evt);
   }
  });

  btn_stock_mnt_print.setText(" Demage");
  btn_stock_mnt_print.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    btn_stock_mnt_printKeyPressed(evt);
   }
  });

  jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
  jLabel2.setText("Date :");

  btn_search_dis_mgmt.setText("Search");
  btn_search_dis_mgmt.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    btn_search_dis_mgmtActionPerformed(evt);
   }
  });
  btn_search_dis_mgmt.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    btn_search_dis_mgmtKeyPressed(evt);
   }
  });

  btn_new_dis_mgmt.setText("New Item");
  btn_new_dis_mgmt.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    btn_new_dis_mgmtActionPerformed(evt);
   }
  });
  btn_new_dis_mgmt.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    btn_new_dis_mgmtKeyPressed(evt);
   }
  });

  jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

  cb_Item_type_dis_mgmt.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-Select-", "Stock Item", "Sada flavour", "Manufacture Item" }));
  cb_Item_type_dis_mgmt.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    cb_Item_type_dis_mgmtActionPerformed(evt);
   }
  });

  jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
  jLabel6.setText("Designation :");

  check_date_dis_mgmt.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    check_date_dis_mgmtActionPerformed(evt);
   }
  });

  jdc_date_dis_mgmt.setDateFormatString("dd/MM/yyyy");
  jdc_date_dis_mgmt.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
   public void propertyChange(java.beans.PropertyChangeEvent evt) {
    jdc_date_dis_mgmtPropertyChange(evt);
   }
  });

  jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
  jLabel3.setText("Month :");

  jmc_month_dis_mgmt.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
   public void propertyChange(java.beans.PropertyChangeEvent evt) {
    jmc_month_dis_mgmtPropertyChange(evt);
   }
  });

  jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
  jLabel4.setText("Year :");

  jyc_year_dis_mgmt.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
   public void propertyChange(java.beans.PropertyChangeEvent evt) {
    jyc_year_dis_mgmtPropertyChange(evt);
   }
  });

  jLabel16.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
  jLabel16.setText("DD NO :");

  txt_dd_dis_mgmt.addCaretListener(new javax.swing.event.CaretListener() {
   public void caretUpdate(javax.swing.event.CaretEvent evt) {
    txt_dd_dis_mgmtCaretUpdate(evt);
   }
  });

  jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
  jLabel17.setText("Item Type");

  txt_designation.addCaretListener(new javax.swing.event.CaretListener() {
   public void caretUpdate(javax.swing.event.CaretEvent evt) {
    txt_designationCaretUpdate(evt);
   }
  });
  txt_designation.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    txt_designationActionPerformed(evt);
   }
  });

  javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
  jPanel8.setLayout(jPanel8Layout);
  jPanel8Layout.setHorizontalGroup(
   jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel8Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jLabel6)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(txt_designation, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(jLabel2)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(check_date_dis_mgmt)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addComponent(jdc_date_dis_mgmt, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(jLabel3)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addComponent(jmc_month_dis_mgmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(jLabel4)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addComponent(jyc_year_dis_mgmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(jLabel16)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addComponent(txt_dd_dis_mgmt, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(jLabel17)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(cb_Item_type_dis_mgmt, 0, 1, Short.MAX_VALUE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(btn_search_dis_mgmt, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addComponent(btn_new_dis_mgmt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addContainerGap())
  );
  jPanel8Layout.setVerticalGroup(
   jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel8Layout.createSequentialGroup()
    .addContainerGap()
    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
     .addComponent(check_date_dis_mgmt)
     .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
      .addComponent(jLabel6)
      .addComponent(jLabel2)
      .addComponent(txt_designation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
     .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
      .addComponent(btn_search_dis_mgmt)
      .addComponent(btn_new_dis_mgmt)
      .addComponent(jLabel16)
      .addComponent(txt_dd_dis_mgmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
      .addComponent(jLabel17)
      .addComponent(cb_Item_type_dis_mgmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
     .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
     .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
      .addComponent(jLabel3)
      .addComponent(jdc_date_dis_mgmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
      .addComponent(jmc_month_dis_mgmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
      .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
      .addComponent(jyc_year_dis_mgmt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
    .addContainerGap())
  );

  jPanel8Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_new_dis_mgmt, btn_search_dis_mgmt, cb_Item_type_dis_mgmt, check_date_dis_mgmt, jLabel16, jLabel2, jLabel3, jLabel4, jLabel6, jSeparator3, jdc_date_dis_mgmt, jmc_month_dis_mgmt, jyc_year_dis_mgmt, txt_dd_dis_mgmt});

  jPanel14.setBackground(new java.awt.Color(255, 255, 255));
  jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Distribute Item", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

  tbl_dis_item.setModel(new javax.swing.table.DefaultTableModel(
   new Object [][] {

   },
   new String [] {
    "ID", "PRODUCT NAME", "QUANTITY", ""
   }
  ) {
   boolean[] canEdit = new boolean [] {
    false, false, false, false
   };

   public boolean isCellEditable(int rowIndex, int columnIndex) {
    return canEdit [columnIndex];
   }
  });
  tbl_dis_item.getTableHeader().setReorderingAllowed(false);
  tbl_dis_item.addMouseListener(new java.awt.event.MouseAdapter() {
   public void mouseClicked(java.awt.event.MouseEvent evt) {
    tbl_dis_itemMouseClicked(evt);
   }
  });
  tbl_dis_item.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    tbl_dis_itemKeyPressed(evt);
   }
  });
  jScrollPane1.setViewportView(tbl_dis_item);
  if (tbl_dis_item.getColumnModel().getColumnCount() > 0) {
   tbl_dis_item.getColumnModel().getColumn(0).setResizable(false);
   tbl_dis_item.getColumnModel().getColumn(1).setResizable(false);
   tbl_dis_item.getColumnModel().getColumn(2).setResizable(false);
   tbl_dis_item.getColumnModel().getColumn(3).setMinWidth(0);
   tbl_dis_item.getColumnModel().getColumn(3).setPreferredWidth(0);
   tbl_dis_item.getColumnModel().getColumn(3).setMaxWidth(0);
  }

  javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
  jPanel14.setLayout(jPanel14Layout);
  jPanel14Layout.setHorizontalGroup(
   jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel14Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 735, Short.MAX_VALUE)
    .addContainerGap())
  );
  jPanel14Layout.setVerticalGroup(
   jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
    .addContainerGap())
  );

  jPanel4.setBackground(new java.awt.Color(255, 255, 255));
  jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Return Item", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

  tbl_ret_item.setModel(new javax.swing.table.DefaultTableModel(
   new Object [][] {

   },
   new String [] {
    "ID", "PRODUCT NAME", "QUANTITY", ""
   }
  ) {
   boolean[] canEdit = new boolean [] {
    false, false, false, false
   };

   public boolean isCellEditable(int rowIndex, int columnIndex) {
    return canEdit [columnIndex];
   }
  });
  tbl_ret_item.getTableHeader().setReorderingAllowed(false);
  tbl_ret_item.addMouseListener(new java.awt.event.MouseAdapter() {
   public void mouseClicked(java.awt.event.MouseEvent evt) {
    tbl_ret_itemMouseClicked(evt);
   }
  });
  tbl_ret_item.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    tbl_ret_itemKeyPressed(evt);
   }
  });
  jScrollPane11.setViewportView(tbl_ret_item);
  if (tbl_ret_item.getColumnModel().getColumnCount() > 0) {
   tbl_ret_item.getColumnModel().getColumn(0).setResizable(false);
   tbl_ret_item.getColumnModel().getColumn(1).setResizable(false);
   tbl_ret_item.getColumnModel().getColumn(2).setResizable(false);
   tbl_ret_item.getColumnModel().getColumn(3).setMinWidth(0);
   tbl_ret_item.getColumnModel().getColumn(3).setPreferredWidth(0);
   tbl_ret_item.getColumnModel().getColumn(3).setMaxWidth(0);
  }

  javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
  jPanel4.setLayout(jPanel4Layout);
  jPanel4Layout.setHorizontalGroup(
   jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel4Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 735, Short.MAX_VALUE)
    .addContainerGap())
  );
  jPanel4Layout.setVerticalGroup(
   jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
    .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
    .addContainerGap())
  );

  jPanel3.setBackground(new java.awt.Color(255, 255, 255));
  jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Distibution Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

  tbl_dis_details_dis_mgmt.setModel(new javax.swing.table.DefaultTableModel(
   new Object [][] {

   },
   new String [] {
    "DD NO.", "DESIGNATION NAME", "DATE", ""
   }
  ) {
   boolean[] canEdit = new boolean [] {
    false, false, false, false
   };

   public boolean isCellEditable(int rowIndex, int columnIndex) {
    return canEdit [columnIndex];
   }
  });
  tbl_dis_details_dis_mgmt.getTableHeader().setReorderingAllowed(false);
  tbl_dis_details_dis_mgmt.addMouseListener(new java.awt.event.MouseAdapter() {
   public void mouseClicked(java.awt.event.MouseEvent evt) {
    tbl_dis_details_dis_mgmtMouseClicked(evt);
   }
  });
  tbl_dis_details_dis_mgmt.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    tbl_dis_details_dis_mgmtKeyPressed(evt);
   }
   public void keyReleased(java.awt.event.KeyEvent evt) {
    tbl_dis_details_dis_mgmtKeyReleased(evt);
   }
   public void keyTyped(java.awt.event.KeyEvent evt) {
    tbl_dis_details_dis_mgmtKeyTyped(evt);
   }
  });
  jScrollPane2.setViewportView(tbl_dis_details_dis_mgmt);
  if (tbl_dis_details_dis_mgmt.getColumnModel().getColumnCount() > 0) {
   tbl_dis_details_dis_mgmt.getColumnModel().getColumn(0).setResizable(false);
   tbl_dis_details_dis_mgmt.getColumnModel().getColumn(0).setPreferredWidth(200);
   tbl_dis_details_dis_mgmt.getColumnModel().getColumn(1).setResizable(false);
   tbl_dis_details_dis_mgmt.getColumnModel().getColumn(1).setPreferredWidth(500);
   tbl_dis_details_dis_mgmt.getColumnModel().getColumn(2).setResizable(false);
   tbl_dis_details_dis_mgmt.getColumnModel().getColumn(2).setPreferredWidth(300);
   tbl_dis_details_dis_mgmt.getColumnModel().getColumn(3).setMinWidth(0);
   tbl_dis_details_dis_mgmt.getColumnModel().getColumn(3).setPreferredWidth(0);
   tbl_dis_details_dis_mgmt.getColumnModel().getColumn(3).setMaxWidth(0);
  }

  javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
  jPanel3.setLayout(jPanel3Layout);
  jPanel3Layout.setHorizontalGroup(
   jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel3Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
    .addContainerGap())
  );
  jPanel3Layout.setVerticalGroup(
   jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
    .addContainerGap())
  );

  jPanel9.setBackground(new java.awt.Color(255, 255, 255));
  jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Demage Item", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

  tbl_dem_item.setModel(new javax.swing.table.DefaultTableModel(
   new Object [][] {

   },
   new String [] {
    "ID", "PRODUCT NAME", "QUANTITY", ""
   }
  ) {
   boolean[] canEdit = new boolean [] {
    false, false, false, false
   };

   public boolean isCellEditable(int rowIndex, int columnIndex) {
    return canEdit [columnIndex];
   }
  });
  tbl_dem_item.getTableHeader().setReorderingAllowed(false);
  tbl_dem_item.addMouseListener(new java.awt.event.MouseAdapter() {
   public void mouseClicked(java.awt.event.MouseEvent evt) {
    tbl_dem_itemMouseClicked(evt);
   }
  });
  tbl_dem_item.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    tbl_dem_itemKeyPressed(evt);
   }
  });
  jScrollPane17.setViewportView(tbl_dem_item);
  if (tbl_dem_item.getColumnModel().getColumnCount() > 0) {
   tbl_dem_item.getColumnModel().getColumn(0).setResizable(false);
   tbl_dem_item.getColumnModel().getColumn(1).setResizable(false);
   tbl_dem_item.getColumnModel().getColumn(2).setResizable(false);
   tbl_dem_item.getColumnModel().getColumn(3).setMinWidth(0);
   tbl_dem_item.getColumnModel().getColumn(3).setPreferredWidth(0);
   tbl_dem_item.getColumnModel().getColumn(3).setMaxWidth(0);
  }

  javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
  jPanel9.setLayout(jPanel9Layout);
  jPanel9Layout.setHorizontalGroup(
   jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel9Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jScrollPane17, javax.swing.GroupLayout.DEFAULT_SIZE, 738, Short.MAX_VALUE)
    .addContainerGap())
  );
  jPanel9Layout.setVerticalGroup(
   jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
    .addComponent(jScrollPane17, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
    .addContainerGap())
  );

  jButton10.setText("Return");

  jPanel15.setBackground(new java.awt.Color(255, 255, 255));
  jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

  txt_dis_details_message.setEditable(false);
  jScrollPane8.setViewportView(txt_dis_details_message);

  javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
  jPanel15.setLayout(jPanel15Layout);
  jPanel15Layout.setHorizontalGroup(
   jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel15Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jScrollPane8)
    .addContainerGap())
  );
  jPanel15Layout.setVerticalGroup(
   jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel15Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jScrollPane8)
    .addContainerGap())
  );

  javax.swing.GroupLayout pnl_employeeLayout = new javax.swing.GroupLayout(pnl_employee);
  pnl_employee.setLayout(pnl_employeeLayout);
  pnl_employeeLayout.setHorizontalGroup(
   pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
   .addGroup(pnl_employeeLayout.createSequentialGroup()
    .addContainerGap()
    .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
     .addGroup(pnl_employeeLayout.createSequentialGroup()
      .addComponent(btn_stock_mnt_print)
      .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
      .addComponent(jButton10)
      .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
      .addComponent(btn_stock_mnt_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
      .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
      .addComponent(btn_stock_mnt_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
     .addGroup(pnl_employeeLayout.createSequentialGroup()
      .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
       .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
       .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
      .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
      .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
       .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
       .addGroup(pnl_employeeLayout.createSequentialGroup()
        .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
         .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addGap(0, 0, Short.MAX_VALUE)))))
    .addContainerGap())
  );

  pnl_employeeLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jPanel14, jPanel4});

  pnl_employeeLayout.setVerticalGroup(
   pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(pnl_employeeLayout.createSequentialGroup()
    .addGap(2, 2, 2)
    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
     .addGroup(pnl_employeeLayout.createSequentialGroup()
      .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
      .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
      .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
     .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
     .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
     .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    .addGap(11, 11, 11)
    .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
     .addComponent(btn_stock_mnt_cancel)
     .addComponent(btn_stock_mnt_view)
     .addComponent(btn_stock_mnt_print)
     .addComponent(jButton10))
    .addContainerGap())
  );

  jTabbedPane2.addTab("Distribution management", pnl_employee);

  jPanel22.setBackground(new java.awt.Color(255, 255, 255));

  jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
  jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

  pnl_adv_report.setBackground(new java.awt.Color(255, 255, 255));

  pnl_adv_report1.setBackground(new java.awt.Color(255, 255, 255));

  jLabel23.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
  jLabel23.setText("Search By :");

  btn_search.setText("Search");
  btn_search.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    btn_searchActionPerformed(evt);
   }
  });
  btn_search.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    btn_searchKeyPressed(evt);
   }
  });

  jmc_report.setVerifyInputWhenFocusTarget(false);
  jmc_report.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
   public void propertyChange(java.beans.PropertyChangeEvent evt) {
    jmc_reportPropertyChange(evt);
   }
  });
  jmc_report.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    jmc_reportKeyPressed(evt);
   }
  });

  jLabel26.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
  jLabel26.setText("Month :");

  jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);

  jyc_report.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
   public void propertyChange(java.beans.PropertyChangeEvent evt) {
    jyc_reportPropertyChange(evt);
   }
  });

  jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
  jLabel7.setText("Year :");

  jButton4.setText("Print");

  jButton5.setText("Cancel");
  jButton5.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    jButton5ActionPerformed(evt);
   }
  });

  jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
  jLabel8.setText("Item Type :");

  cb_Item_type.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-Select-", "Stock Item", "Sada flavour", "Manufacture Item" }));
  cb_Item_type.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    cb_Item_typeActionPerformed(evt);
   }
  });

  jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
  jLabel9.setForeground(new java.awt.Color(51, 51, 51));
  jLabel9.setText("Name :");

  txt_name.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    txt_nameActionPerformed(evt);
   }
  });
  txt_name.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    txt_nameKeyPressed(evt);
   }
  });

  javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
  jPanel24.setLayout(jPanel24Layout);
  jPanel24Layout.setHorizontalGroup(
   jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel24Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addComponent(cb_Item_type, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addGap(18, 18, 18)
    .addComponent(jLabel9)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(txt_name, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(jmc_report, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addGap(2, 2, 2)
    .addComponent(jyc_report, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(btn_search)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addComponent(jButton5)
    .addGap(21, 21, 21))
  );
  jPanel24Layout.setVerticalGroup(
   jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel24Layout.createSequentialGroup()
    .addContainerGap()
    .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
     .addGroup(jPanel24Layout.createSequentialGroup()
      .addGap(0, 0, Short.MAX_VALUE)
      .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
       .addComponent(jSeparator5)
       .addComponent(btn_search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
       .addComponent(jLabel26)
       .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
        .addComponent(jButton4)
        .addComponent(jButton5))
       .addComponent(jyc_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
       .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
       .addComponent(jmc_report, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
     .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
      .addComponent(jLabel23)
      .addComponent(jLabel8)
      .addComponent(cb_Item_type, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
      .addComponent(jLabel9)
      .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
    .addContainerGap())
  );

  jPanel24Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_search, jLabel23, jLabel26, jLabel7, jLabel8, jmc_report, jyc_report});

  jScrollPane13.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

  tbl_report.setModel(new javax.swing.table.DefaultTableModel(
   new Object [][] {

   },
   new String [] {

   }
  ));
  tbl_report.setSelectionBackground(new java.awt.Color(230, 79, 6));
  tbl_report.setSelectionForeground(new java.awt.Color(0, 0, 0));
  tbl_report.getTableHeader().setReorderingAllowed(false);
  tbl_report.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    tbl_reportKeyPressed(evt);
   }
  });
  jScrollPane13.setViewportView(tbl_report);

  javax.swing.GroupLayout pnl_adv_report1Layout = new javax.swing.GroupLayout(pnl_adv_report1);
  pnl_adv_report1.setLayout(pnl_adv_report1Layout);
  pnl_adv_report1Layout.setHorizontalGroup(
   pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addComponent(jPanel24, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
   .addGroup(pnl_adv_report1Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jScrollPane13)
    .addContainerGap())
  );
  pnl_adv_report1Layout.setVerticalGroup(
   pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(pnl_adv_report1Layout.createSequentialGroup()
    .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addComponent(jScrollPane13, javax.swing.GroupLayout.DEFAULT_SIZE, 540, Short.MAX_VALUE))
  );

  javax.swing.GroupLayout pnl_adv_reportLayout = new javax.swing.GroupLayout(pnl_adv_report);
  pnl_adv_report.setLayout(pnl_adv_reportLayout);
  pnl_adv_reportLayout.setHorizontalGroup(
   pnl_adv_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(pnl_adv_reportLayout.createSequentialGroup()
    .addGap(0, 0, 0)
    .addComponent(pnl_adv_report1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    .addGap(0, 0, 0))
  );
  pnl_adv_reportLayout.setVerticalGroup(
   pnl_adv_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(pnl_adv_reportLayout.createSequentialGroup()
    .addGap(2, 2, 2)
    .addComponent(pnl_adv_report1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    .addContainerGap())
  );

  jTabbedPane1.addTab("Receive Report Day wise", pnl_adv_report);

  javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
  jPanel22.setLayout(jPanel22Layout);
  jPanel22Layout.setHorizontalGroup(
   jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addComponent(jTabbedPane1)
  );
  jPanel22Layout.setVerticalGroup(
   jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
  );

  jTabbedPane2.addTab("Reports", jPanel22);

  javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
  jPanel1.setLayout(jPanel1Layout);
  jPanel1Layout.setHorizontalGroup(
   jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addComponent(jTabbedPane2)
  );
  jPanel1Layout.setVerticalGroup(
   jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addComponent(jTabbedPane2)
  );

  jPanel2.setBackground(new java.awt.Color(230, 79, 6));

  jLabel1.setBackground(new java.awt.Color(255, 255, 255));
  jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
  jLabel1.setForeground(new java.awt.Color(255, 255, 255));
  jLabel1.setText("Distribution Management");

  javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
  jPanel2.setLayout(jPanel2Layout);
  jPanel2Layout.setHorizontalGroup(
   jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel2Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jLabel1)
    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
  );
  jPanel2Layout.setVerticalGroup(
   jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
    .addGap(6, 6, 6)
    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    .addContainerGap())
  );

  javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
  getContentPane().setLayout(layout);
  layout.setHorizontalGroup(
   layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
   .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
  );
  layout.setVerticalGroup(
   layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(layout.createSequentialGroup()
    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addGap(0, 0, 0)
    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
  );

  pack();
 }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void btn_stock_mnt_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_stock_mnt_cancelKeyPressed
        //        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_stock_mnt_view.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_dis_item.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            tbl_employee.requestFocus();
            //           }
    }//GEN-LAST:event_btn_stock_mnt_cancelKeyPressed

    private void btn_stock_mnt_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stock_mnt_viewActionPerformed
        try{
        Config.view_distribution_item.onloadReset(tbl_dis_details_dis_mgmt_model.getValueAt(tbl_dis_details_dis_mgmt.getSelectedRow(), 0).toString());    
        Config.view_distribution_item.setVisible(true);
        }catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_stock_mnt_viewActionPerformed

    private void btn_stock_mnt_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_stock_mnt_viewKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_stock_mnt_cancel.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_stock_mnt_print.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_dis_item.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_stock_mnt_cancel.requestFocus();
        }
    }//GEN-LAST:event_btn_stock_mnt_viewKeyPressed

    private void btn_stock_mnt_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_stock_mnt_printKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_stock_mnt_view.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            tbl_dis_item.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_dis_item.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_stock_mnt_view.requestFocus();
        }
    }//GEN-LAST:event_btn_stock_mnt_printKeyPressed

    private void btn_search_dis_mgmtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_search_dis_mgmtActionPerformed
            String sql = "";
            String text=txt_designation.getText().trim().toUpperCase()+"%";
            if(check_date_dis_mgmt.isSelected()){
                if(!txt_dd_dis_mgmt.getText().equals("")) {
                    sql = "select a.* from distribution_details a inner join designation_details b on b.designation_id=a.designation_id where _year='"+jyc_year_dis_mgmt.getYear()+"' and  _month='"+(jmc_month_dis_mgmt.getMonth()+1)+"' and STR_TO_DATE(_date,'%d/%m/%Y')= STR_TO_DATE('"+simple_date_formate.format(jdc_date_dis_mgmt.getDate())+"','%d/%m/%Y') and a.distribution_details_id like '"+txt_dd_dis_mgmt.getText()+"%' and b.designation like '"+text+"' order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                }else{
                    sql = "select a.* from distribution_details a inner join designation_details b on b.designation_id=a.designation_id where a._year='"+jyc_year_dis_mgmt.getYear()+"' and  a._month='"+(jmc_month_dis_mgmt.getMonth()+1)+"' and STR_TO_DATE(a._date,'%d/%m/%Y')= STR_TO_DATE('"+simple_date_formate.format(jdc_date_dis_mgmt.getDate())+"','%d/%m/%Y') and b.designation like '"+text+"' order by  STR_TO_DATE(a._date,'%d/%m/%Y')";
                }
            }else{ 
                if(!txt_dd_dis_mgmt.getText().equals("")){
                    sql = "select a.* from distribution_details a inner join designation_details b on b.designation_id=a.designation_id where _year='"+jyc_year_dis_mgmt.getYear()+"' and  _month='"+(jmc_month_dis_mgmt.getMonth()+1)+"' and a.distribution_details_id like '"+txt_dd_dis_mgmt.getText()+"%' and b.designation like '"+text+"' order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                }else{
                    sql = "select a.* from distribution_details a inner join designation_details b on b.designation_id=a.designation_id where a._year='"+jyc_year_dis_mgmt.getYear()+"' and  a._month='"+(jmc_month_dis_mgmt.getMonth()+1)+"' and b.designation like '"+text+"' order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                }
            }
            setDisDetails(sql);
    }//GEN-LAST:event_btn_search_dis_mgmtActionPerformed

    private void btn_search_dis_mgmtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_search_dis_mgmtKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_new_dis_mgmt.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            //jfmt_stock_mnt_to.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_dis_item.requestFocus();
        }
    }//GEN-LAST:event_btn_search_dis_mgmtKeyPressed

    private void btn_new_dis_mgmtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_new_dis_mgmtActionPerformed
        Config.new_distribution.onloadReset();
        Config.new_distribution.setVisible(true);
    }//GEN-LAST:event_btn_new_dis_mgmtActionPerformed

    private void btn_new_dis_mgmtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_new_dis_mgmtKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            tbl_dis_item.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_search_dis_mgmt.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_dis_item.requestFocus();
        }
    }//GEN-LAST:event_btn_new_dis_mgmtKeyPressed

    private void cb_Item_type_dis_mgmtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_Item_type_dis_mgmtActionPerformed
        //    btn_report_month_searchActionPerformed(null);
    }//GEN-LAST:event_cb_Item_type_dis_mgmtActionPerformed

    private void tbl_dis_itemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_dis_itemKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_stock_mnt_print.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_new_dis_mgmt.requestFocus();
        }
    }//GEN-LAST:event_tbl_dis_itemKeyPressed

    private void tbl_dis_details_dis_mgmtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_dis_details_dis_mgmtKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_dis_details_dis_mgmtKeyPressed

    private void pnl_employeeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pnl_employeeKeyPressed
        System.out.println("sajjjj");
    }//GEN-LAST:event_pnl_employeeKeyPressed

    private void btn_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_searchActionPerformed
            loadReport(null);
    }//GEN-LAST:event_btn_searchActionPerformed

    private void btn_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_searchKeyPressed

    private void jmc_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmc_reportKeyPressed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void tbl_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_reportKeyPressed

    private void check_date_dis_mgmtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_check_date_dis_mgmtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_check_date_dis_mgmtActionPerformed

    private void tbl_ret_itemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_ret_itemKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_ret_itemKeyPressed

    private void tbl_dem_itemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_dem_itemKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_dem_itemKeyPressed

    private void tbl_dis_details_dis_mgmtMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_dis_details_dis_mgmtMouseClicked
        try {
            if(evt.getClickCount()==1){
            setData_Dis_Ret_Dem(tbl_dis_details_dis_mgmt_model.getValueAt(tbl_dis_details_dis_mgmt.getSelectedRow(), 0).toString());    
            setDisDetailsMessage(tbl_dis_details_dis_mgmt_model.getValueAt(tbl_dis_details_dis_mgmt.getSelectedRow(), 3).toString());
            }
            if(evt.getClickCount()==2){
                btn_stock_mnt_viewActionPerformed(null);
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_tbl_dis_details_dis_mgmtMouseClicked

    private void tbl_dis_details_dis_mgmtKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_dis_details_dis_mgmtKeyReleased
        try {
            setData_Dis_Ret_Dem(tbl_dis_details_dis_mgmt_model.getValueAt(tbl_dis_details_dis_mgmt.getSelectedRow(), 0).toString());    
            setDisDetailsMessage(tbl_dis_details_dis_mgmt_model.getValueAt(tbl_dis_details_dis_mgmt.getSelectedRow(), 3).toString());
        } catch (Exception e) {
        }
    }//GEN-LAST:event_tbl_dis_details_dis_mgmtKeyReleased

    private void tbl_dis_itemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_dis_itemMouseClicked
        try {
            if(evt.getClickCount()==2){
            Config.view_distribution.onloadReset(tbl_dis_item_model.getValueAt(tbl_dis_item.getSelectedRow(), 0).toString());
            Config.view_distribution.setVisible(true);
        }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_tbl_dis_itemMouseClicked

    private void tbl_ret_itemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_ret_itemMouseClicked
        try {
            if(evt.getClickCount()==2){
            Config.view_return.onloadReset(tbl_ret_item_model.getValueAt(tbl_ret_item.getSelectedRow(), 0).toString());
            Config.view_return.setVisible(true);
        }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_tbl_ret_itemMouseClicked

    private void tbl_dem_itemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_dem_itemMouseClicked
        try {
            if(evt.getClickCount()==2){
            Config.view_demage.onloadReset(tbl_dem_item_model.getValueAt(tbl_dem_item.getSelectedRow(), 0).toString());
            Config.view_demage.setVisible(true);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_tbl_dem_itemMouseClicked

    private void tbl_dis_details_dis_mgmtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_dis_details_dis_mgmtKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_dis_details_dis_mgmtKeyTyped

    private void txt_designationCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_designationCaretUpdate
        btn_search_dis_mgmtActionPerformed(null);
    }//GEN-LAST:event_txt_designationCaretUpdate

    private void jdc_date_dis_mgmtPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_date_dis_mgmtPropertyChange
        btn_search_dis_mgmtActionPerformed(null);
    }//GEN-LAST:event_jdc_date_dis_mgmtPropertyChange

    private void jmc_month_dis_mgmtPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_month_dis_mgmtPropertyChange
        btn_search_dis_mgmtActionPerformed(null);
    }//GEN-LAST:event_jmc_month_dis_mgmtPropertyChange

    private void jyc_year_dis_mgmtPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_year_dis_mgmtPropertyChange
        btn_search_dis_mgmtActionPerformed(null);
    }//GEN-LAST:event_jyc_year_dis_mgmtPropertyChange

    private void txt_dd_dis_mgmtCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_dd_dis_mgmtCaretUpdate
        btn_search_dis_mgmtActionPerformed(null);
    }//GEN-LAST:event_txt_dd_dis_mgmtCaretUpdate

    private void cb_Item_typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_Item_typeActionPerformed
       
    }//GEN-LAST:event_cb_Item_typeActionPerformed

    private void txt_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nameActionPerformed
        if(cb_Item_type.getSelectedIndex()!=0){
            Config.item_search.onloadReset(6, cb_Item_type.getSelectedIndex());
            Config.item_search.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(this, "Select Item Type ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_nameActionPerformed

    private void txt_nameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nameKeyPressed

    }//GEN-LAST:event_txt_nameKeyPressed

    private void jmc_reportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_reportPropertyChange
        btn_searchActionPerformed(null);
    }//GEN-LAST:event_jmc_reportPropertyChange

    private void jyc_reportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_reportPropertyChange
        btn_searchActionPerformed(null);
    }//GEN-LAST:event_jyc_reportPropertyChange

    private void txt_designationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_designationActionPerformed
      Config.designation_search.onloadReset(6);
      Config.designation_search.setVisible(true);
    }//GEN-LAST:event_txt_designationActionPerformed
    

 // Variables declaration - do not modify//GEN-BEGIN:variables
 private javax.swing.JButton btn_new_dis_mgmt;
 private javax.swing.JButton btn_search;
 private javax.swing.JButton btn_search_dis_mgmt;
 private javax.swing.JButton btn_stock_mnt_cancel;
 private javax.swing.JButton btn_stock_mnt_print;
 private javax.swing.JButton btn_stock_mnt_view;
 private javax.swing.JComboBox cb_Item_type;
 private javax.swing.JComboBox cb_Item_type_dis_mgmt;
 private javax.swing.JCheckBox check_date_dis_mgmt;
 private javax.swing.JButton jButton10;
 private javax.swing.JButton jButton4;
 private javax.swing.JButton jButton5;
 private javax.swing.JLabel jLabel1;
 private javax.swing.JLabel jLabel16;
 private javax.swing.JLabel jLabel17;
 private javax.swing.JLabel jLabel2;
 private javax.swing.JLabel jLabel23;
 private javax.swing.JLabel jLabel26;
 private javax.swing.JLabel jLabel3;
 private javax.swing.JLabel jLabel4;
 private javax.swing.JLabel jLabel6;
 private javax.swing.JLabel jLabel7;
 private javax.swing.JLabel jLabel8;
 private javax.swing.JLabel jLabel9;
 private javax.swing.JPanel jPanel1;
 private javax.swing.JPanel jPanel14;
 private javax.swing.JPanel jPanel15;
 private javax.swing.JPanel jPanel2;
 private javax.swing.JPanel jPanel22;
 private javax.swing.JPanel jPanel24;
 private javax.swing.JPanel jPanel3;
 private javax.swing.JPanel jPanel4;
 private javax.swing.JPanel jPanel8;
 private javax.swing.JPanel jPanel9;
 private javax.swing.JScrollPane jScrollPane1;
 private javax.swing.JScrollPane jScrollPane11;
 private javax.swing.JScrollPane jScrollPane13;
 private javax.swing.JScrollPane jScrollPane17;
 private javax.swing.JScrollPane jScrollPane2;
 private javax.swing.JScrollPane jScrollPane8;
 private javax.swing.JSeparator jSeparator3;
 private javax.swing.JSeparator jSeparator5;
 private javax.swing.JTabbedPane jTabbedPane1;
 private javax.swing.JTabbedPane jTabbedPane2;
 private com.toedter.calendar.JDateChooser jdc_date_dis_mgmt;
 private com.toedter.calendar.JMonthChooser jmc_month_dis_mgmt;
 private com.toedter.calendar.JMonthChooser jmc_report;
 private com.toedter.calendar.JYearChooser jyc_report;
 private com.toedter.calendar.JYearChooser jyc_year_dis_mgmt;
 private javax.swing.JPanel pnl_adv_report;
 private javax.swing.JPanel pnl_adv_report1;
 private javax.swing.JPanel pnl_employee;
 private javax.swing.JTable tbl_dem_item;
 private javax.swing.JTable tbl_dis_details_dis_mgmt;
 private javax.swing.JTable tbl_dis_item;
 private javax.swing.JTable tbl_report;
 private javax.swing.JTable tbl_ret_item;
 private javax.swing.JTextField txt_dd_dis_mgmt;
 private javax.swing.JTextField txt_designation;
 private javax.swing.JTextPane txt_dis_details_message;
 private javax.swing.JTextField txt_name;
 // End of variables declaration//GEN-END:variables

    public void onloadReset() {
    onloadDisDetails();
    }
    public void onloadDisDetails(){
        
        tbl_dis_item_model.setRowCount(0);
        tbl_ret_item_model.setRowCount(0);
        tbl_dem_item_model.setRowCount(0);
        btn_search_dis_mgmtActionPerformed(null);
    }
    public void setDisDetails(String sql) {
         try {
                tbl_dis_item_model.setRowCount(0);
                tbl_ret_item_model.setRowCount(0);
                tbl_dem_item_model.setRowCount(0);
                tbl_dis_details_dis_mgmt_model.setRowCount(0);
                Config.sql =sql;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                while (Config.rs.next()) {
                tbl_dis_details_dis_mgmt_model.addRow(new Object[]{
                Config.rs.getString("distribution_details_id"),
                Config.config_designation_details.get(Config.id_designation_details.indexOf(Config.rs.getString("designation_id"))).getDesignation(),
                Config.rs.getString("_date"),
                Config.rs.getString("message_id")
                });
                }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public void setData_Dis_Ret_Dem(String distribution_details_id) {
        try {
         try {
                tbl_dis_item_model.setRowCount(0);
                Config.sql ="select * from distribution_item where distribution_details_id='"+distribution_details_id+"'";
                Config.rs = Config.stmt.executeQuery(Config.sql);
                while (Config.rs.next()) {
                tbl_dis_item_model.addRow(new Object[]{
                Config.rs.getString("distribution_item_id"),
                Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.rs.getString("item_id"))).getName(),
                Config.rs.getString("quantity"),
                Config.rs.getString("item_id")
                });
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
         //................................................................................................................
         
        try {
                tbl_ret_item_model.setRowCount(0);
                Config.sql ="select * from return_item where distribution_details_id='"+distribution_details_id+"'";
                Config.rs = Config.stmt.executeQuery(Config.sql);
                while (Config.rs.next()) {
                tbl_ret_item_model.addRow(new Object[]{
                Config.rs.getString("return_item_id"),
                Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.rs.getString("item_id"))).getName(),
                Config.rs.getString("quantity"),
                Config.rs.getString("item_id")
                });
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //.....................................................................................................................
        try {
                tbl_dem_item_model.setRowCount(0);
                Config.sql ="select * from demage_item where distribution_details_id='"+distribution_details_id+"'";
                Config.rs = Config.stmt.executeQuery(Config.sql);
                while (Config.rs.next()) {
                tbl_dem_item_model.addRow(new Object[]{
                Config.rs.getString("demage_item_id"),
                Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.rs.getString("item_id"))).getName(),
                Config.rs.getString("quantity"),
                Config.rs.getString("item_id")
                });
                }
        } catch (Exception e) {
            e.printStackTrace();
        }

        } catch (Exception e) {
        }
    }

    private void setDisDetailsMessage(String message_id) {
        try {
                Config.sql ="select * from message_details where message_id='"+message_id+"'";
                Config.rs = Config.stmt.executeQuery(Config.sql);
                if(Config.rs.next()) {
                    txt_dis_details_message.setText(Config.rs.getString("message"));
                }else{
                    txt_dis_details_message.setText("");
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void setThreeTableItem(){
        tbl_dis_details_dis_mgmtKeyReleased(null);
    }

    
private void loadReport(String sql) {
    //.........................................................................................................................................
        int col_height=0;  
        int row=1;  
        int year = jyc_report.getYear();
        int month = jmc_report.getMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        int total = calendar.getActualMaximum(Calendar.DATE);
        int size= Config.config_designation_details.size()+4;
        
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = new boolean [size];
        String []col = new String[size];
        Object [][]obj = new Object[total][size];
        
        
        //...................................................................SET FIRST COLUMN ON DATE.....................................................
        for (int i = 0; i < total; i++) {
         if(i<9){
             if(month<9){
                 obj[i][0]="0"+(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]="0"+(i+1)+"/"+(month+1)+"/"+year;
             }
         }else{
             if(month<9){
                 obj[i][0]=(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]=(i+1)+"/"+(month+1)+"/"+year;
             }
         }
            for (int j = 1; j < size-3; j++) {
                obj[i][j]=0.0;
            }
        }
        //...................................................................SET COLUMN NAME.....................................................
        can[0]=false;
        col[0]="  DATE";
            for (int i = 0; i < Config.config_designation_details.size(); i++) {
            can[row]=false;
            col[row]=Config.config_designation_details.get(i).getDesignation();
            if(col_height<col[row].length()){
                col_height=col[row].length();
            }
            row++;
            }
        can[row]=false;
        col[row]="TOTAL";
            row++;
        can[row]=false;
        col[row]="BALANCE";
            row++;
        can[row]=false;
        col[row]="RECEIVE";
        
        //...................................................................SET DATE ON CELL.....................................................
        double distribution=0.0;
        double stock=0.0;
        try {
            Config.sql ="select\n" +
                        "COALESCE(sum(\n" +
                        "COALESCE((\n" +
                        "select\n" +
                        "COALESCE(sum(quantity),0)\n" +
                        "from distribution_item di group by di.distribution_details_id,di.item_id\n" +
                        "having di.distribution_details_id=a.distribution_details_id and di.item_id='"+item_id+"'\n" +
                        "),0)\n" +
                        "-\n" +
                        "COALESCE((\n" +
                        "select\n" +
                        "COALESCE(sum(quantity),0)\n" +
                        "from return_item di group by di.distribution_details_id,di.item_id\n" +
                        "having di.distribution_details_id=a.distribution_details_id and di.item_id='"+item_id+"'\n" +
                        "),0)\n" +
                        "-\n" +
                        "COALESCE((\n" +
                        "select\n" +
                        "COALESCE(sum(quantity),0)\n" +
                        "from demage_item di group by di.distribution_details_id,di.item_id\n" +
                        "having di.distribution_details_id=a.distribution_details_id and di.item_id='"+item_id+"'\n" +
                        "),0)\n" +
                        "),0)\n" +
                        "as quantity\n" +
                        "from distribution_details a where YEAR(STR_TO_DATE(a._date,'%d/%m/%Y'))<='"+year+"' and MONTH(STR_TO_DATE(a._date,'%d/%m/%Y'))<'"+(month+1)+"'";
            
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if(Config.rs.next()) {
              distribution=Double.parseDouble(Config.rs.getString("quantity"));
                System.out.println("total dis="+distribution);
            }else{
                distribution=0;
            }    
            } catch (Exception e) {
            }
                try {
                Config.sql ="select\n" +
                    "COALESCE(sum(\n" +
                    "COALESCE((\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from receive_stock di\n" +
                    "where di.item_id=a.item_id\n" +
                    "and YEAR(STR_TO_DATE(di._date,'%d/%m/%Y'))<='"+year+"' and MONTH(STR_TO_DATE(di._date,'%d/%m/%Y'))<'"+(month+1)+"'\n" +
                    "),0)\n" +
                    "-\n" +
                    "COALESCE((\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from return_stock di\n" +
                    "where di.item_id=a.item_id\n" +
                    "and YEAR(STR_TO_DATE(di._date,'%d/%m/%Y'))<='"+year+"' and MONTH(STR_TO_DATE(di._date,'%d/%m/%Y'))<'"+(month+1)+"'\n" +
                    "),0)\n" +
                    "-\n" +
                    "COALESCE((\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from demage_stock di\n" +
                    "where di.item_id=a.item_id\n" +
                    "and YEAR(STR_TO_DATE(di._date,'%d/%m/%Y'))<='"+year+"' and MONTH(STR_TO_DATE(di._date,'%d/%m/%Y'))<'"+(month+1)+"'\n" +
                    "),0)\n" +
                    "),0)\n" +
                    "as quantity\n" +
                    "from stock_item a where a.item_id='"+item_id+"'";
                Config.rs = Config.stmt.executeQuery(Config.sql);
                if(Config.rs.next()) {
                stock=Double.parseDouble(Config.rs.getString("quantity"));
                System.out.println("total stock"+stock);
                stock=stock-distribution;
                System.out.println("total stock - dis"+stock);
                }else{
                stock=0;    
                }    
                } catch (Exception e) {
                    e.printStackTrace();
                }
            
                ResultSet dis=null;
                ResultSet recieve_item=null;
                ResultSet return_item=null;
                ResultSet demage_item=null;
                try {
                
                
                
                } catch (Exception e) {
                }
                for (int i = 0; i < total; i++) {
                    double stock_item=0.0;
                    double total_item=0.0;
                        try {
                            Config.sql="select di._date,\n" +
                                "COALESCE(sum(quantity),0) as quantity \n" +
                                "from receive_stock di group by di._year,di._month,di._date,di.item_id\n" +
                                "HAVING\n" +
                                "di._year ='"+year+"' and di._month='"+(month+1)+"' and di.item_id='"+item_id+"' order by di._date;";
                            recieve_item = Config.stmt.executeQuery(Config.sql);
                            while(recieve_item.next()){
                                if(Integer.parseInt(sdf_day.format(simple_date_formate.parse(recieve_item.getString("di._date"))))==i+1){
                                    stock_item=Double.parseDouble(recieve_item.getString("quantity"));
                                    break;
                                }
                            }    
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        try{
                         Config.sql="select di._date,\n" +
                            "COALESCE(sum(quantity),0) as quantity \n" +
                            "from return_stock di group by di._year,di._month,di._date,di.item_id\n" +
                            "HAVING\n" +
                            "di._year ='"+year+"' and di._month='"+(month+1)+"' and di.item_id='"+item_id+"' order by di._date;";
                            return_item = Config.stmt.executeQuery(Config.sql);
                            while(return_item.next()){
                                if(Integer.parseInt(sdf_day.format(simple_date_formate.parse(return_item.getString("di._date"))))==i+1){
                                    stock_item=stock_item-Double.parseDouble(return_item.getString("quantity"));
                                    break;
                                }
                            } 
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        try{
                        Config.sql="select di._date,\n" +
                            "COALESCE(sum(quantity),0) as quantity \n" +
                            "from demage_stock di group by di._year,di._month,di._date,di.item_id\n" +
                            "HAVING\n" +
                            "di._year ='"+year+"' and di._month='"+(month+1)+"' and di.item_id='"+item_id+"' order by di._date;";
                            demage_item = Config.stmt.executeQuery(Config.sql);
                        while(demage_item.next()){
                            if(Integer.parseInt(sdf_day.format(simple_date_formate.parse(demage_item.getString("di._date"))))==i+1){
                                stock_item=stock_item-Double.parseDouble(demage_item.getString("quantity"));
                                break;
                            }
                        }
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        try{
                        stock=stock+stock_item;    
                        Config.sql="select\n" +
                            "*,\n" +
                            "COALESCE(sum(\n" +
                            "COALESCE((\n" +
                            "select\n" +
                            "COALESCE(sum(quantity),0)\n" +
                            "from distribution_item di group by di.distribution_details_id,di.item_id\n" +
                            "having di.distribution_details_id=a.distribution_details_id and di.item_id='"+item_id+"'\n" +
                            "),0)\n" +
                            "-\n" +
                            "COALESCE((\n" +
                            "select\n" +
                            "COALESCE(sum(quantity),0)\n" +
                            "from return_item di group by di.distribution_details_id,di.item_id\n" +
                            "having di.distribution_details_id=a.distribution_details_id and di.item_id='"+item_id+"'\n" +
                            "),0)\n" +
                            "-\n" +
                            "COALESCE((\n" +
                            "select\n" +
                            "COALESCE(sum(quantity),0)\n" +
                            "from demage_item di group by di.distribution_details_id,di.item_id\n" +
                            "having di.distribution_details_id=a.distribution_details_id and di.item_id='"+item_id+"'\n" +
                            "),0)\n" +
                            "),0)\n" +
                            "as quantity\n" +
                            "from distribution_details a group by a.distribution_details_id,a._year,a._month having a._year ='"+year+"' and a._month='"+(month+1)+"' ;";    
                dis = Config.stmt.executeQuery(Config.sql);
                            System.out.println(Config.sql);
                        while(dis.next()){  
                        if(Integer.parseInt(sdf_day.format(simple_date_formate.parse(dis.getString("a._date"))))==i+1){
                            for (int j = 0; j < Config.config_designation_details.size(); j++) {
                                if(Config.config_designation_details.get(j).getDesignation_id().equals(dis.getString("designation_id"))){
                                    total_item=total_item+Double.parseDouble(dis.getString("quantity"));       
                                    obj[i][j+1]=Double.parseDouble(dis.getString("quantity"));
                                }
                            }
                        }
                        }
                        stock=stock-total_item;
                        obj[i][size-3]=total_item;
                        obj[i][size-2]=stock;
                        obj[i][size-1]=stock_item;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                }
        //...................................................................SET TABLE..................................................... 
                tbl_report.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                return can [columnIndex];
                }
                });

                tbl_report.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
                tbl_report.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(100);
                tbl_report.getTableHeader().getColumnModel().getColumn(0).setCellRenderer(new CustomTableCellRenderer());
                for (int j = 1; j < size-1; j++) {
                tbl_report.getTableHeader().getColumnModel().getColumn(j).setResizable(false);
                tbl_report.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(col[j].length()*10);
                tbl_report.getTableHeader().getColumnModel().getColumn(j).setCellRenderer(new CustomTableCellRenderer());
                }    

                tbl_report.getTableHeader().getColumnModel().getColumn(row-2).setResizable(false);
                tbl_report.getTableHeader().getColumnModel().getColumn(row-2).setPreferredWidth(100);
                tbl_report.getTableHeader().getColumnModel().getColumn(row-2).setCellRenderer(new CustomTableCellRenderer());
                tbl_report.getTableHeader().getColumnModel().getColumn(row-1).setResizable(false);
                tbl_report.getTableHeader().getColumnModel().getColumn(row-1).setPreferredWidth(100);
                tbl_report.getTableHeader().getColumnModel().getColumn(row-1).setCellRenderer(new CustomTableCellRenderer());
                tbl_report.getTableHeader().getColumnModel().getColumn(row).setResizable(false);
                tbl_report.getTableHeader().getColumnModel().getColumn(row).setPreferredWidth(100);
                tbl_report.getTableHeader().getColumnModel().getColumn(row).setCellRenderer(new CustomTableCellRenderer());
        //...................................................................SET DATE ON CELL.....................................................    
}    
    public void setData(String item_id) {
        this.item_id = item_id;
        txt_name.setText(Config.config_stock_item.get(Config.id_stock_item.indexOf(item_id)).getName());
        btn_searchActionPerformed(null);
                
     }
    public void setDesignation(String designation_id) {
        this.designation_id = designation_id;
        txt_designation.setText(Config.config_designation_details.get(Config.id_designation_details.indexOf(designation_id)).getDesignation());
    }
}
class CustomTableCellRenderer extends DefaultTableCellRenderer {
        public Component getTableCellRendererComponent (JTable table, Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
            Component cell = super.getTableCellRendererComponent(table, obj, isSelected, hasFocus, row, column);            
            try {
                if (isSelected) {
                    cell.setBackground(new java.awt.Color(230,79,6));                    
                } else {
                    if (Double.parseDouble(table.getValueAt(row, column).toString()) != 0 ) {
                        cell.setBackground(new java.awt.Color(255,216,216));
                    } else {
                        cell.setBackground(Color.WHITE);
                    }
                }
            } catch (Exception e) {
                cell.setBackground(Color.WHITE);
            }
            return cell;

        }

    }
