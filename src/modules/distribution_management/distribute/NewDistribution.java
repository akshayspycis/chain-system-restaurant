package modules.distribution_management.distribute;

import data_manager.DistributionDetails;
import data_manager.DistributionItem;
import data_manager.configuration.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

public class NewDistribution extends javax.swing.JDialog {
    
    String item_list_id = "";
    String item_id = "";
    String message_id = "";
    String designation_id = "";
    String distribution_details_id ="";
    DefaultTableModel tbl_dis_stock_model = null;
    SimpleDateFormat simple_date_formate = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");

public NewDistribution(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        tbl_dis_stock_model=(DefaultTableModel) tbl_dis_stock.getModel();
        
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel6 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        panal = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txt_name = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_price = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_unit = new javax.swing.JTextField();
        txt_quantity = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        cb_Item_type = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jfmt_date = new javax.swing.JFormattedTextField();
        btn_add = new javax.swing.JButton();
        btn_s_reset = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_dis_stock = new javax.swing.JTable();
        btn_tbl_reset = new javax.swing.JButton();
        btn_remove = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        btn_reset = new javax.swing.JButton();
        btn_message = new javax.swing.JButton();
        btn_save = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txt_designation = new javax.swing.JTextField();

        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        panal.setBackground(new java.awt.Color(255, 255, 255));
        panal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Stock Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(51, 51, 51))); // NOI18N

        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Producte Name");

        txt_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nameActionPerformed(evt);
            }
        });
        txt_name.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_nameKeyPressed(evt);
            }
        });

        jLabel10.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setText("Date");

        txt_price.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_priceActionPerformed(evt);
            }
        });

        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Unit");

        txt_unit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_unitKeyPressed(evt);
            }
        });

        txt_quantity.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_quantityCaretUpdate(evt);
            }
        });
        txt_quantity.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_quantityFocusLost(evt);
            }
        });

        jLabel14.setText("Price");

        jLabel24.setText("Quantity");

        cb_Item_type.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-Select-", "Stock Item", "Sada flavour", "Manufacture Item" }));
        cb_Item_type.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_Item_typeActionPerformed(evt);
            }
        });

        jLabel7.setText("Item Type");

        try {
            jfmt_date.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        btn_add.setText("Add");
        btn_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addActionPerformed(evt);
            }
        });

        btn_s_reset.setText("Reset");
        btn_s_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_s_resetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panalLayout = new javax.swing.GroupLayout(panal);
        panal.setLayout(panalLayout);
        panalLayout.setHorizontalGroup(
            panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_unit, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_price)
                            .addGroup(panalLayout.createSequentialGroup()
                                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jfmt_date, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_quantity)
                            .addGroup(panalLayout.createSequentialGroup()
                                .addComponent(jLabel24)
                                .addGap(0, 82, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cb_Item_type, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_name)
                            .addGroup(panalLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panalLayout.createSequentialGroup()
                        .addComponent(btn_s_reset)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_add)))
                .addContainerGap())
        );
        panalLayout.setVerticalGroup(
            panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panalLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cb_Item_type, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_price, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_unit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jfmt_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_add)
                    .addComponent(btn_s_reset))
                .addGap(4, 4, 4))
        );

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setText("Item List Details");

        tbl_dis_stock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PRODUCT NAME", "DATE", "QUANTITY", "ITEM_ID", "ITEM LIST ID"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_dis_stock.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tbl_dis_stock);
        if (tbl_dis_stock.getColumnModel().getColumnCount() > 0) {
            tbl_dis_stock.getColumnModel().getColumn(0).setResizable(false);
            tbl_dis_stock.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_dis_stock.getColumnModel().getColumn(1).setResizable(false);
            tbl_dis_stock.getColumnModel().getColumn(1).setPreferredWidth(200);
            tbl_dis_stock.getColumnModel().getColumn(2).setResizable(false);
            tbl_dis_stock.getColumnModel().getColumn(2).setPreferredWidth(150);
            tbl_dis_stock.getColumnModel().getColumn(3).setMinWidth(0);
            tbl_dis_stock.getColumnModel().getColumn(3).setPreferredWidth(0);
            tbl_dis_stock.getColumnModel().getColumn(3).setMaxWidth(0);
            tbl_dis_stock.getColumnModel().getColumn(4).setMinWidth(0);
            tbl_dis_stock.getColumnModel().getColumn(4).setPreferredWidth(0);
            tbl_dis_stock.getColumnModel().getColumn(4).setMaxWidth(0);
        }

        btn_tbl_reset.setText("Reset");
        btn_tbl_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_tbl_resetActionPerformed(evt);
            }
        });

        btn_remove.setText("Remove");
        btn_remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_removeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(btn_remove)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 177, Short.MAX_VALUE)
                        .addComponent(btn_tbl_reset)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_tbl_reset)
                    .addComponent(btn_remove))
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(230, 79, 6));

        jLabel20.setBackground(new java.awt.Color(255, 255, 255));
        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("Distribution Item");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        btn_reset.setText("Reset");
        btn_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_resetActionPerformed(evt);
            }
        });

        btn_message.setText("Message");
        btn_message.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_messageActionPerformed(evt);
            }
        });

        btn_save.setText("Save");
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });
        btn_save.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_saveKeyPressed(evt);
            }
        });

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });
        btn_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_cancelKeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Designation :");

        txt_designation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_designationActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_reset)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_message)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_save, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancel))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txt_designation)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txt_designation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_reset)
                            .addComponent(btn_message)
                            .addComponent(btn_save)
                            .addComponent(btn_cancel))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );

        getRootPane().setDefaultButton(btn_save);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void btn_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_cancelKeyPressed

    }//GEN-LAST:event_btn_cancelKeyPressed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void btn_saveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_saveKeyPressed

    }//GEN-LAST:event_btn_saveKeyPressed

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        try {
            String str = checkValidityTable();
            if(str.equals("ok")){
                if (chekAvailibility()) {
                
                DistributionDetails bt = new DistributionDetails();
               bt.setDesignation_id(designation_id);
               bt.setDate(jfmt_date.getText());
               bt.setMonth(sdf_month.format(simple_date_formate.parse(jfmt_date.getText())));
               bt.setYear(sdf_year.format(simple_date_formate.parse(jfmt_date.getText())));
               bt.setMessage_id(message_id);
                if(Config.distribution_details_mgr.insDistributionDetails(bt)){
                    try {
                        Config.sql ="select max(distribution_details_id) from distribution_details";
                        Config.rs = Config.stmt.executeQuery(Config.sql);
                        if(Config.rs.next()) {
                            distribution_details_id = Config.rs.getString(1);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ArrayList<DistributionItem> arList_rs = new ArrayList<>();
                    for (int i = 0; i < tbl_dis_stock_model.getRowCount(); i++) {
                        DistributionItem rs = new DistributionItem();
                        rs.setItem_id(tbl_dis_stock_model.getValueAt(i, 3).toString());
                        rs.setItem_list_id(tbl_dis_stock_model.getValueAt(i, 4).toString());
                        rs.setQuantity(tbl_dis_stock_model.getValueAt(i, 2).toString());
                        rs.setDistribution_details_id(distribution_details_id);
                        arList_rs.add(rs);
                    }
                    if(Config.distribution_item_mgr.insDistributionItem(arList_rs)){
                        onloadReset();
                        Config.distribution_management.onloadDisDetails();
                        JOptionPane.showMessageDialog(this, "Stock List created successfully.", "Creation successful", JOptionPane.NO_OPTION);
                    }else{
                        JOptionPane.showMessageDialog(this, "Error in Stock List creation", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }else{
                    JOptionPane.showMessageDialog(this, "Error in Stock List ", "Error", JOptionPane.ERROR_MESSAGE);
                }
                } else {
                       JOptionPane.showMessageDialog(this, "List Alreadey exits  ", "Error", JOptionPane.ERROR_MESSAGE);
                }
                
            }else{
                JOptionPane.showMessageDialog(this, "Check '"+str+"' field.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_saveActionPerformed

    private void btn_messageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_messageActionPerformed
        if(message_id.equals("")){
            Config.new_message.onloadReset(20);
            Config.new_message.setVisible(true);
        }else{
            Config.view_message.onloadReset(20,message_id);
            Config.view_message.setVisible(true);
        }
    }//GEN-LAST:event_btn_messageActionPerformed

    private void btn_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_resetActionPerformed
        onloadReset();
    }//GEN-LAST:event_btn_resetActionPerformed

    private void btn_removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_removeActionPerformed
        try {
            tbl_dis_stock_model.removeRow(tbl_dis_stock.getSelectedRow());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select any Row ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_removeActionPerformed

    private void btn_tbl_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_tbl_resetActionPerformed
        onloadTable();
    }//GEN-LAST:event_btn_tbl_resetActionPerformed

    private void btn_s_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_s_resetActionPerformed
        onloadResetDistributeItem();
    }//GEN-LAST:event_btn_s_resetActionPerformed

    private void btn_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addActionPerformed
        try {
            String str = checkValidity();
            if (str.equals("ok")) {
            if (checkAvailibility()) {
                tbl_dis_stock_model.addRow(new Object[]{
                    txt_name.getText(),
                    jfmt_date.getText(),
                    txt_quantity.getText(),
                    item_id,
                    item_list_id
                });
                JOptionPane.showMessageDialog(this, "Stock List created successfully.", "Creation successful", JOptionPane.NO_OPTION);
                onloadResetDistributeItem();
            }else{
                JOptionPane.showMessageDialog(this, "Item already exits", "Error", JOptionPane.ERROR_MESSAGE);
            }
            }else{
                JOptionPane.showMessageDialog(this, "Check '"+str+"' field.", "Error", JOptionPane.ERROR_MESSAGE);
            }    
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_addActionPerformed

    private void cb_Item_typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_Item_typeActionPerformed
        txt_unit.setText("");
        if(cb_Item_type.getSelectedIndex()!=0){
            switch(cb_Item_type.getSelectedIndex()){
                case 1:
                break;
                case 2:
                break;
                case 3:
                break;
            }
        }
    }//GEN-LAST:event_cb_Item_typeActionPerformed

    private void txt_quantityFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_quantityFocusLost
        if(!txt_quantity.getText().trim().equals("")){
            try {
                Double.parseDouble(txt_quantity.getText().trim());
            }catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Only No.Value Required ", "Error", JOptionPane.ERROR_MESSAGE);
                txt_quantity.setText("");
                txt_quantity.requestFocus();
            }
        }
    }//GEN-LAST:event_txt_quantityFocusLost

    private void txt_quantityCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_quantityCaretUpdate

    }//GEN-LAST:event_txt_quantityCaretUpdate

    private void txt_unitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_unitKeyPressed

    }//GEN-LAST:event_txt_unitKeyPressed

    private void txt_priceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_priceActionPerformed
        if(!item_id.equals("")){
            Config.rate_search.onloadReset(4, item_id);
            Config.rate_search.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(this, "Select Item Categories ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_priceActionPerformed

    private void txt_nameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nameKeyPressed

    }//GEN-LAST:event_txt_nameKeyPressed

    private void txt_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nameActionPerformed
        if(cb_Item_type.getSelectedIndex()!=0){
            Config.item_search.onloadReset(4, cb_Item_type.getSelectedIndex());
            Config.item_search.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(this, "Select Item Type ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_nameActionPerformed

    private void txt_designationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_designationActionPerformed
      Config.designation_search.onloadReset(0);
      Config.designation_search.setVisible(true);
    }//GEN-LAST:event_txt_designationActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_add;
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_message;
    private javax.swing.JButton btn_remove;
    private javax.swing.JButton btn_reset;
    private javax.swing.JButton btn_s_reset;
    private javax.swing.JButton btn_save;
    private javax.swing.JButton btn_tbl_reset;
    private javax.swing.JComboBox cb_Item_type;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JFormattedTextField jfmt_date;
    private javax.swing.JPanel panal;
    private javax.swing.JTable tbl_dis_stock;
    private javax.swing.JTextField txt_designation;
    private javax.swing.JTextField txt_name;
    private javax.swing.JTextField txt_price;
    private javax.swing.JTextField txt_quantity;
    private javax.swing.JTextField txt_unit;
    // End of variables declaration//GEN-END:variables
    public void onloadReset(){
        txt_designation.setText("");
        onloadTable();
        jfmt_date.setText(simple_date_formate.format(new Date()));
    }
    
    public void onloadResetDistributeItem(){
    cb_Item_type.setSelectedIndex(0);
    txt_name.setText("");
    txt_unit.setText("");
    txt_price.setText("");
    txt_quantity.setText("");
    }
    public void onloadTable(){
        tbl_dis_stock_model.setRowCount(0);
    }
    
    public String checkValidity(){
        if(cb_Item_type.getSelectedIndex()==0){
        return "Item Type";    
        }else if(txt_name.getText().equals("")){
        return "Item Name";    
        }else if(jfmt_date.getText().equals("")){
        return "Date ";    
        }else if (Double.parseDouble(txt_quantity.getText())==0.0) {
            return "Quantity";
        }else{
        return "ok";    
        }
    }
    
    public void setDesignation(String designation_id) {
        this.designation_id = designation_id;
        txt_designation.setText(Config.config_designation_details.get(Config.id_designation_details.indexOf(designation_id)).getDesignation());
        
    }
    public void setData(String item_id) {
        this.item_id = item_id;
        txt_name.setText(Config.config_stock_item.get(Config.id_stock_item.indexOf(item_id)).getName());
        int i=0;
        for ( i = 0; i < Config.config_item_list.size(); i++) {
            if(Config.config_item_list.get(i).getItem_id().equals(item_id)){
            item_list_id=Config.config_item_list.get(i).getItem_list_id();
             break;   
            }
        }
       if(i!=Config.config_item_list.size()){ 
       txt_unit.setText(Config.config_stock_item.get(Config.id_stock_item.indexOf(item_id)).getUnit());
       txt_price.setText(Config.config_item_list.get(i).getPrise());
       }else{
           txt_price.setText("");
           txt_unit.setText("");
           JOptionPane.showMessageDialog(this, "Select Item Price is not configure", "Error", JOptionPane.ERROR_MESSAGE);   
       }
     }
    
     public void setMessage(String message_id){
         this.message_id=message_id;
     }

     private String checkValidityTable() {
        if(tbl_dis_stock.getRowCount()==0){
            return "Table";
        }else{
            return "ok";
        }
     }

    private boolean checkAvailibility() {
        for (int i = 0; i < tbl_dis_stock_model.getRowCount(); i++) {
            if(tbl_dis_stock_model.getValueAt(i, 3).toString().equals(item_id)){
             return false;
            }
        }
        return true;
    }
    public void setItemListId(String item_list_id,String price) {
        this.item_list_id=item_list_id;
        txt_price.setText(price);
    }

    private boolean chekAvailibility() {
    try{
            Config.sql="select * from distribution_details where designation_id='"+designation_id+"' and _date='"+jfmt_date.getText()+"'";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if (Config.rs.next()) {
            return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
       return true; 
    }
    

}
