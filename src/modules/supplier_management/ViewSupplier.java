package modules.supplier_management;

import data_manager.AddressDetails;
import data_manager.ContactDetails;
import data_manager.IdentityDetails;
import data_manager.ProfileDetails;
import data_manager.SupplierDetails;
import data_manager.configuration.Config;
import data_manager.configuration.SupplierProfile;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class ViewSupplier extends javax.swing.JDialog {

    Calendar currentDate;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
   
    public ViewSupplier(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        txt_supplierName.setBackground(new Color(240,240,240));
       // setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icon.png")));

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        btn_dalete = new javax.swing.JButton();
        btn_update = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_supplierName = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        cb_gender = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        jdc_dob = new com.toedter.calendar.JDateChooser();
        panal = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        txt_contactNo = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txt_AlternateContactNo = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_address = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txt_locality = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_city = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txt_pincode = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        cb_state = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        txt_org = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_email = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        cb_identityType = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        txt_identityNo = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        txt_accoundno = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txt_bank_name = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        lbl_supplierid = new javax.swing.JLabel();

        jPanel3.setBackground(new java.awt.Color(230, 79, 6));

        jLabel22.setBackground(new java.awt.Color(255, 255, 255));
        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Employee Management");

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel22)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel3Layout.createSequentialGroup()
                .add(6, 6, 6)
                .add(jLabel22, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTextField2.setText("jTextField2");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Payroll Management - New Profile");
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btn_dalete.setText("Delete");
        btn_dalete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_daleteActionPerformed(evt);
            }
        });
        btn_dalete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_daleteKeyPressed(evt);
            }
        });

        btn_update.setText("Update");
        btn_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateActionPerformed(evt);
            }
        });
        btn_update.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_updateKeyPressed(evt);
            }
        });

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });
        btn_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_cancelKeyPressed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Personal Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(51, 51, 51))); // NOI18N

        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Supplier Name *");

        txt_supplierName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_supplierNameKeyPressed(evt);
            }
        });

        jLabel15.setForeground(new java.awt.Color(51, 51, 51));
        jLabel15.setText("Gender *");

        cb_gender.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-Select-", "Male", "Female" }));
        cb_gender.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb_genderKeyPressed(evt);
            }
        });

        jLabel13.setForeground(new java.awt.Color(51, 51, 51));
        jLabel13.setText("DOB *");

        jdc_dob.setDateFormatString("dd/MM/yyyy");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel1)
                    .add(txt_supplierName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 238, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel15)
                    .add(cb_gender, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 113, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jLabel13)
                        .add(0, 0, Short.MAX_VALUE))
                    .add(jdc_dob, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jLabel1)
                        .add(26, 26, 26))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel13)
                            .add(jLabel15))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(cb_gender, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(txt_supplierName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jdc_dob, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panal.setBackground(new java.awt.Color(255, 255, 255));
        panal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Contact Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(51, 51, 51))); // NOI18N

        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Contact No. *");

        jTextField3.setEditable(false);
        jTextField3.setText("+91");
        jTextField3.setEnabled(false);

        txt_contactNo.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_contactNoCaretUpdate(evt);
            }
        });
        txt_contactNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_contactNoKeyPressed(evt);
            }
        });

        jLabel7.setForeground(new java.awt.Color(51, 51, 51));
        jLabel7.setText("Alternate Contact No.");

        txt_AlternateContactNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_AlternateContactNoKeyPressed(evt);
            }
        });

        jLabel8.setForeground(new java.awt.Color(51, 51, 51));
        jLabel8.setText("Address *");

        txt_address.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_addressKeyPressed(evt);
            }
        });

        jLabel9.setForeground(new java.awt.Color(51, 51, 51));
        jLabel9.setText("Address 2");

        txt_locality.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_localityKeyPressed(evt);
            }
        });

        jLabel10.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setText("City *");

        txt_city.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_cityKeyPressed(evt);
            }
        });

        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Pincode");

        txt_pincode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_pincodeKeyPressed(evt);
            }
        });

        jLabel12.setForeground(new java.awt.Color(51, 51, 51));
        jLabel12.setText("State *");

        cb_state.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- Select -", "Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Chandigarh", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Orissa", "Puduchery", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Tripura", "Uttar Pradesh", "Uttaranchal", "West Bengal" }));
        cb_state.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb_stateKeyPressed(evt);
            }
        });

        jLabel11.setForeground(new java.awt.Color(51, 51, 51));
        jLabel11.setText("Organization");

        txt_org.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_orgKeyPressed(evt);
            }
        });

        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Email");

        txt_email.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_emailKeyPressed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout panalLayout = new org.jdesktop.layout.GroupLayout(panal);
        panal.setLayout(panalLayout);
        panalLayout.setHorizontalGroup(
            panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panalLayout.createSequentialGroup()
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, panalLayout.createSequentialGroup()
                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(panalLayout.createSequentialGroup()
                                .addContainerGap()
                                .add(jLabel8))
                            .add(panalLayout.createSequentialGroup()
                                .addContainerGap()
                                .add(jTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(txt_contactNo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 206, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(panalLayout.createSequentialGroup()
                                .add(10, 10, 10)
                                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(panalLayout.createSequentialGroup()
                                        .add(jLabel6)
                                        .add(171, 171, 171))
                                    .add(panalLayout.createSequentialGroup()
                                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                            .add(jLabel10)
                                            .add(txt_city, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 143, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                            .add(jLabel2)
                                            .add(txt_pincode, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 85, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, panalLayout.createSequentialGroup()
                                .addContainerGap()
                                .add(txt_address, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 238, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 10, Short.MAX_VALUE)
                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(txt_AlternateContactNo)
                            .add(jLabel7)
                            .add(jLabel9)
                            .add(txt_locality)
                            .add(jLabel12)
                            .add(cb_state, 0, 238, Short.MAX_VALUE)))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, panalLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(panalLayout.createSequentialGroup()
                                .add(txt_email)
                                .add(10, 10, 10)
                                .add(txt_org))
                            .add(panalLayout.createSequentialGroup()
                                .add(jLabel4)
                                .add(224, 224, 224)
                                .add(jLabel11, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(131, 131, 131)))))
                .addContainerGap())
        );
        panalLayout.setVerticalGroup(
            panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panalLayout.createSequentialGroup()
                .add(6, 6, 6)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel6)
                    .add(jLabel7))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txt_contactNo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txt_AlternateContactNo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel8)
                    .add(jLabel9))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txt_address, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txt_locality, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel10)
                    .add(jLabel2)
                    .add(jLabel12))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txt_city, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txt_pincode, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cb_state, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel11)
                    .add(jLabel4))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txt_org, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txt_email, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel17.setForeground(new java.awt.Color(51, 51, 51));
        jLabel17.setText("Identity Type");

        cb_identityType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- Select -", "Passport", "Adhar card", "Voter ID", "Driving Licances" }));
        cb_identityType.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb_identityTypeKeyPressed(evt);
            }
        });

        jLabel16.setForeground(new java.awt.Color(51, 51, 51));
        jLabel16.setText("Identity No.");

        txt_identityNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_identityNoKeyPressed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setText("Other Details");

        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Account No.");

        txt_accoundno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_accoundnoKeyPressed(evt);
            }
        });

        jLabel18.setForeground(new java.awt.Color(51, 51, 51));
        jLabel18.setText("Bank Name");

        txt_bank_name.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_bank_nameKeyPressed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jSeparator1)
            .add(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel4Layout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(jLabel17)
                            .add(jLabel16)
                            .add(cb_identityType, 0, 135, Short.MAX_VALUE)
                            .add(txt_identityNo)))
                    .add(txt_accoundno)
                    .add(jPanel4Layout.createSequentialGroup()
                        .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel21)
                            .add(jLabel5)
                            .add(jLabel18))
                        .add(0, 0, Short.MAX_VALUE))
                    .add(txt_bank_name))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel21)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(1, 1, 1)
                .add(jLabel17)
                .add(6, 6, 6)
                .add(cb_identityType, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(txt_identityNo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel5)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(txt_accoundno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jLabel18)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(txt_bank_name, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(98, 98, 98))
        );

        jPanel5.setBackground(new java.awt.Color(230, 79, 6));

        jLabel20.setBackground(new java.awt.Color(255, 255, 255));
        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("View Supplier");

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 120, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jLabel20, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(panal, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
            .add(jPanel5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                .add(jPanel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(panal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(0, 0, Short.MAX_VALUE))
                    .add(jPanel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        lbl_supplierid.setForeground(new java.awt.Color(240, 240, 240));

        org.jdesktop.layout.GroupLayout jPanel6Layout = new org.jdesktop.layout.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .add(btn_dalete)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(lbl_supplierid, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 166, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(btn_update, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 67, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btn_cancel)
                .addContainerGap())
            .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(lbl_supplierid, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(btn_cancel)
                        .add(btn_update)
                        .add(btn_dalete)))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getRootPane().setDefaultButton(btn_update);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
        
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void btn_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateActionPerformed
   try {
           String str = checkValidity();
         if (str.equals("ok")) {
             if(checkAvailibility()){
               
                ProfileDetails pd = new ProfileDetails();
                        pd.setProfile_id(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(lbl_supplierid.getText())).getProfile_id());
                        pd.setName(txt_supplierName.getText().trim());
                        pd.setGender(cb_gender.getSelectedItem().toString());
                         pd.setDob(sdf1.format(jdc_dob.getDate()));
                ContactDetails cd = new ContactDetails();
                        cd.setContact_id(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(lbl_supplierid.getText())).getContact_id());
                        cd.setContact_no(txt_contactNo.getText().trim()) ;
                        cd.setAlt_contact_no(txt_AlternateContactNo.getText().trim());
                        cd.setEmail(txt_email.getText().trim());
                        cd.setOther(txt_org.getText().trim());
                AddressDetails ad = new AddressDetails();         
                        ad.setAddress_id(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(lbl_supplierid.getText())).getAddress_id());
                        ad.setAddress_1(txt_address.getText().trim());
                        ad.setAddress_2(txt_locality.getText().trim());
                        ad.setCity(txt_city.getText().trim());
                        ad.setPincode(txt_pincode.getText().trim());
                        ad.setState(cb_state.getSelectedItem().toString().trim());
                IdentityDetails id = new IdentityDetails();        
                        id.setIdentity_id(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(lbl_supplierid.getText())).getIdentity_id());
                        id.setIdentity_type(cb_identityType.getSelectedItem().toString().trim());
                        id.setIdentity_no(txt_identityNo.getText().trim());
                SupplierDetails sd = new SupplierDetails(); 
                        sd.setSupplier_id(lbl_supplierid.getText());
                        sd.setAccount_no(txt_accoundno.getText().trim());
                        sd.setBank_name(txt_bank_name.getText().trim());
                SupplierProfile sp = new SupplierProfile();
                        sp.setProfile_details(pd);
                        sp.setAddress_details(ad);
                        sp.setContact_details(cd);
                        sp.setIdentity_details(id);
                        sp.setSupplier_details(sd);
                if (Config.supplier_details_mgr.updSupplierDetails(sp)) {
                    Config.supplier_management.onloadReset();
                        JOptionPane.showMessageDialog(this, "Supplier porfile updated successfully.", "Creation successful", JOptionPane.NO_OPTION);
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(this, "Error in profile updation.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                        JOptionPane.showMessageDialog(this, "Organization already exits", "Error", JOptionPane.ERROR_MESSAGE);
                    } 
                }else{
             JOptionPane.showMessageDialog(this, "Check '"+str+"' field.", "Error", JOptionPane.ERROR_MESSAGE);
         }
            } catch (Exception e) {
                e.printStackTrace();
        }
    }//GEN-LAST:event_btn_updateActionPerformed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void txt_contactNoCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_contactNoCaretUpdate
       
    }//GEN-LAST:event_txt_contactNoCaretUpdate

    private void txt_supplierNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_supplierNameKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            cb_gender.requestFocus();
            cb_gender.setBackground(new Color(240,240,240));
            txt_supplierName.setBackground(Color.WHITE);
           }        
//        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
//            .requestFocus();
//           }
//        if(evt.getKeyCode() == KeyEvent.VK_UP) {
//            .requestFocus();
//           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            txt_contactNo.requestFocus();
            txt_contactNo.setBackground(new Color(240,240,240));
            txt_supplierName.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_txt_supplierNameKeyPressed

    private void cb_genderKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb_genderKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jdc_dob.requestFocus();
            jdc_dob.setBackground(new Color(240,240,240));
            cb_gender.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_supplierName.requestFocus();
            txt_supplierName.setBackground(new Color(240,240,240));
            cb_gender.setBackground(Color.WHITE);
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            txt_supplierName.requestFocus();
            txt_supplierName.setBackground(new Color(240,240,240));
            cb_gender.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            jdc_dob.requestFocus();
            jdc_dob.setBackground(new Color(240,240,240));
            cb_gender.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_cb_genderKeyPressed

    private void txt_contactNoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_contactNoKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            txt_AlternateContactNo.requestFocus();
            txt_AlternateContactNo.setBackground(new Color(240,240,240));
            txt_contactNo.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jdc_dob.requestFocus();
            jdc_dob.setBackground(new Color(240,240,240));
            txt_contactNo.setBackground(Color.WHITE);            
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            jdc_dob.requestFocus();
            jdc_dob.setBackground(new Color(240,240,240));
            txt_contactNo.setBackground(Color.WHITE);
           }        
        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            txt_AlternateContactNo.requestFocus();
            txt_AlternateContactNo.setBackground(new Color(240,240,240));
            txt_contactNo.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_txt_contactNoKeyPressed

    private void txt_AlternateContactNoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_AlternateContactNoKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            txt_address.requestFocus();
            txt_address.setBackground(new Color(240,240,240));
            txt_AlternateContactNo.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_contactNo.requestFocus();
            txt_contactNo.setBackground(new Color(240,240,240));
            txt_AlternateContactNo.setBackground(Color.WHITE);
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            txt_contactNo.requestFocus();
            txt_contactNo.setBackground(new Color(240,240,240));
            txt_AlternateContactNo.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            txt_address.requestFocus();
            txt_address.setBackground(new Color(240,240,240));
            txt_AlternateContactNo.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_txt_AlternateContactNoKeyPressed

    private void txt_addressKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_addressKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            txt_locality.requestFocus();
            txt_locality.setBackground(new Color(240,240,240));
            txt_address.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_AlternateContactNo.requestFocus();
            txt_AlternateContactNo.setBackground(new Color(240,240,240));
            txt_address.setBackground(Color.WHITE);
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            txt_AlternateContactNo.requestFocus();
            txt_AlternateContactNo.setBackground(new Color(240,240,240));
            txt_address.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            txt_locality.requestFocus();
            txt_locality.setBackground(new Color(240,240,240));
            txt_address.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_txt_addressKeyPressed

    private void txt_localityKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_localityKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            txt_city.requestFocus();
            txt_city.setBackground(new Color(240,240,240));
            txt_locality.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_address.requestFocus();
            txt_address.setBackground(new Color(240,240,240));
            txt_locality.setBackground(Color.WHITE);
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
           txt_address .requestFocus();
           txt_address.setBackground(new Color(240,240,240));
            txt_locality.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            txt_city.requestFocus();
            txt_city.setBackground(new Color(240,240,240));
            txt_locality.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_txt_localityKeyPressed

    private void txt_cityKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_cityKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            txt_pincode.requestFocus();
            txt_pincode.setBackground(new Color(240,240,240));
            txt_city.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_locality.requestFocus();
            txt_locality.setBackground(new Color(240,240,240));
            txt_city.setBackground(Color.WHITE);
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            txt_locality.requestFocus();
            txt_locality.setBackground(new Color(240,240,240));
            txt_city.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            txt_pincode.requestFocus();
            txt_pincode.setBackground(new Color(240,240,240));
            txt_city.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_txt_cityKeyPressed

    private void txt_pincodeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_pincodeKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            cb_state.requestFocus();
            cb_state.setBackground(new Color(240,240,240));
            txt_pincode.setBackground(Color.WHITE);
           }        
        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_city.requestFocus();
            txt_city.setBackground(new Color(240,240,240));
            txt_pincode.setBackground(Color.WHITE);
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            txt_city.requestFocus();
            txt_city.setBackground(new Color(240,240,240));
            txt_pincode.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            cb_state.requestFocus();
            cb_state.setBackground(new Color(240,240,240));
            txt_pincode.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_txt_pincodeKeyPressed

    private void cb_stateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb_stateKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            txt_email.requestFocus();
            txt_email.setBackground(new Color(240,240,240));
            cb_state.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_pincode.requestFocus();
            txt_pincode.setBackground(new Color(240,240,240));
            cb_state.setBackground(Color.WHITE);
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            txt_pincode.requestFocus();
            txt_pincode.setBackground(new Color(240,240,240));
            cb_state.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            txt_email.requestFocus();
            txt_email.setBackground(new Color(240,240,240));
            cb_state.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_cb_stateKeyPressed

    private void txt_emailKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_emailKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            txt_org.requestFocus();
            txt_org.setBackground(new Color(240,240,240));
            txt_email.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            cb_state.requestFocus();
            cb_state.setBackground(new Color(240,240,240));
            txt_email.setBackground(Color.WHITE);
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            cb_state.requestFocus();
            cb_state.setBackground(new Color(240,240,240));
            txt_email.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            txt_org.requestFocus();
            txt_org.setBackground(new Color(240,240,240));
            txt_email.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_txt_emailKeyPressed

    private void txt_orgKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_orgKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            cb_identityType.requestFocus();
            cb_identityType.setBackground(new Color(240,240,240));
            txt_org.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
           txt_email.requestFocus();
           txt_email.setBackground(new Color(240,240,240));
           txt_org.setBackground(Color.WHITE);
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            txt_email.requestFocus();
            txt_email.setBackground(new Color(240,240,240));
            txt_org.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_dalete.requestFocus();
            txt_org.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_txt_orgKeyPressed

    private void cb_identityTypeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb_identityTypeKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            txt_identityNo.requestFocus();
            txt_identityNo.setBackground(new Color(240,240,240));
            cb_identityType.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_org.requestFocus();
            txt_org.setBackground(new Color(240,240,240));
            cb_identityType.setBackground(Color.WHITE);
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            txt_supplierName.requestFocus();
            txt_supplierName.setBackground(new Color(240,240,240));
            cb_identityType.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            txt_identityNo.requestFocus();
            txt_identityNo.setBackground(new Color(240,240,240));
            cb_identityType.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_cb_identityTypeKeyPressed

    private void txt_identityNoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_identityNoKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            txt_accoundno.requestFocus();
            txt_accoundno.setBackground(new Color(240,240,240));
            txt_identityNo.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            cb_identityType.requestFocus();
            cb_identityType.setBackground(new Color(240,240,240));
            txt_identityNo.setBackground(Color.WHITE);
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            cb_identityType.requestFocus();
            cb_identityType.setBackground(new Color(240,240,240));
            txt_identityNo.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            txt_accoundno.requestFocus();
            txt_accoundno.setBackground(new Color(240,240,240));
            txt_identityNo.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_txt_identityNoKeyPressed

    private void txt_accoundnoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_accoundnoKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            txt_bank_name.requestFocus();
            txt_bank_name.setBackground(new Color(240,240,240));
            txt_accoundno.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_identityNo.requestFocus();
            txt_identityNo.setBackground(new Color(240,240,240));
            txt_accoundno.setBackground(Color.WHITE);
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            txt_identityNo.requestFocus();
            txt_identityNo.setBackground(new Color(240,240,240));
            txt_accoundno.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            txt_bank_name.requestFocus();
            txt_bank_name.setBackground(new Color(240,240,240));
            txt_accoundno.setBackground(Color.WHITE);
           }
    }//GEN-LAST:event_txt_accoundnoKeyPressed

    private void txt_bank_nameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_bank_nameKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
           btn_dalete.requestFocus();
           txt_bank_name.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_accoundno.requestFocus();
            txt_accoundno.setBackground(new Color(240,240,240));
            txt_bank_name.setBackground(Color.WHITE);
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            txt_accoundno.requestFocus();
            txt_accoundno.setBackground(new Color(240,240,240));
            txt_bank_name.setBackground(Color.WHITE);
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_dalete.requestFocus();
            txt_bank_name.setBackground(Color.WHITE);
           }       
    }//GEN-LAST:event_txt_bank_nameKeyPressed

    private void btn_daleteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_daleteKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_update.requestFocus();
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_org.requestFocus();
            txt_org.setBackground(new Color(240,240,240));
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            txt_org.requestFocus();
            txt_org.setBackground(new Color(240,240,240));
           }        
//        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
//            txt_employeeName.requestFocus();
//           }
    }//GEN-LAST:event_btn_daleteKeyPressed

    private void btn_updateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_updateKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_cancel.requestFocus();
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
           btn_dalete.requestFocus();
           }
//        if(evt.getKeyCode() == KeyEvent.VK_UP) {
//            .requestFocus();
//           }        
//        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
//            txt_employeeName.requestFocus();
//           }
    }//GEN-LAST:event_btn_updateKeyPressed

    private void btn_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_cancelKeyPressed
       if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            txt_supplierName.requestFocus();
            txt_supplierName.setBackground(new Color(240,240,240));
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_update.requestFocus();
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            btn_update.requestFocus();
           }        
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            txt_supplierName.requestFocus();
            txt_supplierName.setBackground(new Color(240,240,240));
           }
    }//GEN-LAST:event_btn_cancelKeyPressed

    private void btn_daleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_daleteActionPerformed
        try {
            if(Config.supplier_details_mgr.delSupplierDetails(lbl_supplierid.getText())){
                        Config.supplier_management.onloadReset();
                        JOptionPane.showMessageDialog(this, "Supplier porfile delete successfully.", "Creation successful", JOptionPane.NO_OPTION);
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(this, "Error in profile delete.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
        } catch (Exception e) {
            
        }
    }//GEN-LAST:event_btn_daleteActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_dalete;
    private javax.swing.JButton btn_update;
    private javax.swing.JComboBox cb_gender;
    private javax.swing.JComboBox cb_identityType;
    private javax.swing.JComboBox cb_state;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private com.toedter.calendar.JDateChooser jdc_dob;
    private javax.swing.JLabel lbl_supplierid;
    private javax.swing.JPanel panal;
    private javax.swing.JTextField txt_AlternateContactNo;
    private javax.swing.JTextField txt_accoundno;
    private javax.swing.JTextField txt_address;
    private javax.swing.JTextField txt_bank_name;
    private javax.swing.JTextField txt_city;
    private javax.swing.JTextField txt_contactNo;
    private javax.swing.JTextField txt_email;
    private javax.swing.JTextField txt_identityNo;
    private javax.swing.JTextField txt_locality;
    private javax.swing.JTextField txt_org;
    private javax.swing.JTextField txt_pincode;
    private javax.swing.JTextField txt_supplierName;
    // End of variables declaration//GEN-END:variables
    
    public void onloadReset(int index) {        
        try {
            
        SupplierDetails sd = Config.config_supplier_details.get(index);
        lbl_supplierid.setText(sd.getSupplier_id());
        txt_supplierName.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(sd.getProfile_id())).getName());
        cb_gender.setSelectedItem(Config.config_profile_details.get(Config.id_profile_details.indexOf(sd.getProfile_id())).getGender());
        jdc_dob.setDate(sdf1.parse(Config.config_profile_details.get(Config.id_profile_details.indexOf(sd.getProfile_id())).getDob()));
        txt_contactNo.setText(Config.config_contact_details.get(Config.id_contact_details.indexOf(sd.getContact_id())).getContact_no());
        txt_AlternateContactNo.setText(Config.config_contact_details.get(Config.id_contact_details.indexOf(sd.getContact_id())).getAlt_contact_no());
        txt_address.setText(Config.config_address_details.get(Config.id_address_details.indexOf(sd.getAddress_id())).getAddress_1());
        txt_locality.setText(Config.config_address_details.get(Config.id_address_details.indexOf(sd.getAddress_id())).getAddress_2());
        txt_city.setText(Config.config_address_details.get(Config.id_address_details.indexOf(sd.getAddress_id())).getCity());
        txt_pincode.setText(Config.config_address_details.get(Config.id_address_details.indexOf(sd.getAddress_id())).getPincode());
        cb_state.setSelectedItem(Config.config_address_details.get(Config.id_address_details.indexOf(sd.getAddress_id())).getState());
        txt_email.setText(Config.config_contact_details.get(Config.id_contact_details.indexOf(sd.getContact_id())).getEmail());
        txt_org.setText(Config.config_contact_details.get(Config.id_contact_details.indexOf(sd.getContact_id())).getOther());
        cb_identityType.setSelectedItem(Config.config_identity_details.get(Config.id_identity_details.indexOf(sd.getIdentity_id())).getIdentity_type());
        txt_identityNo.setText(Config.config_identity_details.get(Config.id_identity_details.indexOf(sd.getIdentity_id())).getIdentity_no());
        txt_accoundno.setText(sd.getAccount_no());
        txt_bank_name.setText(sd.getBank_name());
            
        } catch (Exception e) {
        }    
    }
    
    private String checkValidity() {
        if (txt_supplierName.getText().equals("")) {
            return "Supplier Name";
        }
        else if (cb_gender.getSelectedIndex()==0) {
            return "Gender";
        }
        else if (jdc_dob.getDate().equals("")) {
            return "DOB";
        }
       
        else if (txt_contactNo.getText().equals("") || txt_contactNo.getText().length() != 10) {
            return "Contact No.";
        }
        else if (txt_address.getText().equals("")) {
            return "Address";
        }
        else if (txt_locality.getText().equals("")) {
            return "Locality";
        }
        else if (txt_city.getText().equals("")) {
            return "City";
        }
        else if (cb_state.getSelectedIndex()==0) {
            return "State";
        }
        else if (cb_identityType.getSelectedIndex()==0) {
            return "IdentityType";
        }
        else if (txt_identityNo.getText().equals("")) {
            return "IdentityNo";
        }
        else if (txt_address.getText().equals("")) {
            return "Address";
        }
        else if (txt_bank_name.getText().equals("")) {
            return "Bank Name";
        }
        else {
            return "ok";
        }
    }
    
    private boolean checkAvailibility() {
    int i =0;        
        for ( i = 0; i < Config.supplier_management.tbl_supplier_model.getRowCount(); i++) {
            if(txt_supplierName.getText().trim().toUpperCase().equals(Config.supplier_management.tbl_supplier_model.getValueAt(i, 3).toString())&& lbl_supplierid.getText().equals(Config.supplier_management.tbl_supplier_model.getValueAt(i, 0).toString()) ){
                break;
            }
        }
        if(i == Config.supplier_management.tbl_supplier_model.getRowCount()){
         return true;    
        }else{
         return false; 
        }    
    }
}
