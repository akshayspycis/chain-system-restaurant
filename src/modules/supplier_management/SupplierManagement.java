package modules.supplier_management;

import data_manager.SupplierDetails;
import data_manager.configuration.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

public class SupplierManagement extends javax.swing.JDialog {
     public DefaultTableModel tbl_supplier_model = null;

    
    public SupplierManagement(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        tbl_supplier_model = (DefaultTableModel) tbl_supplier.getModel();
       
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        pnl_employee = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txt_supplier_search = new javax.swing.JTextField();
        btn_supplier_search = new javax.swing.JButton();
        btn_supplier_new = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        txt_supplier_suppliername = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        txt_supplier_address = new javax.swing.JTextField();
        txt_supplier_city = new javax.swing.JTextField();
        txt_supplier_pincode = new javax.swing.JTextField();
        txt_supplier_state = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txt_supplier_payment = new javax.swing.JTable();
        txt_supplier_dob = new javax.swing.JTextField();
        jPanel14 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txt_supplier_message = new javax.swing.JTextPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_supplier = new javax.swing.JTable();
        btn_supplier_cancel = new javax.swing.JButton();
        btn_supplier_print = new javax.swing.JButton();
        txt_supplier_gender = new javax.swing.JTextField();
        btn_supplier_delete = new javax.swing.JButton();
        btn_supplier_view = new javax.swing.JButton();
        jPanel22 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane2.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        pnl_employee.setBackground(new java.awt.Color(255, 255, 255));
        pnl_employee.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pnl_employeeKeyPressed(evt);
            }
        });

        jLabel5.setText("Supplier Name");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Search By :");

        txt_supplier_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_supplier_searchCaretUpdate(evt);
            }
        });
        txt_supplier_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_supplier_searchKeyPressed(evt);
            }
        });

        btn_supplier_search.setText("Search");
        btn_supplier_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_supplier_searchActionPerformed(evt);
            }
        });
        btn_supplier_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_supplier_searchKeyPressed(evt);
            }
        });

        btn_supplier_new.setText("New Supplier");
        btn_supplier_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_supplier_newActionPerformed(evt);
            }
        });
        btn_supplier_new.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_supplier_newKeyPressed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_supplier_search, javax.swing.GroupLayout.DEFAULT_SIZE, 478, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_supplier_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_supplier_new)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_supplier_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_supplier_search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_supplier_new))
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator3))
                .addContainerGap())
        );

        txt_supplier_suppliername.setEditable(false);
        txt_supplier_suppliername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_supplier_suppliernameActionPerformed(evt);
            }
        });

        jLabel12.setText("Gender");

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Address", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_supplier_address.setEditable(false);
        txt_supplier_address.setBackground(new java.awt.Color(255, 255, 255));
        txt_supplier_address.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_supplier_address.setBorder(null);

        txt_supplier_city.setEditable(false);
        txt_supplier_city.setBackground(new java.awt.Color(255, 255, 255));
        txt_supplier_city.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_supplier_city.setBorder(null);
        txt_supplier_city.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_supplier_cityActionPerformed(evt);
            }
        });

        txt_supplier_pincode.setEditable(false);
        txt_supplier_pincode.setBackground(new java.awt.Color(255, 255, 255));
        txt_supplier_pincode.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_supplier_pincode.setBorder(null);
        txt_supplier_pincode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_supplier_pincodeActionPerformed(evt);
            }
        });

        txt_supplier_state.setEditable(false);
        txt_supplier_state.setBackground(new java.awt.Color(255, 255, 255));
        txt_supplier_state.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_supplier_state.setBorder(null);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_supplier_pincode)
                    .addComponent(txt_supplier_state)
                    .addComponent(txt_supplier_city)
                    .addComponent(txt_supplier_address))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(txt_supplier_address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_supplier_city, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_supplier_pincode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_supplier_state, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel13.setText("Date of Birth");

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Payment Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jScrollPane6.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane6.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        txt_supplier_payment.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txt_supplier_payment.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "ADVANCE AMT", "DUE AMT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        txt_supplier_payment.setAutoscrolls(false);
        txt_supplier_payment.setRowHeight(25);
        txt_supplier_payment.getTableHeader().setReorderingAllowed(false);
        jScrollPane6.setViewportView(txt_supplier_payment);
        if (txt_supplier_payment.getColumnModel().getColumnCount() > 0) {
            txt_supplier_payment.getColumnModel().getColumn(0).setResizable(false);
            txt_supplier_payment.getColumnModel().getColumn(1).setResizable(false);
        }

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        txt_supplier_dob.setEditable(false);

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_supplier_message.setEditable(false);
        jScrollPane5.setViewportView(txt_supplier_message);

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tbl_supplier.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "SUPPLIER NAME", "CONTACT NO.", "ORGANIZATION", "EMIAL", "IDENTITY TYPE", "IDENTITY NO.", "ACCOUNT NO.", "BANK NAME"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_supplier.getTableHeader().setReorderingAllowed(false);
        tbl_supplier.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tbl_supplierFocusGained(evt);
            }
        });
        tbl_supplier.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_supplierMouseClicked(evt);
            }
        });
        tbl_supplier.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_supplierKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbl_supplierKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_supplier);
        if (tbl_supplier.getColumnModel().getColumnCount() > 0) {
            tbl_supplier.getColumnModel().getColumn(0).setResizable(false);
            tbl_supplier.getColumnModel().getColumn(0).setPreferredWidth(50);
            tbl_supplier.getColumnModel().getColumn(1).setResizable(false);
            tbl_supplier.getColumnModel().getColumn(1).setPreferredWidth(150);
            tbl_supplier.getColumnModel().getColumn(2).setResizable(false);
            tbl_supplier.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_supplier.getColumnModel().getColumn(3).setResizable(false);
            tbl_supplier.getColumnModel().getColumn(3).setPreferredWidth(110);
            tbl_supplier.getColumnModel().getColumn(4).setResizable(false);
            tbl_supplier.getColumnModel().getColumn(4).setPreferredWidth(200);
            tbl_supplier.getColumnModel().getColumn(5).setResizable(false);
            tbl_supplier.getColumnModel().getColumn(5).setPreferredWidth(100);
            tbl_supplier.getColumnModel().getColumn(6).setResizable(false);
            tbl_supplier.getColumnModel().getColumn(6).setPreferredWidth(100);
            tbl_supplier.getColumnModel().getColumn(7).setResizable(false);
            tbl_supplier.getColumnModel().getColumn(7).setPreferredWidth(100);
            tbl_supplier.getColumnModel().getColumn(8).setResizable(false);
            tbl_supplier.getColumnModel().getColumn(8).setPreferredWidth(100);
        }

        btn_supplier_cancel.setText("Cancel");
        btn_supplier_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_supplier_cancelActionPerformed(evt);
            }
        });
        btn_supplier_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_supplier_cancelKeyPressed(evt);
            }
        });

        btn_supplier_print.setText("Print");
        btn_supplier_print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_supplier_printKeyPressed(evt);
            }
        });

        txt_supplier_gender.setEditable(false);

        btn_supplier_delete.setText("Delete");
        btn_supplier_delete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_supplier_deleteKeyPressed(evt);
            }
        });

        btn_supplier_view.setText("View");
        btn_supplier_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_supplier_viewActionPerformed(evt);
            }
        });
        btn_supplier_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_supplier_viewKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout pnl_employeeLayout = new javax.swing.GroupLayout(pnl_employee);
        pnl_employee.setLayout(pnl_employeeLayout);
        pnl_employeeLayout.setHorizontalGroup(
            pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_employeeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnl_employeeLayout.createSequentialGroup()
                                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(txt_supplier_suppliername, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(txt_supplier_gender, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13)
                                    .addComponent(txt_supplier_dob, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_employeeLayout.createSequentialGroup()
                        .addComponent(btn_supplier_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_supplier_delete, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_supplier_view)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_supplier_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pnl_employeeLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_supplier_cancel, btn_supplier_delete, btn_supplier_print, btn_supplier_view});

        pnl_employeeLayout.setVerticalGroup(
            pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_employeeLayout.createSequentialGroup()
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_supplier_suppliername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_supplier_dob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_supplier_gender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_supplier_cancel)
                    .addComponent(btn_supplier_print)
                    .addComponent(btn_supplier_delete)
                    .addComponent(btn_supplier_view))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Supplier Information", pnl_employee);

        jPanel22.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        jTabbedPane2.addTab("Reports", jPanel22);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );

        jPanel2.setBackground(new java.awt.Color(230, 79, 6));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Supplier Management");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void pnl_employeeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pnl_employeeKeyPressed
        System.out.println("sajjjj");
    }//GEN-LAST:event_pnl_employeeKeyPressed

    private void btn_supplier_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_supplier_printKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_supplier_delete.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            tbl_supplier.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_supplier.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_supplier_delete.requestFocus();
        }
    }//GEN-LAST:event_btn_supplier_printKeyPressed

    private void btn_supplier_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_supplier_cancelKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_supplier_view.requestFocus();
        }
//      if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
  //            .requestFocus()
//      }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
//            tbl_supplier.requestFocus();
//        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            tbl_employee.requestFocus();
            //           }
    }//GEN-LAST:event_btn_supplier_cancelKeyPressed

    private void tbl_supplierKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_supplierKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_supplier_print.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_supplier_new.requestFocus();
        }
//        if(evt.getKeyCode() == KeyEvent.VK_UP) {
//           if(tbl_supplier_model.getRowCount()>-1){
//               setData();
//           }
//        }
//        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
//           if(tbl_supplier_model.getRowCount()>-1){
//               setData();
//           }
//        }    
            
        
    }//GEN-LAST:event_tbl_supplierKeyPressed

    private void txt_supplier_pincodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_supplier_pincodeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_supplier_pincodeActionPerformed

    private void txt_supplier_cityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_supplier_cityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_supplier_cityActionPerformed

    private void txt_supplier_suppliernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_supplier_suppliernameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_supplier_suppliernameActionPerformed

    private void btn_supplier_newKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_supplier_newKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            tbl_supplier.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_supplier_search.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_supplier.requestFocus();
        }
    }//GEN-LAST:event_btn_supplier_newKeyPressed

    private void btn_supplier_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_supplier_newActionPerformed
        Config.new_supplier.onloadReset();
        Config.new_supplier.setVisible(true);
    }//GEN-LAST:event_btn_supplier_newActionPerformed

    private void btn_supplier_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_supplier_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_supplier_new.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_supplier_search.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_supplier.requestFocus();
        }
    }//GEN-LAST:event_btn_supplier_searchKeyPressed

    private void txt_supplier_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_supplier_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_supplier_search.requestFocus();
        }
//        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
//            pnl_adv_report.requestFocus();
//        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_supplier.requestFocus();
        }

    }//GEN-LAST:event_txt_supplier_searchKeyPressed

    private void btn_supplier_deleteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_supplier_deleteKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_supplier_view.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_supplier_print.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_supplier_view.requestFocus();
        }
    }//GEN-LAST:event_btn_supplier_deleteKeyPressed

    private void btn_supplier_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_supplier_viewKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_supplier_cancel.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_supplier_delete.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_supplier_cancel.requestFocus();
        }
    }//GEN-LAST:event_btn_supplier_viewKeyPressed

    private void btn_supplier_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_supplier_viewActionPerformed
    
        try {
            Config.view_supplier.onloadReset(tbl_supplier.getSelectedRow());  
        Config.view_supplier.setVisible(true);  
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,"Select any row", "Error", JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_btn_supplier_viewActionPerformed

    private void btn_supplier_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_supplier_cancelActionPerformed
         dispose();
    }//GEN-LAST:event_btn_supplier_cancelActionPerformed

    private void btn_supplier_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_supplier_searchActionPerformed
    try {
        tbl_supplier_model.setRowCount(0);
        if(!txt_supplier_search.getText().equals("")){
           
            for (int i = 0; i < Config.config_supplier_details.size(); i++) {
              SupplierDetails sd = Config.config_supplier_details.get(i);
              if(Config.config_profile_details.get(Config.id_profile_details.indexOf(sd.getProfile_id())).getName().toUpperCase().startsWith(txt_supplier_search.getText().trim().toUpperCase())  ){
            tbl_supplier_model.addRow(new Object[]{
            sd.getSupplier_id(),    
            Config.config_profile_details.get(Config.id_profile_details.indexOf(sd.getProfile_id())).getName(),
            Config.config_contact_details.get(Config.id_contact_details.indexOf(sd.getContact_id())).getContact_no(),    
            Config.config_contact_details.get(Config.id_contact_details.indexOf(sd.getContact_id())).getOther(),
            Config.config_contact_details.get(Config.id_contact_details.indexOf(sd.getContact_id())).getEmail(),
            Config.config_identity_details.get(Config.id_identity_details.indexOf(sd.getIdentity_id())).getIdentity_type(),
            Config.config_identity_details.get(Config.id_identity_details.indexOf(sd.getIdentity_id())).getIdentity_no(),
            Config.config_supplier_details.get(i).getAccount_no(),
            Config.config_supplier_details.get(i).getBank_name()
            });
            }
            }
            
        }else{
            for (int i = 0; i < Config.config_supplier_details.size(); i++) {
            SupplierDetails sd = Config.config_supplier_details.get(i);
            tbl_supplier_model.addRow(new Object[]{
            sd.getSupplier_id(),    
            Config.config_profile_details.get(Config.id_profile_details.indexOf(sd.getProfile_id())).getName(),
            Config.config_contact_details.get(Config.id_contact_details.indexOf(sd.getContact_id())).getContact_no(),    
            Config.config_contact_details.get(Config.id_contact_details.indexOf(sd.getContact_id())).getOther(),
            Config.config_contact_details.get(Config.id_contact_details.indexOf(sd.getContact_id())).getEmail(),
            Config.config_identity_details.get(Config.id_identity_details.indexOf(sd.getIdentity_id())).getIdentity_type(),
            Config.config_identity_details.get(Config.id_identity_details.indexOf(sd.getIdentity_id())).getIdentity_no(),
            Config.config_supplier_details.get(i).getAccount_no(),
            Config.config_supplier_details.get(i).getBank_name()
            });
            }
        }
    } catch (Exception e) {
      e.printStackTrace();
    }
    }//GEN-LAST:event_btn_supplier_searchActionPerformed

    private void txt_supplier_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_supplier_searchCaretUpdate
        btn_supplier_searchActionPerformed(null);
    }//GEN-LAST:event_txt_supplier_searchCaretUpdate

    private void tbl_supplierFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tbl_supplierFocusGained
      setData();
    }//GEN-LAST:event_tbl_supplierFocusGained

    private void tbl_supplierMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_supplierMouseClicked
        try {
            if(evt.getClickCount()==1){
            setData();
        }
        } catch (Exception e) {
            setData(); 
        }
        if(evt.getClickCount()==2){
            btn_supplier_viewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_supplierMouseClicked

    private void tbl_supplierKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_supplierKeyReleased
        setData();
    }//GEN-LAST:event_tbl_supplierKeyReleased
        
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_supplier_cancel;
    private javax.swing.JButton btn_supplier_delete;
    private javax.swing.JButton btn_supplier_new;
    private javax.swing.JButton btn_supplier_print;
    private javax.swing.JButton btn_supplier_search;
    private javax.swing.JButton btn_supplier_view;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JPanel pnl_employee;
    private javax.swing.JTable tbl_supplier;
    private javax.swing.JTextField txt_supplier_address;
    private javax.swing.JTextField txt_supplier_city;
    private javax.swing.JTextField txt_supplier_dob;
    private javax.swing.JTextField txt_supplier_gender;
    private javax.swing.JTextPane txt_supplier_message;
    private javax.swing.JTable txt_supplier_payment;
    private javax.swing.JTextField txt_supplier_pincode;
    private javax.swing.JTextField txt_supplier_search;
    private javax.swing.JTextField txt_supplier_state;
    private javax.swing.JTextField txt_supplier_suppliername;
    // End of variables declaration//GEN-END:variables
  
    public void onloadReset(){
        onloadSupplier();
    }
    public void onloadSupplier(){
      btn_supplier_searchActionPerformed(null);  
      }
    
    public void setData(){
       
    SupplierDetails sd = Config.config_supplier_details.get(Config.id_supplier_details.indexOf(tbl_supplier_model.getValueAt(tbl_supplier.getSelectedRow(), 0).toString()));
        txt_supplier_suppliername.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(sd.getProfile_id())).getName());
        txt_supplier_gender.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(sd.getProfile_id())).getGender());
        txt_supplier_dob.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(sd.getProfile_id())).getDob());
        txt_supplier_address.setText(Config.config_address_details.get(Config.id_address_details.indexOf(sd.getAddress_id())).getAddress_1());
        txt_supplier_city.setText(Config.config_address_details.get(Config.id_address_details.indexOf(sd.getAddress_id())).getCity());
        txt_supplier_pincode.setText(Config.config_address_details.get(Config.id_address_details.indexOf(sd.getAddress_id())).getPincode());
        txt_supplier_state.setText(Config.config_address_details.get(Config.id_address_details.indexOf(sd.getAddress_id())).getState());
       
        
        }
    }
