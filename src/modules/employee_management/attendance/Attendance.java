package modules.employee_management.attendance;

import data_manager.AttendenceDetails;
import data_manager.configuration.AttendanceObj;
import data_manager.configuration.Config;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Group;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class Attendance extends javax.swing.JDialog implements ActionListener{
int index = 0;
    ArrayList<AttendanceObj> attendance_list = null;
    SimpleDateFormat simple_date_formate = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
    public Attendance(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        btn_reset = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        jdc_date = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jCheckBox1 = new javax.swing.JCheckBox();
        txt_search = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();

        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 636, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 303, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(jPanel2);

        btn_reset.setText("Reset");
        btn_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_resetActionPerformed(evt);
            }
        });

        jButton2.setText("Cancel");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Save");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jPanel6.setBackground(new java.awt.Color(230, 79, 6));

        jLabel21.setBackground(new java.awt.Color(255, 255, 255));
        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setText("Attendance");

        jdc_date.setDateFormatString("dd/MM/yyyy");
        jdc_date.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jdc_dateMouseClicked(evt);
            }
        });
        jdc_date.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdc_datePropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("S.No.");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Employee Name");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Time In");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Time Out");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Status");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Message");

        jCheckBox1.setText("Select All");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        txt_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_searchCaretUpdate(evt);
            }
        });

        jLabel7.setText("Search :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_reset)
                        .addGap(32, 32, 32)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(txt_search, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jCheckBox1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(73, 73, 73)
                        .addComponent(jLabel3)
                        .addGap(85, 85, 85)
                        .addComponent(jLabel4)
                        .addGap(56, 56, 56)
                        .addComponent(jLabel5)
                        .addGap(26, 26, 26)
                        .addComponent(jLabel6)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_reset)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jCheckBox1)
                    .addComponent(txt_search, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(13, 13, 13))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void btn_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_resetActionPerformed
        reset();
    }//GEN-LAST:event_btn_resetActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try {
        String str = checkValidity();
        if (str.equals("ok")) {        
            if (checkAvailibility()) {
                ArrayList<AttendenceDetails> ad = new ArrayList<>();
        for (int i = 0; i < attendance_list.size(); i++) {
            AttendenceDetails a = new AttendenceDetails();
            a.setEmployee_id(attendance_list.get(i).getSno().getText());
            a.setDate(simple_date_formate.format(jdc_date.getDate()));
            try {
                a.setMonth(sdf_month.format(simple_date_formate.parse(simple_date_formate.format(jdc_date.getDate()))));
            } catch (Exception e) {
            }
            try {
                a.setYear(sdf_year.format(simple_date_formate.parse(simple_date_formate.format(jdc_date.getDate()))));
            } catch (Exception e) {
            }
            a.setStatus(String.valueOf(attendance_list.get(i).getStatus().isSelected()));
            a.setTime_in(attendance_list.get(i).getTime_in().getText());
            a.setTime_out(attendance_list.get(i).getTime_out().getText());
            a.setMessage_id(attendance_list.get(i).getMessage_id().getText());
            ad.add(a);
        }
            if (Config.attendence_details_mgr.insAttendenceDetails(ad)) {
            Config.employee_management.setAttendence(simple_date_formate.format(jdc_date.getDate()));
            JOptionPane.showMessageDialog(this, "Attendence inserted successfully.", "Creation successful", JOptionPane.NO_OPTION);               
            reset();
            } else {
            JOptionPane.showMessageDialog(this, "Error in attendence insertion.", "Error", JOptionPane.ERROR_MESSAGE);
            }
            } else {
            JOptionPane.showMessageDialog(this, "Attendance Already fill", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Check '"+str+"' field.", "Error", JOptionPane.ERROR_MESSAGE);
        }          
        } catch (Exception e) {
            e.printStackTrace();
        }   
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jdc_dateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jdc_dateMouseClicked

    }//GEN-LAST:event_jdc_dateMouseClicked

    private void jdc_datePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_datePropertyChange
        
    }//GEN-LAST:event_jdc_datePropertyChange

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        for (int i = 0; i < attendance_list.size(); i++) {
            attendance_list.get(i).getStatus().setSelected(true);
        }
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void txt_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_searchCaretUpdate
        scroll();
    }//GEN-LAST:event_txt_searchCaretUpdate
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_reset;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private com.toedter.calendar.JDateChooser jdc_date;
    private javax.swing.JTextField txt_search;
    // End of variables declaration//GEN-END:variables

public void onloadReset(){
    jdc_date.setDate(null);
    jCheckBox1.setSelected(false);
    jPanel2.removeAll();
    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel1Layout);

    Group groupLabels = jPanel1Layout.createParallelGroup();
    Group groupFields = jPanel1Layout.createParallelGroup();
    Group groupCheck =  jPanel1Layout.createParallelGroup();
    Group groupTimeIn = jPanel1Layout.createParallelGroup();
    Group groupTimeOut= jPanel1Layout.createParallelGroup();
    Group groupButton= jPanel1Layout.createParallelGroup();
    Group groupMessage_id = jPanel1Layout.createParallelGroup();
    Group groupRows = jPanel1Layout.createSequentialGroup();
    jPanel1Layout.setHorizontalGroup(jPanel1Layout.createSequentialGroup()
        .addGap(20)
        .addGroup(groupLabels)
        .addGap(20)
        .addGroup(groupFields)
        .addGap(20)
        .addGroup(groupTimeIn)
        .addGap(20)
        .addGroup(groupTimeOut)
        .addGap(20)
        .addGroup(groupCheck)
        .addGap(20)
        .addGroup(groupButton)
        .addGap(20)
        .addGroup(groupMessage_id)
        .addGap(20)
    );
    jPanel1Layout.setVerticalGroup(groupRows);

        for (int i = 0; i < Config.id_profile_details.size(); i++) {
            System.out.println(Config.id_profile_details.get(i));
        }

    attendance_list = new ArrayList<AttendanceObj>();
    for (int i = 0; i < Config.config_employee_details.size(); i++) {
    AttendanceObj a = new AttendanceObj();
        a.setSno(new JLabel(Config.config_employee_details.get(i).getEmployee_id()));
        a.setName(new JTextField(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(i).getProfile_id())).getName()));
        a.getName().setBorder(null);
        a.setTime_in(new JFormattedTextField());
        a.setTime_out(new JFormattedTextField());
        a.setStatus(new JCheckBox());
        a.setMessage(new JButton("Message"));
        a.setMessage_id(new JLabel(""));
        a.getMessage_id().setVisible(false);
        attendance_list.add(a);
        try {
                a.getTime_in().setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##:##")));
                a.getTime_out().setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##:##")));
        } catch (java.text.ParseException ex) {
                ex.printStackTrace();
        }
        groupLabels.addComponent(a.getSno());
        groupFields.addComponent(a.getName());
        groupTimeIn.addComponent(a.getTime_in());
        groupTimeOut.addComponent(a.getTime_out());
        groupCheck.addComponent(a.getStatus());
        groupButton.addComponent(a.getMessage());
        groupMessage_id.addComponent(a.getMessage_id());

        groupRows.addGroup(jPanel1Layout.createParallelGroup()
        .addGap(40)
        .addComponent(a.getSno())
        .addComponent(a.getName(),GroupLayout.DEFAULT_SIZE, 15,GroupLayout.PREFERRED_SIZE)
        .addComponent(a.getTime_in(),GroupLayout.DEFAULT_SIZE, 5, GroupLayout.PREFERRED_SIZE)
        .addComponent(a.getTime_out(),GroupLayout.DEFAULT_SIZE, 5, GroupLayout.PREFERRED_SIZE)
        .addComponent(a.getStatus())         
        .addComponent(a.getMessage())         
        .addComponent(a.getMessage_id())         
        );
        a.getMessage().addActionListener(this);
    }

}
    public void actionPerformed(java.awt.event.ActionEvent evt) {
       final JButton source = (JButton)evt.getSource();
        for (int i = 0; i < attendance_list.size(); i++) {
            if(source.equals(attendance_list.get(i).getMessage())){
                    if(attendance_list.get(i).getMessage_id().getText().equals("")){
                       Config.new_message.onloadResetAttendance(14,i);
                       Config.new_message.setVisible(true);
                    }else{
                       Config.view_message.onloadResetAttendance(14,attendance_list.get(i).getMessage_id().getText(),i);
                       Config.view_message.setVisible(true);
                    }
                break;
            }
        }
    }
    private String checkValidity() {
    if (jdc_date.getDate() == null) {
            return "Date";
        }
        else {
            return "ok";
        }     
    }
    private boolean checkAvailibility() {
        try {
            Config.sql ="SELECT *  FROM  attendence_details a where _date='"+simple_date_formate.format(jdc_date.getDate())+"'";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if( Config.rs.next())
            {
            return false;    
            }else{
            return true;        
            }       
        } catch (Exception e) {
            return false;
        }
    }
    public void setMessage(String message_id, int index) {
        attendance_list.get(index).getMessage_id().setText(message_id);
        attendance_list.get(index).getMessage().setText("View Msg");
    }
    public void reset(){
                jPanel2.removeAll();
                onloadReset();
                jCheckBox1.setSelected(false);
                jdc_date.setDate(null);
    }
    
    public void scroll() {
        Rectangle visible = jPanel2.getVisibleRect();
        Rectangle bounds = jPanel2.getBounds();
        Dimension d =null;
        for (int i = 0; i < Config.config_employee_details.size(); i++) {
            if(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(i).getProfile_id())).getName().toUpperCase().startsWith(txt_search.getText().toUpperCase())){
                jScrollPane1.getViewport().setViewPosition(attendance_list.get(i).getName().getLocation());
                break;
            }
        }
    }
}
