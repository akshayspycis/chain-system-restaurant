package modules.employee_management.salary_information;

import data_manager.EmployeeDetails;
import data_manager.SalaryDistribution;
import data_manager.configuration.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;


public class AddSalaryInfo extends javax.swing.JDialog {
String employee_id = "";
String message_id = "";
String salary_id = "";
SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");    

SimpleDateFormat sdf_month = new SimpleDateFormat("M");
SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
    public AddSalaryInfo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_amount = new javax.swing.JTextField();
        txt_name = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_salary = new javax.swing.JTextField();
        jdc_date = new com.toedter.calendar.JDateChooser();
        lbl_no_of_days = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btn_save = new javax.swing.JButton();
        btn_msg = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Salary", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Employee Name");

        jLabel2.setText("Date ");

        jLabel3.setText("Amount");

        txt_amount.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_amountCaretUpdate(evt);
            }
        });
        txt_amount.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_amountFocusLost(evt);
            }
        });
        txt_amount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_amountKeyPressed(evt);
            }
        });

        txt_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nameActionPerformed(evt);
            }
        });

        jLabel4.setText("Salary");

        jdc_date.setDateFormatString("dd/MM/yyyy");
        jdc_date.setMaxSelectableDate(new java.util.Date(253370748696000L));
        jdc_date.setMinSelectableDate(new java.util.Date(-62135785704000L));
        jdc_date.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdc_datePropertyChange(evt);
            }
        });

        lbl_no_of_days.setText("0");

        jLabel5.setText("Days");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lbl_no_of_days))
                            .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_salary)
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addGap(45, 45, 45))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_salary, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(lbl_no_of_days)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        btn_save.setText("Save");
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });
        btn_save.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_saveKeyPressed(evt);
            }
        });

        btn_msg.setText("Message");
        btn_msg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_msgActionPerformed(evt);
            }
        });
        btn_msg.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_msgKeyPressed(evt);
            }
        });

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });

        jPanel13.setBackground(new java.awt.Color(230, 79, 6));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Add Salary");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_msg)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_save, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancel)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_cancel, btn_save});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_cancel)
                    .addComponent(btn_save)
                    .addComponent(btn_msg))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void txt_amountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_amountKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            btn_msg.requestFocus();                    
        }
         if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            btn_msg.requestFocus();                    
        }
         if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jdc_date.requestFocus();
        }
         if (evt.getKeyCode() == KeyEvent.VK_UP) {
           txt_name.requestFocus();
        }
    }//GEN-LAST:event_txt_amountKeyPressed

    private void btn_msgKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_msgKeyPressed
       if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            btn_save
                    .requestFocus();                    
        }
         if(evt.getKeyCode() == KeyEvent.VK_DOWN){
           txt_name.requestFocus();                    
        }
         if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_amount.requestFocus();
        }
         if (evt.getKeyCode() == KeyEvent.VK_UP) {
           txt_amount.requestFocus();
        }
    }//GEN-LAST:event_btn_msgKeyPressed

    private void btn_saveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_saveKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            txt_name.requestFocus();                    
        }
         if(evt.getKeyCode() == KeyEvent.VK_DOWN){
           txt_name.requestFocus();                    
        }
         if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_msg.requestFocus();
        }
         if (evt.getKeyCode() == KeyEvent.VK_UP) {
           txt_amount.requestFocus();
        }
    }//GEN-LAST:event_btn_saveKeyPressed

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        try {
            String str = checkValidity();
        if(str.equals("ok")){
            if(checkAvailability()){
               SalaryDistribution sd = new SalaryDistribution();
               sd.setEmployee_id(employee_id);
               sd.setDate(sdf.format(jdc_date.getDate()));
               sd.setMonth(sdf_month.format(sdf.parse(sdf.format(jdc_date.getDate()))));
               sd.setYear(sdf_year.format(sdf.parse(sdf.format(jdc_date.getDate()))));
               sd.setAmount(txt_amount.getText());
               sd.setMessage_id(message_id);
               sd.setSalary_id(salary_id);
               if(Config.salary_distribution_mgr.insSalaryDistribution(sd)){
               Config.employee_management.setSalaryData(txt_name.getText(), sd.getDate());
               JOptionPane.showMessageDialog(this, "Salary Details Insertion successfully", "Success", JOptionPane.NO_OPTION);
               onloadReset();
               }else{
               JOptionPane.showMessageDialog(this, "Error in insertion ", "Error", JOptionPane.ERROR_MESSAGE);   
               }
            }else{
            JOptionPane.showMessageDialog(this, "Check salary details", "Error", JOptionPane.ERROR_MESSAGE);
        }
        }else{
            JOptionPane.showMessageDialog(this, str +"should not be blank", "Error", JOptionPane.ERROR_MESSAGE);
        }
        } catch (Exception e) {
            
        }
    }//GEN-LAST:event_btn_saveActionPerformed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void txt_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nameActionPerformed
        if(txt_name.getText().equals("")){
           Config.employee_search.onloadReset(4);
           Config.employee_search.setVisible(true);
        }
    }//GEN-LAST:event_txt_nameActionPerformed

    private void btn_msgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_msgActionPerformed
        if(message_id.equals("")){
           Config.new_message.onloadReset(10);
           Config.new_message.setVisible(true);
        }else{
           Config.view_message.onloadReset(10,message_id);
           Config.view_message.setVisible(true);
        }
    }//GEN-LAST:event_btn_msgActionPerformed

    private void txt_amountCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_amountCaretUpdate
    }//GEN-LAST:event_txt_amountCaretUpdate

    private void txt_amountFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_amountFocusLost
        if(!txt_amount.getText().trim().equals("")){
        try {
        Double.parseDouble(txt_amount.getText().trim());
        }catch (Exception e) {
        JOptionPane.showMessageDialog(this, "Only No.Value Required ", "Error", JOptionPane.ERROR_MESSAGE);   
        txt_amount.setText("");
        txt_amount.requestFocus();
        }
        }
    }//GEN-LAST:event_txt_amountFocusLost

    private void jdc_datePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_datePropertyChange
        setSalary();
    }//GEN-LAST:event_jdc_datePropertyChange
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_msg;
    private javax.swing.JButton btn_save;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private com.toedter.calendar.JDateChooser jdc_date;
    private javax.swing.JLabel lbl_no_of_days;
    private javax.swing.JTextField txt_amount;
    private javax.swing.JTextField txt_name;
    private javax.swing.JTextField txt_salary;
    // End of variables declaration//GEN-END:variables

    public void onloadReset() {        
        message_id=""; 
        txt_name.setText("");
        jdc_date.setDate(null);             
        txt_amount.setText("");
        txt_salary.setText("");
    }
     
    public void setData(int selectedRow) {
       for (int i = 0; i < Config.config_employee_details.size(); i++) {
       EmployeeDetails ed = Config.config_employee_details.get(i);
       txt_name.setText(Config.config_employee_details.get(selectedRow).getEmployee_id());
       }
    }
     
    private String checkValidity() {
       
        if (txt_name.getText().equals("")) {
            return "Employee Name";
        }
        else if (jdc_date.getDate().equals("")) {
            return "Date";
        }
        else if (Double.parseDouble(txt_amount.getText())<=0.0 ) {
            return "Amount";
        }
        else {
            return "ok";
        }
    }
    
    public void setEmployeeName(String employee_id) {
       this.employee_id=employee_id;    
       txt_name.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(employee_id)).getProfile_id())).getName());
       int i=0;
        for (i = 0; i < Config.config_salary_details.size(); i++) {
            if(Config.config_salary_details.get(i).getEmployee_id().equals(employee_id)){
            txt_salary.setText(Config.config_salary_details.get(i).getSalary());
            salary_id=Config.config_salary_details.get(i).getSalary_id();
            setSalary();
            break;
        }
        }
        if(i==Config.config_salary_details.size()){
            JOptionPane.showMessageDialog(this, "Salary does not configure", "Error", JOptionPane.ERROR_MESSAGE);   
        }
    }
    
    public void setMessage(String message_id){
         this.message_id=message_id;
    }

    private boolean checkAvailability() {
        if (!txt_salary.getText().trim().equals("")) {
            try {
            Double amount=0.0;
            Config.sql ="SELECT IFNULL(sum(amount),0) FROM salary_distribution a where employee_id='"+employee_id+"' and _month='"+sdf_month.format(sdf.parse(sdf.format(jdc_date.getDate())))+"' and _year='"+sdf_year.format(sdf.parse(sdf.format(jdc_date.getDate())))+"' ";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if( Double.parseDouble(txt_amount.getText().trim())<=Double.parseDouble(txt_amount.getText()))
            {
                if(Config.rs.next()){
                    try {
                        amount=Double.parseDouble(Config.rs.getString(1))+Double.parseDouble(txt_amount.getText());
                        if(amount>Double.parseDouble(txt_salary.getText())){
                        return setRemaingAmount(Config.rs.getString(1));
                        }else{
                        return true;
                        }
                    } catch (Exception e) {
                        return false;
                    }
                    }else{
                    return true;
                    }
            }else{
             if(Config.rs.next()){
                 return setRemaingAmount(Config.rs.getString(1));
             }else{
                 return true;
             }   
            }
         }catch (Exception e) {
           
            return false;
         }
        } else {
            return false;
        }
    }

    private boolean setRemaingAmount(String sum) {
        try {
                JOptionPane.showMessageDialog(this, "Your Salary is not enough ", "Error", JOptionPane.ERROR_MESSAGE);       
                int ad = JOptionPane.showConfirmDialog(this,"Are you really want to set Remaining payment?","Confirm Delete",JOptionPane.YES_NO_OPTION);
                if (ad == 0){
                txt_amount.setText(String.valueOf(Double.parseDouble(txt_salary.getText())-Double.parseDouble(sum)));
                return false;
                }else{
                txt_amount.setText("");
                return false;
             }
        } catch (Exception e) {
            txt_amount.setText("");
            return false;
        }
            
    }

    private void setSalary() {
        try{
          Calendar calendar = Calendar.getInstance();
          String year =sdf_year.format(sdf.parse(sdf.format(jdc_date.getDate())));
          String month =sdf_month.format(sdf.parse(sdf.format(jdc_date.getDate())));
          calendar.set(Calendar.YEAR,Integer.parseInt(year));
          calendar.set(Calendar.MONTH,Integer.parseInt(month));
          int total = calendar.getActualMaximum(Calendar.DATE);
        Config.sql ="select count(*) as days from attendence_details c where _month='"+month+"' and _year='"+year+"' and c.employee_id='"+employee_id +"' and status='true'";
            System.out.println(Config.sql);
        Config.rs = Config.stmt.executeQuery(Config.sql);
        if(Config.rs.next()){
        String sal =txt_salary.getText();
        String days=Config.rs.getString("days");
        txt_salary.setText(sal);
        lbl_no_of_days.setText(days);
        double amt =(Double.parseDouble(sal)/total)*Integer.parseInt(days);
        txt_amount.setText(String.valueOf(Math.round(amt)));
        
        }}catch (Exception e) {
            e.printStackTrace();
        }
    }
}
