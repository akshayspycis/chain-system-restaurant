package modules.employee_management.salary_information;

import data_manager.SalaryDistribution;
import data_manager.configuration.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class ViewSalaryInfo extends javax.swing.JDialog {
    String employee_id = "";
    String message_id= "";
    String salary_id = "";
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");

    public ViewSalaryInfo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_name = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_amount = new javax.swing.JTextField();
        txt_salary = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jdc_date = new com.toedter.calendar.JDateChooser();
        btn_update = new javax.swing.JButton();
        btn_delete = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        lbl_salary_id = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Salary Information", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Employee Name");

        txt_name.setEditable(false);
        txt_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nameActionPerformed(evt);
            }
        });
        txt_name.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_nameKeyPressed(evt);
            }
        });

        jLabel2.setText("Date ");

        jLabel3.setText("Amount");

        txt_amount.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_amountCaretUpdate(evt);
            }
        });
        txt_amount.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_amountFocusLost(evt);
            }
        });
        txt_amount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_amountKeyPressed(evt);
            }
        });

        jLabel4.setText("Salary");

        jdc_date.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jdc_date, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(5, 5, 5)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(txt_salary))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(35, 35, 35)))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_salary, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        btn_update.setText("Update");
        btn_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateActionPerformed(evt);
            }
        });
        btn_update.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_updateKeyPressed(evt);
            }
        });

        btn_delete.setText("Message");
        btn_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteActionPerformed(evt);
            }
        });
        btn_delete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_deleteKeyPressed(evt);
            }
        });

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });

        jPanel13.setBackground(new java.awt.Color(230, 79, 6));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("View Salary");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        lbl_salary_id.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_delete)
                        .addGap(6, 6, 6)
                        .addComponent(lbl_salary_id, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_update)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancel)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_cancel, btn_update});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_cancel)
                            .addComponent(btn_update))
                        .addComponent(lbl_salary_id))
                    .addComponent(btn_delete, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_delete, lbl_salary_id});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void txt_amountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_amountKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            btn_delete.requestFocus();                    
        }
         if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            btn_delete.requestFocus();                    
        }
         if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jdc_date.requestFocus();
        }
         if (evt.getKeyCode() == KeyEvent.VK_UP) {
           txt_name.requestFocus();
        }
    }//GEN-LAST:event_txt_amountKeyPressed

    private void btn_deleteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_deleteKeyPressed
       if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            btn_update
                    .requestFocus();                    
        }
         if(evt.getKeyCode() == KeyEvent.VK_DOWN){
           txt_name.requestFocus();                    
        }
         if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_amount.requestFocus();
        }
         if (evt.getKeyCode() == KeyEvent.VK_UP) {
           txt_amount.requestFocus();
        }
    }//GEN-LAST:event_btn_deleteKeyPressed

    private void btn_updateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_updateKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            txt_name.requestFocus();                    
        }
         if(evt.getKeyCode() == KeyEvent.VK_DOWN){
           txt_name.requestFocus();                    
        }
         if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_delete.requestFocus();
        }
         if (evt.getKeyCode() == KeyEvent.VK_UP) {
           txt_amount.requestFocus();
        }
    }//GEN-LAST:event_btn_updateKeyPressed

    private void btn_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateActionPerformed
        try {
           String str = checkValidity();
         if (str.equals("ok")) {
            if(checkAvailability()){
            SalaryDistribution sd = new SalaryDistribution();
                   sd.setSal_dis_id(lbl_salary_id.getText().trim());
                   sd.setEmployee_id(employee_id);
                   sd.setDate(sdf.format(jdc_date.getDate()));
                   sd.setMonth(sdf_month.format(sdf.parse(sdf.format(jdc_date.getDate()))));
                   sd.setYear(sdf_year.format(sdf.parse(sdf.format(jdc_date.getDate()))));
                   sd.setAmount(txt_amount.getText().trim());
                   sd.setMessage_id(message_id);
                   sd.setSalary_id(salary_id);
                if (Config.salary_distribution_mgr.updSalaryDistribution(sd)) {
                    Config.employee_management.setSalaryData(txt_name.getText(), sd.getDate());
                      JOptionPane.showMessageDialog(this, "Salary updated successfully.", "Creation successful", JOptionPane.NO_OPTION);
                      dispose();  
                } else {
                      JOptionPane.showMessageDialog(this, "Error in Salary updation.", "Error", JOptionPane.ERROR_MESSAGE);
                      }
                }else{
               JOptionPane.showMessageDialog(this, "Error in Updations ", "Error", JOptionPane.ERROR_MESSAGE);   
               }
                }else{
                     JOptionPane.showMessageDialog(this, "Check '"+str+"' field.", "Error", JOptionPane.ERROR_MESSAGE);
                     }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }//GEN-LAST:event_btn_updateActionPerformed

    private void btn_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteActionPerformed
       if(message_id.equals("")){
           Config.new_message.onloadReset(11);
           Config.new_message.setVisible(true);
       }else{
           Config.view_message.onloadReset(11,message_id);
           Config.view_message.setVisible(true);
       }
    }//GEN-LAST:event_btn_deleteActionPerformed

    private void txt_nameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nameKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            jdc_date.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            jdc_date.requestFocus();
        }
    }//GEN-LAST:event_txt_nameKeyPressed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
       dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void txt_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nameActionPerformed
        if(!txt_name.getText().equals("")){
           Config.employee_search.onloadReset(5);
           Config.employee_search.setVisible(true);
        }
    }//GEN-LAST:event_txt_nameActionPerformed

    private void txt_amountCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_amountCaretUpdate
if(!txt_amount.getText().trim().equals("")){
        try {
        Double.parseDouble(txt_amount.getText().trim());
        } catch (Exception e) {
        JOptionPane.showMessageDialog(this, "Only No.Value Required ", "Error", JOptionPane.ERROR_MESSAGE);   
        txt_amount.setText("");
        }
}
    }//GEN-LAST:event_txt_amountCaretUpdate

    private void txt_amountFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_amountFocusLost
        if(!txt_amount.getText().trim().equals("")){
        try {
        Double.parseDouble(txt_amount.getText().trim());
        }catch (Exception e) {
        JOptionPane.showMessageDialog(this, "Only No.Value Required ", "Error", JOptionPane.ERROR_MESSAGE);   
        txt_amount.setText("");
        txt_amount.requestFocus();
        }
        }
    }//GEN-LAST:event_txt_amountFocusLost
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_delete;
    private javax.swing.JButton btn_update;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private com.toedter.calendar.JDateChooser jdc_date;
    private javax.swing.JLabel lbl_salary_id;
    private javax.swing.JTextField txt_amount;
    private javax.swing.JTextField txt_name;
    private javax.swing.JTextField txt_salary;
    // End of variables declaration//GEN-END:variables
    double amount=0.0;
    
    public void onloadReset(String id) {
        lbl_salary_id.setText(id);
        try {
            Config.sql = "Select * from salary_distribution b where sal_dis_id= '"+id+"'";
            Config.rs = Config.stmt.executeQuery(Config.sql);            
            if (Config.rs.next()) {
              employee_id=Config.rs.getString("employee_id");
              txt_name.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.rs.getString("employee_id"))).getProfile_id())).getName());
              jdc_date.setDate(sdf.parse(Config.rs.getString("_date")));
              txt_amount.setText(Config.rs.getString("amount"));
              this.amount=Double.parseDouble((Config.rs.getString("amount")));
              message_id=Config.rs.getString("message_id");
            }    
            setSalary();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private String checkValidity() {
       if (txt_name.getText().equals("")) {
            return "Employee Name";
        }
        else if (jdc_date.getDate().equals("")) {
            return "Date";
        }
        else if (Double.parseDouble(txt_amount.getText())==0.0) {
            return "Amount";
        }
        else {
            return "ok";
        }
    }
    public void setEmployeeName(String employee_id) {
      this.employee_id=employee_id;    
      System.out.println(employee_id);
      txt_name.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(employee_id)).getProfile_id())).getName());
      }
    
    public void setMessage(String message_id){
         this.message_id=message_id;
     }
    
    private boolean checkAvailability() {
        try {
            Double amount=0.0;
            Config.sql ="SELECT IFNULL(sum(amount),0) FROM salary_distribution a where employee_id='"+employee_id+"' and _month='"+sdf_month.format(sdf.parse(sdf.format(jdc_date.getDate())))+"' and _year='"+sdf_year.format(sdf.parse(sdf.format(jdc_date.getDate())))+"' ";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if( Double.parseDouble(txt_amount.getText().trim())<=Double.parseDouble(txt_amount.getText()))
            {
                if(Config.rs.next()){
                    try {
                        amount=Double.parseDouble(Config.rs.getString(1))+Double.parseDouble(txt_amount.getText());
                        if(amount<Double.parseDouble(txt_salary.getText())){
                        return setRemaingAmount(Config.rs.getString(1));
                        }else{
                        return true;
                        }
                    } catch (Exception e) {
                        return false;
                    }
                    }else{
                    return true;
                    }
            }else{
             if(Config.rs.next()){
                 return setRemaingAmount(Config.rs.getString(1));
             }else{
                 return true;
             }   
            }
         }catch (Exception e) {
           e.printStackTrace();
            return false;
         }
    }

    private boolean setRemaingAmount(String sum) {
        try {
                JOptionPane.showMessageDialog(this, "Your Salary is not enough ", "Error", JOptionPane.ERROR_MESSAGE);       
                int ad = JOptionPane.showConfirmDialog(this,"Are you really want to set Remaining payment?","Confirm Delete",JOptionPane.YES_NO_OPTION);
                if (ad == 0){
                txt_amount.setText(String.valueOf(Double.parseDouble(txt_salary.getText())-Double.parseDouble(sum)));
                return false;
                }else{
                txt_amount.setText("");
                return false;
             }
        } catch (Exception e) {
            txt_amount.setText("");
            return false;
        }
    }
        private void setSalary() {
        try{
        Config.sql ="SELECT salary_id,salary FROM salary_details a INNER JOIN (SELECT employee_id,max(STR_TO_DATE(_date,'%d/%m/%Y')) max_date FROM salary_details GROUP BY employee_id,_year,_month having _month='"+sdf_month.format(sdf.parse(sdf.format(jdc_date.getDate())))+"' and _year='"+sdf_year.format(sdf.parse(sdf.format(jdc_date.getDate())))+"') b where a.employee_id = b.employee_id and a.employee_id='"+employee_id+"' AND STR_TO_DATE(a._date,'%d/%m/%Y') = b.max_date";
        Config.rs = Config.stmt.executeQuery(Config.sql);
        System.out.println(Config.sql);
        if(Config.rs.next()){
        salary_id=Config.rs.getString("salary_id");    
        txt_salary.setText(Config.rs.getString("salary"));
        }else{System.out.println("dasdasd sad asdda ssdd");}
        }catch (Exception e) {}
    }
}