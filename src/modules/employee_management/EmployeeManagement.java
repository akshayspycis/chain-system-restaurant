package modules.employee_management;

import data_manager.AttendenceDetails;
import data_manager.EmployeeDetails;
import data_manager.configuration.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

public class EmployeeManagement extends javax.swing.JDialog {
  String employee_id = null;
  String attendance_id = null;
  SimpleDateFormat sdf_month = new SimpleDateFormat("M");
  SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
  SimpleDateFormat sdf_day = new SimpleDateFormat("dd");
  public DefaultTableModel  tbl_employee_model = null;
  public DefaultTableModel  tbl_salary_adv_model = null;
  public DefaultTableModel  tbl_salary_sal_model = null;
  public DefaultTableModel  tbl_attendence_model = null;
  public DefaultTableModel  tbl_attendence_attendence_model = null;
  public DefaultTableModel  tbl_report_attendance_model = null;
  public DefaultTableModel  txt_emp_attendence_model = null;
  public DefaultTableModel  tbl_emp_other_details_model = null;
  public DefaultTableModel  tbl_emp_payment_model = null;
  public DefaultTableModel  tbl_adv_report_model = null;
  public DefaultTableModel  tbl_sal_report_model = null;
  public DefaultTableModel  tbl_sal_dis_report_model = null;

  ArrayList<String> salary_distribution_id = null;
    
  SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
  DateFormat sdf1 = new SimpleDateFormat("hh:mm:ss a"); 
      
public EmployeeManagement(java.awt.Frame parent, boolean modal) {
       super(parent, modal);
       initComponents();
       this.setLocationRelativeTo(null);
    
       jdc_att_date.setMaxSelectableDate(new Date());
        
        tbl_employee_model = (DefaultTableModel) tbl_employee.getModel();
        tbl_salary_adv_model = (DefaultTableModel) tbl_salary_adv.getModel();
        tbl_salary_sal_model = (DefaultTableModel) tbl_salary_sal.getModel();
        tbl_attendence_model = (DefaultTableModel) tbl_attendence.getModel();
        tbl_attendence_attendence_model = (DefaultTableModel) tbl_attendence_attdncdetail.getModel();
        tbl_report_attendance_model = (DefaultTableModel) tbl_att_report.getModel();
        txt_emp_attendence_model=(DefaultTableModel) tbl_emp_attendence.getModel();
        tbl_emp_other_details_model=(DefaultTableModel) tbl_emp_other_details.getModel();
        tbl_emp_payment_model=(DefaultTableModel) tbl_emp_payment.getModel();
        tbl_adv_report_model=(DefaultTableModel) tbl_adv_report.getModel();
        tbl_sal_report_model=(DefaultTableModel) tbl_sal_report_month.getModel();
        tbl_sal_dis_report_model=(DefaultTableModel) tbl_sal_dis_report.getModel();
        
        tbl_attendence.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane16 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        pnl_employee = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txt_emp_search = new javax.swing.JTextField();
        btn_emp_search = new javax.swing.JButton();
        btn_emp_new = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        txt_emp_employeename = new javax.swing.JTextField();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        tbl_emp_other_details = new javax.swing.JTable();
        jLabel12 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        txt_emp_address = new javax.swing.JTextField();
        txt_emp_city = new javax.swing.JTextField();
        txt_emp_pincode = new javax.swing.JTextField();
        txt_emp_state = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tbl_emp_payment = new javax.swing.JTable();
        txt_emp_dob = new javax.swing.JTextField();
        jPanel14 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txt_emp_message = new javax.swing.JTextPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_employee = new javax.swing.JTable();
        jPanel19 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        tbl_emp_attendence = new javax.swing.JTable();
        btn_emp_cancel = new javax.swing.JButton();
        btn_emp_view = new javax.swing.JButton();
        btn_emp_print = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txt_emp_gender = new javax.swing.JTextField();
        txt_emp_department = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        pnl_salary_info = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        pnl_salary = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txt_salary_sal_search = new javax.swing.JTextField();
        btn_salary_sal_search = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_salary_sal = new javax.swing.JTable();
        btn_salary_sal_view = new javax.swing.JButton();
        btn_salary_sal_add = new javax.swing.JButton();
        jyc_year = new com.toedter.calendar.JYearChooser();
        jSeparator7 = new javax.swing.JSeparator();
        jmc_month = new com.toedter.calendar.JMonthChooser();
        btn_sal_delete = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();
        txt_salary_total = new javax.swing.JTextField();
        pnl_advance = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txt_salary_adv_search = new javax.swing.JTextField();
        btn_salary_adv_search = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_salary_adv = new javax.swing.JTable();
        btn_salary_adv_view = new javax.swing.JButton();
        btn_salary_adv_add = new javax.swing.JButton();
        jmc_month_adv = new com.toedter.calendar.JMonthChooser();
        jyc_year_adv = new com.toedter.calendar.JYearChooser();
        jSeparator10 = new javax.swing.JSeparator();
        btn_adv_delete = new javax.swing.JButton();
        jLabel26 = new javax.swing.JLabel();
        txt_advance_total = new javax.swing.JTextField();
        jPanel15 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        txt_salary_message = new javax.swing.JTextPane();
        jPanel16 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel8 = new javax.swing.JLabel();
        total_grand_due = new javax.swing.JTextField();
        txt_grand_amount = new javax.swing.JTextField();
        txt_grand_salary = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        btn_salary_cancel = new javax.swing.JButton();
        btn_salary_view = new javax.swing.JButton();
        btn_salary_print = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        pnl_attendence = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txt_attendence_dob = new javax.swing.JTextField();
        jPanel33 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        txt_attendence_search = new javax.swing.JTextField();
        btn_attendence_search = new javax.swing.JButton();
        jSeparator8 = new javax.swing.JSeparator();
        jdc_att_date = new com.toedter.calendar.JDateChooser();
        jLabel15 = new javax.swing.JLabel();
        jPanel34 = new javax.swing.JPanel();
        jScrollPane17 = new javax.swing.JScrollPane();
        txt_attendence_message = new javax.swing.JTextPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbl_attendence = new javax.swing.JTable();
        jPanel35 = new javax.swing.JPanel();
        txt_attendence_address = new javax.swing.JTextField();
        txt_attendence_city = new javax.swing.JTextField();
        txt_attendence_pincode = new javax.swing.JTextField();
        txt_attendence_state = new javax.swing.JTextField();
        btn_attendence_reset = new javax.swing.JButton();
        btn_attendence_view = new javax.swing.JButton();
        btn_attendence_cancel = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        jPanel36 = new javax.swing.JPanel();
        jScrollPane18 = new javax.swing.JScrollPane();
        tbl_attendence_attdncdetail = new javax.swing.JTable();
        txt_attendence_empname = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txt_attendence_gender = new javax.swing.JTextField();
        txt_attendence_department = new javax.swing.JTextField();
        btn_add = new javax.swing.JButton();
        btn_view = new javax.swing.JButton();
        jPanel22 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        pnl_adv_report = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        txt_adv_report = new javax.swing.JTextField();
        btn_adv_report_search = new javax.swing.JButton();
        jmc_adv_report = new com.toedter.calendar.JMonthChooser();
        jyc_adv_report = new com.toedter.calendar.JYearChooser();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        btn_adv_report_view = new javax.swing.JButton();
        btn_adv_report_reset = new javax.swing.JButton();
        btn_adv_report_cancel = new javax.swing.JButton();
        jLabel24 = new javax.swing.JLabel();
        txt_total_employee = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txt_total_advance = new javax.swing.JTextField();
        jScrollPane8 = new javax.swing.JScrollPane();
        jPanel17 = new javax.swing.JPanel();
        jScrollPane12 = new javax.swing.JScrollPane();
        tbl_adv_report = new javax.swing.JTable();
        btn_adv_report_reset1 = new javax.swing.JButton();
        pnl_attendence_report = new javax.swing.JPanel();
        jPanel25 = new javax.swing.JPanel();
        jLabel32 = new javax.swing.JLabel();
        txt_att_report_search = new javax.swing.JTextField();
        btn_att_report_search = new javax.swing.JButton();
        jmc_att_report = new com.toedter.calendar.JMonthChooser();
        jyc_att_report = new com.toedter.calendar.JYearChooser();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        btn_att_report_cancel = new javax.swing.JButton();
        btn_att_report_view = new javax.swing.JButton();
        btn_att_report_print = new javax.swing.JButton();
        jScrollPane19 = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane14 = new javax.swing.JScrollPane();
        tbl_att_report = new javax.swing.JTable();
        pnl_salary_report = new javax.swing.JPanel();
        btn_sal_report_save = new javax.swing.JButton();
        btn_sal_report_cancel = new javax.swing.JButton();
        txt_salary_report_total_due = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txt_salary_report_total_emp = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        btn_sal_report_print = new javax.swing.JButton();
        btn_sal_report_reset = new javax.swing.JButton();
        txt_salary_report_total_adv = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        txt_salary_report_total_sal = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        jScrollPane13 = new javax.swing.JScrollPane();
        tbl_sal_report_month = new javax.swing.JTable();
        jPanel26 = new javax.swing.JPanel();
        jLabel39 = new javax.swing.JLabel();
        txt_salary_report_search = new javax.swing.JTextField();
        btn_sal_report_search = new javax.swing.JButton();
        jmc_sal_report = new com.toedter.calendar.JMonthChooser();
        jyc_sal_report = new com.toedter.calendar.JYearChooser();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        jPanel3 = new javax.swing.JPanel();
        pnl_adv_report1 = new javax.swing.JPanel();
        jPanel24 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        txt_sal_dis_report = new javax.swing.JTextField();
        btn_sal_dis_report_search = new javax.swing.JButton();
        jmc_sal_dis_report = new com.toedter.calendar.JMonthChooser();
        jyc_sal_dis_report = new com.toedter.calendar.JYearChooser();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jSeparator9 = new javax.swing.JSeparator();
        btn_adv_report_view1 = new javax.swing.JButton();
        btn_adv_report_reset2 = new javax.swing.JButton();
        btn_adv_report_cancel1 = new javax.swing.JButton();
        jLabel30 = new javax.swing.JLabel();
        txt_sal_dis_total_employee = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        txt_sal_dis_total_advance1 = new javax.swing.JTextField();
        jScrollPane11 = new javax.swing.JScrollPane();
        jPanel18 = new javax.swing.JPanel();
        jScrollPane15 = new javax.swing.JScrollPane();
        tbl_sal_dis_report = new javax.swing.JTable();
        btn_adv_report_reset3 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane2.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        pnl_employee.setBackground(new java.awt.Color(255, 255, 255));
        pnl_employee.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pnl_employeeKeyPressed(evt);
            }
        });

        jLabel5.setText("Employee Name");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Search By :");

        txt_emp_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_emp_searchCaretUpdate(evt);
            }
        });
        txt_emp_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_emp_searchKeyPressed(evt);
            }
        });

        btn_emp_search.setText("Search");
        btn_emp_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_emp_searchActionPerformed(evt);
            }
        });
        btn_emp_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_emp_searchKeyPressed(evt);
            }
        });

        btn_emp_new.setText("New Employee");
        btn_emp_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_emp_newActionPerformed(evt);
            }
        });
        btn_emp_new.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_emp_newKeyPressed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_emp_search, javax.swing.GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_emp_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_emp_new)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_emp_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_emp_search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_emp_new))
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator3))
                .addContainerGap())
        );

        txt_emp_employeename.setEditable(false);
        txt_emp_employeename.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_emp_employeenameActionPerformed(evt);
            }
        });

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Other Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jScrollPane10.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane10.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        tbl_emp_other_details.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbl_emp_other_details.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "JOINING DATE", "LEAVING DATE", "WORKING TIME IN", "WORKING TIME OUT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_emp_other_details.setAutoscrolls(false);
        tbl_emp_other_details.setRowHeight(25);
        tbl_emp_other_details.getTableHeader().setReorderingAllowed(false);
        jScrollPane10.setViewportView(tbl_emp_other_details);
        if (tbl_emp_other_details.getColumnModel().getColumnCount() > 0) {
            tbl_emp_other_details.getColumnModel().getColumn(0).setResizable(false);
            tbl_emp_other_details.getColumnModel().getColumn(1).setResizable(false);
            tbl_emp_other_details.getColumnModel().getColumn(2).setResizable(false);
            tbl_emp_other_details.getColumnModel().getColumn(3).setResizable(false);
        }

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel12.setText("Gender");

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Address", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_emp_address.setEditable(false);
        txt_emp_address.setBackground(new java.awt.Color(255, 255, 255));
        txt_emp_address.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_emp_address.setBorder(null);

        txt_emp_city.setEditable(false);
        txt_emp_city.setBackground(new java.awt.Color(255, 255, 255));
        txt_emp_city.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_emp_city.setBorder(null);

        txt_emp_pincode.setEditable(false);
        txt_emp_pincode.setBackground(new java.awt.Color(255, 255, 255));
        txt_emp_pincode.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_emp_pincode.setBorder(null);

        txt_emp_state.setEditable(false);
        txt_emp_state.setBackground(new java.awt.Color(255, 255, 255));
        txt_emp_state.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_emp_state.setBorder(null);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_emp_pincode, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                    .addComponent(txt_emp_state)
                    .addComponent(txt_emp_city)
                    .addComponent(txt_emp_address))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(txt_emp_address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_emp_city, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_emp_pincode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_emp_state, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel13.setText("Date of Birth");

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Last Month Payment Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jScrollPane6.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane6.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        tbl_emp_payment.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbl_emp_payment.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "SALARY ", "TOTAL AMT", "ADVANCE AMT", "DUE AMT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_emp_payment.setAutoscrolls(false);
        tbl_emp_payment.setRowHeight(25);
        tbl_emp_payment.getTableHeader().setReorderingAllowed(false);
        jScrollPane6.setViewportView(tbl_emp_payment);
        if (tbl_emp_payment.getColumnModel().getColumnCount() > 0) {
            tbl_emp_payment.getColumnModel().getColumn(0).setResizable(false);
            tbl_emp_payment.getColumnModel().getColumn(1).setResizable(false);
            tbl_emp_payment.getColumnModel().getColumn(2).setResizable(false);
            tbl_emp_payment.getColumnModel().getColumn(3).setResizable(false);
        }

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 544, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        txt_emp_dob.setEditable(false);

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_emp_message.setEditable(false);
        jScrollPane5.setViewportView(txt_emp_message);

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5)
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tbl_employee.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "EMPLOYEE NAME", "CONTACT NO.", "ORGANIZATION", "EMAIL", "IDENTITY TYPE", "IDENTITY NO.", "JOINING DATE", "LEAVING DATE", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_employee.getTableHeader().setReorderingAllowed(false);
        tbl_employee.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tbl_employeeFocusGained(evt);
            }
        });
        tbl_employee.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_employeeMouseClicked(evt);
            }
        });
        tbl_employee.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tbl_employeePropertyChange(evt);
            }
        });
        tbl_employee.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_employeeKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbl_employeeKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_employee);
        if (tbl_employee.getColumnModel().getColumnCount() > 0) {
            tbl_employee.getColumnModel().getColumn(0).setResizable(false);
            tbl_employee.getColumnModel().getColumn(0).setPreferredWidth(50);
            tbl_employee.getColumnModel().getColumn(1).setResizable(false);
            tbl_employee.getColumnModel().getColumn(1).setPreferredWidth(150);
            tbl_employee.getColumnModel().getColumn(2).setResizable(false);
            tbl_employee.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_employee.getColumnModel().getColumn(3).setResizable(false);
            tbl_employee.getColumnModel().getColumn(3).setPreferredWidth(110);
            tbl_employee.getColumnModel().getColumn(4).setResizable(false);
            tbl_employee.getColumnModel().getColumn(4).setPreferredWidth(200);
            tbl_employee.getColumnModel().getColumn(5).setResizable(false);
            tbl_employee.getColumnModel().getColumn(5).setPreferredWidth(100);
            tbl_employee.getColumnModel().getColumn(6).setResizable(false);
            tbl_employee.getColumnModel().getColumn(6).setPreferredWidth(100);
            tbl_employee.getColumnModel().getColumn(7).setResizable(false);
            tbl_employee.getColumnModel().getColumn(7).setPreferredWidth(100);
            tbl_employee.getColumnModel().getColumn(8).setResizable(false);
            tbl_employee.getColumnModel().getColumn(8).setPreferredWidth(100);
            tbl_employee.getColumnModel().getColumn(9).setMinWidth(0);
            tbl_employee.getColumnModel().getColumn(9).setPreferredWidth(0);
            tbl_employee.getColumnModel().getColumn(9).setMaxWidth(0);
        }

        jPanel19.setBackground(new java.awt.Color(255, 255, 255));
        jPanel19.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Last Month Attendence Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jScrollPane9.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane9.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        tbl_emp_attendence.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbl_emp_attendence.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null}
            },
            new String [] {
                "TOTAL DAYS", "TOTAL PRESENT", "TOTAL ABSENT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_emp_attendence.setAutoscrolls(false);
        tbl_emp_attendence.setRowHeight(25);
        tbl_emp_attendence.getTableHeader().setReorderingAllowed(false);
        jScrollPane9.setViewportView(tbl_emp_attendence);
        if (tbl_emp_attendence.getColumnModel().getColumnCount() > 0) {
            tbl_emp_attendence.getColumnModel().getColumn(0).setResizable(false);
            tbl_emp_attendence.getColumnModel().getColumn(1).setResizable(false);
            tbl_emp_attendence.getColumnModel().getColumn(2).setResizable(false);
        }

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane9)
                .addContainerGap())
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel19Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btn_emp_cancel.setText("Cancel");
        btn_emp_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_emp_cancelActionPerformed(evt);
            }
        });
        btn_emp_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_emp_cancelKeyPressed(evt);
            }
        });

        btn_emp_view.setText("View");
        btn_emp_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_emp_viewActionPerformed(evt);
            }
        });
        btn_emp_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_emp_viewKeyPressed(evt);
            }
        });

        btn_emp_print.setText("Print");
        btn_emp_print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_emp_printKeyPressed(evt);
            }
        });

        jLabel3.setText("Department");

        txt_emp_gender.setEditable(false);

        txt_emp_department.setEditable(false);

        javax.swing.GroupLayout pnl_employeeLayout = new javax.swing.GroupLayout(pnl_employee);
        pnl_employee.setLayout(pnl_employeeLayout);
        pnl_employeeLayout.setHorizontalGroup(
            pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_employeeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnl_employeeLayout.createSequentialGroup()
                                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(txt_emp_employeename, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(txt_emp_gender, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13)
                                    .addComponent(txt_emp_dob, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(txt_emp_department, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_employeeLayout.createSequentialGroup()
                        .addComponent(btn_emp_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_emp_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_emp_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        pnl_employeeLayout.setVerticalGroup(
            pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_employeeLayout.createSequentialGroup()
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_emp_employeename, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_emp_dob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_emp_gender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_emp_department, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_emp_cancel)
                    .addComponent(btn_emp_view)
                    .addComponent(btn_emp_print))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Employee Information", pnl_employee);

        pnl_salary_info.setBackground(new java.awt.Color(255, 255, 255));

        jSplitPane1.setBorder(null);
        jSplitPane1.setDividerLocation(550);
        jSplitPane1.setPreferredSize(new java.awt.Dimension(507, 348));

        pnl_salary.setBackground(new java.awt.Color(255, 255, 255));
        pnl_salary.setMinimumSize(new java.awt.Dimension(500, 100));
        pnl_salary.setPreferredSize(new java.awt.Dimension(515, 346));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Salary", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel4.setText("Search By :");

        txt_salary_sal_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_salary_sal_searchCaretUpdate(evt);
            }
        });
        txt_salary_sal_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_salary_sal_searchKeyPressed(evt);
            }
        });

        btn_salary_sal_search.setText("Search");
        btn_salary_sal_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salary_sal_searchActionPerformed(evt);
            }
        });
        btn_salary_sal_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_salary_sal_searchKeyPressed(evt);
            }
        });

        tbl_salary_sal.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "EMPLOYEE NAME", "DATE", "AMOUNT", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_salary_sal.getTableHeader().setReorderingAllowed(false);
        tbl_salary_sal.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tbl_salary_salFocusGained(evt);
            }
        });
        tbl_salary_sal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_salary_salMouseClicked(evt);
            }
        });
        tbl_salary_sal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_salary_salKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbl_salary_salKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(tbl_salary_sal);
        if (tbl_salary_sal.getColumnModel().getColumnCount() > 0) {
            tbl_salary_sal.getColumnModel().getColumn(0).setResizable(false);
            tbl_salary_sal.getColumnModel().getColumn(1).setResizable(false);
            tbl_salary_sal.getColumnModel().getColumn(2).setResizable(false);
            tbl_salary_sal.getColumnModel().getColumn(3).setResizable(false);
            tbl_salary_sal.getColumnModel().getColumn(4).setMinWidth(0);
            tbl_salary_sal.getColumnModel().getColumn(4).setPreferredWidth(0);
            tbl_salary_sal.getColumnModel().getColumn(4).setMaxWidth(0);
            tbl_salary_sal.getColumnModel().getColumn(5).setMinWidth(0);
            tbl_salary_sal.getColumnModel().getColumn(5).setPreferredWidth(0);
            tbl_salary_sal.getColumnModel().getColumn(5).setMaxWidth(0);
        }

        btn_salary_sal_view.setText("View");
        btn_salary_sal_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salary_sal_viewActionPerformed(evt);
            }
        });
        btn_salary_sal_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_salary_sal_viewKeyPressed(evt);
            }
        });

        btn_salary_sal_add.setText("Add");
        btn_salary_sal_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salary_sal_addActionPerformed(evt);
            }
        });
        btn_salary_sal_add.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_salary_sal_addKeyPressed(evt);
            }
        });

        jyc_year.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jyc_yearMouseClicked(evt);
            }
        });
        jyc_year.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_yearPropertyChange(evt);
            }
        });

        jSeparator7.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jmc_month.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_monthPropertyChange(evt);
            }
        });

        btn_sal_delete.setText("Delete");
        btn_sal_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_sal_deleteActionPerformed(evt);
            }
        });

        jLabel20.setText("Total :");

        txt_salary_total.setEditable(false);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_salary_sal_search, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jmc_month, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jyc_year, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_salary_sal_search, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(btn_salary_sal_add, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_sal_delete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_salary_total, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_salary_sal_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(txt_salary_sal_search, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_salary_sal_search))
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jSeparator7, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jyc_year, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jmc_month, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_salary_sal_view)
                    .addComponent(btn_salary_sal_add)
                    .addComponent(btn_sal_delete)
                    .addComponent(jLabel20)
                    .addComponent(txt_salary_total, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6))
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_salary_sal_search, jmc_month, jyc_year, txt_salary_sal_search});

        javax.swing.GroupLayout pnl_salaryLayout = new javax.swing.GroupLayout(pnl_salary);
        pnl_salary.setLayout(pnl_salaryLayout);
        pnl_salaryLayout.setHorizontalGroup(
            pnl_salaryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_salaryLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnl_salaryLayout.setVerticalGroup(
            pnl_salaryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_salaryLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(pnl_salary);

        pnl_advance.setBackground(new java.awt.Color(255, 255, 255));
        pnl_advance.setPreferredSize(new java.awt.Dimension(500, 346));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Advance", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel6.setText("Search By :");

        txt_salary_adv_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_salary_adv_searchCaretUpdate(evt);
            }
        });
        txt_salary_adv_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_salary_adv_searchKeyPressed(evt);
            }
        });

        btn_salary_adv_search.setText("Search");
        btn_salary_adv_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salary_adv_searchActionPerformed(evt);
            }
        });
        btn_salary_adv_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_salary_adv_searchKeyPressed(evt);
            }
        });

        tbl_salary_adv.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "EMPLOYEE NAME", "DATE", "AMOUNT", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_salary_adv.getTableHeader().setReorderingAllowed(false);
        tbl_salary_adv.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_salary_advMouseClicked(evt);
            }
        });
        tbl_salary_adv.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_salary_advKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbl_salary_advKeyReleased(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_salary_adv);
        if (tbl_salary_adv.getColumnModel().getColumnCount() > 0) {
            tbl_salary_adv.getColumnModel().getColumn(0).setResizable(false);
            tbl_salary_adv.getColumnModel().getColumn(1).setResizable(false);
            tbl_salary_adv.getColumnModel().getColumn(2).setResizable(false);
            tbl_salary_adv.getColumnModel().getColumn(3).setResizable(false);
            tbl_salary_adv.getColumnModel().getColumn(4).setMinWidth(0);
            tbl_salary_adv.getColumnModel().getColumn(4).setPreferredWidth(0);
            tbl_salary_adv.getColumnModel().getColumn(4).setMaxWidth(0);
            tbl_salary_adv.getColumnModel().getColumn(5).setMinWidth(0);
            tbl_salary_adv.getColumnModel().getColumn(5).setPreferredWidth(0);
            tbl_salary_adv.getColumnModel().getColumn(5).setMaxWidth(0);
        }

        btn_salary_adv_view.setText("View");
        btn_salary_adv_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salary_adv_viewActionPerformed(evt);
            }
        });
        btn_salary_adv_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_salary_adv_viewKeyPressed(evt);
            }
        });

        btn_salary_adv_add.setText("Add");
        btn_salary_adv_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salary_adv_addActionPerformed(evt);
            }
        });
        btn_salary_adv_add.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_salary_adv_addKeyPressed(evt);
            }
        });

        jmc_month_adv.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_month_advPropertyChange(evt);
            }
        });

        jyc_year_adv.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_year_advPropertyChange(evt);
            }
        });

        jSeparator10.setOrientation(javax.swing.SwingConstants.VERTICAL);

        btn_adv_delete.setText("Delete");
        btn_adv_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_adv_deleteActionPerformed(evt);
            }
        });

        jLabel26.setText("Total :");

        txt_advance_total.setEditable(false);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_salary_adv_search, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jmc_month_adv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jyc_year_adv, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_salary_adv_search, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addComponent(btn_salary_adv_add, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_adv_delete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_advance_total, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_salary_adv_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(txt_salary_adv_search, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_salary_adv_search))
                    .addComponent(jyc_year_adv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jmc_month_adv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel26)
                        .addComponent(txt_advance_total, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_salary_adv_view)
                        .addComponent(btn_salary_adv_add)
                        .addComponent(btn_adv_delete)))
                .addGap(6, 6, 6))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_salary_adv_search, jmc_month_adv, jyc_year_adv, txt_salary_adv_search});

        javax.swing.GroupLayout pnl_advanceLayout = new javax.swing.GroupLayout(pnl_advance);
        pnl_advance.setLayout(pnl_advanceLayout);
        pnl_advanceLayout.setHorizontalGroup(
            pnl_advanceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_advanceLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnl_advanceLayout.setVerticalGroup(
            pnl_advanceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_advanceLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSplitPane1.setRightComponent(pnl_advance);

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_salary_message.setEditable(false);
        jScrollPane7.setViewportView(txt_salary_message);

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7)
                .addContainerGap())
        );

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Grand Total", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel7.setText("Total Salary");

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel8.setText("Due Amount");

        total_grand_due.setEditable(false);
        total_grand_due.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        total_grand_due.setText("     ");
        total_grand_due.setBorder(null);
        total_grand_due.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                total_grand_dueActionPerformed(evt);
            }
        });

        txt_grand_amount.setEditable(false);
        txt_grand_amount.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_grand_amount.setText("     ");
        txt_grand_amount.setBorder(null);

        txt_grand_salary.setEditable(false);
        txt_grand_salary.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_grand_salary.setText("     ");
        txt_grand_salary.setBorder(null);

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel9.setText("Total Amount");

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(txt_grand_salary, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(jLabel7)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(txt_grand_amount, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(jLabel9)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(jLabel8)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                        .addComponent(total_grand_due, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addComponent(jSeparator2)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(total_grand_due, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_grand_salary)
                            .addComponent(txt_grand_amount))))
                .addContainerGap())
        );

        btn_salary_cancel.setText("Cancel");
        btn_salary_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salary_cancelActionPerformed(evt);
            }
        });
        btn_salary_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_salary_cancelKeyPressed(evt);
            }
        });

        btn_salary_view.setText("View");
        btn_salary_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_salary_viewKeyPressed(evt);
            }
        });

        btn_salary_print.setText("Print");
        btn_salary_print.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salary_printActionPerformed(evt);
            }
        });
        btn_salary_print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_salary_printKeyPressed(evt);
            }
        });

        jButton1.setText("Salary Management");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnl_salary_infoLayout = new javax.swing.GroupLayout(pnl_salary_info);
        pnl_salary_info.setLayout(pnl_salary_infoLayout);
        pnl_salary_infoLayout.setHorizontalGroup(
            pnl_salary_infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_salary_infoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_salary_infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_salary_infoLayout.createSequentialGroup()
                        .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnl_salary_infoLayout.createSequentialGroup()
                        .addComponent(btn_salary_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_salary_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_salary_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pnl_salary_infoLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_salary_cancel, btn_salary_print, btn_salary_view});

        pnl_salary_infoLayout.setVerticalGroup(
            pnl_salary_infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_salary_infoLayout.createSequentialGroup()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_salary_infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_salary_infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_salary_cancel)
                    .addComponent(btn_salary_view)
                    .addComponent(btn_salary_print)
                    .addComponent(jButton1))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnl_salary_info, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnl_salary_info, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("Salary Information", jPanel7);

        pnl_attendence.setBackground(new java.awt.Color(255, 255, 255));

        jLabel16.setText("Gender");

        jLabel10.setText("Employee Name");

        txt_attendence_dob.setEditable(false);

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setText("Search By :");

        txt_attendence_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_attendence_searchCaretUpdate(evt);
            }
        });
        txt_attendence_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_attendence_searchKeyPressed(evt);
            }
        });

        btn_attendence_search.setText("Search");
        btn_attendence_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_attendence_searchActionPerformed(evt);
            }
        });
        btn_attendence_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_attendence_searchKeyPressed(evt);
            }
        });

        jSeparator8.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jdc_att_date.setDateFormatString("dd/MM/yyyy");
        jdc_att_date.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jdc_att_dateMouseClicked(evt);
            }
        });
        jdc_att_date.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdc_att_datePropertyChange(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Date :");

        javax.swing.GroupLayout jPanel33Layout = new javax.swing.GroupLayout(jPanel33);
        jPanel33.setLayout(jPanel33Layout);
        jPanel33Layout.setHorizontalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_attendence_search, javax.swing.GroupLayout.PREFERRED_SIZE, 446, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_attendence_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdc_att_date, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel33Layout.setVerticalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator8)
                    .addComponent(jLabel17, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel33Layout.createSequentialGroup()
                        .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txt_attendence_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_attendence_search))
                            .addComponent(jdc_att_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel33Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_attendence_search, jLabel15, jLabel17, jdc_att_date, txt_attendence_search});

        jPanel34.setBackground(new java.awt.Color(255, 255, 255));
        jPanel34.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_attendence_message.setEditable(false);
        jScrollPane17.setViewportView(txt_attendence_message);

        javax.swing.GroupLayout jPanel34Layout = new javax.swing.GroupLayout(jPanel34);
        jPanel34.setLayout(jPanel34Layout);
        jPanel34Layout.setHorizontalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane17, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel34Layout.setVerticalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane17, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tbl_attendence.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_attendence.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "EMPLOYEE NAME", "DESIGNATION", "DATE", "TIME IN", "TIME OUT", "STATUS", "", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_attendence.getTableHeader().setReorderingAllowed(false);
        tbl_attendence.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_attendenceMouseClicked(evt);
            }
        });
        tbl_attendence.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_attendenceKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbl_attendenceKeyReleased(evt);
            }
        });
        jScrollPane4.setViewportView(tbl_attendence);
        if (tbl_attendence.getColumnModel().getColumnCount() > 0) {
            tbl_attendence.getColumnModel().getColumn(0).setResizable(false);
            tbl_attendence.getColumnModel().getColumn(0).setPreferredWidth(50);
            tbl_attendence.getColumnModel().getColumn(1).setResizable(false);
            tbl_attendence.getColumnModel().getColumn(1).setPreferredWidth(150);
            tbl_attendence.getColumnModel().getColumn(2).setResizable(false);
            tbl_attendence.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_attendence.getColumnModel().getColumn(3).setResizable(false);
            tbl_attendence.getColumnModel().getColumn(3).setPreferredWidth(110);
            tbl_attendence.getColumnModel().getColumn(4).setResizable(false);
            tbl_attendence.getColumnModel().getColumn(4).setPreferredWidth(200);
            tbl_attendence.getColumnModel().getColumn(5).setResizable(false);
            tbl_attendence.getColumnModel().getColumn(5).setPreferredWidth(100);
            tbl_attendence.getColumnModel().getColumn(6).setResizable(false);
            tbl_attendence.getColumnModel().getColumn(6).setPreferredWidth(100);
            tbl_attendence.getColumnModel().getColumn(7).setMinWidth(0);
            tbl_attendence.getColumnModel().getColumn(7).setPreferredWidth(0);
            tbl_attendence.getColumnModel().getColumn(7).setMaxWidth(0);
            tbl_attendence.getColumnModel().getColumn(8).setMinWidth(0);
            tbl_attendence.getColumnModel().getColumn(8).setPreferredWidth(0);
            tbl_attendence.getColumnModel().getColumn(8).setMaxWidth(0);
        }

        jPanel35.setBackground(new java.awt.Color(255, 255, 255));
        jPanel35.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Address", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_attendence_address.setEditable(false);
        txt_attendence_address.setBackground(new java.awt.Color(255, 255, 255));
        txt_attendence_address.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_attendence_address.setBorder(null);

        txt_attendence_city.setEditable(false);
        txt_attendence_city.setBackground(new java.awt.Color(255, 255, 255));
        txt_attendence_city.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_attendence_city.setBorder(null);
        txt_attendence_city.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_attendence_cityActionPerformed(evt);
            }
        });

        txt_attendence_pincode.setEditable(false);
        txt_attendence_pincode.setBackground(new java.awt.Color(255, 255, 255));
        txt_attendence_pincode.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_attendence_pincode.setBorder(null);
        txt_attendence_pincode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_attendence_pincodeActionPerformed(evt);
            }
        });

        txt_attendence_state.setEditable(false);
        txt_attendence_state.setBackground(new java.awt.Color(255, 255, 255));
        txt_attendence_state.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_attendence_state.setBorder(null);

        javax.swing.GroupLayout jPanel35Layout = new javax.swing.GroupLayout(jPanel35);
        jPanel35.setLayout(jPanel35Layout);
        jPanel35Layout.setHorizontalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel35Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_attendence_pincode, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)
                    .addComponent(txt_attendence_state)
                    .addComponent(txt_attendence_city)
                    .addComponent(txt_attendence_address))
                .addContainerGap())
        );
        jPanel35Layout.setVerticalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel35Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(txt_attendence_address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_attendence_city, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_attendence_pincode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_attendence_state, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btn_attendence_reset.setText("Reset");
        btn_attendence_reset.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_attendence_resetKeyPressed(evt);
            }
        });

        btn_attendence_view.setText("View");
        btn_attendence_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_attendence_viewKeyPressed(evt);
            }
        });

        btn_attendence_cancel.setText("Cancel");
        btn_attendence_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_attendence_cancelActionPerformed(evt);
            }
        });
        btn_attendence_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_attendence_cancelKeyPressed(evt);
            }
        });

        jLabel18.setText("Department");

        jPanel36.setBackground(new java.awt.Color(255, 255, 255));
        jPanel36.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Last Month Attendence Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jScrollPane18.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane18.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        tbl_attendence_attdncdetail.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbl_attendence_attdncdetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null}
            },
            new String [] {
                "TOTAL DAY", "TOTAL PRESENT", "TOTAL ABSENT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_attendence_attdncdetail.setAutoscrolls(false);
        tbl_attendence_attdncdetail.setRowHeight(25);
        tbl_attendence_attdncdetail.getTableHeader().setReorderingAllowed(false);
        jScrollPane18.setViewportView(tbl_attendence_attdncdetail);
        if (tbl_attendence_attdncdetail.getColumnModel().getColumnCount() > 0) {
            tbl_attendence_attdncdetail.getColumnModel().getColumn(0).setResizable(false);
            tbl_attendence_attdncdetail.getColumnModel().getColumn(1).setResizable(false);
            tbl_attendence_attdncdetail.getColumnModel().getColumn(2).setResizable(false);
        }

        javax.swing.GroupLayout jPanel36Layout = new javax.swing.GroupLayout(jPanel36);
        jPanel36.setLayout(jPanel36Layout);
        jPanel36Layout.setHorizontalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel36Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane18, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel36Layout.setVerticalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel36Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane18, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        txt_attendence_empname.setEditable(false);
        txt_attendence_empname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_attendence_empnameActionPerformed(evt);
            }
        });

        jLabel19.setText("Date of Birth");

        txt_attendence_gender.setEditable(false);

        txt_attendence_department.setEditable(false);

        btn_add.setText("Add");
        btn_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addActionPerformed(evt);
            }
        });

        btn_view.setText("View");
        btn_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnl_attendenceLayout = new javax.swing.GroupLayout(pnl_attendence);
        pnl_attendence.setLayout(pnl_attendenceLayout);
        pnl_attendenceLayout.setHorizontalGroup(
            pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_attendenceLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnl_attendenceLayout.createSequentialGroup()
                        .addGroup(pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnl_attendenceLayout.createSequentialGroup()
                                .addGroup(pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel10)
                                    .addComponent(txt_attendence_empname, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel16)
                                    .addComponent(txt_attendence_gender, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_attendence_dob, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel19))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel18)
                                    .addComponent(txt_attendence_department, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 256, Short.MAX_VALUE))
                            .addComponent(jPanel33, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_attendenceLayout.createSequentialGroup()
                        .addComponent(btn_attendence_reset, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_attendence_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_attendence_cancel))
                    .addGroup(pnl_attendenceLayout.createSequentialGroup()
                        .addComponent(jPanel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 70, Short.MAX_VALUE))
                    .addGroup(pnl_attendenceLayout.createSequentialGroup()
                        .addComponent(btn_add)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_view)))
                .addContainerGap())
        );

        pnl_attendenceLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_add, btn_attendence_cancel, btn_attendence_reset, btn_attendence_view, btn_view});

        pnl_attendenceLayout.setVerticalGroup(
            pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_attendenceLayout.createSequentialGroup()
                .addGroup(pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_attendenceLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jPanel35, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl_attendenceLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addGroup(pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(jLabel16)
                            .addComponent(jLabel19)
                            .addComponent(jLabel18))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_attendence_empname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_attendence_dob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_attendence_gender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_attendence_department, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_add, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_view, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel36, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_attendenceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_attendence_cancel)
                    .addComponent(btn_attendence_view)
                    .addComponent(btn_attendence_reset))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Attendence ", pnl_attendence);

        jPanel22.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        pnl_adv_report.setBackground(new java.awt.Color(255, 255, 255));

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel21.setText("Search By :");

        txt_adv_report.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_adv_reportCaretUpdate(evt);
            }
        });
        txt_adv_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_adv_reportKeyPressed(evt);
            }
        });

        btn_adv_report_search.setText("Search");
        btn_adv_report_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_adv_report_searchActionPerformed(evt);
            }
        });
        btn_adv_report_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_adv_report_searchKeyPressed(evt);
            }
        });

        jmc_adv_report.setVerifyInputWhenFocusTarget(false);
        jmc_adv_report.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_adv_reportPropertyChange(evt);
            }
        });
        jmc_adv_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_adv_reportKeyPressed(evt);
            }
        });

        jyc_adv_report.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_adv_reportPropertyChange(evt);
            }
        });
        jyc_adv_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jyc_adv_reportKeyPressed(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel22.setText("Month :");

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel23.setText("Year :");

        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_adv_report)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jmc_adv_report, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jyc_adv_report, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_adv_report_search)
                .addContainerGap())
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel23Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jmc_adv_report, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_adv_report, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btn_adv_report_search, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                        .addComponent(jyc_adv_report, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                        .addComponent(jLabel23, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                        .addComponent(jSeparator4, javax.swing.GroupLayout.Alignment.LEADING)))
                .addGap(10, 10, 10))
        );

        jPanel23Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_adv_report_search, jLabel21, jLabel22, jLabel23, jmc_adv_report, jyc_adv_report, txt_adv_report});

        btn_adv_report_view.setText("View");
        btn_adv_report_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_adv_report_viewKeyPressed(evt);
            }
        });

        btn_adv_report_reset.setText("Reset");
        btn_adv_report_reset.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_adv_report_resetKeyPressed(evt);
            }
        });

        btn_adv_report_cancel.setText("Cancel");
        btn_adv_report_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_adv_report_cancelKeyPressed(evt);
            }
        });

        jLabel24.setText("Total Employee :");

        txt_total_employee.setEditable(false);

        jLabel25.setText("Total Advance :");

        txt_total_advance.setEditable(false);

        jScrollPane8.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        jPanel17.setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane12.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane12.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tbl_adv_report.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID", "EMPLOYEE NAME", "DESIGNATION", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22 ", "23", "24", "25", "26", "27", "28", "29", "30", "31", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_adv_report.getTableHeader().setReorderingAllowed(false);
        tbl_adv_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_adv_reportKeyPressed(evt);
            }
        });
        jScrollPane12.setViewportView(tbl_adv_report);
        if (tbl_adv_report.getColumnModel().getColumnCount() > 0) {
            tbl_adv_report.getColumnModel().getColumn(0).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(1).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(1).setPreferredWidth(200);
            tbl_adv_report.getColumnModel().getColumn(2).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_adv_report.getColumnModel().getColumn(3).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(4).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(5).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(6).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(7).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(8).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(9).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(10).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(11).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(12).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(13).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(14).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(15).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(16).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(17).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(18).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(19).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(20).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(21).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(22).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(23).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(24).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(25).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(26).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(27).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(28).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(29).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(30).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(31).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(32).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(33).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(34).setResizable(false);
            tbl_adv_report.getColumnModel().getColumn(34).setPreferredWidth(100);
        }

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 1814, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 441, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 43, Short.MAX_VALUE))
        );

        jScrollPane8.setViewportView(jPanel17);

        btn_adv_report_reset1.setText("Print");
        btn_adv_report_reset1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_adv_report_reset1KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout pnl_adv_reportLayout = new javax.swing.GroupLayout(pnl_adv_report);
        pnl_adv_report.setLayout(pnl_adv_reportLayout);
        pnl_adv_reportLayout.setHorizontalGroup(
            pnl_adv_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnl_adv_reportLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_adv_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(pnl_adv_reportLayout.createSequentialGroup()
                        .addComponent(btn_adv_report_reset, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_adv_report_reset1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel24)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_total_employee, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_total_advance, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 437, Short.MAX_VALUE)
                        .addComponent(btn_adv_report_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_adv_report_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pnl_adv_reportLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txt_total_advance, txt_total_employee});

        pnl_adv_reportLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_adv_report_cancel, btn_adv_report_reset, btn_adv_report_view});

        pnl_adv_reportLayout.setVerticalGroup(
            pnl_adv_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_reportLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_adv_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_adv_report_reset)
                    .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_total_employee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel25)
                    .addComponent(txt_total_advance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_adv_report_view)
                    .addComponent(btn_adv_report_cancel)
                    .addComponent(btn_adv_report_reset1))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Advance Report Day wise", pnl_adv_report);

        pnl_attendence_report.setBackground(new java.awt.Color(255, 255, 255));

        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel32.setText("Search By :");

        txt_att_report_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_att_report_searchCaretUpdate(evt);
            }
        });
        txt_att_report_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_att_report_searchKeyPressed(evt);
            }
        });

        btn_att_report_search.setText("Search");
        btn_att_report_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_att_report_searchActionPerformed(evt);
            }
        });
        btn_att_report_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_searchKeyPressed(evt);
            }
        });

        jmc_att_report.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_att_reportPropertyChange(evt);
            }
        });
        jmc_att_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_att_reportKeyPressed(evt);
            }
        });

        jyc_att_report.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_att_reportPropertyChange(evt);
            }
        });
        jyc_att_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jyc_att_reportKeyPressed(evt);
            }
        });

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel33.setText("Month :");

        jLabel34.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel34.setText("Year :");

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_att_report_search, javax.swing.GroupLayout.DEFAULT_SIZE, 662, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jmc_att_report, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jyc_att_report, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_att_report_search)
                .addContainerGap())
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jSeparator5)
                        .addComponent(jmc_att_report, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, Short.MAX_VALUE)
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_att_report_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jyc_att_report, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                            .addComponent(jLabel34, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE))
                        .addComponent(btn_att_report_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel32, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        jPanel25Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_att_report_search, jLabel32, jLabel33, jLabel34, jmc_att_report, jyc_att_report, txt_att_report_search});

        btn_att_report_cancel.setText("Cancel");
        btn_att_report_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_cancelKeyPressed(evt);
            }
        });

        btn_att_report_view.setText("View");
        btn_att_report_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_viewKeyPressed(evt);
            }
        });

        btn_att_report_print.setText("Print");
        btn_att_report_print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_printKeyPressed(evt);
            }
        });

        jScrollPane14.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        tbl_att_report.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID", "EMPLOYEE NAME", "DESIGNATION", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22 ", "23", "24", "25", "26", "27", "28", "29", "30", "31", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_att_report.getTableHeader().setReorderingAllowed(false);
        tbl_att_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_att_reportKeyPressed(evt);
            }
        });
        jScrollPane14.setViewportView(tbl_att_report);
        if (tbl_att_report.getColumnModel().getColumnCount() > 0) {
            tbl_att_report.getColumnModel().getColumn(0).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(0).setPreferredWidth(100);
            tbl_att_report.getColumnModel().getColumn(1).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(1).setPreferredWidth(800);
            tbl_att_report.getColumnModel().getColumn(2).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(2).setPreferredWidth(500);
            tbl_att_report.getColumnModel().getColumn(3).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(4).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(5).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(6).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(7).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(8).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(9).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(10).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(11).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(12).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(13).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(14).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(15).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(16).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(17).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(18).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(19).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(20).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(21).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(22).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(23).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(24).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(24).setPreferredWidth(100);
            tbl_att_report.getColumnModel().getColumn(25).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(26).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(27).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(28).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(29).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(30).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(31).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(32).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(33).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(34).setResizable(false);
            tbl_att_report.getColumnModel().getColumn(34).setPreferredWidth(300);
        }

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jScrollPane14, javax.swing.GroupLayout.DEFAULT_SIZE, 1177, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane14, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
        );

        jScrollPane19.setViewportView(jPanel4);

        javax.swing.GroupLayout pnl_attendence_reportLayout = new javax.swing.GroupLayout(pnl_attendence_report);
        pnl_attendence_report.setLayout(pnl_attendence_reportLayout);
        pnl_attendence_reportLayout.setHorizontalGroup(
            pnl_attendence_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnl_attendence_reportLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_att_report_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_att_report_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_att_report_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jScrollPane19, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        pnl_attendence_reportLayout.setVerticalGroup(
            pnl_attendence_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_attendence_reportLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane19, javax.swing.GroupLayout.DEFAULT_SIZE, 471, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl_attendence_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_att_report_cancel)
                    .addComponent(btn_att_report_view)
                    .addComponent(btn_att_report_print))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Attendence Report Day wise", pnl_attendence_report);

        pnl_salary_report.setBackground(new java.awt.Color(255, 255, 255));

        btn_sal_report_save.setText("Save");
        btn_sal_report_save.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_sal_report_saveKeyPressed(evt);
            }
        });

        btn_sal_report_cancel.setText("Cancel");
        btn_sal_report_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_sal_report_cancelKeyPressed(evt);
            }
        });

        txt_salary_report_total_due.setEditable(false);

        jLabel11.setText("Total Salary Amt:");

        txt_salary_report_total_emp.setEditable(false);

        jLabel36.setText("Total Salary:");

        btn_sal_report_print.setText("Print");
        btn_sal_report_print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_sal_report_printKeyPressed(evt);
            }
        });

        btn_sal_report_reset.setText("Reset");
        btn_sal_report_reset.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_sal_report_resetKeyPressed(evt);
            }
        });

        txt_salary_report_total_adv.setEditable(false);

        jLabel37.setText("Total Due :");

        txt_salary_report_total_sal.setEditable(false);

        jLabel38.setText("Total Avance :");

        tbl_sal_report_month.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID", "EMPLOYEE NAME", "CONTACT NO.", "DESIGNATION", "MONTHLY SALARY", "NO. OF WORKING DAYS", "SALARY", "ADVANCE", "DUE AMOUNT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_sal_report_month.getTableHeader().setReorderingAllowed(false);
        tbl_sal_report_month.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_sal_report_monthKeyPressed(evt);
            }
        });
        jScrollPane13.setViewportView(tbl_sal_report_month);

        jPanel26.setPreferredSize(new java.awt.Dimension(457, 45));

        jLabel39.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel39.setText("Search By :");

        txt_salary_report_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_salary_report_searchCaretUpdate(evt);
            }
        });
        txt_salary_report_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_salary_report_searchKeyPressed(evt);
            }
        });

        btn_sal_report_search.setText("Search");
        btn_sal_report_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_sal_report_searchActionPerformed(evt);
            }
        });
        btn_sal_report_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_sal_report_searchKeyPressed(evt);
            }
        });

        jmc_sal_report.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_sal_reportPropertyChange(evt);
            }
        });
        jmc_sal_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_sal_reportKeyPressed(evt);
            }
        });

        jyc_sal_report.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_sal_reportPropertyChange(evt);
            }
        });
        jyc_sal_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jyc_sal_reportKeyPressed(evt);
            }
        });

        jLabel40.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel40.setText("Month :");

        jLabel41.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel41.setText("Year :");

        jSeparator6.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel26Layout = new javax.swing.GroupLayout(jPanel26);
        jPanel26.setLayout(jPanel26Layout);
        jPanel26Layout.setHorizontalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel26Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_salary_report_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jmc_sal_report, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jyc_sal_report, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_sal_report_search)
                .addContainerGap())
        );
        jPanel26Layout.setVerticalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel26Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_salary_report_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel39, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jSeparator6, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel40, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_sal_report_search, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                                .addComponent(jyc_sal_report, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                                .addComponent(jLabel41, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                                .addComponent(jmc_sal_report, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, Short.MAX_VALUE)))))
                .addGap(10, 10, 10))
        );

        jPanel26Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_sal_report_search, jLabel39, jLabel40, jLabel41, jmc_sal_report, jyc_sal_report});

        javax.swing.GroupLayout pnl_salary_reportLayout = new javax.swing.GroupLayout(pnl_salary_report);
        pnl_salary_report.setLayout(pnl_salary_reportLayout);
        pnl_salary_reportLayout.setHorizontalGroup(
            pnl_salary_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_salary_reportLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_salary_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane13)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_salary_reportLayout.createSequentialGroup()
                        .addComponent(btn_sal_report_reset, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_sal_report_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel36)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
                        .addComponent(txt_salary_report_total_emp, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_salary_report_total_sal, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel38)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_salary_report_total_adv, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel37)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_salary_report_total_due, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(37, 37, 37)
                        .addComponent(btn_sal_report_save, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_sal_report_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addComponent(jPanel26, javax.swing.GroupLayout.DEFAULT_SIZE, 1107, Short.MAX_VALUE)
        );

        pnl_salary_reportLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_sal_report_print, btn_sal_report_reset});

        pnl_salary_reportLayout.setVerticalGroup(
            pnl_salary_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_salary_reportLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel26, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_salary_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_salary_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_sal_report_cancel)
                        .addComponent(btn_sal_report_save)
                        .addComponent(jLabel37)
                        .addComponent(txt_salary_report_total_adv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel38)
                        .addComponent(txt_salary_report_total_sal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel11)
                        .addComponent(txt_salary_report_total_due, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_salary_report_total_emp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel36))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_salary_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_sal_report_reset)
                        .addComponent(btn_sal_report_print)))
                .addGap(11, 11, 11))
        );

        jTabbedPane1.addTab("Salary Report Month/Year wise", pnl_salary_report);

        pnl_adv_report1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel27.setText("Search By :");

        txt_sal_dis_report.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_sal_dis_reportCaretUpdate(evt);
            }
        });
        txt_sal_dis_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_sal_dis_reportKeyPressed(evt);
            }
        });

        btn_sal_dis_report_search.setText("Search");
        btn_sal_dis_report_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_sal_dis_report_searchActionPerformed(evt);
            }
        });
        btn_sal_dis_report_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_sal_dis_report_searchKeyPressed(evt);
            }
        });

        jmc_sal_dis_report.setVerifyInputWhenFocusTarget(false);
        jmc_sal_dis_report.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_sal_dis_reportPropertyChange(evt);
            }
        });
        jmc_sal_dis_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_sal_dis_reportKeyPressed(evt);
            }
        });

        jyc_sal_dis_report.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_sal_dis_reportPropertyChange(evt);
            }
        });
        jyc_sal_dis_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jyc_sal_dis_reportKeyPressed(evt);
            }
        });

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel28.setText("Month :");

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel29.setText("Year :");

        jSeparator9.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_sal_dis_report)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jmc_sal_dis_report, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jyc_sal_dis_report, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_sal_dis_report_search)
                .addContainerGap())
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel24Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jmc_sal_dis_report, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_sal_dis_report, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btn_sal_dis_report_search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jyc_sal_dis_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel29, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jSeparator9, javax.swing.GroupLayout.Alignment.LEADING)))
                .addGap(10, 10, 10))
        );

        btn_adv_report_view1.setText("View");
        btn_adv_report_view1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_adv_report_view1KeyPressed(evt);
            }
        });

        btn_adv_report_reset2.setText("Reset");
        btn_adv_report_reset2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_adv_report_reset2KeyPressed(evt);
            }
        });

        btn_adv_report_cancel1.setText("Cancel");
        btn_adv_report_cancel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_adv_report_cancel1KeyPressed(evt);
            }
        });

        jLabel30.setText("Total Employee :");

        txt_sal_dis_total_employee.setEditable(false);

        jLabel31.setText("Total Dis.Salary :");

        txt_sal_dis_total_advance1.setEditable(false);

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane15.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane15.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        tbl_sal_dis_report.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID", "EMPLOYEE NAME", "DESIGNATION", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22 ", "23", "24", "25", "26", "27", "28", "29", "30", "31", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_sal_dis_report.getTableHeader().setReorderingAllowed(false);
        tbl_sal_dis_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_sal_dis_reportKeyPressed(evt);
            }
        });
        jScrollPane15.setViewportView(tbl_sal_dis_report);
        if (tbl_sal_dis_report.getColumnModel().getColumnCount() > 0) {
            tbl_sal_dis_report.getColumnModel().getColumn(0).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(1).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(1).setPreferredWidth(200);
            tbl_sal_dis_report.getColumnModel().getColumn(2).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_sal_dis_report.getColumnModel().getColumn(3).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(4).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(5).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(6).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(7).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(8).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(9).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(10).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(11).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(12).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(13).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(14).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(15).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(16).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(17).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(18).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(19).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(20).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(21).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(22).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(23).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(24).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(25).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(26).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(27).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(28).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(29).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(30).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(31).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(32).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(33).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(34).setResizable(false);
            tbl_sal_dis_report.getColumnModel().getColumn(34).setPreferredWidth(100);
        }

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 1814, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane15, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
        );

        jScrollPane11.setViewportView(jPanel18);

        btn_adv_report_reset3.setText("Print");
        btn_adv_report_reset3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_adv_report_reset3KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout pnl_adv_report1Layout = new javax.swing.GroupLayout(pnl_adv_report1);
        pnl_adv_report1.setLayout(pnl_adv_report1Layout);
        pnl_adv_report1Layout.setHorizontalGroup(
            pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnl_adv_report1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(pnl_adv_report1Layout.createSequentialGroup()
                        .addComponent(btn_adv_report_reset2, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_adv_report_reset3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel30)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_sal_dis_total_employee, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel31)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_sal_dis_total_advance1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 431, Short.MAX_VALUE)
                        .addComponent(btn_adv_report_view1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_adv_report_cancel1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnl_adv_report1Layout.setVerticalGroup(
            pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report1Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_adv_report_reset2)
                    .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_sal_dis_total_employee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31)
                    .addComponent(txt_sal_dis_total_advance1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_adv_report_view1)
                    .addComponent(btn_adv_report_cancel1)
                    .addComponent(btn_adv_report_reset3))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnl_adv_report1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(pnl_adv_report1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Salary Distribution Report Day Wise", jPanel3);

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 22, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Reports", jPanel22);

        jPanel2.setBackground(new java.awt.Color(230, 79, 6));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Employee Management");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jTabbedPane2)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane2))
        );

        jScrollPane16.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane16)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane16, javax.swing.GroupLayout.DEFAULT_SIZE, 671, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void txt_attendence_cityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_attendence_cityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_attendence_cityActionPerformed

    private void txt_attendence_pincodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_attendence_pincodeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_attendence_pincodeActionPerformed

    private void txt_attendence_empnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_attendence_empnameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_attendence_empnameActionPerformed

    private void btn_salary_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_salary_printKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_salary_view.requestFocus();
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
           btn_salary_adv_view.requestFocus();
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            btn_salary_adv_view.requestFocus();
           }
    }//GEN-LAST:event_btn_salary_printKeyPressed

    private void btn_salary_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_salary_viewKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_salary_cancel.requestFocus();
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
         btn_salary_print.requestFocus();
           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            btn_salary_adv_view.requestFocus();
           } 
    }//GEN-LAST:event_btn_salary_viewKeyPressed

    private void btn_salary_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_salary_cancelKeyPressed
//        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
//            btn_salary_adv_search.requestFocus();
//           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
           btn_salary_view.requestFocus();
           }
//        if(evt.getKeyCode() == KeyEvent.VK_UP) {
//            .requestFocus();
//           }        
//        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
//            tbl_salary_adv.requestFocus();
//           }
    }//GEN-LAST:event_btn_salary_cancelKeyPressed

    private void txt_attendence_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_attendence_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_attendence_search.requestFocus();
           }        
//        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
//           btn_salary_reset.requestFocus();
//           }
//        if(evt.getKeyCode() == KeyEvent.VK_UP) {
//            btn_salary_adv_view.requestFocus();
//           } 
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_attendence.requestFocus();
           } 
    }//GEN-LAST:event_txt_attendence_searchKeyPressed

    private void btn_attendence_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_attendence_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jdc_att_date.requestFocus();
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
           txt_attendence_search.requestFocus();
           }
//        if(evt.getKeyCode() == KeyEvent.VK_UP) {
//            btn_salary_adv_view.requestFocus();
//           } 
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_attendence.requestFocus();
           }
    }//GEN-LAST:event_btn_attendence_searchKeyPressed

    private void btn_attendence_resetKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_attendence_resetKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_attendence_view.requestFocus();
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
           tbl_attendence.requestFocus();
           }
//        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
//            btn_salary_adv_view.requestFocus();
//           } 
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_attendence.requestFocus();
           }
    }//GEN-LAST:event_btn_attendence_resetKeyPressed

    private void btn_attendence_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_attendence_viewKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_attendence_cancel.requestFocus();
           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
           btn_attendence_reset.requestFocus();
           }
//        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
//            btn_salary_adv_view.requestFocus();
//           } 
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_attendence.requestFocus();
           }
    }//GEN-LAST:event_btn_attendence_viewKeyPressed

    private void btn_attendence_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_attendence_cancelKeyPressed
//        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
//            btn_attendence_search.requestFocus();
//           }        
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
//           btn_attendence_new.requestFocus();
           }
//        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
//            btn_salary_adv_view.requestFocus();
//           } 
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_attendence.requestFocus();
           }
    }//GEN-LAST:event_btn_attendence_cancelKeyPressed

    private void btn_salary_printActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salary_printActionPerformed
        
    }//GEN-LAST:event_btn_salary_printActionPerformed

    private void total_grand_dueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_total_grand_dueActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_total_grand_dueActionPerformed

    private void btn_salary_adv_addKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_salary_adv_addKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_salary_adv_view.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            tbl_salary_adv.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            txt_salary_sal_search.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
          btn_salary_print.requestFocus();
        }
    }//GEN-LAST:event_btn_salary_adv_addKeyPressed

    private void btn_salary_adv_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salary_adv_addActionPerformed
        Config.add_advance.onloadReset();
        Config.add_advance.setVisible(true);
    }//GEN-LAST:event_btn_salary_adv_addActionPerformed

    private void btn_salary_adv_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_salary_adv_viewKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
          btn_salary_print.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_salary_adv_add.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_salary_adv.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
          btn_salary_print.requestFocus();
        }
    }//GEN-LAST:event_btn_salary_adv_viewKeyPressed

    private void tbl_salary_advKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_salary_advKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_salary_adv_add.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_salary_adv_search.requestFocus();
        }
    }//GEN-LAST:event_tbl_salary_advKeyPressed

    private void btn_salary_adv_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_salary_adv_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            tbl_salary_adv.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_salary_adv_search.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_salary_adv.requestFocus();
        }
    }//GEN-LAST:event_btn_salary_adv_searchKeyPressed

    private void txt_salary_adv_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_salary_adv_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_salary_adv_search.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_salary_sal_search.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_salary_adv.requestFocus();
        }
    }//GEN-LAST:event_txt_salary_adv_searchKeyPressed

    private void btn_salary_sal_addKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_salary_sal_addKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_salary_sal_view.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            tbl_salary_sal.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_salary_sal.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
          btn_salary_print.requestFocus();
        }
    }//GEN-LAST:event_btn_salary_sal_addKeyPressed

    private void btn_salary_sal_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salary_sal_addActionPerformed
      Config.add_salary_info.onloadReset();
      Config.add_salary_info.setVisible(true);
    }//GEN-LAST:event_btn_salary_sal_addActionPerformed

    private void btn_salary_sal_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_salary_sal_viewKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            txt_salary_sal_search.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_salary_sal_add.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_salary_sal.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
          btn_salary_print.requestFocus();
        }
    }//GEN-LAST:event_btn_salary_sal_viewKeyPressed

    private void btn_salary_sal_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salary_sal_viewActionPerformed
        try {
            Config.view_salary_info.onloadReset(tbl_salary_sal_model.getValueAt(tbl_salary_sal.getSelectedRow(),0).toString());
            Config.view_salary_info.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_salary_sal_viewActionPerformed

    private void tbl_salary_salKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_salary_salKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_salary_sal_add.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_salary_sal_search.requestFocus();
        }
    }//GEN-LAST:event_tbl_salary_salKeyPressed

    private void btn_salary_sal_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_salary_sal_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            txt_salary_adv_search.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_salary_sal_search.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_salary_sal.requestFocus();
        }
    }//GEN-LAST:event_btn_salary_sal_searchKeyPressed

    private void txt_salary_sal_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_salary_sal_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_salary_sal_search.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            //            .requestFocus();
            //           }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_salary_sal.requestFocus();
        }
    }//GEN-LAST:event_txt_salary_sal_searchKeyPressed

    private void pnl_employeeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pnl_employeeKeyPressed
       
    }//GEN-LAST:event_pnl_employeeKeyPressed

    private void btn_emp_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_emp_printKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_emp_view.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            tbl_employee.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_employee.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_emp_view.requestFocus();
        }
    }//GEN-LAST:event_btn_emp_printKeyPressed

    private void btn_emp_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_emp_viewKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_emp_cancel.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_emp_print.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_employee.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_emp_cancel.requestFocus();
        }
    }//GEN-LAST:event_btn_emp_viewKeyPressed

    private void btn_emp_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_emp_viewActionPerformed
         try {
               Config.view_employee.onloadReset(tbl_employee_model.getValueAt(tbl_employee.getSelectedRow(), 0).toString());
               Config.view_employee.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,"Select any row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_emp_viewActionPerformed

    private void btn_emp_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_emp_cancelKeyPressed
        //        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_emp_view.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_employee.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            tbl_employee.requestFocus();
            //           }
    }//GEN-LAST:event_btn_emp_cancelKeyPressed

    private void tbl_employeeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_employeeKeyPressed
       
    }//GEN-LAST:event_tbl_employeeKeyPressed

    private void txt_emp_employeenameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_emp_employeenameActionPerformed
        
    }//GEN-LAST:event_txt_emp_employeenameActionPerformed

    private void btn_emp_newKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_emp_newKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            tbl_employee.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_emp_search.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_employee.requestFocus();
        }
    }//GEN-LAST:event_btn_emp_newKeyPressed

    private void btn_emp_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_emp_newActionPerformed
        Config.new_employee.onloadReset();
        Config.new_employee.setVisible(true);
    }//GEN-LAST:event_btn_emp_newActionPerformed

    private void btn_emp_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_emp_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_emp_new.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_emp_search.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_employee.requestFocus();
        }
    }//GEN-LAST:event_btn_emp_searchKeyPressed

    private void txt_emp_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_emp_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_emp_search.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            pnl_adv_report.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_employee.requestFocus();
        }

    }//GEN-LAST:event_txt_emp_searchKeyPressed

    private void btn_emp_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_emp_searchActionPerformed
    
    try {
        tbl_employee_model.setRowCount(0);
        if(!txt_emp_search.getText().equals("")){
            for (int i = 0; i < Config.config_employee_details.size(); i++) {
            EmployeeDetails ed = Config.config_employee_details.get(i);
            if(Config.config_profile_details.get(Config.id_profile_details.indexOf(ed.getProfile_id())).getName().toUpperCase().startsWith(txt_emp_search.getText().trim().toUpperCase())){
            tbl_employee_model.addRow(new Object[]{
            ed.getEmployee_id(),    
            Config.config_profile_details. get(Config.id_profile_details.indexOf(ed.getProfile_id())).getName(),
            Config.config_contact_details. get(Config.id_contact_details.indexOf(ed.getContact_id())).getContact_no(),    
            Config.config_contact_details. get(Config.id_contact_details.indexOf(ed.getContact_id())).getOther(),
            Config.config_contact_details. get(Config.id_contact_details.indexOf(ed.getContact_id())).getEmail(),
            Config.config_identity_details.get(Config.id_identity_details.indexOf(ed.getIdentity_id())).getIdentity_type(),
            Config.config_identity_details.get(Config.id_identity_details.indexOf(ed.getIdentity_id())).getIdentity_no(),
            Config.config_employee_details.get(i).getJoining_date(),
            Config.config_employee_details.get(i).getLeaving_date(),
            Config.config_employee_details.get(i).getMessage_id()
            });
            }
        }
    }else{
            for (int i = 0; i < Config.config_employee_details.size(); i++) {
            EmployeeDetails ed = Config.config_employee_details.get(i);
            if(Config.config_profile_details.get(Config.id_profile_details.indexOf(ed.getProfile_id())).getName().toUpperCase().startsWith(txt_emp_search.getText().trim().toUpperCase())){
            tbl_employee_model.addRow(new Object[]{
            ed.getEmployee_id(),    
            Config.config_profile_details.get(Config.id_profile_details.indexOf(ed.getProfile_id())).getName(),
            Config.config_contact_details.get(Config.id_contact_details.indexOf(ed.getContact_id())).getContact_no(),    
            Config.config_contact_details.get(Config.id_contact_details.indexOf(ed.getContact_id())).getOther(),
            Config.config_contact_details.get(Config.id_contact_details.indexOf(ed.getContact_id())).getEmail(),
            Config.config_identity_details.get(Config.id_identity_details.indexOf(ed.getIdentity_id())).getIdentity_type(),
            Config.config_identity_details.get(Config.id_identity_details.indexOf(ed.getIdentity_id())).getIdentity_no(),
            Config.config_employee_details.get(i).getJoining_date(),
            Config.config_employee_details.get(i).getLeaving_date(),
            Config.config_employee_details.get(i).getMessage_id()
            });
            }
        }
    }
    } catch (Exception e) {
      
    }
    txt_emp_address.setText("");
    txt_emp_city.setText("");
    txt_emp_pincode.setText("");
    txt_emp_state.setText("");
    txt_emp_employeename.setText("");
    txt_emp_dob.setText("");
    txt_emp_department.setText("");
    txt_emp_gender.setText("");
        
    }//GEN-LAST:event_btn_emp_searchActionPerformed

    private void tbl_employeeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tbl_employeeFocusGained
      setEmployeeData();
    }//GEN-LAST:event_tbl_employeeFocusGained

    private void tbl_employeeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_employeeMouseClicked
      try {
            if(evt.getClickCount()==1){
                try {
                    setEmployeeData();
                    onloadEmployeeMessage(tbl_employee_model.getValueAt(tbl_employee.getSelectedRow(),9).toString());
                    setAttendance();
                    setEmployeeOtherDetails();
                    setEmployeeSalary();
                } catch (Exception e) {
                    
                    onloadEmployeeMessage("");
                }
            }
            if(evt.getClickCount()==2){
                btn_emp_viewActionPerformed(null);
            }
        } catch (Exception e) {
            
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
      }
    }//GEN-LAST:event_tbl_employeeMouseClicked

    private void btn_emp_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_emp_cancelActionPerformed
      dispose();
    }//GEN-LAST:event_btn_emp_cancelActionPerformed

    private void btn_salary_adv_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salary_adv_searchActionPerformed
        String sql = "";
            String text=txt_salary_adv_search.getText().trim().toUpperCase()+"%";
                if (!txt_salary_adv_search.getText().equals("")) {
                     sql = "SELECT ad.* FROM advance_details as ad INNER JOIN employee_details AS ed ON ad.employee_id = ed.employee_id INNER JOIN profile_details AS pd ON ed.profile_id = pd.profile_id and UPPER(pd.name) like '"+text+"' and _month='"+(jmc_month.getMonth()+1)+"' and _year='"+jyc_year.getYear()+"'" ;
                } else {
                    sql = "SELECT ad.* FROM advance_details as ad INNER JOIN employee_details AS ed ON ad.employee_id = ed.employee_id INNER JOIN profile_details AS pd ON ed.profile_id = pd.profile_id and _month='"+(jmc_month.getMonth()+1)+"' and _year='"+jyc_year.getYear()+"'" ;
                }
//            (sql);
            setAdvanceTable(sql);
    }//GEN-LAST:event_btn_salary_adv_searchActionPerformed

    private void btn_salary_adv_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salary_adv_viewActionPerformed
        try {
           Config.view_advance.onloadReset(tbl_salary_adv_model.getValueAt(tbl_salary_adv.getSelectedRow(),0).toString());
           Config.view_advance.setVisible(true);
        } catch (Exception e) {
           JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_salary_adv_viewActionPerformed

    private void btn_attendence_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_attendence_cancelActionPerformed
       dispose();
    }//GEN-LAST:event_btn_attendence_cancelActionPerformed

    private void btn_salary_sal_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salary_sal_searchActionPerformed
        String sql = "";
            String text=txt_salary_sal_search.getText().trim().toUpperCase()+"%";
                if (!txt_salary_sal_search.getText().equals("")) {
                    sql = "SELECT ad.* FROM salary_distribution as ad INNER JOIN employee_details AS ed ON ad.employee_id = ed.employee_id INNER JOIN profile_details AS pd ON ed.profile_id = pd.profile_id and UPPER(pd.name) like '"+text+"' and _month='"+(jmc_month.getMonth()+1)+"' and _year='"+jyc_year.getYear()+"'" ;
                } else {
                    sql = "SELECT ad.* FROM salary_distribution as ad INNER JOIN employee_details AS ed ON ad.employee_id = ed.employee_id INNER JOIN profile_details AS pd ON ed.profile_id = pd.profile_id and _month='"+(jmc_month.getMonth()+1)+"' and _year='"+jyc_year.getYear()+"'" ;
                }
//            (sql);
            setSalaryTable(sql);
    }//GEN-LAST:event_btn_salary_sal_searchActionPerformed

    private void txt_emp_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_emp_searchCaretUpdate
        btn_emp_searchActionPerformed(null);
    }//GEN-LAST:event_txt_emp_searchCaretUpdate

    private void txt_salary_sal_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_salary_sal_searchCaretUpdate
        onloadSalaryInformation();
        btn_salary_sal_searchActionPerformed(null);
        
        if (txt_salary_sal_search.getText().equals("")) {
            txt_salary_adv_search.setText("");            
        }else{
//       btn_salary_sal_searchActionPerformed(null);
        }
        
    }//GEN-LAST:event_txt_salary_sal_searchCaretUpdate

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Config.salary_management.onloadReset();
        Config.salary_management.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jmc_monthPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_monthPropertyChange
        jmc_month_adv.setMonth(jmc_month.getMonth());
        btn_salary_sal_searchActionPerformed(null);
       
    }//GEN-LAST:event_jmc_monthPropertyChange

    private void jyc_yearPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_yearPropertyChange
//        jyc_year_adv.setYear(jyc_year.getYear());
        btn_salary_sal_searchActionPerformed(null);
    }//GEN-LAST:event_jyc_yearPropertyChange

    private void jmc_month_advPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_month_advPropertyChange
        jmc_month.setMonth(jmc_month_adv.getMonth());
        btn_salary_adv_searchActionPerformed(null);
    }//GEN-LAST:event_jmc_month_advPropertyChange

    private void jyc_year_advPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_year_advPropertyChange
//        jyc_year.setYear(jyc_year_adv.getYear());
        btn_salary_adv_searchActionPerformed(null);
    }//GEN-LAST:event_jyc_year_advPropertyChange

    private void txt_salary_adv_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_salary_adv_searchCaretUpdate
        btn_salary_adv_searchActionPerformed(null);
         
    }//GEN-LAST:event_txt_salary_adv_searchCaretUpdate

    private void tbl_salary_salFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tbl_salary_salFocusGained
       setData(tbl_employee_model.getValueAt(tbl_salary_sal.getSelectedRow(), 5).toString());
    }//GEN-LAST:event_tbl_salary_salFocusGained

    private void tbl_salary_salMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_salary_salMouseClicked
         try {
            String employee_id= tbl_salary_sal_model.getValueAt(tbl_salary_sal.getSelectedRow(), 5).toString();
            String message_id=tbl_salary_sal_model.getValueAt(tbl_salary_sal.getSelectedRow(), 4).toString();
            if(evt.getClickCount()==1){
                try {
                    setname(employee_id);
                    setData(employee_id);
                    onloadSalaryMessage(message_id);
                } catch (Exception e) {
                    onloadSalaryMessage("");
                }
            }if(evt.getClickCount()==2){
                btn_salary_sal_viewActionPerformed(null);
            }
      } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
      }
    }//GEN-LAST:event_tbl_salary_salMouseClicked

    private void tbl_salary_salKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_salary_salKeyReleased
       
      try {
      String employee_id= tbl_salary_sal_model.getValueAt(tbl_salary_sal.getSelectedRow(), 5).toString();
      String message_id=tbl_salary_sal_model.getValueAt(tbl_salary_sal.getSelectedRow(), 4).toString(); 
      setname(employee_id);        
      setData(employee_id);
      onloadSalaryMessage(message_id);
      } catch (Exception e) {
      onloadEmployeeMessage("");
      }
    }//GEN-LAST:event_tbl_salary_salKeyReleased

    private void btn_salary_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salary_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_salary_cancelActionPerformed

    private void tbl_employeePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tbl_employeePropertyChange
        
    }//GEN-LAST:event_tbl_employeePropertyChange

    private void tbl_employeeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_employeeKeyReleased
      try {
      setEmployeeData();
      setAttendance();    
      onloadEmployeeMessage(tbl_employee_model.getValueAt(tbl_employee.getSelectedRow(),9).toString());
      setEmployeeOtherDetails();
              setEmployeeSalary();
      } catch (Exception e) {
      onloadEmployeeMessage("");
      }
    }//GEN-LAST:event_tbl_employeeKeyReleased

    private void tbl_salary_advMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_salary_advMouseClicked
        try {
           if(evt.getClickCount()==1){
            try {
                    onloadAdvanceMessage(tbl_salary_adv_model.getValueAt(tbl_salary_adv.getSelectedRow(),4).toString());
                } catch (Exception e) {
                    onloadEmployeeMessage("");
                }
            }if(evt.getClickCount()==2){
                btn_salary_adv_viewActionPerformed(null);
            }
      } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
      }
    }//GEN-LAST:event_tbl_salary_advMouseClicked

    private void btn_sal_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_sal_deleteActionPerformed
      try {
         int sd = JOptionPane.showConfirmDialog(this,"Are you really want to delete this entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
         if (sd == 0){
            if(Config.salary_distribution_mgr.delSalaryDistribution(tbl_salary_sal_model.getValueAt(tbl_salary_sal.getSelectedRow(), 0).toString())){
                        Config.employee_management.onloadSalaryInfo();
                        JOptionPane.showMessageDialog(this, "Salary delete successfully.", "Creation successful", JOptionPane.NO_OPTION);
                    } else {
                        JOptionPane.showMessageDialog(this, "Error in salary delete.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
         }
         
        } catch (Exception e) {            
        }
    }//GEN-LAST:event_btn_sal_deleteActionPerformed

    private void tbl_attendenceKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_attendenceKeyPressed
      
    }//GEN-LAST:event_tbl_attendenceKeyPressed

    private void tbl_attendenceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_attendenceMouseClicked
    try {
            if(evt.getClickCount()==1){
                try {
                    setEmployeeAttendance();
                    setAttendanceEmployeeData();
                    onloadAttendanceMessage(tbl_attendence_model.getValueAt(tbl_attendence.getSelectedRow(),7).toString());
                } catch (Exception e) {
                    onloadAttendanceMessage("");
                }
            }
            if(evt.getClickCount()==2){
                btn_viewActionPerformed(null);
            }
        } catch (Exception e) {
            
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
      }
    }//GEN-LAST:event_tbl_attendenceMouseClicked

    private void btn_adv_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_adv_deleteActionPerformed
     try {
         int ad = JOptionPane.showConfirmDialog(this,"Are you really want to delete this entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
         if (ad == 0){
            if(Config.advnace_details_mgr.delAdvanceDetails(tbl_salary_adv_model.getValueAt(tbl_salary_adv.getSelectedRow(), 0).toString())){
                        Config.employee_management.onloadAdvance();
                        JOptionPane.showMessageDialog(this, "Advance delete successfully.", "Creation successful", JOptionPane.NO_OPTION);
                    } else {
                        JOptionPane.showMessageDialog(this, "Error in advance delete.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
         }
        } catch (Exception e) {            
        }
    }//GEN-LAST:event_btn_adv_deleteActionPerformed

    private void jyc_yearMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jyc_yearMouseClicked
    }//GEN-LAST:event_jyc_yearMouseClicked

    private void btn_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addActionPerformed
         Config.attendence.onloadReset();
         Config.attendence.setVisible(true);
    }//GEN-LAST:event_btn_addActionPerformed

    private void btn_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewActionPerformed
        Config.view_attendence.onloadReset(tbl_attendence_model.getValueAt(tbl_attendence.getSelectedRow(), 0).toString());
        Config.view_attendence.setVisible(true);
    }//GEN-LAST:event_btn_viewActionPerformed

    private void tbl_salary_advKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_salary_advKeyReleased
        try {
        onloadAdvanceMessage(tbl_salary_adv_model.getValueAt(tbl_salary_adv.getSelectedRow(),4).toString());
        } catch (Exception e) {
        onloadAdvanceMessage("");
        }
    }//GEN-LAST:event_tbl_salary_advKeyReleased

    private void jdc_att_datePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_att_datePropertyChange
        btn_attendence_searchActionPerformed(null);
    }//GEN-LAST:event_jdc_att_datePropertyChange

    private void jdc_att_dateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jdc_att_dateMouseClicked

    }//GEN-LAST:event_jdc_att_dateMouseClicked

    private void tbl_attendenceKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_attendenceKeyReleased
      try {
      setEmployeeAttendance();
      setAttendanceEmployeeData();
      onloadAttendanceMessage(tbl_attendence_model.getValueAt(tbl_attendence.getSelectedRow(),7).toString());
      } catch (Exception e) {
      onloadAttendanceMessage("");
      }
    }//GEN-LAST:event_tbl_attendenceKeyReleased

    private void btn_attendence_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_attendence_searchActionPerformed
        try {
            String sql = "";
            String text=txt_attendence_search.getText().trim().toUpperCase()+"%";
                if (!txt_attendence_search.getText().equals("")) {
                    sql = "SELECT ad.* FROM attendence_details as ad INNER JOIN employee_details AS ed ON ad.employee_id = ed.employee_id INNER JOIN profile_details AS pd ON ed.profile_id = pd.profile_id and UPPER(pd.name) like '"+text+"' and _date='"+sdf.format(jdc_att_date.getDate())+"'";
                } else {
                    sql = "SELECT ad.* FROM attendence_details as ad INNER JOIN employee_details AS ed ON ad.employee_id = ed.employee_id INNER JOIN profile_details AS pd ON ed.profile_id = pd.profile_id and _date='"+sdf.format(jdc_att_date.getDate())+"'";
                }
//            (sql);
            setAttendanceTable(sql);
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btn_attendence_searchActionPerformed

    private void txt_attendence_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_attendence_searchCaretUpdate
        btn_attendence_searchActionPerformed(null);
    }//GEN-LAST:event_txt_attendence_searchCaretUpdate

    private void jyc_sal_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jyc_sal_reportKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_sal_report_search.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jmc_sal_report.requestFocus();
        }
    }//GEN-LAST:event_jyc_sal_reportKeyPressed

    private void jmc_sal_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_sal_reportKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jyc_sal_report.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_salary_report_search.requestFocus();
        }
    }//GEN-LAST:event_jmc_sal_reportKeyPressed

    private void btn_sal_report_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_sal_report_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            tbl_sal_report_month.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jyc_sal_report.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_sal_report_month.requestFocus();
        }
    }//GEN-LAST:event_btn_sal_report_searchKeyPressed

    private void txt_salary_report_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_salary_report_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jmc_sal_report.requestFocus();
        }if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_sal_report_month.requestFocus();
        }
    }//GEN-LAST:event_txt_salary_report_searchKeyPressed

    private void tbl_sal_report_monthKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_sal_report_monthKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_sal_report_reset.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_adv_report_search.requestFocus();
        }

    }//GEN-LAST:event_tbl_sal_report_monthKeyPressed

    private void btn_sal_report_resetKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_sal_report_resetKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_sal_report_print.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            tbl_sal_report_month.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            tbl_sal_report.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_sal_report_month.requestFocus();
        }
    }//GEN-LAST:event_btn_sal_report_resetKeyPressed

    private void btn_sal_report_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_sal_report_printKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_sal_report_save.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_sal_report_reset.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            tbl_sal_report.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_sal_report_month.requestFocus();
        }
    }//GEN-LAST:event_btn_sal_report_printKeyPressed

    private void btn_sal_report_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_sal_report_cancelKeyPressed
        //        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            //            btn_s.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_sal_report_save.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            tbl_sal_report.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_sal_report_month.requestFocus();
        }
    }//GEN-LAST:event_btn_sal_report_cancelKeyPressed

    private void btn_sal_report_saveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_sal_report_saveKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_sal_report_cancel.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_sal_report_print.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            tbl_sal_report.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_sal_report_month.requestFocus();
        }
    }//GEN-LAST:event_btn_sal_report_saveKeyPressed

    private void btn_att_report_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_printKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_printKeyPressed

    private void tbl_att_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_att_reportKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_att_report_print.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_att_report_search.requestFocus();
        }
    }//GEN-LAST:event_tbl_att_reportKeyPressed

    private void btn_att_report_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_viewKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_att_report_cancel.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_att_report_view.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            btn_salary_adv_view.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_att_report.requestFocus();
        }
    }//GEN-LAST:event_btn_att_report_viewKeyPressed

    private void btn_att_report_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_cancelKeyPressed
        //         if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            //            btn_adv_report_view.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_att_report_view.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            btn_salary_adv_view.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_att_report.requestFocus();
        }
    }//GEN-LAST:event_btn_att_report_cancelKeyPressed

    private void jyc_att_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jyc_att_reportKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_att_report_search.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jmc_att_report.requestFocus();
        }
    }//GEN-LAST:event_jyc_att_reportKeyPressed

    private void jmc_att_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_att_reportKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jyc_att_report.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_att_report_search.requestFocus();
        }
    }//GEN-LAST:event_jmc_att_reportKeyPressed

    private void btn_att_report_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            tbl_att_report.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jyc_att_report.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_att_report.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            tbl_adv_report.requestFocus();
            //           }
    }//GEN-LAST:event_btn_att_report_searchKeyPressed

    private void btn_att_report_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_att_report_searchActionPerformed
        String sql  = "SELECT * FROM attendence_details where _month='"+(jmc_att_report.getMonth()+1)+"' and _year='"+jyc_att_report.getYear()+"' order by  STR_TO_DATE(_date,'%d/%m/%Y') ";
//        (sql);
        setReportAttendanceTable(sql);
    }//GEN-LAST:event_btn_att_report_searchActionPerformed

    private void txt_att_report_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_att_report_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jmc_att_report.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            //           btn_adv_report_view.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_att_report.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            tbl_adv_report.requestFocus();
            //           }
    }//GEN-LAST:event_txt_att_report_searchKeyPressed

    private void txt_att_report_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_att_report_searchCaretUpdate
        btn_att_report_searchActionPerformed(null);
    }//GEN-LAST:event_txt_att_report_searchCaretUpdate

    private void btn_adv_report_reset1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_adv_report_reset1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_adv_report_reset1KeyPressed

    private void tbl_adv_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_adv_reportKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_adv_report_reset.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_adv_report_search.requestFocus();
        }
    }//GEN-LAST:event_tbl_adv_reportKeyPressed

    private void btn_adv_report_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_adv_report_cancelKeyPressed
        //        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            //            btn_adv_report_view.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_adv_report_view.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            btn_salary_adv_view.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_adv_report.requestFocus();
        }
    }//GEN-LAST:event_btn_adv_report_cancelKeyPressed

    private void btn_adv_report_resetKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_adv_report_resetKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_adv_report_view.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            tbl_adv_report.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            btn_salary_adv_view.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_adv_report.requestFocus();
        }
    }//GEN-LAST:event_btn_adv_report_resetKeyPressed

    private void btn_adv_report_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_adv_report_viewKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_adv_report_cancel.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_adv_report_reset.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            btn_salary_adv_view.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_adv_report.requestFocus();
        }
    }//GEN-LAST:event_btn_adv_report_viewKeyPressed

    private void jyc_adv_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jyc_adv_reportKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_adv_report_search.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jmc_adv_report.requestFocus();
        }
    }//GEN-LAST:event_jyc_adv_reportKeyPressed

    private void jmc_adv_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_adv_reportKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jyc_adv_report.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_adv_report.requestFocus();
        }

    }//GEN-LAST:event_jmc_adv_reportKeyPressed

    private void btn_adv_report_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_adv_report_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            tbl_adv_report.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            jyc_adv_report.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_adv_report.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            tbl_attendence.requestFocus();
            //           }
    }//GEN-LAST:event_btn_adv_report_searchKeyPressed

    private void txt_adv_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_adv_reportKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jmc_adv_report.requestFocus();
            //           jmc_adv_report.set;
        }

        //        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            //           btn_attendence_new.requestFocus();
            //           }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            btn_salary_adv_view.requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_adv_report.requestFocus();
        }
    }//GEN-LAST:event_txt_adv_reportKeyPressed

    private void jmc_att_reportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_att_reportPropertyChange
        btn_att_report_searchActionPerformed(null);
    }//GEN-LAST:event_jmc_att_reportPropertyChange

    private void jyc_att_reportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_att_reportPropertyChange
        btn_att_report_searchActionPerformed(null);
    }//GEN-LAST:event_jyc_att_reportPropertyChange

    private void btn_adv_report_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_adv_report_searchActionPerformed
        setReportAdvanceTable("");
    }//GEN-LAST:event_btn_adv_report_searchActionPerformed

    private void txt_adv_reportCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_adv_reportCaretUpdate
        btn_adv_report_searchActionPerformed(null);
    }//GEN-LAST:event_txt_adv_reportCaretUpdate

    private void jmc_adv_reportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_adv_reportPropertyChange
        btn_adv_report_searchActionPerformed(null);
    }//GEN-LAST:event_jmc_adv_reportPropertyChange

    private void jyc_adv_reportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_adv_reportPropertyChange
        btn_adv_report_searchActionPerformed(null);
    }//GEN-LAST:event_jyc_adv_reportPropertyChange

    private void txt_salary_report_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_salary_report_searchCaretUpdate
        btn_sal_report_searchActionPerformed(null);
    }//GEN-LAST:event_txt_salary_report_searchCaretUpdate

    private void btn_sal_report_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_sal_report_searchActionPerformed

        String sql = "";
            String text=txt_salary_report_search.getText().trim().toUpperCase()+"%";
                if (!txt_salary_report_search.getText().equals("")) {
                    sql = "SELECT a.employee_id,a.salary_id,a.salary,pd.name,\n" +
                    "(select count(*) from attendence_details c where c.employee_id = b.employee_id and _month='"+(jmc_sal_report.getMonth()+1)+"' and _year='"+jyc_sal_report.getYear()+"' and status='true') as days,\n" +
                    "(select COALESCE(sum(amount),0) from salary_distribution c where c.employee_id = b.employee_id and _month='"+(jmc_sal_report.getMonth()+1)+"' and _year='"+jyc_sal_report.getYear()+"' ) as amount ,\n" +
                    "(select COALESCE(sum(amount),0) from advance_details c where c.employee_id = b.employee_id and _month='"+(jmc_sal_report.getMonth()+1)+"' and _year='"+jyc_sal_report.getYear()+"') as advance\n" +
                    "FROM salary_details a\n" +
                    "INNER JOIN\n" +
                    "(SELECT employee_id,salary_id,max(STR_TO_DATE(_date,'%d/%m/%Y')) max_date FROM salary_details GROUP BY employee_id,_month,_year having  _month='"+(jmc_sal_report.getMonth()+1)+"' and _year='"+jyc_sal_report.getYear()+"') b\n" +
                    "INNER JOIN employee_details AS ed ON b.employee_id = ed.employee_id\n" +
                    "INNER JOIN profile_details AS pd ON ed.profile_id = pd.profile_id\n" +
                    "where a.employee_id = b.employee_id AND STR_TO_DATE(a._date,'%d/%m/%Y') = b.max_date and UPPER(pd.name) like '"+text+"'" ;
                } else {
                    sql = "SELECT a.employee_id,a.salary_id,a.salary,pd.name,\n" +
                    "(select count(*) from attendence_details c where c.employee_id = b.employee_id and _month='"+(jmc_sal_report.getMonth()+1)+"' and _year='"+jyc_sal_report.getYear()+"' and status='true') as days,\n" +
                    "(select COALESCE(sum(amount),0) from salary_distribution c where c.employee_id = b.employee_id and _month='"+(jmc_sal_report.getMonth()+1)+"' and _year='"+jyc_sal_report.getYear()+"' ) as amount ,\n" +
                    "(select COALESCE(sum(amount),0) from advance_details c where c.employee_id = b.employee_id and _month='"+(jmc_sal_report.getMonth()+1)+"' and _year='"+jyc_sal_report.getYear()+"') as advance\n" +
                    "FROM salary_details a\n" +
                    "INNER JOIN\n" +
                    "(SELECT employee_id,salary_id,max(STR_TO_DATE(_date,'%d/%m/%Y')) max_date FROM salary_details GROUP BY employee_id,_month,_year having  _month='"+(jmc_sal_report.getMonth()+1)+"' and _year='"+jyc_sal_report.getYear()+"') b\n" +
                    "INNER JOIN employee_details AS ed ON b.employee_id = ed.employee_id\n" +
                    "INNER JOIN profile_details AS pd ON ed.profile_id = pd.profile_id\n" +
                    "where a.employee_id = b.employee_id AND STR_TO_DATE(a._date,'%d/%m/%Y') = b.max_date " ;
                }
            setReportSalaryTable(sql);
    }//GEN-LAST:event_btn_sal_report_searchActionPerformed

    private void jmc_sal_reportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_sal_reportPropertyChange
        btn_sal_report_searchActionPerformed(null);
    }//GEN-LAST:event_jmc_sal_reportPropertyChange

    private void jyc_sal_reportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_sal_reportPropertyChange
        btn_sal_report_searchActionPerformed(null);
    }//GEN-LAST:event_jyc_sal_reportPropertyChange

    private void txt_sal_dis_reportCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_sal_dis_reportCaretUpdate
        btn_sal_dis_report_searchActionPerformed(null);
    }//GEN-LAST:event_txt_sal_dis_reportCaretUpdate

    private void txt_sal_dis_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_sal_dis_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_sal_dis_reportKeyPressed

    private void btn_sal_dis_report_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_sal_dis_report_searchActionPerformed
    setReportSalaryDisTable();
    }//GEN-LAST:event_btn_sal_dis_report_searchActionPerformed

    private void btn_sal_dis_report_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_sal_dis_report_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_sal_dis_report_searchKeyPressed

    private void jmc_sal_dis_reportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_sal_dis_reportPropertyChange
        btn_sal_dis_report_searchActionPerformed(null);
    }//GEN-LAST:event_jmc_sal_dis_reportPropertyChange

    private void jmc_sal_dis_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_sal_dis_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmc_sal_dis_reportKeyPressed

    private void jyc_sal_dis_reportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_sal_dis_reportPropertyChange
        btn_sal_dis_report_searchActionPerformed(null);
    }//GEN-LAST:event_jyc_sal_dis_reportPropertyChange

    private void jyc_sal_dis_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jyc_sal_dis_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jyc_sal_dis_reportKeyPressed

    private void btn_adv_report_view1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_adv_report_view1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_adv_report_view1KeyPressed

    private void btn_adv_report_reset2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_adv_report_reset2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_adv_report_reset2KeyPressed

    private void btn_adv_report_cancel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_adv_report_cancel1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_adv_report_cancel1KeyPressed

    private void tbl_sal_dis_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_sal_dis_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_sal_dis_reportKeyPressed

    private void btn_adv_report_reset3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_adv_report_reset3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_adv_report_reset3KeyPressed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_add;
    private javax.swing.JButton btn_adv_delete;
    private javax.swing.JButton btn_adv_report_cancel;
    private javax.swing.JButton btn_adv_report_cancel1;
    private javax.swing.JButton btn_adv_report_reset;
    private javax.swing.JButton btn_adv_report_reset1;
    private javax.swing.JButton btn_adv_report_reset2;
    private javax.swing.JButton btn_adv_report_reset3;
    private javax.swing.JButton btn_adv_report_search;
    private javax.swing.JButton btn_adv_report_view;
    private javax.swing.JButton btn_adv_report_view1;
    private javax.swing.JButton btn_att_report_cancel;
    private javax.swing.JButton btn_att_report_print;
    private javax.swing.JButton btn_att_report_search;
    private javax.swing.JButton btn_att_report_view;
    private javax.swing.JButton btn_attendence_cancel;
    private javax.swing.JButton btn_attendence_reset;
    private javax.swing.JButton btn_attendence_search;
    private javax.swing.JButton btn_attendence_view;
    private javax.swing.JButton btn_emp_cancel;
    private javax.swing.JButton btn_emp_new;
    private javax.swing.JButton btn_emp_print;
    private javax.swing.JButton btn_emp_search;
    private javax.swing.JButton btn_emp_view;
    private javax.swing.JButton btn_sal_delete;
    private javax.swing.JButton btn_sal_dis_report_search;
    private javax.swing.JButton btn_sal_report_cancel;
    private javax.swing.JButton btn_sal_report_print;
    private javax.swing.JButton btn_sal_report_reset;
    private javax.swing.JButton btn_sal_report_save;
    private javax.swing.JButton btn_sal_report_search;
    private javax.swing.JButton btn_salary_adv_add;
    private javax.swing.JButton btn_salary_adv_search;
    private javax.swing.JButton btn_salary_adv_view;
    private javax.swing.JButton btn_salary_cancel;
    private javax.swing.JButton btn_salary_print;
    private javax.swing.JButton btn_salary_sal_add;
    private javax.swing.JButton btn_salary_sal_search;
    private javax.swing.JButton btn_salary_sal_view;
    private javax.swing.JButton btn_salary_view;
    private javax.swing.JButton btn_view;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel35;
    private javax.swing.JPanel jPanel36;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane17;
    private javax.swing.JScrollPane jScrollPane18;
    private javax.swing.JScrollPane jScrollPane19;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private com.toedter.calendar.JDateChooser jdc_att_date;
    private com.toedter.calendar.JMonthChooser jmc_adv_report;
    private com.toedter.calendar.JMonthChooser jmc_att_report;
    private com.toedter.calendar.JMonthChooser jmc_month;
    private com.toedter.calendar.JMonthChooser jmc_month_adv;
    private com.toedter.calendar.JMonthChooser jmc_sal_dis_report;
    private com.toedter.calendar.JMonthChooser jmc_sal_report;
    private com.toedter.calendar.JYearChooser jyc_adv_report;
    private com.toedter.calendar.JYearChooser jyc_att_report;
    private com.toedter.calendar.JYearChooser jyc_sal_dis_report;
    private com.toedter.calendar.JYearChooser jyc_sal_report;
    private com.toedter.calendar.JYearChooser jyc_year;
    private com.toedter.calendar.JYearChooser jyc_year_adv;
    private javax.swing.JPanel pnl_adv_report;
    private javax.swing.JPanel pnl_adv_report1;
    private javax.swing.JPanel pnl_advance;
    private javax.swing.JPanel pnl_attendence;
    private javax.swing.JPanel pnl_attendence_report;
    private javax.swing.JPanel pnl_employee;
    private javax.swing.JPanel pnl_salary;
    private javax.swing.JPanel pnl_salary_info;
    private javax.swing.JPanel pnl_salary_report;
    private javax.swing.JTable tbl_adv_report;
    private javax.swing.JTable tbl_att_report;
    private javax.swing.JTable tbl_attendence;
    private javax.swing.JTable tbl_attendence_attdncdetail;
    private javax.swing.JTable tbl_emp_attendence;
    private javax.swing.JTable tbl_emp_other_details;
    private javax.swing.JTable tbl_emp_payment;
    private javax.swing.JTable tbl_employee;
    private javax.swing.JTable tbl_sal_dis_report;
    private javax.swing.JTable tbl_sal_report_month;
    private javax.swing.JTable tbl_salary_adv;
    private javax.swing.JTable tbl_salary_sal;
    private javax.swing.JTextField total_grand_due;
    private javax.swing.JTextField txt_adv_report;
    private javax.swing.JTextField txt_advance_total;
    private javax.swing.JTextField txt_att_report_search;
    private javax.swing.JTextField txt_attendence_address;
    private javax.swing.JTextField txt_attendence_city;
    private javax.swing.JTextField txt_attendence_department;
    private javax.swing.JTextField txt_attendence_dob;
    private javax.swing.JTextField txt_attendence_empname;
    private javax.swing.JTextField txt_attendence_gender;
    private javax.swing.JTextPane txt_attendence_message;
    private javax.swing.JTextField txt_attendence_pincode;
    private javax.swing.JTextField txt_attendence_search;
    private javax.swing.JTextField txt_attendence_state;
    private javax.swing.JTextField txt_emp_address;
    private javax.swing.JTextField txt_emp_city;
    private javax.swing.JTextField txt_emp_department;
    private javax.swing.JTextField txt_emp_dob;
    private javax.swing.JTextField txt_emp_employeename;
    private javax.swing.JTextField txt_emp_gender;
    private javax.swing.JTextPane txt_emp_message;
    private javax.swing.JTextField txt_emp_pincode;
    private javax.swing.JTextField txt_emp_search;
    private javax.swing.JTextField txt_emp_state;
    private javax.swing.JTextField txt_grand_amount;
    private javax.swing.JTextField txt_grand_salary;
    private javax.swing.JTextField txt_sal_dis_report;
    private javax.swing.JTextField txt_sal_dis_total_advance1;
    private javax.swing.JTextField txt_sal_dis_total_employee;
    private javax.swing.JTextField txt_salary_adv_search;
    private javax.swing.JTextPane txt_salary_message;
    private javax.swing.JTextField txt_salary_report_search;
    private javax.swing.JTextField txt_salary_report_total_adv;
    private javax.swing.JTextField txt_salary_report_total_due;
    private javax.swing.JTextField txt_salary_report_total_emp;
    private javax.swing.JTextField txt_salary_report_total_sal;
    private javax.swing.JTextField txt_salary_sal_search;
    private javax.swing.JTextField txt_salary_total;
    private javax.swing.JTextField txt_total_advance;
    private javax.swing.JTextField txt_total_employee;
    // End of variables declaration//GEN-END:variables
    
    public void onloadReset(){
      employee_id="";
      onloadEmployee();
      onloadAdvance();
      onloadSalaryInfo();
      onloadAttendence();
      onloadReport();
    }
    public void onloadEmployee(){
      btn_emp_searchActionPerformed(null);
    }
    public void onloadAdvance(){
      btn_salary_adv_searchActionPerformed(null);
    }
    public void onloadSalaryInfo(){
      btn_salary_sal_searchActionPerformed(null);
    }
    public void onloadAttendence(){
      jdc_att_datePropertyChange(null);
    }
    public void setEmployeeData(){
        try {
        EmployeeDetails ed = Config.config_employee_details.get(Config.id_employee_details.indexOf(tbl_employee_model.getValueAt(tbl_employee.getSelectedRow(), 0).toString()));
        txt_emp_employeename.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(ed.getProfile_id())).getName());
        txt_emp_gender.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(ed.getProfile_id())).getGender());
        txt_emp_dob.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(ed.getProfile_id())).getDob());
        txt_emp_address.setText(Config.config_address_details.get(Config.id_address_details.indexOf(ed.getAddress_id())).getAddress_1());
        txt_emp_city.setText(Config.config_address_details.get(Config.id_address_details.indexOf(ed.getAddress_id())).getCity());
        txt_emp_pincode.setText(Config.config_address_details.get(Config.id_address_details.indexOf(ed.getAddress_id())).getPincode());
        txt_emp_state.setText(Config.config_address_details.get(Config.id_address_details.indexOf(ed.getAddress_id())).getState());
        txt_emp_department.setText(Config.config_designation_details.get(Config.id_designation_details.indexOf(ed.getDesignation_id())).getDesignation());
        } catch (Exception e) {
        }
    }
    public void onloadReport(){
        btn_att_report_searchActionPerformed(null);
        btn_adv_report_searchActionPerformed(null);
        btn_sal_dis_report_searchActionPerformed(null);
        btn_sal_report_searchActionPerformed(null);
    }
    public void setname(String employee_id){
        try {
            txt_salary_adv_search.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(employee_id)).getProfile_id())).getName());
        } catch (Exception e) {
        }
    }
    
    public void setData(String employee_id){
        Double salary=0.0;
        Double advance=0.0;
        try {
            txt_salary_sal_search.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(employee_id)).getProfile_id())).getName());
            txt_salary_adv_search.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(employee_id)).getProfile_id())).getName());
            for (int i = 0; i < tbl_salary_sal_model.getRowCount(); i++) {
            salary=salary+Double.parseDouble(tbl_salary_sal_model.getValueAt(i, 3).toString());
        }
        for (int i = 0; i < tbl_salary_adv_model.getRowCount(); i++) {
            advance=advance+Double.parseDouble(tbl_salary_adv_model.getValueAt(i, 3).toString());
        }
        txt_salary_total.setText(String.valueOf(salary));
        txt_advance_total.setText(String.valueOf(advance));
        Config.sql ="SELECT salary FROM salary_details a INNER JOIN (SELECT employee_id,max(STR_TO_DATE(_date,'%d/%m/%Y')) max_date FROM salary_details GROUP BY employee_id,_year,_month having _month='"+(jmc_month.getMonth()+1)+"' and _year='"+jyc_year.getYear()+"') b where a.employee_id = b.employee_id and a.employee_id='"+employee_id+"' AND STR_TO_DATE(a._date,'%d/%m/%Y') = b.max_date";
        Config.rs = Config.stmt.executeQuery(Config.sql);
//        (Config.sql);
        if(Config.rs.next()){
        txt_grand_salary.setText(Config.rs.getString("salary"));    
        total_grand_due.setText(String.valueOf(Double.parseDouble(Config.rs.getString("salary"))-(salary+advance)));
        txt_grand_amount.setText(String.valueOf(salary+advance));
        }else{
//            ("dasdasd sad asdda ssdd");
        }
        }catch (Exception e) {}
    }

    private void onloadEmployeeMessage(String message_id) {
               txt_emp_message.setText(loadMessage(message_id));
    }
    
    public void setSalaryTable(String sql) {
        try {
          tbl_salary_sal_model.setRowCount(0);
            Config.sql =sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
            tbl_salary_sal_model.addRow(new Object[]{
            Config.rs.getString("sal_dis_id"),
            Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.rs.getString("employee_id"))).getProfile_id())).getName(),
            Config.rs.getString("_date"),
            Config.rs.getString("amount"),
            Config.rs.getString("message_id"),
            Config.rs.getString("employee_id")
            });
            }
        } catch (Exception e) {
          
        }
        setSalaryDisReportDate();
    }
    
    public void setAdvanceTable(String sql) {
        try {
          tbl_salary_adv_model.setRowCount(0);
            Config.sql =sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
            tbl_salary_adv_model.addRow(new Object[]{
            Config.rs.getString("advance_id"),
            Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.rs.getString("employee_id"))).getProfile_id())).getName(),
            Config.rs.getString("_date"),
            Config.rs.getString("amount"),
            Config.rs.getString("message_id"),
            Config.rs.getString("employee_id")
            });
            }
        } catch (Exception e) {
            //
        }
    }
    public void setAttendanceTable(String sql) {
        try {
          tbl_attendence_model.setRowCount(0);
            Config.sql =sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while( Config.rs.next())
            {
                tbl_attendence_model.addRow(new Object[]{
                    Config.rs.getString("attendence_id"),
                    Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.rs.getString("employee_id"))).getProfile_id())).getName(),
                    Config.config_designation_details.get(Config.id_designation_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.rs.getString("employee_id"))).getDesignation_id())).getDesignation(),
                    Config.rs.getString("_date"),
                    Config.rs.getString("time_in"),
                    Config.rs.getString("time_out"),
                    Boolean.parseBoolean(Config.rs.getString("status")),
                    Config.rs.getString("message_id"),
                    Config.rs.getString("employee_id")
                });
            }
        } catch (Exception e) {
            
        }
    }
    
    public void setAttendence(String date) {
            try {
                jdc_att_date.setDate(sdf.parse(date));
            } catch (Exception e) {
                jdc_att_date.setDate(null);
            }            
    }
    public void updAttendence() {
                btn_attendence_searchActionPerformed(null);
    }
    
    private void onloadSalaryMessage(String message_id) {
        txt_salary_message.setText(loadMessage(message_id));
    }
    
    private void onloadAdvanceMessage(String message_id) {
        txt_salary_message.setText(loadMessage(message_id));
    }
    
    public void setSalaryData(String name,String _date){
        try {
        txt_salary_sal_search.setText(name);
        jmc_month.setMonth(Integer.parseInt(sdf_month.format(sdf.parse(_date)))-1);
        jyc_year.setYear(Integer.parseInt(sdf_year.format(sdf.parse(_date))));
        } catch (Exception e) {
        }
    }

    public void setAdvanceData(String name,String _date){
        try {
        txt_salary_adv_search.setText(name);
        jmc_month_adv.setMonth(Integer.parseInt(sdf_month.format(sdf.parse(_date)))-1);
        jyc_year_adv.setYear(Integer.parseInt(sdf_year.format(sdf.parse(_date))));
        } catch (Exception e) {        
        }
    }
    
    
    public void onloadSalaryInformation(){
    txt_salary_total.setText("");
    txt_advance_total.setText("");
    txt_grand_salary.setText("");
    txt_grand_amount.setText("");
    total_grand_due.setText("");
    }

    private void onloadAttendanceMessage(String message_id) {
       txt_attendence_message.setText(loadMessage(message_id));
    }
    public String loadMessage(String message_id){
        try {
            Config.sql ="select * from message_details where message_id='"+message_id+"'";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if(Config.rs.next()) {
            return Config.rs.getString("message");
            }else{
            return "";
            }
        } catch (Exception e) {
            
            return "";
        }
    }

    private void setAttendanceEmployeeData() {
        try {
        EmployeeDetails ed = Config.config_employee_details.get(Config.id_employee_details.indexOf(tbl_attendence_model.getValueAt(tbl_attendence.getSelectedRow(), 8).toString()));
        txt_attendence_empname.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(ed.getProfile_id())).getName());
        txt_attendence_gender.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(ed.getProfile_id())).getGender());
        txt_attendence_dob.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(ed.getProfile_id())).getDob());
        txt_attendence_address.setText(Config.config_address_details.get(Config.id_address_details.indexOf(ed.getAddress_id())).getAddress_1());
        txt_attendence_city.setText(Config.config_address_details.get(Config.id_address_details.indexOf(ed.getAddress_id())).getCity());
        txt_attendence_pincode.setText(Config.config_address_details.get(Config.id_address_details.indexOf(ed.getAddress_id())).getPincode());
        txt_attendence_state.setText(Config.config_address_details.get(Config.id_address_details.indexOf(ed.getAddress_id())).getState());
        txt_attendence_department.setText(Config.config_designation_details.get(Config.id_designation_details.indexOf(ed.getDesignation_id())).getDesignation());
        } catch (Exception e) {
        }
    }
    
    
    public void setEmployeeAttendance(){
        try {
        Config.sql ="select DAY(LAST_DAY(STR_TO_DATE('"+sdf.format(jdc_att_date.getDate())+"','%d/%m/%Y'))) as no ,count(*) as present from attendence_details where employee_id='"+tbl_attendence_model.getValueAt(tbl_attendence.getSelectedRow(),8).toString()+"' and _month='"+(Integer.parseInt(sdf_month.format(sdf.parse(sdf.format(jdc_att_date.getDate())))))+"' and _year='"+sdf_year.format(sdf.parse(sdf.format(jdc_att_date.getDate())))+"' and status='true'";
        Config.rs = Config.stmt.executeQuery(Config.sql);
            if(Config.rs.next()) {
                tbl_attendence_attendence_model.setValueAt(Config.rs.getString("no"), 0, 0);
                tbl_attendence_attendence_model.setValueAt(Config.rs.getString("present"), 0, 1);
                tbl_attendence_attendence_model.setValueAt(Integer.parseInt(Config.rs.getString("no"))-Integer.parseInt(Config.rs.getString("present")), 0, 2);
            }
        } catch (Exception e) {
            
        }
    }
    public void setAttendance(){
        try {
        Config.sql ="select DAY(LAST_DAY(STR_TO_DATE('"+sdf.format(new Date())+"','%d/%m/%Y'))) as no ,count(*) as present from attendence_details where employee_id='"+tbl_employee_model.getValueAt(tbl_employee.getSelectedRow(),0).toString()+"' and _month='"+(Integer.parseInt(sdf_month.format(sdf.parse(sdf.format(new Date()))))-1)+"' and _year='"+sdf_year.format(sdf.parse(sdf.format(new Date())))+"' and status='true'";
        Config.rs = Config.stmt.executeQuery(Config.sql);
            if(Config.rs.next()) {
                txt_emp_attendence_model.setValueAt(Config.rs.getString("no"), 0, 0);
                txt_emp_attendence_model.setValueAt(Config.rs.getString("present"), 0, 1);
                txt_emp_attendence_model.setValueAt(Integer.parseInt(Config.rs.getString("no"))-Integer.parseInt(Config.rs.getString("present")), 0, 2);
            }
        } catch (Exception e) {
            
        }
    }
    private void setReportAttendanceTable(String sql) {
        try {
          tbl_report_attendance_model.setRowCount(0);
          tbl_report_attendance_model.setRowCount(Config.config_employee_details.size());
          int year = jyc_att_report.getYear();
          int month = jmc_att_report.getMonth()+1;
          Calendar calendar = Calendar.getInstance();
          calendar.set(Calendar.YEAR,year);
          calendar.set(Calendar.MONTH,month-1);
          int total = calendar.getActualMaximum(Calendar.DATE);
          
        //............................................................................................................................   
        if(!txt_att_report_search.getText().equals("")){
        int row=0;    
        for (int i = 0; i < Config.config_employee_details.size(); i++) {
        int a=0;
        if(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.config_employee_details.get(i).getEmployee_id())).getProfile_id())).getName().toUpperCase().startsWith(txt_att_report_search.getText().toUpperCase())){
        Config.sql ="SELECT * FROM attendence_details where employee_id='"+Config.config_employee_details.get(i).getEmployee_id()+"' and  _month='"+(jmc_att_report.getMonth()+1)+"' and _year='"+jyc_att_report.getYear()+"'  order by  STR_TO_DATE(_date,'%d/%m/%Y') ";
        Config.rs = Config.stmt.executeQuery(Config.sql);
               tbl_report_attendance_model.setValueAt(Config.config_employee_details.get(i).getEmployee_id(), row, 0);
               tbl_report_attendance_model.setValueAt(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.config_employee_details.get(i).getEmployee_id())).getProfile_id())).getName(), row, 1);
               tbl_report_attendance_model.setValueAt(Config.config_designation_details.get(Config.id_designation_details.indexOf(Config.config_employee_details.get(i).getDesignation_id())).getDesignation(), row, 2);
        while( Config.rs.next()){
                     for (int j = 3; j < total+3; j++) {
                         
                        if(Integer.parseInt(sdf_day.format(sdf.parse(Config.rs.getString("_date"))))==(j-2)){
                        if(Config.rs.getString("status").equals("true")){
                                tbl_report_attendance_model.setValueAt("P", row, j);
                                a++;
                            }else{
                                tbl_report_attendance_model.setValueAt("A", row, j);
                            }
                        }
                     }     
               tbl_report_attendance_model.setValueAt(a, row, 34);
            }
        row++;
        }    
        }
        }else{
        for (int i = 0; i < Config.config_employee_details.size(); i++) {
        int a=0;
        Config.sql ="SELECT * FROM attendence_details where employee_id='"+Config.config_employee_details.get(i).getEmployee_id()+"' and  _month='"+(jmc_att_report.getMonth()+1)+"' and _year='"+jyc_att_report.getYear()+"'  order by  STR_TO_DATE(_date,'%d/%m/%Y') ";
        Config.rs = Config.stmt.executeQuery(Config.sql);
        tbl_report_attendance_model.setValueAt(Config.config_employee_details.get(i).getEmployee_id(), i, 0);
               tbl_report_attendance_model.setValueAt(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.config_employee_details.get(i).getEmployee_id())).getProfile_id())).getName(), i, 1);
               tbl_report_attendance_model.setValueAt(Config.config_designation_details.get(Config.id_designation_details.indexOf(Config.config_employee_details.get(i).getDesignation_id())).getDesignation(), i, 2);
        while( Config.rs.next()){
                     for (int j = 3; j < total+3; j++) {
                         if(Integer.parseInt(sdf_day.format(sdf.parse(Config.rs.getString("_date"))))==(j-2)){
                        if(Config.rs.getString("status").equals("true")){
                                tbl_report_attendance_model.setValueAt("P", i, j);
                                a++;
                            }else{
                                tbl_report_attendance_model.setValueAt("A", i, j);
                            }
                        }
                     }     
               tbl_report_attendance_model.setValueAt(a, i, 34);
            }
        }            
          }
        } catch (Exception e) {
            
        }
    }
    
    public void setEmployeeSalary(){
        Double salary=0.0;
        Double advance=0.0;
        try {
        Config.sql ="select (SELECT COALESCE(sum(amount),0) FROM salary_distribution where  employee_id='"+tbl_employee_model.getValueAt(tbl_employee.getSelectedRow(),0).toString()+"' and _month='"+(Integer.parseInt(sdf_month.format(sdf.parse(sdf.format(new Date()))))-1)+"' and _year='"+sdf_year.format(sdf.parse(sdf.format(new Date())))+"') as amount,(SELECT COALESCE(sum(amount),0) FROM advance_details where  employee_id='"+tbl_employee_model.getValueAt(tbl_employee.getSelectedRow(),0).toString()+"' and _month='"+(Integer.parseInt(sdf_month.format(sdf.parse(sdf.format(new Date()))))-1)+"' and _year='"+sdf_year.format(sdf.parse(sdf.format(new Date())))+"') as advance";
        Config.rs = Config.stmt.executeQuery(Config.sql);
//        (Config.sql);
        if(Config.rs.next()){
        tbl_emp_payment_model.setValueAt(Config.rs.getString("amount"), 0, 1);
        tbl_emp_payment_model.setValueAt(Config.rs.getString("advance"), 0, 2);
        tbl_emp_payment_model.setValueAt(String.valueOf(Double.parseDouble(Config.rs.getString("salary"))-(salary+advance)),0,3);
        }else{
//            ("dasdasd sad asdda ssdd");
        }
        }catch (Exception e) {}
        
    }
    
    public void setEmployeeOtherDetails(){
        try {
        tbl_emp_other_details_model.setValueAt(Config.config_employee_details.get(Config.id_employee_details.indexOf(tbl_employee_model.getValueAt(tbl_employee.getSelectedRow(),0).toString())).getJoining_date(), 0, 0);
        tbl_emp_other_details_model.setValueAt(Config.config_employee_details.get(Config.id_employee_details.indexOf(tbl_employee_model.getValueAt(tbl_employee.getSelectedRow(),0).toString())).getLeaving_date(), 0, 1);
        int i=0;
            for (i = 0; i < Config.config_salary_details.size(); i++) {
                if(Config.config_salary_details.get(i).getEmployee_id().equals(tbl_employee_model.getValueAt(tbl_employee.getSelectedRow(),0).toString())){
                    tbl_emp_other_details_model.setValueAt(Config.config_salary_details.get(i).getTime_in(), 0, 2);
                    tbl_emp_other_details_model.setValueAt(Config.config_salary_details.get(i).getTime_out(), 0, 3);
                    tbl_emp_payment_model.setValueAt(Config.config_salary_details.get(i).getSalary(), 0, 0);
                    break;
                }
            }
            if(i==Config.config_salary_details.size()){
                tbl_emp_other_details_model.setValueAt(null, 0, 2);
                tbl_emp_other_details_model.setValueAt(null, 0, 3);
                tbl_emp_payment_model.setValueAt(0, 0, 0);
            }
        } catch (Exception e) {
        }
    }

    private void setReportAdvanceTable(String sql) {
        try {
          tbl_adv_report_model.setRowCount(0);
          tbl_adv_report_model.setRowCount(Config.config_employee_details.size());
          int year = jyc_att_report.getYear();
          int month = jmc_att_report.getMonth()+1;
          Calendar calendar = Calendar.getInstance();
          calendar.set(Calendar.YEAR,year);
          calendar.set(Calendar.MONTH,month-1);
          int total = calendar.getActualMaximum(Calendar.DATE);
        //............................................................................................................................   
        if(!txt_adv_report.getText().equals("")){
        int row=0;    
        for (int i = 0; i < Config.config_employee_details.size(); i++) {
        double a=0;
        if(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.config_employee_details.get(i).getEmployee_id())).getProfile_id())).getName().toUpperCase().startsWith(txt_adv_report.getText().toUpperCase())){
        Config.sql ="SELECT * FROM advance_details where employee_id='"+Config.config_employee_details.get(i).getEmployee_id()+"' and  _month='"+(jmc_adv_report.getMonth()+1)+"' and _year='"+jyc_adv_report.getYear()+"'  order by  STR_TO_DATE(_date,'%d/%m/%Y') ";
        Config.rs = Config.stmt.executeQuery(Config.sql);
               tbl_adv_report_model.setValueAt(Config.config_employee_details.get(i).getEmployee_id(), row, 0);
               tbl_adv_report_model.setValueAt(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.config_employee_details.get(i).getEmployee_id())).getProfile_id())).getName(), row, 1);
               tbl_adv_report_model.setValueAt(Config.config_designation_details.get(Config.id_designation_details.indexOf(Config.config_employee_details.get(i).getDesignation_id())).getDesignation(), row, 2);
        while( Config.rs.next()){
                     for (int j = 3; j < total+3; j++) {
                        if(Integer.parseInt(sdf_day.format(sdf.parse(Config.rs.getString("_date"))))==(j-2)){
                        Double advance=0.0;
                            try {
                                advance= Double.parseDouble(tbl_adv_report_model.getValueAt( row, j).toString());
                                advance=advance+Double.parseDouble(Config.rs.getString("amount").toString());
                            } catch (Exception e) {
                                advance=advance+Double.parseDouble(Config.rs.getString("amount").toString());
                            }
                        tbl_adv_report_model.setValueAt(advance, row, j);
                        a=a+Double.parseDouble(Config.rs.getString("amount"));
                        }
                     }     
               tbl_adv_report_model.setValueAt(a, row, 34);
            }
        row++;
        }    
        }
        txt_total_employee.setText(String.valueOf(row));
        }else{
            int i=0;
        for ( i = 0; i < Config.config_employee_details.size(); i++) {
        double a=0;
        Config.sql ="SELECT * FROM advance_details where employee_id='"+Config.config_employee_details.get(i).getEmployee_id()+"' and  _month='"+(jmc_adv_report.getMonth()+1)+"' and _year='"+jyc_adv_report.getYear()+"'  order by  STR_TO_DATE(_date,'%d/%m/%Y') ";
        Config.rs = Config.stmt.executeQuery(Config.sql);
        tbl_adv_report_model.setValueAt(Config.config_employee_details.get(i).getEmployee_id(), i, 0);
        tbl_adv_report_model.setValueAt(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.config_employee_details.get(i).getEmployee_id())).getProfile_id())).getName(), i, 1);
        tbl_adv_report_model.setValueAt(Config.config_designation_details.get(Config.id_designation_details.indexOf(Config.config_employee_details.get(i).getDesignation_id())).getDesignation(), i, 2);
        while( Config.rs.next()){
                     for (int j = 3; j < total+3; j++) {
                        if(Integer.parseInt(sdf_day.format(sdf.parse(Config.rs.getString("_date"))))==(j-2)){
                            Double advance=0.0;
                            try {
                                advance= Double.parseDouble(tbl_adv_report_model.getValueAt( i, j).toString());
                                advance=advance+Double.parseDouble(Config.rs.getString("amount").toString());
                            } catch (Exception e) {
                                advance=advance+Double.parseDouble(Config.rs.getString("amount").toString());
                            }
                        tbl_adv_report_model.setValueAt(advance, i, j);
                        a=a+Double.parseDouble(Config.rs.getString("amount"));
                     }     
               tbl_adv_report_model.setValueAt(a, i, 34);
                        }
        }            
        }
        txt_total_employee.setText(String.valueOf(i));
        }
        
        } catch (Exception e) {
           
        }
        setAdvanceReportDate();
    }
    
    public void setAdvanceReportDate(){
       Double total=0.0;
       for (int i = 0; i < tbl_adv_report.getRowCount(); i++) {
           try {
               total=total+Double.parseDouble(tbl_adv_report_model.getValueAt(i, 34).toString());
           } catch (Exception e) {
           }
       }
       txt_total_advance.setText(String.valueOf(total));
   }
    private void setReportSalaryTable(String sql) {
        try {
            tbl_sal_report_model.setRowCount(0);
            Config.sql =sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
            tbl_sal_report_model.addRow(new Object[]{
            Config.rs.getString("a.employee_id"),
            Config.rs.getString("pd.name"),
            Config.config_contact_details.get(Config.id_contact_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.rs.getString("a.employee_id"))).getContact_id())).getContact_no(),
            Config.config_designation_details.get(Config.id_designation_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.rs.getString("a.employee_id"))).getDesignation_id())).getDesignation(),
            Config.rs.getString("a.salary"),
            Config.rs.getString("days"),
            Config.rs.getString("amount"),
            Config.rs.getString("advance"),
            Double.parseDouble(Config.rs.getString("a.salary"))-(Double.parseDouble(Config.rs.getString("amount"))+Double.parseDouble(Config.rs.getString("advance")))
            });
            }
        int i=0;
        Double sal=0.0;
        Double amt=0.0;
        Double adv=0.0;
        Double due=0.0;
        for (i = 0; i < tbl_sal_report_model.getRowCount(); i++) {
        sal=sal+Double.parseDouble(tbl_sal_report_model.getValueAt(i, 4).toString());
        amt=amt+Double.parseDouble(tbl_sal_report_model.getValueAt(i, 6).toString());
        adv=adv+Double.parseDouble(tbl_sal_report_model.getValueAt(i, 7).toString());
        due=due+Double.parseDouble(tbl_sal_report_model.getValueAt(i, 8).toString());
        }
        txt_salary_report_total_emp.setText(String.valueOf(sal));
        txt_salary_report_total_sal.setText(String.valueOf(amt));
        txt_salary_report_total_adv.setText(String.valueOf(adv));
        txt_salary_report_total_due.setText(String.valueOf(due));
            
        } catch (Exception e) {
            
        }
    }
    private void setReportSalaryDisTable() {
        try {
          tbl_sal_dis_report_model.setRowCount(0);
          tbl_sal_dis_report_model.setRowCount(Config.config_employee_details.size());
          int year = jyc_att_report.getYear();
          int month = jmc_att_report.getMonth()+1;
          Calendar calendar = Calendar.getInstance();
          calendar.set(Calendar.YEAR,year);
          calendar.set(Calendar.MONTH,month-1);
          int total = calendar.getActualMaximum(Calendar.DATE);
        //............................................................................................................................   
        if(!txt_sal_dis_report.getText().equals("")){
        int row=0;    
        for (int i = 0; i < Config.config_employee_details.size(); i++) {
        double a=0;
        if(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.config_employee_details.get(i).getEmployee_id())).getProfile_id())).getName().toUpperCase().startsWith(txt_sal_dis_report.getText().toUpperCase())){
        Config.sql ="SELECT * FROM salary_distribution where employee_id='"+Config.config_employee_details.get(i).getEmployee_id()+"' and  _month='"+(jmc_sal_dis_report.getMonth()+1)+"' and _year='"+jyc_sal_dis_report.getYear()+"'  order by  STR_TO_DATE(_date,'%d/%m/%Y') ";
        Config.rs = Config.stmt.executeQuery(Config.sql);
               tbl_sal_dis_report_model.setValueAt(Config.config_employee_details.get(i).getEmployee_id(), row, 0);
               tbl_sal_dis_report_model.setValueAt(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.config_employee_details.get(i).getEmployee_id())).getProfile_id())).getName(), row, 1);
               tbl_sal_dis_report_model.setValueAt(Config.config_designation_details.get(Config.id_designation_details.indexOf(Config.config_employee_details.get(i).getDesignation_id())).getDesignation(), row, 2);
        while( Config.rs.next()){
                     for (int j = 3; j < total+3; j++) {
                        if(Integer.parseInt(sdf_day.format(sdf.parse(Config.rs.getString("_date"))))==(j-2)){
                        Double advance=0.0;
                            try {
                                advance= Double.parseDouble(tbl_sal_dis_report_model.getValueAt(row, j).toString());
                                advance=advance+Double.parseDouble(Config.rs.getString("amount").toString());
                            } catch (Exception e) {
                                advance=advance+Double.parseDouble(Config.rs.getString("amount").toString());
                            }
                        tbl_sal_dis_report_model.setValueAt(advance, row, j);
                        a=a+Double.parseDouble(Config.rs.getString("amount"));
                        }
                     }     
               tbl_sal_dis_report_model.setValueAt(a, row, 34);
            }
        row++;
        }    
        }
        txt_sal_dis_total_employee.setText(String.valueOf(row));
        }else{
            int i=0;
        for ( i = 0; i < Config.config_employee_details.size(); i++) {
        double a=0;
        Config.sql ="SELECT * FROM salary_distribution where employee_id='"+Config.config_employee_details.get(i).getEmployee_id()+"' and  _month='"+(jmc_sal_dis_report.getMonth()+1)+"' and _year='"+jyc_sal_dis_report.getYear()+"'  order by  STR_TO_DATE(_date,'%d/%m/%Y') ";
        Config.rs = Config.stmt.executeQuery(Config.sql);
        tbl_sal_dis_report_model.setValueAt(Config.config_employee_details.get(i).getEmployee_id(), i, 0);
        tbl_sal_dis_report_model.setValueAt(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.config_employee_details.get(i).getEmployee_id())).getProfile_id())).getName(), i, 1);
        tbl_sal_dis_report_model.setValueAt(Config.config_designation_details.get(Config.id_designation_details.indexOf(Config.config_employee_details.get(i).getDesignation_id())).getDesignation(), i, 2);
        while( Config.rs.next()){
                     for (int j = 3; j < total+3; j++) {
                        if(Integer.parseInt(sdf_day.format(sdf.parse(Config.rs.getString("_date"))))==(j-2)){
                            Double advance=0.0;
                            try {
                                advance= Double.parseDouble(tbl_sal_dis_report_model.getValueAt( i, j).toString());
                                advance=advance+Double.parseDouble(Config.rs.getString("amount").toString());
                            } catch (Exception e) {
                                advance=advance+Double.parseDouble(Config.rs.getString("amount").toString());
                            }
                        tbl_sal_dis_report_model.setValueAt(advance, i, j);
                        a=a+Double.parseDouble(Config.rs.getString("amount"));
                     }     
               tbl_sal_dis_report_model.setValueAt(a, i, 34);
                        }
        }            
        }
        txt_sal_dis_total_employee.setText(String.valueOf(i));
        }
        
        } catch (Exception e) {
            
        }
        setSalaryDisReportDate();
    }
    
    public void setSalaryDisReportDate(){
       Double total=0.0;
       for (int i = 0; i < tbl_sal_dis_report.getRowCount(); i++) {
            try {
               total=total+Double.parseDouble(tbl_sal_dis_report_model.getValueAt(i, 34).toString());
            } catch (Exception e) {
            }
       }
       txt_sal_dis_total_advance1.setText(String.valueOf(total));
   }        
}
