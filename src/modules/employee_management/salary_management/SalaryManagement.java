package modules.employee_management.salary_management;

import data_manager.configuration.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

public class SalaryManagement extends javax.swing.JDialog {
      public DefaultTableModel  tbl_employee_model = null;
      public DefaultTableModel  tbl_salary_mng_model = null;
      
    SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
    
    public SalaryManagement(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
         
        tbl_salary_mng_model = (DefaultTableModel) tbl_salary_mng.getModel();
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
              }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMonthChooser1 = new com.toedter.calendar.JMonthChooser();
        jRadioButton1 = new javax.swing.JRadioButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_salary_mng = new javax.swing.JTextField();
        btn_salary_mng_add = new javax.swing.JButton();
        btn_salary_search = new javax.swing.JButton();
        jmc_month = new com.toedter.calendar.JMonthChooser();
        jLabel6 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jyc_year = new com.toedter.calendar.JYearChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_salary_mng = new javax.swing.JTable();
        btn_reset = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        btn_view = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        lbl_salary = new javax.swing.JLabel();

        jRadioButton1.setText("jRadioButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        jPanel13.setBackground(new java.awt.Color(230, 79, 6));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Salary Management");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Employee Name :");

        txt_salary_mng.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_salary_mngCaretUpdate(evt);
            }
        });
        txt_salary_mng.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_salary_mngKeyPressed(evt);
            }
        });

        btn_salary_mng_add.setText("Add");
        btn_salary_mng_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salary_mng_addActionPerformed(evt);
            }
        });

        btn_salary_search.setText("Search");
        btn_salary_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salary_searchActionPerformed(evt);
            }
        });

        jmc_month.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_monthPropertyChange(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Month :");

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Year :");

        jyc_year.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_yearPropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_salary_mng)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jmc_month, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jyc_year, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_salary_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_salary_mng_add)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txt_salary_mng, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel6)
                                .addComponent(jLabel1))
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(btn_salary_mng_add)
                                        .addComponent(btn_salary_search))
                                    .addComponent(jSeparator1))
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jmc_month, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addComponent(jyc_year, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_salary_mng_add, btn_salary_search, jLabel1, jLabel6, jmc_month, txt_salary_mng});

        tbl_salary_mng.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "SALARY ID", "EMPLOYEE NAME", "DATE", "MONTH", "YEAR", "TIME IN", "TIME OUT", "SALARY"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_salary_mng.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_salary_mngMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_salary_mng);
        if (tbl_salary_mng.getColumnModel().getColumnCount() > 0) {
            tbl_salary_mng.getColumnModel().getColumn(0).setResizable(false);
            tbl_salary_mng.getColumnModel().getColumn(0).setPreferredWidth(100);
            tbl_salary_mng.getColumnModel().getColumn(1).setResizable(false);
            tbl_salary_mng.getColumnModel().getColumn(1).setPreferredWidth(200);
            tbl_salary_mng.getColumnModel().getColumn(2).setResizable(false);
            tbl_salary_mng.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_salary_mng.getColumnModel().getColumn(3).setResizable(false);
            tbl_salary_mng.getColumnModel().getColumn(3).setPreferredWidth(100);
            tbl_salary_mng.getColumnModel().getColumn(4).setResizable(false);
            tbl_salary_mng.getColumnModel().getColumn(4).setPreferredWidth(100);
            tbl_salary_mng.getColumnModel().getColumn(5).setResizable(false);
            tbl_salary_mng.getColumnModel().getColumn(5).setPreferredWidth(100);
            tbl_salary_mng.getColumnModel().getColumn(6).setResizable(false);
            tbl_salary_mng.getColumnModel().getColumn(6).setPreferredWidth(100);
            tbl_salary_mng.getColumnModel().getColumn(7).setResizable(false);
            tbl_salary_mng.getColumnModel().getColumn(7).setPreferredWidth(100);
        }

        btn_reset.setText("Reset");

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });

        btn_view.setText("View");
        btn_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewActionPerformed(evt);
            }
        });

        jButton1.setText("Remove");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 764, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btn_reset)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_salary, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_view)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_reset)
                    .addComponent(btn_cancel)
                    .addComponent(btn_view)
                    .addComponent(jButton1)
                    .addComponent(lbl_salary))
                .addGap(11, 11, 11))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jButton1, lbl_salary});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose(); 
    }//GEN-LAST:event_closeDialog

    private void btn_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewActionPerformed
        try {
            Config.view_salary.onloadReset(tbl_salary_mng_model.getValueAt(tbl_salary_mng.getSelectedRow(), 0).toString());
            Config.view_salary.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_viewActionPerformed

    private void txt_salary_mngKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_salary_mngKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_salary_mngKeyPressed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
       dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void btn_salary_mng_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salary_mng_addActionPerformed
       Config.new_salary.onloadReset();
       Config.new_salary.setVisible(true);
    }//GEN-LAST:event_btn_salary_mng_addActionPerformed

    private void btn_salary_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salary_searchActionPerformed
        String sql = "";
            String text=txt_salary_mng.getText().trim().toUpperCase()+"%";
                if (!txt_salary_mng.getText().equals("")) {
                    sql = "SELECT * FROM salary_details as ad INNER JOIN employee_details AS ed ON ad.employee_id = ed.employee_id INNER JOIN profile_details AS pd ON ed.profile_id = pd.profile_id and UPPER(pd.name) like '"+text+"' and _month='"+(jmc_month.getMonth()+1)+"' and _year='"+jyc_year.getYear()+"'" ;
                } else {
                    sql = "SELECT ad.* FROM salary_details as ad INNER JOIN employee_details AS ed ON ad.employee_id = ed.employee_id INNER JOIN profile_details AS pd ON ed.profile_id = pd.profile_id and _month='"+(jmc_month.getMonth()+1)+"' and _year='"+jyc_year.getYear()+"'" ;
                }
            setSalaryTable(sql);
    }//GEN-LAST:event_btn_salary_searchActionPerformed

    private void txt_salary_mngCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_salary_mngCaretUpdate
        btn_salary_searchActionPerformed(null);
    }//GEN-LAST:event_txt_salary_mngCaretUpdate

    private void tbl_salary_mngMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_salary_mngMouseClicked
        if(evt.getClickCount()== 2){
            btn_viewActionPerformed(null);            
        }
    }//GEN-LAST:event_tbl_salary_mngMouseClicked

    private void jmc_monthPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_monthPropertyChange
        btn_salary_searchActionPerformed(null);
    }//GEN-LAST:event_jmc_monthPropertyChange

    private void jyc_yearPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_yearPropertyChange
        btn_salary_searchActionPerformed(null);
    }//GEN-LAST:event_jyc_yearPropertyChange

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int dd = JOptionPane.showConfirmDialog(this,"Are you really want to delete this entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
        if (dd == 0){
             try {
            if (Config.salary_details_mgr.delSalaryDetails(tbl_salary_mng_model.getValueAt(tbl_salary_mng.getSelectedRow(), 0).toString())){
                Config.salary_management.onloadReset();
                JOptionPane.showMessageDialog(this, "Salary Management Details Delete successfully", "Success", JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion ", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        }
    }//GEN-LAST:event_jButton1ActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_reset;
    private javax.swing.JButton btn_salary_mng_add;
    private javax.swing.JButton btn_salary_search;
    private javax.swing.JButton btn_view;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private com.toedter.calendar.JMonthChooser jMonthChooser1;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private com.toedter.calendar.JMonthChooser jmc_month;
    private com.toedter.calendar.JYearChooser jyc_year;
    private javax.swing.JLabel lbl_salary;
    private javax.swing.JTable tbl_salary_mng;
    private javax.swing.JTextField txt_salary_mng;
    // End of variables declaration//GEN-END:variables
    public void onloadReset(){
        txt_salary_mng.setText("");
        btn_salary_searchActionPerformed(null);
    }

    public void setSalaryTable(String sql) {
        try {
                tbl_salary_mng_model.setRowCount(0);
                Config.sql =sql;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                while (Config.rs.next()) {
                tbl_salary_mng_model.addRow(new Object[]{
                Config.rs.getString("salary_id"),
                Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_employee_details.get(Config.id_employee_details.indexOf(Config.rs.getString("employee_id"))).getProfile_id())).getName(),
                Config.rs.getString("_date"),
                Config.rs.getString("_month"),
                Config.rs.getString("_year"),
                Config.rs.getString("time_in"),
                Config.rs.getString("time_out"),
                Config.rs.getString("salary")
                });
                }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
}
