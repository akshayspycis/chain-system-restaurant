/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modules.sales_management;

import data_manager.SalesDetails;
import data_manager.configuration.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author akshay
 */
public class ViewSaleItem extends javax.swing.JDialog {
        String item_id = "";
    String item_list_id = "";
    String message_id = "";
    String designation_id = "";
    String distribution_details_id ="";

    SimpleDateFormat simple_date_formate = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_day = new SimpleDateFormat("dd");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
    
    DefaultTableModel tbl_dis_item_model=null;
    DefaultTableModel tbl_sale_model=null;


    public ViewSaleItem(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        tbl_dis_item_model=(DefaultTableModel) tbl_dis_tem.getModel();
        tbl_sale_model=(DefaultTableModel) tbl_sale.getModel();

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel4 = new javax.swing.JPanel();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel7 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_sale = new javax.swing.JTable();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        cb_Item_type = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        txt_name = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_unit = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txt_price = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_item_total = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txt_quantity = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_balance = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txt_total_sale = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txt_designation = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jdc_date = new com.toedter.calendar.JDateChooser();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_dis_tem = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jPanel2.setBackground(new java.awt.Color(230, 79, 6));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("View Sale Item");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jSplitPane1.setDividerLocation(400);
        jSplitPane1.setEnabled(false);

        jSplitPane2.setDividerLocation(200);
        jSplitPane2.setEnabled(false);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Sales Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tbl_sale.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ITEM", "ITEM QUANTITY", "SALE", "BALANCE", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_sale.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tbl_sale);
        if (tbl_sale.getColumnModel().getColumnCount() > 0) {
            tbl_sale.getColumnModel().getColumn(0).setResizable(false);
            tbl_sale.getColumnModel().getColumn(1).setResizable(false);
            tbl_sale.getColumnModel().getColumn(2).setResizable(false);
            tbl_sale.getColumnModel().getColumn(3).setResizable(false);
            tbl_sale.getColumnModel().getColumn(4).setMinWidth(0);
            tbl_sale.getColumnModel().getColumn(4).setPreferredWidth(0);
            tbl_sale.getColumnModel().getColumn(4).setMaxWidth(0);
            tbl_sale.getColumnModel().getColumn(5).setMinWidth(0);
            tbl_sale.getColumnModel().getColumn(5).setPreferredWidth(0);
            tbl_sale.getColumnModel().getColumn(5).setMaxWidth(0);
        }

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 427, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                .addContainerGap())
        );

        jButton4.setText("Cancel");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("Reset");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addComponent(jButton5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton4)
                        .addGap(2, 2, 2)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton4)
                    .addComponent(jButton5))
                .addContainerGap())
        );

        jSplitPane2.setRightComponent(jPanel7);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        jLabel3.setText("Item Type :");

        cb_Item_type.setEditable(true);
        cb_Item_type.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-Select-", "Stock Item", "Sada flavour", "Manufacture Item" }));
        cb_Item_type.setEnabled(false);
        cb_Item_type.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_Item_typeActionPerformed(evt);
            }
        });

        jLabel4.setText("Product Name :");

        txt_name.setEditable(false);

        jLabel5.setText("Unit");

        txt_unit.setEditable(false);

        jLabel6.setText("Price ");

        txt_price.setEditable(false);

        jLabel8.setText("Item Total");

        txt_item_total.setEditable(false);

        jLabel9.setText("Quantity");

        txt_quantity.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_quantityCaretUpdate(evt);
            }
        });
        txt_quantity.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_quantityFocusLost(evt);
            }
        });
        txt_quantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_quantityActionPerformed(evt);
            }
        });

        jLabel10.setText("Balance Item");

        txt_balance.setEditable(false);

        jLabel11.setText("Total Sale");

        txt_total_sale.setEditable(false);

        jButton1.setText("Delete");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Update");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txt_name, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cb_Item_type, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel8Layout.createSequentialGroup()
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel8Layout.createSequentialGroup()
                                        .addComponent(txt_item_total, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel8Layout.createSequentialGroup()
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txt_unit, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel5)
                                            .addComponent(jLabel8)
                                            .addComponent(jLabel3))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txt_price)
                                            .addGroup(jPanel8Layout.createSequentialGroup()
                                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel9)
                                                    .addComponent(jLabel6))
                                                .addGap(0, 0, Short.MAX_VALUE))))
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addComponent(jButton1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton2))
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel10)
                                            .addComponent(txt_balance, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel11)
                                            .addComponent(txt_total_sale, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(23, 23, 23))))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_Item_type, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_unit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_price, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_item_total, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_balance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_total_sale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSplitPane2.setLeftComponent(jPanel8);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 685, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane2)
        );

        jSplitPane1.setRightComponent(jPanel4);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        jLabel7.setText("Designation Details");

        txt_designation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_designationActionPerformed(evt);
            }
        });

        jLabel2.setText("Date ");

        jdc_date.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdc_datePropertyChange(evt);
            }
        });

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Distribution Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tbl_dis_tem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ITEM", "DIS.", "LAST BALANCE", "TOTAL", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_dis_tem.getTableHeader().setReorderingAllowed(false);
        tbl_dis_tem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_dis_temMouseClicked(evt);
            }
        });
        tbl_dis_tem.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbl_dis_temKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_dis_tem);
        if (tbl_dis_tem.getColumnModel().getColumnCount() > 0) {
            tbl_dis_tem.getColumnModel().getColumn(0).setResizable(false);
            tbl_dis_tem.getColumnModel().getColumn(1).setResizable(false);
            tbl_dis_tem.getColumnModel().getColumn(2).setResizable(false);
            tbl_dis_tem.getColumnModel().getColumn(3).setResizable(false);
            tbl_dis_tem.getColumnModel().getColumn(4).setMinWidth(0);
            tbl_dis_tem.getColumnModel().getColumn(4).setPreferredWidth(0);
            tbl_dis_tem.getColumnModel().getColumn(4).setMaxWidth(0);
            tbl_dis_tem.getColumnModel().getColumn(5).setMinWidth(0);
            tbl_dis_tem.getColumnModel().getColumn(5).setPreferredWidth(0);
            tbl_dis_tem.getColumnModel().getColumn(5).setMaxWidth(0);
        }

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(0, 146, Short.MAX_VALUE))
                            .addComponent(txt_designation))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txt_designation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jSplitPane1.setLeftComponent(jPanel5);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1091, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
    dispose();
    }//GEN-LAST:event_closeDialog

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        onloadSalItem();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void cb_Item_typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_Item_typeActionPerformed
        txt_unit.setText("");
        if(cb_Item_type.getSelectedIndex()!=0){
            switch(cb_Item_type.getSelectedIndex()){
                case 1:
                break;
                case 2:
                break;
                case 3:
                break;
            }
        }
    }//GEN-LAST:event_cb_Item_typeActionPerformed

    private void txt_quantityCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_quantityCaretUpdate
        try {
            txt_balance.setText(String.valueOf(Double.parseDouble(txt_item_total.getText().trim())-Double.parseDouble(txt_quantity.getText().trim())));
            txt_total_sale.setText(String.valueOf(Double.parseDouble(txt_price.getText().trim())*Double.parseDouble(txt_quantity.getText().trim())));
        }catch (Exception e) {
            txt_balance.setText("0.0");
            txt_total_sale.setText("0.0");
        }
    }//GEN-LAST:event_txt_quantityCaretUpdate

    private void txt_quantityFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_quantityFocusLost
        if(!txt_quantity.getText().trim().equals("")){
            try {
                Double.parseDouble(txt_quantity.getText().trim());
            }catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Only No.Value Required ", "Error", JOptionPane.ERROR_MESSAGE);
                txt_quantity.setText("");
                txt_quantity.requestFocus();
            }
        }
    }//GEN-LAST:event_txt_quantityFocusLost

    private void txt_quantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_quantityActionPerformed

    }//GEN-LAST:event_txt_quantityActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            if(Config.sales_details_mgr.delSalesDetailsItem(distribution_details_id)){
            JOptionPane.showMessageDialog(this, "Deletion successfully.", "Creation successful", JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
    try {
        String str = checkValidity();
        if(str.equals("ok")){
                if (checkAvailibilitySale()) {
                    tbl_sale_model.addRow(new Object[]{
                        txt_name.getText(),
                        txt_quantity.getText(),
                        txt_total_sale.getText(),
                        txt_balance.getText(),
                        item_id,
                        item_list_id
                });
                ArrayList<SalesDetails> arList_rs = new ArrayList<>();
                    SalesDetails rs = new SalesDetails();
                    rs.setItem_id(tbl_sale_model.getValueAt(tbl_sale_model.getRowCount()-1, 4).toString());
                    rs.setItem_list_id(tbl_sale_model.getValueAt(tbl_sale_model.getRowCount()-1, 5).toString());
                    rs.setQuantity(tbl_sale_model.getValueAt(tbl_sale_model.getRowCount()-1, 1).toString());
                    rs.setDistribution_details_id(distribution_details_id);
                    rs.setSale(tbl_sale_model.getValueAt(tbl_sale_model.getRowCount()-1, 2).toString());
                    arList_rs.add(rs);
                
                if(Config.sales_details_mgr.insSalesDetailsItem(arList_rs)){
                   Config.sale_management.loadSaleDetails();
                    JOptionPane.showMessageDialog(this, "Stock List created successfully.", "Creation successful", JOptionPane.NO_OPTION);
                }else{
                    JOptionPane.showMessageDialog(this, "Error in Stock List creation", "Error", JOptionPane.ERROR_MESSAGE);
                }
                }else{
                JOptionPane.showMessageDialog(this, "Sales Details Already Exites", "Error", JOptionPane.ERROR_MESSAGE);    
                }
        }else{
            JOptionPane.showMessageDialog(this, "Check '"+str+"' field.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txt_designationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_designationActionPerformed
        Config.designation_search.onloadReset(3);
        Config.designation_search.setVisible(true);
    }//GEN-LAST:event_txt_designationActionPerformed

    private void jdc_datePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_datePropertyChange
        onloadDisItem();
    }//GEN-LAST:event_jdc_datePropertyChange

    private void tbl_dis_temMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_dis_temMouseClicked
        setItemDetails();
    }//GEN-LAST:event_tbl_dis_temMouseClicked

    private void tbl_dis_temKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_dis_temKeyReleased
        setItemDetails();
    }//GEN-LAST:event_tbl_dis_temKeyReleased

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton4ActionPerformed
    


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cb_Item_type;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private com.toedter.calendar.JDateChooser jdc_date;
    private javax.swing.JTable tbl_dis_tem;
    private javax.swing.JTable tbl_sale;
    private javax.swing.JTextField txt_balance;
    private javax.swing.JTextField txt_designation;
    private javax.swing.JTextField txt_item_total;
    private javax.swing.JTextField txt_name;
    private javax.swing.JTextField txt_price;
    private javax.swing.JTextField txt_quantity;
    private javax.swing.JTextField txt_total_sale;
    private javax.swing.JTextField txt_unit;
    // End of variables declaration//GEN-END:variables

   public void onloadReset(String distribution_details_id,String designation_id,String date) {
     this.designation_id=designation_id;
     item_id="";
     item_list_id="";
     message_id = "";
     this.distribution_details_id =distribution_details_id;
     txt_designation.setText(Config.config_designation_details.get(Config.id_designation_details.indexOf(designation_id)).getDesignation());
    try {
           jdc_date.setDate(simple_date_formate.parse(date));
    } catch (Exception e) {
           jdc_date.setDate(null);
    }
    onloadDisTable();
    onloadSaleTable();
    cb_Item_type.setSelectedIndex(0);
    txt_name.setText("");
    txt_price.setText("");    
    txt_unit.setText("");
    txt_item_total.setText("");    
    txt_quantity.setText("");
    onloadDisItem();
    onloadSalItem();
   }

   
   public void onloadDisItem(){
       try {
            Config.sql ="SELECT\n" +
                    "b.*,d.name,\n" +
                    "COALESCE(\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from distribution_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from return_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from demage_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    ",0)\n" +
                    "as dis ,\n" +
                    "COALESCE(\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from distribution_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')<STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from return_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')<STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from demage_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')<STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from sales_details di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')<STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    ",0)\n" +
                    "as bal\n" +
                    "FROM distribution_item b\n" +
                    "inner join distribution_details c on c.distribution_details_id=b.distribution_details_id\n" +
                    "inner join stock_item d on d.item_id=b.item_id\n" +
                    "where c.designation_id='"+designation_id+"'\n" +
                    "and STR_TO_DATE(c._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y') and d.identity='M'";
            System.out.println(Config.sql);
            Config.rs = Config.stmt.executeQuery(Config.sql);
            tbl_dis_item_model.setRowCount(0);
            while(Config.rs.next()) {
                distribution_details_id=Config.rs.getString("b.distribution_details_id");
                tbl_dis_item_model.addRow(new Object[]{
                Config.rs.getString("d.name"),
                Config.rs.getString("dis"),
                Config.rs.getString("bal"),
                Double.parseDouble(Config.rs.getString("dis"))+Double.parseDouble(Config.rs.getString("bal")),
                Config.rs.getString("item_id"),
                Config.rs.getString("item_list_id")
                });
            }
       } catch (Exception e) {
           //e.printStackTrace();
       }
    }
   
   public void onloadSalItem(){
       try {
                    Config.sql ="SELECT\n" +
                    "b.*,d.name,\n" +
                    "(select prise from item_list where item_list_id=b.item_list_id) as price,\n" +
                    "COALESCE(\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from distribution_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from return_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from demage_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    ",0)\n" +
                    "as dis\n ," +
                    "COALESCE(\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from distribution_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')<STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from return_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')<STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from demage_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')<STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from sales_details di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')<STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    ",0)\n" +
                    "as quan\n" +
                    "FROM sales_details b\n" +
                    "inner join distribution_details c on c.distribution_details_id=b.distribution_details_id\n" +
                    "inner join stock_item d on d.item_id=b.item_id\n" +
                    "where c.designation_id='"+designation_id+"'\n" +
                    "and STR_TO_DATE(c._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_date.getDate())+"','%d/%m/%Y') and d.identity='M'";

            System.out.println(Config.sql);
            Config.rs = Config.stmt.executeQuery(Config.sql);
            tbl_sale_model.setRowCount(0);
            while(Config.rs.next()) {
            tbl_sale_model.addRow(new Object[]{
                Config.rs.getString("d.name"),
                Config.rs.getString("quantity"),
                Config.rs.getString("sale"),
                Double.parseDouble(Config.rs.getString("dis"))-Double.parseDouble(Config.rs.getString("quantity")),
                Config.rs.getString("item_id"),
                Config.rs.getString("item_list_id")
                });
            }
       } catch (Exception e) {
           e.printStackTrace();
       }
    }
   
    public void setDesignation(String designation_id) {
        this.designation_id = designation_id;
        txt_designation.setText(Config.config_designation_details.get(Config.id_designation_details.indexOf(designation_id)).getDesignation());
    }

    public void setItemDetails(){
        try {
        item_id=tbl_dis_item_model.getValueAt(tbl_dis_tem.getSelectedRow(), 4).toString();
        switch(Config.config_stock_item.get(Config.id_stock_item.indexOf(item_id)).getIdentity()){
            case "ST":
                cb_Item_type.setSelectedIndex(1);
            break;    
            case "S":
                cb_Item_type.setSelectedIndex(2);
            break;
            case "M":
                cb_Item_type.setSelectedIndex(3);
            break;
        }
        txt_name.setText(Config.config_stock_item.get(Config.id_stock_item.indexOf(item_id)).getName());
        item_list_id=tbl_dis_item_model.getValueAt(tbl_dis_tem.getSelectedRow(), 5).toString();
        try {
            Config.sql="select prise from item_list where item_list_id="+item_list_id;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if (Config.rs.next()) {
            txt_price.setText(Config.rs.getString("prise"));    
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        txt_unit.setText(Config.config_stock_item.get(Config.id_stock_item.indexOf(item_id)).getUnit());
        txt_item_total.setText(tbl_dis_item_model.getValueAt(tbl_dis_tem.getSelectedRow(), 3).toString());    
        txt_quantity.setText("");
        } catch (Exception e) {
        }
     }
    
    public void onloadDisTable(){
        tbl_dis_item_model.setRowCount(0);
    }
    public void onloadSaleTable(){
        tbl_sale_model.setRowCount(0);
    }
    
    public String checkValidity(){
        if (Double.parseDouble(txt_quantity.getText())==0.0) {
            return "Quantity";
        }else{
        return "ok";    
        }
    }
    
    
     public void setMessage(String message_id){
         this.message_id=message_id;
     }

    
     private String checkValidityTable() {
        if(tbl_sale.getRowCount()==0){
            return "Table";
        }else{
            return "ok";
        }
    }

    private boolean checkAvailibility() {
        for (int i = 0; i < tbl_sale_model.getRowCount(); i++) {
            if(tbl_sale_model.getValueAt(i, 4).toString().equals(item_id)){
             return false;
            }
        }
        return true;
    }

    private boolean checkAvailibilitySale() {
        try{
            Config.sql="select * from sales_details where distribution_details_id='"+distribution_details_id+"' and item_id='"+item_id+"'";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if (Config.rs.next()) {
            return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
       return true; 
    }
}
