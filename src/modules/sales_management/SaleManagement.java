package modules.sales_management;

import data_manager.configuration.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

public class SaleManagement extends javax.swing.JDialog {
    String designation_id = "";
    String sale_report_designation_wiseitem_id="";
    String sale_report_item_wise_designation_id = "";
    String dis_report_item_wise_designation_id = "";
    SimpleDateFormat simple_date_formate = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_day = new SimpleDateFormat("dd");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
    
    DefaultTableModel tbl_sale_item_model=null;

    public SaleManagement(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        tbl_sale_item_model=(DefaultTableModel) tbl_sale_item.getModel();
        tbl_sale_report_item_wise.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        tbl_dis_report_item_wise.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        tbl_total_sale_report_item_wise.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        tbl_total_sale.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        tbl_total_item.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        tbl_total_designation_sale.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_sale_item = new javax.swing.JTable();
        jPanel10 = new javax.swing.JPanel();
        btn_sale_search = new javax.swing.JButton();
        btn_sale_add = new javax.swing.JButton();
        txt_designation = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jdc_sale_date = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jTabbedPane4 = new javax.swing.JTabbedPane();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_sale_report_item_wise = new javax.swing.JTable();
        jPanel13 = new javax.swing.JPanel();
        btn_sale_report_item_wise_search = new javax.swing.JButton();
        txt_sale_report_item_wise_search = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        jmc_txt_sale_report_item_wise_month = new com.toedter.calendar.JMonthChooser();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jyc_txt_sale_report_item_wise_year = new com.toedter.calendar.JYearChooser();
        jPanel33 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_dis_report_item_wise = new javax.swing.JTable();
        jPanel14 = new javax.swing.JPanel();
        btn_dis_report_item_wise_search = new javax.swing.JButton();
        txt_dis_report_item_wise_search = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        jmc_dis_report_item_wise_month1 = new com.toedter.calendar.JMonthChooser();
        jLabel8 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jyc_dis_report_item_wise_year = new com.toedter.calendar.JYearChooser();
        jPanel37 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbl_total_sale_report_item_wise = new javax.swing.JTable();
        jPanel15 = new javax.swing.JPanel();
        btn_total_sale_report_item_wise_search = new javax.swing.JButton();
        txt_total_sale_report_item_wise_search = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();
        jmc_total_sale_report_item_wise_month2 = new com.toedter.calendar.JMonthChooser();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jyc_total_sale_report_item_wise_year = new com.toedter.calendar.JYearChooser();
        jPanel9 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbl_total_sale = new javax.swing.JTable();
        jPanel16 = new javax.swing.JPanel();
        btn_total_sale = new javax.swing.JButton();
        jmc_total_sale = new com.toedter.calendar.JMonthChooser();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jyc_total_sale = new com.toedter.calendar.JYearChooser();
        jdc_total_sale = new com.toedter.calendar.JDateChooser();
        check_date = new javax.swing.JCheckBox();
        jPanel12 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tbl_total_item = new javax.swing.JTable();
        jPanel18 = new javax.swing.JPanel();
        btn_total_item = new javax.swing.JButton();
        jmc_total_item = new com.toedter.calendar.JMonthChooser();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jyc_total_item = new com.toedter.calendar.JYearChooser();
        jdc_total_item = new com.toedter.calendar.JDateChooser();
        check_date_total_item = new javax.swing.JCheckBox();
        jPanel19 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tbl_total_designation_sale = new javax.swing.JTable();
        jPanel21 = new javax.swing.JPanel();
        btn_total_designation_sale_search = new javax.swing.JButton();
        jmc_total_designation_sale = new com.toedter.calendar.JMonthChooser();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jyc_total_designation_sale = new com.toedter.calendar.JYearChooser();
        jdc_total_designation_sale = new com.toedter.calendar.JDateChooser();
        check_total_designation_sale = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        jButton1.setText("jButton1");

        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane3.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        tbl_sale_item.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "ITEM", "PRICE", "DIS ITEM", "LAST BALANCE ITEM", "TOTAL ITEM", "QUANTITY", "TOTAL SALE", "BALANCE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_sale_item.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_sale_itemMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_sale_item);
        if (tbl_sale_item.getColumnModel().getColumnCount() > 0) {
            tbl_sale_item.getColumnModel().getColumn(0).setResizable(false);
            tbl_sale_item.getColumnModel().getColumn(1).setResizable(false);
            tbl_sale_item.getColumnModel().getColumn(2).setResizable(false);
            tbl_sale_item.getColumnModel().getColumn(3).setResizable(false);
            tbl_sale_item.getColumnModel().getColumn(4).setResizable(false);
            tbl_sale_item.getColumnModel().getColumn(5).setResizable(false);
            tbl_sale_item.getColumnModel().getColumn(6).setResizable(false);
            tbl_sale_item.getColumnModel().getColumn(7).setResizable(false);
            tbl_sale_item.getColumnModel().getColumn(8).setResizable(false);
        }

        btn_sale_search.setText("Search");
        btn_sale_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_sale_searchActionPerformed(evt);
            }
        });

        btn_sale_add.setText("New Sale");
        btn_sale_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_sale_addActionPerformed(evt);
            }
        });

        txt_designation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_designationActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Designation :");

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jdc_sale_date.setDateFormatString("dd/MM/yyyy");
        jdc_sale_date.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdc_sale_datePropertyChange(evt);
            }
        });

        jButton2.setText("View Sale");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("Date :");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_designation, javax.swing.GroupLayout.PREFERRED_SIZE, 395, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_sale_date, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_sale_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_sale_add)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel10Layout.createSequentialGroup()
                            .addGap(1, 1, 1)
                            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txt_designation, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addComponent(jLabel6)))
                        .addComponent(jdc_sale_date, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btn_sale_add, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton2))
                        .addComponent(jSeparator2))
                    .addComponent(btn_sale_search))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel10Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel2, jdc_sale_date, txt_designation});

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1025, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 513, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Sales Item ", jPanel5);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        jTabbedPane3.addTab("Sales Management", jPanel3);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane4.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        tbl_sale_report_item_wise.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbl_sale_report_item_wise.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_sale_report_item_wiseKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tbl_sale_report_item_wise);

        btn_sale_report_item_wise_search.setText("Search");
        btn_sale_report_item_wise_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_sale_report_item_wise_searchActionPerformed(evt);
            }
        });
        btn_sale_report_item_wise_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_sale_report_item_wise_searchKeyPressed(evt);
            }
        });

        txt_sale_report_item_wise_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_sale_report_item_wise_searchActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Search :");

        jSeparator6.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jmc_txt_sale_report_item_wise_month.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_txt_sale_report_item_wise_monthPropertyChange(evt);
            }
        });
        jmc_txt_sale_report_item_wise_month.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_txt_sale_report_item_wise_monthKeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Month :");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Year :");

        jyc_txt_sale_report_item_wise_year.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_txt_sale_report_item_wise_yearPropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_sale_report_item_wise_search, javax.swing.GroupLayout.PREFERRED_SIZE, 599, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jmc_txt_sale_report_item_wise_month, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jyc_txt_sale_report_item_wise_year, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btn_sale_report_item_wise_search)
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel13Layout.createSequentialGroup()
                            .addGap(4, 4, 4)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btn_sale_report_item_wise_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jSeparator6)
                                .addComponent(jyc_txt_sale_report_item_wise_year, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jmc_txt_sale_report_item_wise_month, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addComponent(txt_sale_report_item_wise_search, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel13Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_sale_report_item_wise_search, jLabel4, jLabel5, jmc_txt_sale_report_item_wise_month, jyc_txt_sale_report_item_wise_year});

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane4.addTab("Sale Report Item Wise", jPanel6);

        jPanel33.setBackground(new java.awt.Color(255, 255, 255));

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));

        tbl_dis_report_item_wise.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbl_dis_report_item_wise.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_dis_report_item_wiseKeyPressed(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_dis_report_item_wise);

        btn_dis_report_item_wise_search.setText("Search");
        btn_dis_report_item_wise_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_dis_report_item_wise_searchActionPerformed(evt);
            }
        });
        btn_dis_report_item_wise_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_dis_report_item_wise_searchKeyPressed(evt);
            }
        });

        txt_dis_report_item_wise_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_dis_report_item_wise_searchActionPerformed(evt);
            }
        });
        txt_dis_report_item_wise_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_dis_report_item_wise_searchKeyPressed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("Search :");

        jSeparator7.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jmc_dis_report_item_wise_month1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_dis_report_item_wise_month1PropertyChange(evt);
            }
        });
        jmc_dis_report_item_wise_month1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_dis_report_item_wise_month1KeyPressed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setText("Month :");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setText("Year :");

        jyc_dis_report_item_wise_year.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_dis_report_item_wise_yearPropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_dis_report_item_wise_search, javax.swing.GroupLayout.PREFERRED_SIZE, 599, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jmc_dis_report_item_wise_month1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jyc_dis_report_item_wise_year, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btn_dis_report_item_wise_search)
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txt_dis_report_item_wise_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_dis_report_item_wise_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jSeparator7)
                            .addComponent(jyc_dis_report_item_wise_year, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel14Layout.createSequentialGroup()
                            .addGap(3, 3, 3)
                            .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jmc_dis_report_item_wise_month1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel33Layout = new javax.swing.GroupLayout(jPanel33);
        jPanel33.setLayout(jPanel33Layout);
        jPanel33Layout.setHorizontalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel33Layout.setVerticalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane4.addTab("Dis Item Report Item Wise", jPanel33);

        jPanel37.setBackground(new java.awt.Color(255, 255, 255));

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        tbl_total_sale_report_item_wise.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbl_total_sale_report_item_wise.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_total_sale_report_item_wiseKeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(tbl_total_sale_report_item_wise);

        btn_total_sale_report_item_wise_search.setText("Search");
        btn_total_sale_report_item_wise_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_total_sale_report_item_wise_searchActionPerformed(evt);
            }
        });
        btn_total_sale_report_item_wise_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_total_sale_report_item_wise_searchKeyPressed(evt);
            }
        });

        txt_total_sale_report_item_wise_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_total_sale_report_item_wise_searchActionPerformed(evt);
            }
        });
        txt_total_sale_report_item_wise_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_total_sale_report_item_wise_searchKeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setText("Search :");

        jSeparator8.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jmc_total_sale_report_item_wise_month2.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_total_sale_report_item_wise_month2PropertyChange(evt);
            }
        });
        jmc_total_sale_report_item_wise_month2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_total_sale_report_item_wise_month2KeyPressed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setText("Month :");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel13.setText("Year :");

        jyc_total_sale_report_item_wise_year.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_total_sale_report_item_wise_yearPropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_total_sale_report_item_wise_search, javax.swing.GroupLayout.PREFERRED_SIZE, 599, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jmc_total_sale_report_item_wise_month2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jyc_total_sale_report_item_wise_year, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btn_total_sale_report_item_wise_search)
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txt_total_sale_report_item_wise_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_total_sale_report_item_wise_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jSeparator8)
                            .addComponent(jyc_total_sale_report_item_wise_year, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel15Layout.createSequentialGroup()
                            .addGap(3, 3, 3)
                            .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jmc_total_sale_report_item_wise_month2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel37Layout = new javax.swing.GroupLayout(jPanel37);
        jPanel37.setLayout(jPanel37Layout);
        jPanel37Layout.setHorizontalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel37Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel37Layout.setVerticalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel37Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane4.addTab("Sale Report Designation Wise", jPanel37);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));

        tbl_total_sale.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbl_total_sale.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_total_saleKeyPressed(evt);
            }
        });
        jScrollPane5.setViewportView(tbl_total_sale);

        btn_total_sale.setText("Search");
        btn_total_sale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_total_saleActionPerformed(evt);
            }
        });
        btn_total_sale.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_total_saleKeyPressed(evt);
            }
        });

        jmc_total_sale.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_total_salePropertyChange(evt);
            }
        });
        jmc_total_sale.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_total_saleKeyPressed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel14.setText("Month :");

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel15.setText("Year :");

        jyc_total_sale.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_total_salePropertyChange(evt);
            }
        });

        jdc_total_sale.setDateFormatString("dd/MM/yyyy");
        jdc_total_sale.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdc_total_salePropertyChange(evt);
            }
        });

        check_date.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        check_date.setText("  Date:");

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(check_date)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_total_sale, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jmc_total_sale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jyc_total_sale, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btn_total_sale)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jdc_total_sale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jmc_total_sale, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_total_sale, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jyc_total_sale, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel16Layout.createSequentialGroup()
                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1))
                        .addComponent(check_date)))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 1025, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane4.addTab("Total Sale", jPanel9);

        jPanel17.setBackground(new java.awt.Color(255, 255, 255));

        tbl_total_item.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbl_total_item.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_total_itemKeyPressed(evt);
            }
        });
        jScrollPane6.setViewportView(tbl_total_item);

        btn_total_item.setText("Search");
        btn_total_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_total_itemActionPerformed(evt);
            }
        });
        btn_total_item.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_total_itemKeyPressed(evt);
            }
        });

        jmc_total_item.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_total_itemPropertyChange(evt);
            }
        });
        jmc_total_item.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_total_itemKeyPressed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel16.setText("Month :");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel17.setText("Year :");

        jyc_total_item.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_total_itemPropertyChange(evt);
            }
        });

        jdc_total_item.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdc_total_itemPropertyChange(evt);
            }
        });

        check_date_total_item.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        check_date_total_item.setText("  Date:");

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(check_date_total_item)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_total_item, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jmc_total_item, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jyc_total_item, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btn_total_item)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jdc_total_item, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jmc_total_item, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_total_item, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jyc_total_item, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel18Layout.createSequentialGroup()
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1))
                        .addComponent(check_date_total_item)))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 1025, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane4.addTab("Total Item", jPanel12);

        jPanel20.setBackground(new java.awt.Color(255, 255, 255));

        tbl_total_designation_sale.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbl_total_designation_sale.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_total_designation_saleKeyPressed(evt);
            }
        });
        jScrollPane7.setViewportView(tbl_total_designation_sale);

        btn_total_designation_sale_search.setText("Search");
        btn_total_designation_sale_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_total_designation_sale_searchActionPerformed(evt);
            }
        });
        btn_total_designation_sale_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_total_designation_sale_searchKeyPressed(evt);
            }
        });

        jmc_total_designation_sale.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_total_designation_salePropertyChange(evt);
            }
        });
        jmc_total_designation_sale.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_total_designation_saleKeyPressed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel18.setText("Month :");

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel19.setText("Year :");

        jyc_total_designation_sale.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_total_designation_salePropertyChange(evt);
            }
        });

        jdc_total_designation_sale.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdc_total_designation_salePropertyChange(evt);
            }
        });

        check_total_designation_sale.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        check_total_designation_sale.setText("  Date:");

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(check_total_designation_sale)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_total_designation_sale, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jmc_total_designation_sale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jyc_total_designation_sale, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btn_total_designation_sale_search)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel21Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jdc_total_designation_sale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jmc_total_designation_sale, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_total_designation_sale_search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jyc_total_designation_sale, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel21Layout.createSequentialGroup()
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1))
                        .addComponent(check_total_designation_sale)))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 1025, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane4.addTab("Total Designation Sale", jPanel19);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane4)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane4)
        );

        jTabbedPane3.addTab("Sales Report", jPanel4);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane3)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane3)
        );

        jPanel2.setBackground(new java.awt.Color(230, 79, 6));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Sales Management");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void tbl_sale_report_item_wiseKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_sale_report_item_wiseKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_sale_report_item_wiseKeyPressed

    private void btn_sale_report_item_wise_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_sale_report_item_wise_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_sale_report_item_wise_searchKeyPressed

    private void jmc_txt_sale_report_item_wise_monthKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_txt_sale_report_item_wise_monthKeyPressed
        
    }//GEN-LAST:event_jmc_txt_sale_report_item_wise_monthKeyPressed

    private void btn_sale_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_sale_addActionPerformed
        Config.new_sales.onloadReset();
        Config.new_sales.setVisible(true);
    }//GEN-LAST:event_btn_sale_addActionPerformed

    private void btn_sale_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_sale_searchActionPerformed
        loadSaleDetails();
    }//GEN-LAST:event_btn_sale_searchActionPerformed

    private void txt_designationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_designationActionPerformed
      Config.designation_search.onloadReset(2);
      Config.designation_search.setVisible(true);
    }//GEN-LAST:event_txt_designationActionPerformed

    private void tbl_sale_itemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_sale_itemMouseClicked
        try {
            if (evt.getClickCount()==2) {
                Config.view_sales.onloadReset(tbl_sale_item_model.getValueAt(tbl_sale_item.getSelectedRow(),0).toString());
            Config.view_sales.setVisible(true);
            }
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_tbl_sale_itemMouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            Config.view_sales_item.onloadReset(tbl_sale_item_model.getValueAt(tbl_sale_item.getSelectedRow(), 0).toString(), designation_id, simple_date_formate.format(jdc_sale_date.getDate()));
            Config.view_sales_item.setVisible(true);
        } catch (Exception e) {
            
            JOptionPane.showMessageDialog(this, "Select Any row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jdc_sale_datePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_sale_datePropertyChange
        btn_sale_searchActionPerformed(null);
    }//GEN-LAST:event_jdc_sale_datePropertyChange

    private void btn_sale_report_item_wise_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_sale_report_item_wise_searchActionPerformed
        if(!txt_sale_report_item_wise_search.getText().equals("")){
            onloadSaleReport_ItemWise();
        }else{
            JOptionPane.showMessageDialog(this,"Set Designation Details", designation_id, WIDTH);
        }
    }//GEN-LAST:event_btn_sale_report_item_wise_searchActionPerformed

    private void txt_sale_report_item_wise_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_sale_report_item_wise_searchActionPerformed
      Config.designation_search.onloadReset(4);
      Config.designation_search.setVisible(true);
    }//GEN-LAST:event_txt_sale_report_item_wise_searchActionPerformed

    private void jmc_txt_sale_report_item_wise_monthPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_txt_sale_report_item_wise_monthPropertyChange
         onloadSaleReport_ItemWise();
    }//GEN-LAST:event_jmc_txt_sale_report_item_wise_monthPropertyChange

    private void jyc_txt_sale_report_item_wise_yearPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_txt_sale_report_item_wise_yearPropertyChange
        onloadSaleReport_ItemWise();
        
    }//GEN-LAST:event_jyc_txt_sale_report_item_wise_yearPropertyChange

    private void jyc_dis_report_item_wise_yearPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_dis_report_item_wise_yearPropertyChange
        onloadDisReport_ItemWise();
    }//GEN-LAST:event_jyc_dis_report_item_wise_yearPropertyChange

    private void jmc_dis_report_item_wise_month1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_dis_report_item_wise_month1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmc_dis_report_item_wise_month1KeyPressed

    private void jmc_dis_report_item_wise_month1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_dis_report_item_wise_month1PropertyChange
        onloadDisReport_ItemWise();
    }//GEN-LAST:event_jmc_dis_report_item_wise_month1PropertyChange

    private void txt_dis_report_item_wise_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_dis_report_item_wise_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_dis_report_item_wise_searchKeyPressed

    private void txt_dis_report_item_wise_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_dis_report_item_wise_searchActionPerformed
        Config.designation_search.onloadReset(5);
        Config.designation_search.setVisible(true);
    }//GEN-LAST:event_txt_dis_report_item_wise_searchActionPerformed

    private void btn_dis_report_item_wise_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_dis_report_item_wise_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_dis_report_item_wise_searchKeyPressed

    private void btn_dis_report_item_wise_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_dis_report_item_wise_searchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_dis_report_item_wise_searchActionPerformed

    private void tbl_dis_report_item_wiseKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_dis_report_item_wiseKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_dis_report_item_wiseKeyPressed

    private void tbl_total_sale_report_item_wiseKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_total_sale_report_item_wiseKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_total_sale_report_item_wiseKeyPressed

    private void btn_total_sale_report_item_wise_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_total_sale_report_item_wise_searchActionPerformed
        onloadSaleReport_DesignationWise();
    }//GEN-LAST:event_btn_total_sale_report_item_wise_searchActionPerformed

    private void btn_total_sale_report_item_wise_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_total_sale_report_item_wise_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_total_sale_report_item_wise_searchKeyPressed

    private void txt_total_sale_report_item_wise_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_total_sale_report_item_wise_searchActionPerformed
            Config.item_search.onloadReset(7, 3);
            Config.item_search.setVisible(true);
        
    }//GEN-LAST:event_txt_total_sale_report_item_wise_searchActionPerformed

    private void txt_total_sale_report_item_wise_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_total_sale_report_item_wise_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_total_sale_report_item_wise_searchKeyPressed

    private void jmc_total_sale_report_item_wise_month2PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_total_sale_report_item_wise_month2PropertyChange
        onloadSaleReport_DesignationWise();
    }//GEN-LAST:event_jmc_total_sale_report_item_wise_month2PropertyChange

    private void jmc_total_sale_report_item_wise_month2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_total_sale_report_item_wise_month2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmc_total_sale_report_item_wise_month2KeyPressed

    private void jyc_total_sale_report_item_wise_yearPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_total_sale_report_item_wise_yearPropertyChange
        onloadSaleReport_DesignationWise();
    }//GEN-LAST:event_jyc_total_sale_report_item_wise_yearPropertyChange

    private void tbl_total_saleKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_total_saleKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_total_saleKeyPressed

    private void btn_total_saleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_total_saleActionPerformed
        onloadTotalSaleReport_ItemWise();
    }//GEN-LAST:event_btn_total_saleActionPerformed

    private void btn_total_saleKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_total_saleKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_total_saleKeyPressed

    private void jmc_total_salePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_total_salePropertyChange
        onloadTotalSaleReport_ItemWise();
    }//GEN-LAST:event_jmc_total_salePropertyChange

    private void jmc_total_saleKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_total_saleKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmc_total_saleKeyPressed

    private void jyc_total_salePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_total_salePropertyChange
        onloadTotalSaleReport_ItemWise();
    }//GEN-LAST:event_jyc_total_salePropertyChange

    private void jdc_total_salePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_total_salePropertyChange
        onloadTotalSaleReport_ItemWise();
    }//GEN-LAST:event_jdc_total_salePropertyChange

    private void tbl_total_itemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_total_itemKeyPressed
        onloadTotalItemReport_ItemWise();
    }//GEN-LAST:event_tbl_total_itemKeyPressed

    private void btn_total_itemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_total_itemActionPerformed
        onloadTotalItemReport_ItemWise();
    }//GEN-LAST:event_btn_total_itemActionPerformed

    private void btn_total_itemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_total_itemKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_total_itemKeyPressed

    private void jmc_total_itemPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_total_itemPropertyChange
        onloadTotalItemReport_ItemWise();
    }//GEN-LAST:event_jmc_total_itemPropertyChange

    private void jmc_total_itemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_total_itemKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmc_total_itemKeyPressed

    private void jyc_total_itemPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_total_itemPropertyChange
        onloadTotalItemReport_ItemWise();
    }//GEN-LAST:event_jyc_total_itemPropertyChange

    private void jdc_total_itemPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_total_itemPropertyChange
        onloadTotalItemReport_ItemWise();
    }//GEN-LAST:event_jdc_total_itemPropertyChange

    private void tbl_total_designation_saleKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_total_designation_saleKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_total_designation_saleKeyPressed

    private void btn_total_designation_sale_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_total_designation_sale_searchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_total_designation_sale_searchActionPerformed

    private void btn_total_designation_sale_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_total_designation_sale_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_total_designation_sale_searchKeyPressed

    private void jmc_total_designation_salePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_total_designation_salePropertyChange
        onloadTotalDesignationSale_Report_DesignationWise();
    }//GEN-LAST:event_jmc_total_designation_salePropertyChange

    private void jmc_total_designation_saleKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_total_designation_saleKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmc_total_designation_saleKeyPressed

    private void jyc_total_designation_salePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_total_designation_salePropertyChange
        onloadTotalDesignationSale_Report_DesignationWise();
    }//GEN-LAST:event_jyc_total_designation_salePropertyChange

    private void jdc_total_designation_salePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_total_designation_salePropertyChange
        onloadTotalDesignationSale_Report_DesignationWise();
    }//GEN-LAST:event_jdc_total_designation_salePropertyChange
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_dis_report_item_wise_search;
    private javax.swing.JButton btn_sale_add;
    private javax.swing.JButton btn_sale_report_item_wise_search;
    private javax.swing.JButton btn_sale_search;
    private javax.swing.JButton btn_total_designation_sale_search;
    private javax.swing.JButton btn_total_item;
    private javax.swing.JButton btn_total_sale;
    private javax.swing.JButton btn_total_sale_report_item_wise_search;
    private javax.swing.JCheckBox check_date;
    private javax.swing.JCheckBox check_date_total_item;
    private javax.swing.JCheckBox check_total_designation_sale;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel37;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTabbedPane jTabbedPane4;
    private com.toedter.calendar.JDateChooser jdc_sale_date;
    private com.toedter.calendar.JDateChooser jdc_total_designation_sale;
    private com.toedter.calendar.JDateChooser jdc_total_item;
    private com.toedter.calendar.JDateChooser jdc_total_sale;
    private com.toedter.calendar.JMonthChooser jmc_dis_report_item_wise_month1;
    private com.toedter.calendar.JMonthChooser jmc_total_designation_sale;
    private com.toedter.calendar.JMonthChooser jmc_total_item;
    private com.toedter.calendar.JMonthChooser jmc_total_sale;
    private com.toedter.calendar.JMonthChooser jmc_total_sale_report_item_wise_month2;
    private com.toedter.calendar.JMonthChooser jmc_txt_sale_report_item_wise_month;
    private com.toedter.calendar.JYearChooser jyc_dis_report_item_wise_year;
    private com.toedter.calendar.JYearChooser jyc_total_designation_sale;
    private com.toedter.calendar.JYearChooser jyc_total_item;
    private com.toedter.calendar.JYearChooser jyc_total_sale;
    private com.toedter.calendar.JYearChooser jyc_total_sale_report_item_wise_year;
    private com.toedter.calendar.JYearChooser jyc_txt_sale_report_item_wise_year;
    private javax.swing.JTable tbl_dis_report_item_wise;
    private javax.swing.JTable tbl_sale_item;
    private javax.swing.JTable tbl_sale_report_item_wise;
    private javax.swing.JTable tbl_total_designation_sale;
    private javax.swing.JTable tbl_total_item;
    private javax.swing.JTable tbl_total_sale;
    private javax.swing.JTable tbl_total_sale_report_item_wise;
    private javax.swing.JTextField txt_designation;
    private javax.swing.JTextField txt_dis_report_item_wise_search;
    private javax.swing.JTextField txt_sale_report_item_wise_search;
    private javax.swing.JTextField txt_total_sale_report_item_wise_search;
    // End of variables declaration//GEN-END:variables
    
    public void onloadReset(){
        txt_designation.setText("");
        designation_id="";
        jdc_sale_date.setDate(null);
    }

    public void loadSaleDetails() {
               try {
            Config.sql ="SELECT\n" +
                    "b.*,d.name,\n" +
                    "(select prise from item_list where item_list_id=b.item_list_id) as price,\n" +
                    "COALESCE(\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from distribution_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_sale_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from return_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_sale_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from demage_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where di.distribution_details_id=b.distribution_details_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_sale_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    ",0)\n" +
                    "as dis\n ," +
                    "COALESCE(\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from distribution_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where f.designation_id=c.designation_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')<STR_TO_DATE('"+simple_date_formate.format(jdc_sale_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from return_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where f.designation_id=c.designation_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')<STR_TO_DATE('"+simple_date_formate.format(jdc_sale_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from demage_item di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where f.designation_id=c.designation_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')<STR_TO_DATE('"+simple_date_formate.format(jdc_sale_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    "-\n" +
                    "(\n" +
                    "select\n" +
                    "COALESCE(sum(quantity),0)\n" +
                    "from sales_details di\n" +
                    "inner join distribution_details f on f.distribution_details_id=di.distribution_details_id\n" +
                    "where f.designation_id=c.designation_id and di.item_id=b.item_id\n" +
                    "and STR_TO_DATE(f._date,'%d/%m/%Y')<STR_TO_DATE('"+simple_date_formate.format(jdc_sale_date.getDate())+"','%d/%m/%Y')\n" +
                    ")\n" +
                    ",0)\n" +
                    "as quan\n" +
                    "FROM sales_details b\n" +
                    "inner join distribution_details c on c.distribution_details_id=b.distribution_details_id\n" +
                    "inner join stock_item d on d.item_id=b.item_id\n" +
                    "where c.designation_id='"+designation_id+"'\n" +
                    "and STR_TO_DATE(c._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_sale_date.getDate())+"','%d/%m/%Y') and d.identity='M'";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            tbl_sale_item_model.setRowCount(0);
            while(Config.rs.next()) {
                tbl_sale_item_model.addRow(new Object[]{
                Config.rs.getString("b.sales_details_id"),
                Config.rs.getString("d.name"),
                Config.rs.getString("price"),
                Double.parseDouble(Config.rs.getString("dis")),
                Config.rs.getString("quan"),
                Double.parseDouble(Config.rs.getString("dis"))+Double.parseDouble(Config.rs.getString("quan")),
                Config.rs.getString("b.quantity"),
                Config.rs.getString("b.sale"),
                Double.parseDouble(Config.rs.getString("dis"))+Double.parseDouble(Config.rs.getString("quan"))-Double.parseDouble(Config.rs.getString("b.quantity")),
                Config.rs.getString("b.item_list_id")
                });
            }
           
       } catch (Exception e) {
           
       }
    }

    public void setDesignation(String designation_id) {
        this.designation_id = designation_id;
        txt_designation.setText(Config.config_designation_details.get(Config.id_designation_details.indexOf(designation_id)).getDesignation());
        btn_sale_searchActionPerformed(null);
    }
    public void setDesignationAndDate(String designation_id ,String date){
        this.designation_id = designation_id;
        txt_designation.setText(Config.config_designation_details.get(Config.id_designation_details.indexOf(designation_id)).getDesignation());
        try {
            jdc_sale_date.setDate(simple_date_formate.parse(date));
        } catch (Exception e) {
        }
        btn_sale_searchActionPerformed(null);
        
    }

    private void onloadSaleReport_ItemWise() {
            //.........................................................................................................................................
        int col_height=0;  
        int row=1;  
        int year = jyc_txt_sale_report_item_wise_year.getYear();
        int month = jmc_txt_sale_report_item_wise_month.getMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        int total = calendar.getActualMaximum(Calendar.DATE);
        int count=0;
        for (int i = 0; i < Config.config_stock_item.size(); i++) {
            if(Config.config_stock_item.get(i).getIdentity().equals("M")){
                count++;
            }
        }
        
        
        int size= count+2;
        
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = new boolean [size];
        String []col = new String[size];
        Object [][]obj = new Object[total][size];
        
        
        //...................................................................SET FIRST COLUMN ON DATE.....................................................
        for (int i = 0; i < total; i++) {
         if(i<9){
             if(month<9){
                 obj[i][0]="0"+(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]="0"+(i+1)+"/"+(month+1)+"/"+year;
             }
         }else{
             if(month<9){
                 obj[i][0]=(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]=(i+1)+"/"+(month+1)+"/"+year;
             }
         }

            for (int j = 1; j < size-1; j++) {
                obj[i][j]=0.0;
            }
        }
        //...................................................................SET COLUMN NAME.....................................................
        can[0]=false;
        col[0]="  DATE";
            for (int i = 0; i < Config.config_stock_item.size(); i++) {
            if(Config.config_stock_item.get(i).getIdentity().equals("M")){
            can[row]=false;
            col[row]=Config.config_stock_item.get(i).getName();
            row++;
            }
            }
        can[row]=false;
        col[row]="TOTAL";
            row=1;
        
        //...................................................................SET DATE ON CELL.....................................................
            try {
            Config.sql ="SELECT s.*,d._date "
                        + "FROM sales_details s "
                        + "inner join distribution_details d "
                        + "on d.distribution_details_id=s.distribution_details_id "
                        + "where  d._year='"+year+"' and d._month='"+(month+1)+"' and d.designation_id='"+sale_report_item_wise_designation_id+"'";
//            (Config.sql);
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()){
            int d=1;
                for (int j = 0; j < Config.config_stock_item.size(); j++) {
                    if(Config.config_stock_item.get(j).getIdentity().equals("M")){
                    if(Config.config_stock_item.get(j).getItem_id().equals(Config.rs.getString("s.item_id"))){
//                    (Config.rs.getString("s.sale"));
                    obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("d._date"))))-1][d]=Config.rs.getString("s.sale");
                    
                    break;
                    }
                    d++;
                    }
            }
            }
            }catch(Exception  e){
               
            }
        //------------------------------------------------------------------total----------------------------------------------------------
            for (int i = 0; i < total; i++) {
                Double amount=0.0;
                for (int j = 1; j < size-1; j++) {
                    try {
                        amount=amount+Double.parseDouble(obj[i][j].toString());
                    } catch (Exception e) {
                    }
                }
                obj[i][size-1]=amount;
            }
            
        //...................................................................SET TABLE..................................................... 
                tbl_sale_report_item_wise.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                return can [columnIndex];
                }
                });

                tbl_sale_report_item_wise.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
                tbl_sale_report_item_wise.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(100);
                
                for (int j = 1; j < size-1; j++) {
                tbl_sale_report_item_wise.getTableHeader().getColumnModel().getColumn(j).setResizable(false);
                tbl_sale_report_item_wise.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(col[j].length()*10);
                
                row++;
                }    
                tbl_sale_report_item_wise.getTableHeader().getColumnModel().getColumn(row).setResizable(false);
                tbl_sale_report_item_wise.getTableHeader().getColumnModel().getColumn(row).setPreferredWidth(100);
                
        //...................................................................SET DATE ON CELL.....................................................    

    }

    public void setDesignation_SaleReport_ItemWise(String sale_report_item_wise_designation_id) {
        this.sale_report_item_wise_designation_id = sale_report_item_wise_designation_id;
        txt_sale_report_item_wise_search.setText(Config.config_designation_details.get(Config.id_designation_details.indexOf(sale_report_item_wise_designation_id)).getDesignation());
        onloadSaleReport_ItemWise();
    }

    public void setDesignation_ItemReport_ItemWise(String dis_report_item_wise_designation_id) {
        this.dis_report_item_wise_designation_id = dis_report_item_wise_designation_id;
        txt_dis_report_item_wise_search.setText(Config.config_designation_details.get(Config.id_designation_details.indexOf(dis_report_item_wise_designation_id)).getDesignation());
        onloadDisReport_ItemWise();
    }

    private void onloadDisReport_ItemWise() {
            //.........................................................................................................................................
        int col_height=0;  
        int row=1;  
        int year = jyc_dis_report_item_wise_year.getYear();
        int month = jmc_dis_report_item_wise_month1.getMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        int total = calendar.getActualMaximum(Calendar.DATE);
        int count=0;
        for (int i = 0; i < Config.config_stock_item.size(); i++) {
            if(Config.config_stock_item.get(i).getIdentity().equals("M")){
                count++;
            }
        }
        
        
        int size= count+2;
        
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = new boolean [size];
        String []col = new String[size];
        Object [][]obj = new Object[total][size];
        
        
        //...................................................................SET FIRST COLUMN ON DATE.....................................................
        for (int i = 0; i < total; i++) {
         if(i<9){
             if(month<9){
                 obj[i][0]="0"+(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]="0"+(i+1)+"/"+(month+1)+"/"+year;
             }
         }else{
             if(month<9){
                 obj[i][0]=(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]=(i+1)+"/"+(month+1)+"/"+year;
             }
         }

            for (int j = 1; j < size-1; j++) {
                obj[i][j]=0.0;
            }
        }
        //...................................................................SET COLUMN NAME.....................................................
        can[0]=false;
        col[0]="  DATE";
            for (int i = 0; i < Config.config_stock_item.size(); i++) {
            if(Config.config_stock_item.get(i).getIdentity().equals("M")){
            can[row]=false;
            col[row]=Config.config_stock_item.get(i).getName();
            row++;
            }
            }
        can[row]=false;
        col[row]="TOTAL";
            row=1;
        
        //...................................................................SET DATE ON CELL.....................................................
            try {
            Config.sql ="SELECT s.*,d._date "
                        + "FROM sales_details s "
                        + "inner join distribution_details d "
                        + "on d.distribution_details_id=s.distribution_details_id "
                        + "where  d._year='"+year+"' and d._month='"+(month+1)+"' and d.designation_id='"+dis_report_item_wise_designation_id+"'";
//            (Config.sql);
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()){
            int d=1;
                for (int j = 0; j < Config.config_stock_item.size(); j++) {
                    if(Config.config_stock_item.get(j).getIdentity().equals("M")){
                    if(Config.config_stock_item.get(j).getItem_id().equals(Config.rs.getString("s.item_id"))){
//                    (Config.rs.getString("s.sale"));
                    obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("d._date"))))-1][d]=Config.rs.getString("s.quantity");
                    
                    break;
                    }
                    d++;
                    }
            }
            }
            }catch(Exception  e){
               
            }
        //------------------------------------------------------------------total----------------------------------------------------------
            
        //...................................................................SET TABLE..................................................... 
                tbl_dis_report_item_wise.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                return can [columnIndex];
                }
                });

                tbl_dis_report_item_wise.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
                tbl_dis_report_item_wise.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(100);
                
                for (int j = 1; j < size-1; j++) {
                tbl_dis_report_item_wise.getTableHeader().getColumnModel().getColumn(j).setResizable(false);
                tbl_dis_report_item_wise.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(col[j].length()*10);
                
                row++;
                }    
                tbl_dis_report_item_wise.getTableHeader().getColumnModel().getColumn(row).setResizable(false);
                tbl_dis_report_item_wise.getTableHeader().getColumnModel().getColumn(row).setPreferredWidth(100);
                
        //...................................................................SET DATE ON CELL.....................................................    
    }

    
    private void onloadSaleReport_DesignationWise() {
            //.........................................................................................................................................
        int col_height=0;  
        int row=1;  
        int year = jyc_total_sale_report_item_wise_year.getYear();
        int month = jmc_total_sale_report_item_wise_month2.getMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        int total = calendar.getActualMaximum(Calendar.DATE);
        
        int size= Config.config_designation_details.size()+2;
        
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = new boolean [size];
        String []col = new String[size];
        Object [][]obj = new Object[total][size];
        
        
        //...................................................................SET FIRST COLUMN ON DATE.....................................................
        for (int i = 0; i < total; i++) {
         if(i<9){
             if(month<9){
                 obj[i][0]="0"+(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]="0"+(i+1)+"/"+(month+1)+"/"+year;
             }
         }else{
             if(month<9){
                 obj[i][0]=(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]=(i+1)+"/"+(month+1)+"/"+year;
             }
         }

            for (int j = 1; j < size-1; j++) {
                obj[i][j]=0.0;
            }
        }
        //...................................................................SET COLUMN NAME.....................................................
        can[0]=false;
        col[0]="  DATE";
            for (int i = 0; i < Config.config_designation_details.size(); i++) {
            can[row]=false;
            col[row]=Config.config_designation_details.get(i).getDesignation();
            row++;
            }
        can[row]=false;
        col[row]="TOTAL";
            row=1;
        
        //...................................................................SET DATE ON CELL.....................................................
            try {
            Config.sql ="SELECT s.*,d._date,d.designation_id\n" +
                        "FROM sales_details s\n" +
                        "inner join distribution_details d\n" +
                        "on d.distribution_details_id=s.distribution_details_id\n" +
                        "where d._year='"+year+"' and d._month='"+(month+1)+"' and s.item_id='"+sale_report_designation_wiseitem_id+"'";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()){
                for (int j = 0; j < Config.config_designation_details.size(); j++) {
                    if(Config.config_designation_details.get(j).getDesignation_id().equals(Config.rs.getString("d.designation_id"))){
                    obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("d._date"))))-1][j+1]=Config.rs.getString("s.sale");
                    break;
                }
                }
             }
            }catch(Exception  e){
               
            }
        //------------------------------------------------------------------total----------------------------------------------------------
            for (int i = 0; i < total; i++) {
                Double amount=0.0;
                for (int j = 1; j < size-1; j++) {
                    try {
                        amount=amount+Double.parseDouble(obj[i][j].toString());
                    } catch (Exception e) {
                    }
                }
                obj[i][size-1]=amount;
            }
            

        //...................................................................SET TABLE..................................................... 
                tbl_total_sale_report_item_wise.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                return can [columnIndex];
                }
                });

                tbl_total_sale_report_item_wise.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
                tbl_total_sale_report_item_wise.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(100);
                
                for (int j = 1; j < size-1; j++) {
                tbl_total_sale_report_item_wise.getTableHeader().getColumnModel().getColumn(j).setResizable(false);
                tbl_total_sale_report_item_wise.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(col[j].length()*10);
                
                row++;
                }    
                tbl_total_sale_report_item_wise.getTableHeader().getColumnModel().getColumn(row).setResizable(false);
                tbl_total_sale_report_item_wise.getTableHeader().getColumnModel().getColumn(row).setPreferredWidth(100);
                
        //...................................................................SET DATE ON CELL.....................................................    
    }
    public void setSaleReportDesignationWiseItem(String sale_report_designation_wiseitem_id) {
        this.sale_report_designation_wiseitem_id = sale_report_designation_wiseitem_id;
        txt_total_sale_report_item_wise_search.setText(Config.config_stock_item.get(Config.id_stock_item.indexOf(sale_report_designation_wiseitem_id)).getName());
        onloadSaleReport_DesignationWise();
    }

    private void onloadTotalSaleReport_ItemWise() {
            //.........................................................................................................................................
        int col_height=0;  
        int row=1;  
        int year = jyc_total_sale.getYear();
        int month = jmc_total_sale.getMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        int total = calendar.getActualMaximum(Calendar.DATE);
        int count=0;
        for (int i = 0; i < Config.config_stock_item.size(); i++) {
            if(Config.config_stock_item.get(i).getIdentity().equals("M")){
                count++;
            }
        }
        int size= count+2;
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = new boolean [size];
        String []col = new String[size];
        Object [][]obj = new Object[total][size];
        //...................................................................SET FIRST COLUMN ON DATE.....................................................
        for (int i = 0; i < total; i++) {
         if(i<9){
             if(month<9){
                 obj[i][0]="0"+(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]="0"+(i+1)+"/"+(month+1)+"/"+year;
             }
         }else{
             if(month<9){
                 obj[i][0]=(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]=(i+1)+"/"+(month+1)+"/"+year;
             }
         }

            for (int j = 1; j < size-1; j++) {
                obj[i][j]=0.0;
            }
        }
        //...................................................................SET COLUMN NAME.....................................................
        can[0]=false;
        col[0]="  DATE";
            for (int i = 0; i < Config.config_stock_item.size(); i++) {
            if(Config.config_stock_item.get(i).getIdentity().equals("M")){
            can[row]=false;
            col[row]=Config.config_stock_item.get(i).getName();
            row++;
            }
            }
        can[row]=false;
        col[row]="TOTAL";
            row=1;
        
        //...................................................................SET DATE ON CELL.....................................................
            try {
                if(check_date.isSelected()){
                    if(jdc_total_sale.getDate()==null){
                      JOptionPane.showMessageDialog(this, "Select Any Date", "Error", JOptionPane.ERROR_MESSAGE);
                }else{
                    Config.sql ="SELECT s.item_id,d._date,sum(s.sale) as total_sal\n" +
                                "FROM sales_details s\n" +
                                "inner join distribution_details d\n" +
                                "on d.distribution_details_id=s.distribution_details_id\n" +
                                "group by s.item_id,d._year,d._month\n" +
                                "having d._year='"+year+"' and d._month='"+(month+1)+"' and STR_TO_DATE(d._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_total_sale.getDate())+"','%d/%m/%Y')";
                }
                }else{
                                Config.sql ="SELECT s.item_id,d._date,sum(s.sale) as total_sal\n" +
                                "FROM sales_details s\n" +
                                "inner join distribution_details d\n" +
                                "on d.distribution_details_id=s.distribution_details_id\n" +
                                "group by s.item_id,d._year,d._month\n" +
                                "having d._year='"+year+"' and d._month='"+(month+1)+"'";
                }
                
//            (Config.sql);
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()){
            int d=1;
                for (int j = 0; j < Config.config_stock_item.size(); j++) {
                    if(Config.config_stock_item.get(j).getIdentity().equals("M")){
                    if(Config.config_stock_item.get(j).getItem_id().equals(Config.rs.getString("s.item_id"))){
                    obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("d._date"))))-1][d]=Config.rs.getString("total_sal");
                    break;
                    }
                    d++;
                    }
            }
            }
            }catch(Exception  e){
               
            }
        //------------------------------------------------------------------total----------------------------------------------------------
            for (int i = 0; i < total; i++) {
                Double amount=0.0;
                for (int j = 1; j < size-1; j++) {
                    try {
                        amount=amount+Double.parseDouble(obj[i][j].toString());
                    } catch (Exception e) {
                    }
                }
                obj[i][size-1]=amount;
            }
        //...................................................................SET TABLE..................................................... 
                tbl_total_sale.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                return can [columnIndex];
                }
                });

                tbl_total_sale.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
                tbl_total_sale.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(100);
                
                for (int j = 1; j < size-1; j++) {
                tbl_total_sale.getTableHeader().getColumnModel().getColumn(j).setResizable(false);
                tbl_total_sale.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(col[j].length()*10);
                row++;
                }    
                tbl_total_sale.getTableHeader().getColumnModel().getColumn(row).setResizable(false);
                tbl_total_sale.getTableHeader().getColumnModel().getColumn(row).setPreferredWidth(100);
                
        //...................................................................SET DATE ON CELL.....................................................    
    }

    private void onloadTotalItemReport_ItemWise() {
        //.........................................................................................................................................
        int col_height=0;  
        int row=1;  
        int year = jyc_total_item.getYear();
        int month = jmc_total_item.getMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        int total = calendar.getActualMaximum(Calendar.DATE);
        int count=0;
        for (int i = 0; i < Config.config_stock_item.size(); i++) {
            if(Config.config_stock_item.get(i).getIdentity().equals("M")){
                count++;
            }
        }
        int size= count+2;
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = new boolean [size];
        String []col = new String[size];
        Object [][]obj = new Object[total][size];
        //...................................................................SET FIRST COLUMN ON DATE.....................................................
        for (int i = 0; i < total; i++) {
         if(i<9){
             if(month<9){
                 obj[i][0]="0"+(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]="0"+(i+1)+"/"+(month+1)+"/"+year;
             }
         }else{
             if(month<9){
                 obj[i][0]=(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]=(i+1)+"/"+(month+1)+"/"+year;
             }
         }

            for (int j = 1; j < size-1; j++) {
                obj[i][j]=0.0;
            }
        }
        //...................................................................SET COLUMN NAME.....................................................
        can[0]=false;
        col[0]="  DATE";
            for (int i = 0; i < Config.config_stock_item.size(); i++) {
            if(Config.config_stock_item.get(i).getIdentity().equals("M")){
            can[row]=false;
            col[row]=Config.config_stock_item.get(i).getName();
            row++;
            }
            }
        can[row]=false;
        col[row]="TOTAL";
            row=1;
        
        //...................................................................SET DATE ON CELL.....................................................
            try {
                if(check_date_total_item.isSelected()){
                    if(jdc_total_item.getDate()==null){
                      JOptionPane.showMessageDialog(this, "Select Any Date", "Error", JOptionPane.ERROR_MESSAGE);
                }else{
                    Config.sql ="SELECT s.item_id,d._date,sum(s.quantity) as total_quantity\n" +
                                "FROM sales_details s\n" +
                                "inner join distribution_details d\n" +
                                "on d.distribution_details_id=s.distribution_details_id\n" +
                                "group by s.item_id,d._year,d._month\n" +
                                "having d._year='"+year+"' and d._month='"+(month+1)+"' and STR_TO_DATE(d._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_total_item.getDate())+"','%d/%m/%Y')";
                }
                }else{
                                Config.sql ="SELECT s.item_id,d._date,sum(s.quantity) as total_quantity\n" +
                                "FROM sales_details s\n" +
                                "inner join distribution_details d\n" +
                                "on d.distribution_details_id=s.distribution_details_id\n" +
                                "group by s.item_id,d._year,d._month\n" +
                                "having d._year='"+year+"' and d._month='"+(month+1)+"'";
                }
                
//            (Config.sql);
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()){
            int d=1;
                for (int j = 0; j < Config.config_stock_item.size(); j++) {
                    if(Config.config_stock_item.get(j).getIdentity().equals("M")){
                    if(Config.config_stock_item.get(j).getItem_id().equals(Config.rs.getString("s.item_id"))){
                    obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("d._date"))))-1][d]=Config.rs.getString("total_quantity");
                    break;
                    }
                    d++;
                    }
            }
            }
            }catch(Exception  e){
               
            }
        //------------------------------------------------------------------total----------------------------------------------------------
            for (int i = 0; i < total; i++) {
                Double amount=0.0;
                for (int j = 1; j < size-1; j++) {
                    try {
                        amount=amount+Double.parseDouble(obj[i][j].toString());
                    } catch (Exception e) {
                    }
                }
                obj[i][size-1]=amount;
            }
        //...................................................................SET TABLE..................................................... 
                tbl_total_item.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                return can [columnIndex];
                }
                });

                tbl_total_item.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
                tbl_total_item.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(100);
                
                for (int j = 1; j < size-1; j++) {
                tbl_total_item.getTableHeader().getColumnModel().getColumn(j).setResizable(false);
                tbl_total_item.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(col[j].length()*10);
                row++;
                }    
                tbl_total_item.getTableHeader().getColumnModel().getColumn(row).setResizable(false);
                tbl_total_item.getTableHeader().getColumnModel().getColumn(row).setPreferredWidth(100);
                
        //...................................................................SET DATE ON CELL.....................................................    
    }

    private void onloadTotalDesignationSale_Report_DesignationWise() {
            //.........................................................................................................................................
        int col_height=0;  
        int row=1;  
        int year = jyc_total_designation_sale.getYear();
        int month = jmc_total_designation_sale.getMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        int total = calendar.getActualMaximum(Calendar.DATE);
        
        int size= Config.config_designation_details.size()+2;
        
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = new boolean [size];
        String []col = new String[size];
        Object [][]obj = new Object[total][size];
        
        
        //...................................................................SET FIRST COLUMN ON DATE.....................................................
        for (int i = 0; i < total; i++) {
         if(i<9){
             if(month<9){
                 obj[i][0]="0"+(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]="0"+(i+1)+"/"+(month+1)+"/"+year;
             }
         }else{
             if(month<9){
                 obj[i][0]=(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]=(i+1)+"/"+(month+1)+"/"+year;
             }
         }

            for (int j = 1; j < size-1; j++) {
                obj[i][j]=0.0;
            }
        }
        //...................................................................SET COLUMN NAME.....................................................
        can[0]=false;
        col[0]="  DATE";
            for (int i = 0; i < Config.config_designation_details.size(); i++) {
            can[row]=false;
            col[row]=Config.config_designation_details.get(i).getDesignation();
            row++;
            }
        can[row]=false;
        col[row]="TOTAL";
            row=1;
        
        //...................................................................SET DATE ON CELL.....................................................
            try {
                if(check_date_total_item.isSelected()){
                    if(jdc_total_item.getDate()==null){
                      JOptionPane.showMessageDialog(this, "Select Any Date", "Error", JOptionPane.ERROR_MESSAGE);
                }else{
                    Config.sql ="SELECT d.designation_id,d._date,sum(s.sale) as total_sale\n" +
                                "FROM sales_details s\n" +
                                "inner join distribution_details d\n" +
                                "on d.distribution_details_id=s.distribution_details_id\n" +
                                "group by d.designation_id,d._year,d._month\n" +
                                "having d._year='"+year+"' and d._month='"+(month+1)+"' and STR_TO_DATE(d._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_total_designation_sale.getDate())+"','%d/%m/%Y')";
                        }
                }else{
                        Config.sql ="SELECT d.designation_id,d._date,sum(s.sale) as total_sale\n" +
                                "FROM sales_details s\n" +
                                "inner join distribution_details d\n" +
                                "on d.distribution_details_id=s.distribution_details_id\n" +
                                "group by d.designation_id,d._year,d._month\n" +
                                "having d._year='"+year+"' and d._month='"+(month+1)+"'";
                }
                
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()){
                for (int j = 0; j < Config.config_designation_details.size(); j++) {
                    if(Config.config_designation_details.get(j).getDesignation_id().equals(Config.rs.getString("d.designation_id"))){
                    obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("d._date"))))-1][j+1]=Config.rs.getString("total_sale");
                    break;
                }
                }
             }
            }catch(Exception  e){
               
            }
        //------------------------------------------------------------------total----------------------------------------------------------
            for (int i = 0; i < total; i++) {
                Double amount=0.0;
                for (int j = 1; j < size-1; j++) {
                    try {
                        amount=amount+Double.parseDouble(obj[i][j].toString());
                    } catch (Exception e) {
                    }
                }
                obj[i][size-1]=amount;
            }
            

        //...................................................................SET TABLE..................................................... 
                tbl_total_designation_sale.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                return can [columnIndex];
                }
                });

                tbl_total_designation_sale.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
                tbl_total_designation_sale.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(100);
                
                for (int j = 1; j < size-1; j++) {
                tbl_total_designation_sale.getTableHeader().getColumnModel().getColumn(j).setResizable(false);
                tbl_total_designation_sale.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(col[j].length()*10);
                
                row++;
                }    
                tbl_total_designation_sale.getTableHeader().getColumnModel().getColumn(row).setResizable(false);
                tbl_total_designation_sale.getTableHeader().getColumnModel().getColumn(row).setPreferredWidth(100);
                
        //...................................................................SET DATE ON CELL.....................................................    
    }
    
}
