package modules.admin_management.stock;

import data_manager.configuration.Config;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class stockitem extends javax.swing.JPanel {
   
    public DefaultTableModel tbl_stock_item_model = null;
    
    public stockitem() {
        initComponents();
        tbl_stock_item_model = (DefaultTableModel) tbl_stockitem.getModel();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txt_stockitem = new javax.swing.JTextField();
        btn_stockitem_search = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        btn_stockitem_new = new javax.swing.JButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        tbl_stockitem = new javax.swing.JTable();
        btn_stockitem_remove = new javax.swing.JButton();
        btn_stockitem_view = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(835, 620));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Stock Details");

        jTabbedPane3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Item :");

        txt_stockitem.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_stockitemCaretUpdate(evt);
            }
        });

        btn_stockitem_search.setText("Search");
        btn_stockitem_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stockitem_searchActionPerformed(evt);
            }
        });

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        btn_stockitem_new.setText("New");
        btn_stockitem_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stockitem_newActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_stockitem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_stockitem_search, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_stockitem_new, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_stockitem_new, btn_stockitem_search});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btn_stockitem_new, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(txt_stockitem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_stockitem_search)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_stockitem_new, btn_stockitem_search, jLabel5, jSeparator2, txt_stockitem});

        tbl_stockitem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "ITEM NAME", "UNIT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_stockitem.getTableHeader().setReorderingAllowed(false);
        tbl_stockitem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_stockitemMouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(tbl_stockitem);
        if (tbl_stockitem.getColumnModel().getColumnCount() > 0) {
            tbl_stockitem.getColumnModel().getColumn(0).setResizable(false);
            tbl_stockitem.getColumnModel().getColumn(2).setResizable(false);
        }

        btn_stockitem_remove.setText("Remove");
        btn_stockitem_remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stockitem_removeActionPerformed(evt);
            }
        });

        btn_stockitem_view.setText("View");
        btn_stockitem_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stockitem_viewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 790, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_stockitem_remove, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_stockitem_view, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_stockitem_remove, btn_stockitem_view});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 453, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_stockitem_remove)
                    .addComponent(btn_stockitem_view))
                .addContainerGap())
        );

        jTabbedPane3.addTab("Stock Item", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jTabbedPane3))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 578, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tbl_stockitemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_stockitemMouseClicked
        if(evt.getClickCount()==2){
            btn_stockitem_viewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_stockitemMouseClicked

    private void btn_stockitem_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stockitem_viewActionPerformed
        try {
            Config.view_stock_item.onloadReset(tbl_stockitem.getValueAt(tbl_stockitem.getSelectedRow(), 0).toString());
            Config.view_stock_item.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,"Select Any row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_stockitem_viewActionPerformed

    private void btn_stockitem_removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stockitem_removeActionPerformed
        try {
             int dd = JOptionPane.showConfirmDialog(this,"Are you really want to delete this entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
         if (dd == 0){
            if(Config.stock_item_mgr.delStockItem(Config.id_stock_item.get(tbl_stockitem.getSelectedRow()))){
                Config.stock_item.onloadStockItem();
                JOptionPane.showMessageDialog(this, "Stock Details Delete successfully", "Success", JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion ", "Error", JOptionPane.ERROR_MESSAGE);
            }
         }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_stockitem_removeActionPerformed

    private void txt_stockitemCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_stockitemCaretUpdate
        btn_stockitem_searchActionPerformed(null);
    }//GEN-LAST:event_txt_stockitemCaretUpdate

    private void btn_stockitem_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stockitem_searchActionPerformed
        try {
            tbl_stock_item_model.setRowCount(0);
            if(!txt_stockitem.getText().equals("")){
                for (int i = 0; i < Config.config_stock_item.size(); i++) {
                    if(Config.config_stock_item.get(i).getName().toUpperCase().startsWith(txt_stockitem.getText().toUpperCase()) &&  Config.config_stock_item.get(i).getIdentity().equals("ST")){

                        tbl_stock_item_model.addRow(new Object[]{
                         Config.config_stock_item.get(i).getItem_id(),
                         Config.config_stock_item.get(i).getName(),
                         Config.config_stock_item.get(i).getUnit()
                         });
                        }
                    }
            } else {
                for (int i = 0; i < Config.config_stock_item.size(); i++) {
                    if(Config.config_stock_item.get(i).getName().toUpperCase().startsWith(txt_stockitem.getText().toUpperCase()) &&  Config.config_stock_item.get(i).getIdentity().equals("ST")){
                    tbl_stock_item_model.addRow(new Object[]{
                         Config.config_stock_item.get(i).getItem_id(),
                         Config.config_stock_item.get(i).getName(),
                         Config.config_stock_item.get(i).getUnit()
                         });
                    }
                }
            }
        } catch (Exception e) {
        e.printStackTrace();
        }        
    }//GEN-LAST:event_btn_stockitem_searchActionPerformed

    private void btn_stockitem_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stockitem_newActionPerformed
        Config.new_stock_item.onloadReset();
        Config.new_stock_item.setVisible(true);
    }//GEN-LAST:event_btn_stockitem_newActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_stockitem_new;
    private javax.swing.JButton btn_stockitem_remove;
    private javax.swing.JButton btn_stockitem_search;
    private javax.swing.JButton btn_stockitem_view;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTable tbl_stockitem;
    private javax.swing.JTextField txt_stockitem;
    // End of variables declaration//GEN-END:variables

    public void onloadReset() {        
        onloadStockItem();
    }    
    void onloadStockItem() {
        txt_stockitem.setText("");
        btn_stockitem_searchActionPerformed(null);
    }
}
