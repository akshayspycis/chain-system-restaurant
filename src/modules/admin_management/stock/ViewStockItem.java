package modules.admin_management.stock;

import data_manager.StockItem;
import data_manager.configuration.Config;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class ViewStockItem extends javax.swing.JDialog {

    public ViewStockItem(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        txt_name.setBackground(new Color(240,240,240));
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_name = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        cb_unit = new javax.swing.JComboBox();
        btn_delete = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        btn_update = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        lbl_id = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Stock Item", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Item Name");

        txt_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nameActionPerformed(evt);
            }
        });
        txt_name.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_nameKeyPressed(evt);
            }
        });

        jLabel3.setText("Unit");

        cb_unit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-Select-", "KG.", "LIT.", "PCS.", "NOS." }));
        cb_unit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_unitActionPerformed(evt);
            }
        });
        cb_unit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cb_unitKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cb_unit, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txt_name))
                        .addContainerGap())))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cb_unit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btn_delete.setText("Delete");
        btn_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteActionPerformed(evt);
            }
        });
        btn_delete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_deleteKeyPressed(evt);
            }
        });

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });
        btn_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_cancelKeyPressed(evt);
            }
        });

        btn_update.setText("Update");
        btn_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateActionPerformed(evt);
            }
        });
        btn_update.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_updateKeyPressed(evt);
            }
        });

        jPanel13.setBackground(new java.awt.Color(230, 79, 6));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("View Stock");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        lbl_id.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_delete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_id, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 153, Short.MAX_VALUE)
                        .addComponent(btn_update)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancel)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_cancel, btn_delete, btn_update});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_cancel)
                            .addComponent(btn_update)
                            .addComponent(btn_delete)))
                    .addComponent(lbl_id, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void txt_nameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nameKeyPressed
               if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            cb_unit.requestFocus();
            txt_name.setBackground(Color.WHITE);
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            cb_unit.requestFocus();
            txt_name.setBackground(Color.WHITE);
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT){
            btn_cancel.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP){
            btn_cancel.requestFocus();
        }  
    }//GEN-LAST:event_txt_nameKeyPressed

    private void btn_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_cancelKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            txt_name.requestFocus();
            txt_name.setBackground(new Color(240,240,240));
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            txt_name.requestFocus();
            txt_name.setBackground(new Color(240,240,240));
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT){
            btn_update.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP){
            cb_unit.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_SPACE){
            btn_cancelActionPerformed(null);
        }
    }//GEN-LAST:event_btn_cancelKeyPressed

    private void btn_deleteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_deleteKeyPressed
if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            btn_update.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            txt_name.requestFocus();
            txt_name.setBackground(new Color(240,240,240));
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT){
            cb_unit.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP){
            cb_unit.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_SPACE){
            btn_deleteActionPerformed(null);
        }          
    }//GEN-LAST:event_btn_deleteKeyPressed

    private void btn_updateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_updateKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            btn_cancel.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            txt_name.requestFocus();
            txt_name.setBackground(new Color(240,240,240));
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT){
            btn_delete.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP){
            cb_unit.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_SPACE){
            btn_updateActionPerformed(null);
        }
    }//GEN-LAST:event_btn_updateKeyPressed

    private void txt_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nameActionPerformed
        if(!txt_name.getText().equals("")){
            cb_unit.requestFocus();
        }
    }//GEN-LAST:event_txt_nameActionPerformed

    private void cb_unitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cb_unitKeyPressed
          if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            btn_update.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT){
            txt_name.requestFocus();
            txt_name.setBackground(new Color(240,240,240));
        }
        
    }//GEN-LAST:event_cb_unitKeyPressed

    private void cb_unitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_unitActionPerformed
        
    }//GEN-LAST:event_cb_unitActionPerformed

    private void btn_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateActionPerformed
         try {
            String str = checkValidity();
        if(str.equals("ok")){
//            if(checkAvailibility()){
                StockItem si = new StockItem();
                 si.setIdentity("ST");
                 si.setItem_id(lbl_id.getText());
                 si.setName(txt_name.getText().trim().toUpperCase());
                 si.setUnit(cb_unit.getSelectedItem().toString().trim().toUpperCase());
               
                if(Config.stock_item_mgr.updStockItem(si)){
                    Config.stock_item.onloadStockItem();
                    JOptionPane.showMessageDialog(this, "Stock Item Updation successfully", "Success", JOptionPane.NO_OPTION);
                    dispose();
                }else{
                    JOptionPane.showMessageDialog(this, "Error in updation ", "Error", JOptionPane.ERROR_MESSAGE);   
                }
//            }else{
//             JOptionPane.showMessageDialog(this, "Stock Item already exits", "Error", JOptionPane.ERROR_MESSAGE);   
//            }
        }else{
            JOptionPane.showMessageDialog(this, str +"should not be blank", "Error", JOptionPane.ERROR_MESSAGE);
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_updateActionPerformed

    private void btn_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteActionPerformed
           try {
                int dd = JOptionPane.showConfirmDialog(this,"Are you really want to delete this entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
         if (dd == 0){
            if (Config.stock_item_mgr.delStockItem(lbl_id.getText())){
                Config.stock_item.onloadStockItem();
                JOptionPane.showMessageDialog(this, "Stock Item Details Delete successfully", "Success", JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion ", "Error", JOptionPane.ERROR_MESSAGE);
            }
         }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_deleteActionPerformed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_delete;
    private javax.swing.JButton btn_update;
    private javax.swing.JComboBox cb_unit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lbl_id;
    private javax.swing.JTextField txt_name;
    // End of variables declaration//GEN-END:variables
  
    
    public void onloadReset(String id){
        btn_cancel.requestFocus();
        StockItem dd = Config.config_stock_item.get(Config.id_stock_item.indexOf(id));
        lbl_id.setText(dd.getItem_id());
        txt_name.setText(dd.getName());
        cb_unit.setSelectedItem(dd.getUnit());
    }

    private String checkValidity() {
        if(txt_name.getText().equals("")){
            return "Name";
        }else if(cb_unit.getSelectedIndex() == 0 ){
            return "Unit";
        }else{
            return "ok";
        }
    }
//
//    public boolean checkAvailibility() {
//        int i =0;        
//        for ( i = 0; i < Config.stock_item.tbl_stock_item_model.getRowCount(); i++) {
//            if(txt_name.getText().trim().toUpperCase().equals(Config.stock_item.tbl_stock_item_model.getValueAt(i, 1).toString()) && !lbl_id.getText().equals(Config.stock_item.tbl_stock_item_model.getValueAt(i, 0).toString())){
//                break;
//            }
//        }
//        if(i == Config.stock_item.tbl_stock_item_model.getRowCount()){
//         return true;    
//        }else{
//         return false; 
//        }
//    }
}
