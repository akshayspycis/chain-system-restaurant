package modules.admin_management.soda;

import data_manager.configuration.Config;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class Soda extends javax.swing.JPanel {
   
    public DefaultTableModel tbl_soda_model = null;
    
    public Soda() {
        initComponents();
        tbl_soda_model = (DefaultTableModel) tbl_soda.getModel();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txt_soda = new javax.swing.JTextField();
        btn_soda_search = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        btn_soda_new = new javax.swing.JButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        tbl_soda = new javax.swing.JTable();
        btn_soda_remove = new javax.swing.JButton();
        btn_soda_view = new javax.swing.JButton();
        txt_soda_cancel = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(835, 620));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Soda Details");

        jTabbedPane3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Item :");

        txt_soda.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_sodaCaretUpdate(evt);
            }
        });

        btn_soda_search.setText("Search");
        btn_soda_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_soda_searchActionPerformed(evt);
            }
        });

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        btn_soda_new.setText("New");
        btn_soda_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_soda_newActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_soda)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_soda_search, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_soda_new, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_soda_new, btn_soda_search});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btn_soda_new, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(txt_soda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_soda_search)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_soda_new, btn_soda_search, jLabel5, jSeparator2, txt_soda});

        tbl_soda.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "ITEM NAME", "UNIT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_soda.getTableHeader().setReorderingAllowed(false);
        tbl_soda.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_sodaMouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(tbl_soda);
        if (tbl_soda.getColumnModel().getColumnCount() > 0) {
            tbl_soda.getColumnModel().getColumn(0).setResizable(false);
            tbl_soda.getColumnModel().getColumn(2).setResizable(false);
        }

        btn_soda_remove.setText("Remove");
        btn_soda_remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_soda_removeActionPerformed(evt);
            }
        });

        btn_soda_view.setText("View");
        btn_soda_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_soda_viewActionPerformed(evt);
            }
        });

        txt_soda_cancel.setText("Cancel");
        txt_soda_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_soda_cancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 790, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_soda_remove, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_soda_view, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)
                        .addComponent(txt_soda_cancel)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_soda_remove, btn_soda_view, txt_soda_cancel});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 453, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_soda_remove)
                    .addComponent(btn_soda_view)
                    .addComponent(txt_soda_cancel))
                .addContainerGap())
        );

        jTabbedPane3.addTab("Soda Item", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jTabbedPane3))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 578, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tbl_sodaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_sodaMouseClicked
        if(evt.getClickCount()==2){
            btn_soda_viewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_sodaMouseClicked

    private void btn_soda_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_soda_viewActionPerformed
        try {
            Config.view_soda.onloadReset(tbl_soda.getValueAt(tbl_soda.getSelectedRow(), 0).toString());
            Config.view_soda.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,"Select Any row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_soda_viewActionPerformed

    private void btn_soda_removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_soda_removeActionPerformed
        try {
            int dd = JOptionPane.showConfirmDialog(this,"Are you really want to delete this entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
            if (dd == 0){
            if(Config.stock_item_mgr.delStockItem(tbl_soda_model.getValueAt(tbl_soda.getSelectedRow(),0).toString())){
               onloadSodaItem();
               JOptionPane.showMessageDialog(this,"Item delete Succesefully ", "Success", JOptionPane.NO_OPTION);
            }else{
               JOptionPane.showMessageDialog(this,"Error in deletion", "Error", JOptionPane.ERROR_MESSAGE);
            }
            }
        }catch (Exception e) {
            JOptionPane.showMessageDialog(this,"Select Any row", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_soda_removeActionPerformed

    private void txt_sodaCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_sodaCaretUpdate
        btn_soda_searchActionPerformed(null);
    }//GEN-LAST:event_txt_sodaCaretUpdate

    private void btn_soda_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_soda_searchActionPerformed
        
        try {
            tbl_soda_model.setRowCount(0);
            if(!txt_soda.getText().equals("")){
                for (int i = 0; i < Config.config_stock_item.size(); i++) {
                    if(Config.config_stock_item.get(i).getName().toUpperCase().startsWith(txt_soda.getText().toUpperCase()) &&  Config.config_stock_item.get(i).getIdentity().equals("S")){

                        tbl_soda_model.addRow(new Object[]{
                         Config.config_stock_item.get(i).getItem_id(),
                         Config.config_stock_item.get(i).getName(),
                         Config.config_stock_item.get(i).getUnit()
                         });
                        }
                    }
            } else {
                for (int i = 0; i < Config.config_stock_item.size(); i++) {
                    if(Config.config_stock_item.get(i).getName().toUpperCase().startsWith(txt_soda.getText().toUpperCase()) &&  Config.config_stock_item.get(i).getIdentity().equals("S")){
                    tbl_soda_model.addRow(new Object[]{
                         Config.config_stock_item.get(i).getItem_id(),
                         Config.config_stock_item.get(i).getName(),
                         Config.config_stock_item.get(i).getUnit()
                         });
                    }
                }
            }
        } catch (Exception e) {
        e.printStackTrace();
        }        
    }//GEN-LAST:event_btn_soda_searchActionPerformed

    private void btn_soda_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_soda_newActionPerformed
        Config.new_soda.onloadReset();
        Config.new_soda.setVisible(true);
    }//GEN-LAST:event_btn_soda_newActionPerformed

    private void txt_soda_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_soda_cancelActionPerformed
       
    }//GEN-LAST:event_txt_soda_cancelActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_soda_new;
    private javax.swing.JButton btn_soda_remove;
    private javax.swing.JButton btn_soda_search;
    private javax.swing.JButton btn_soda_view;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTable tbl_soda;
    private javax.swing.JTextField txt_soda;
    private javax.swing.JButton txt_soda_cancel;
    // End of variables declaration//GEN-END:variables

    public void onloadReset() {        
        onloadSodaItem();
    }    
    void onloadSodaItem() {
        txt_soda.setText("");
        btn_soda_searchActionPerformed(null);
    }
}
