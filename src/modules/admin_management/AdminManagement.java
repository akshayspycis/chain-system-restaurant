package modules.admin_management;

import data_manager.configuration.Config;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

public class AdminManagement extends javax.swing.JDialog {
    private CardLayout cl;    
   
    public AdminManagement(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);        
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jPanel24 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel7 = new javax.swing.JPanel();
        btn_productconfig = new javax.swing.JButton();
        btn_expenses = new javax.swing.JButton();
        btn_stock = new javax.swing.JButton();
        btn_item_liist = new javax.swing.JButton();
        btn_soda = new javax.swing.JButton();
        btn_report = new javax.swing.JButton();
        btn_manufacture = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        pnl_addpanel = new javax.swing.JPanel();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "MODULE NAME", "INSERT", "DELETE", "UPDATE", "SELECT", "PRINT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel1.setForeground(new java.awt.Color(153, 153, 153));
        jPanel1.setPreferredSize(new java.awt.Dimension(1047, 619));

        jPanel24.setBackground(new java.awt.Color(230, 79, 6));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Admin Management");

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
        );

        jSplitPane2.setBorder(null);
        jSplitPane2.setDividerLocation(220);
        jSplitPane2.setDividerSize(2);
        jSplitPane2.setEnabled(false);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));

        btn_productconfig.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/sidebar_pc.jpg"))); // NOI18N
        btn_productconfig.setBorder(null);
        btn_productconfig.setBorderPainted(false);
        btn_productconfig.setContentAreaFilled(false);
        btn_productconfig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_productconfigActionPerformed(evt);
            }
        });

        btn_expenses.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/sidebar_ed.jpg"))); // NOI18N
        btn_expenses.setBorder(null);
        btn_expenses.setBorderPainted(false);
        btn_expenses.setContentAreaFilled(false);
        btn_expenses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_expensesActionPerformed(evt);
            }
        });

        btn_stock.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/sidebar_sd.jpg"))); // NOI18N
        btn_stock.setBorder(null);
        btn_stock.setBorderPainted(false);
        btn_stock.setContentAreaFilled(false);
        btn_stock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stockActionPerformed(evt);
            }
        });

        btn_item_liist.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/sidebar_it.jpg"))); // NOI18N
        btn_item_liist.setBorder(null);
        btn_item_liist.setBorderPainted(false);
        btn_item_liist.setContentAreaFilled(false);
        btn_item_liist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_item_liistActionPerformed(evt);
            }
        });

        btn_soda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/sidebar_soda.jpg"))); // NOI18N
        btn_soda.setBorder(null);
        btn_soda.setBorderPainted(false);
        btn_soda.setContentAreaFilled(false);
        btn_soda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_sodaActionPerformed(evt);
            }
        });

        btn_report.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/sidebar_re.jpg"))); // NOI18N
        btn_report.setBorder(null);
        btn_report.setBorderPainted(false);
        btn_report.setContentAreaFilled(false);
        btn_report.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reportActionPerformed(evt);
            }
        });

        btn_manufacture.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/sidebar_md.jpg"))); // NOI18N
        btn_manufacture.setBorder(null);
        btn_manufacture.setBorderPainted(false);
        btn_manufacture.setContentAreaFilled(false);
        btn_manufacture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_manufactureActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_productconfig)
                    .addComponent(btn_expenses)
                    .addComponent(btn_stock)
                    .addComponent(btn_item_liist)
                    .addComponent(btn_soda)
                    .addComponent(btn_report)
                    .addComponent(btn_manufacture))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(btn_productconfig, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btn_expenses, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btn_stock, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btn_item_liist, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btn_soda, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btn_manufacture, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btn_report, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 332, Short.MAX_VALUE))
        );

        jSplitPane2.setLeftComponent(jPanel7);

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));

        pnl_addpanel.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout pnl_addpanelLayout = new javax.swing.GroupLayout(pnl_addpanel);
        pnl_addpanel.setLayout(pnl_addpanelLayout);
        pnl_addpanelLayout.setHorizontalGroup(
            pnl_addpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 836, Short.MAX_VALUE)
        );
        pnl_addpanelLayout.setVerticalGroup(
            pnl_addpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 620, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnl_addpanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnl_addpanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        cl = new CardLayout();
        pnl_addpanel.setLayout(cl);

        jSplitPane2.setRightComponent(jPanel9);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSplitPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1058, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSplitPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 620, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1060, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 661, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
      dispose();
    }//GEN-LAST:event_closeDialog

    private void btn_productconfigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_productconfigActionPerformed
        cl.show(pnl_addpanel, "1");
    }//GEN-LAST:event_btn_productconfigActionPerformed

    private void btn_expensesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_expensesActionPerformed
        cl.show(pnl_addpanel, "2");        
    }//GEN-LAST:event_btn_expensesActionPerformed

    private void btn_stockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stockActionPerformed
        cl.show(pnl_addpanel, "3");   
    }//GEN-LAST:event_btn_stockActionPerformed

    private void btn_item_liistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_item_liistActionPerformed
        cl.show(pnl_addpanel, "4");   
    }//GEN-LAST:event_btn_item_liistActionPerformed

    private void btn_sodaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_sodaActionPerformed
       cl.show(pnl_addpanel, "5");
    }//GEN-LAST:event_btn_sodaActionPerformed

    private void btn_reportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_reportActionPerformed
       cl.show(pnl_addpanel, "7");
    }//GEN-LAST:event_btn_reportActionPerformed

    private void btn_manufactureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_manufactureActionPerformed
       cl.show(pnl_addpanel, "6");
    }//GEN-LAST:event_btn_manufactureActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_expenses;
    private javax.swing.JButton btn_item_liist;
    private javax.swing.JButton btn_manufacture;
    private javax.swing.JButton btn_productconfig;
    private javax.swing.JButton btn_report;
    private javax.swing.JButton btn_soda;
    private javax.swing.JButton btn_stock;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JPanel pnl_addpanel;
    // End of variables declaration//GEN-END:variables
    
    public void onloadReset() {
        onloadProdut();
        onloadExpensese();
        onloadStockItem();
        onloadItemList();
        onloadSodaItem();
        onloadManufactureItem();
        onloadReport();
    }
    void onloadProdut(){
        Config.product_configuration.onloadReset();
        pnl_addpanel.add(Config.product_configuration, "1");
        }
    void onloadExpensese(){
        Config.expenses_details.onloadReset();
        pnl_addpanel.add(Config.expenses_details, "2");
    }
    void onloadStockItem(){
        Config.stock_item.onloadReset();
        pnl_addpanel.add(Config.stock_item, "3");
    }
    void onloadItemList(){
        Config.item_list.onloadReset();
        pnl_addpanel.add(Config.item_list, "4");
    }
    void onloadSodaItem(){
        Config.soda.onloadReset();
        pnl_addpanel.add(Config.soda, "5");
    }
    void onloadManufactureItem(){
        Config.manufactureitem.onloadReset();
        pnl_addpanel.add(Config.manufactureitem, "6");
    }
    void onloadReport(){
//        Config.report.onloadReset();
//        pnl_addpanel.add(Config.report, "7");
    }
}
