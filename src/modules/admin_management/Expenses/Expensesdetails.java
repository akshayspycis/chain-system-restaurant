package modules.admin_management.Expenses;

import data_manager.ExpensesDetails;
import data_manager.configuration.Config;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Expensesdetails extends javax.swing.JPanel {
  public DefaultTableModel tbl_expenses_model = null;
    
    public Expensesdetails() {
        initComponents();
        tbl_expenses_model = (DefaultTableModel) tbl_expenses.getModel();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel40 = new javax.swing.JPanel();
        jScrollPane29 = new javax.swing.JScrollPane();
        tbl_expenses = new javax.swing.JTable();
        btn_exp_view = new javax.swing.JButton();
        btn_exp_remove = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        txt_exp = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        btn_exp_add = new javax.swing.JButton();
        btn_exp_search = new javax.swing.JButton();
        jSeparator23 = new javax.swing.JSeparator();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(835, 620));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Expenses Details");

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jPanel40.setBackground(new java.awt.Color(255, 255, 255));

        tbl_expenses.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "TYPE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane29.setViewportView(tbl_expenses);
        if (tbl_expenses.getColumnModel().getColumnCount() > 0) {
            tbl_expenses.getColumnModel().getColumn(0).setResizable(false);
            tbl_expenses.getColumnModel().getColumn(1).setResizable(false);
        }

        btn_exp_view.setText("View");
        btn_exp_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_exp_viewActionPerformed(evt);
            }
        });

        btn_exp_remove.setText("Remove");
        btn_exp_remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_exp_removeActionPerformed(evt);
            }
        });

        txt_exp.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_expCaretUpdate(evt);
            }
        });
        txt_exp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_expKeyPressed(evt);
            }
        });

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel26.setText("Expenses :");

        btn_exp_add.setText("Add");
        btn_exp_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_exp_addActionPerformed(evt);
            }
        });
        btn_exp_add.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_exp_addKeyPressed(evt);
            }
        });

        btn_exp_search.setText("Search");
        btn_exp_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_exp_searchActionPerformed(evt);
            }
        });
        btn_exp_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_exp_searchKeyPressed(evt);
            }
        });

        jSeparator23.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel26)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_exp)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_exp_search, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_exp_add, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_exp_add, btn_exp_search});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator23)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_exp, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_exp_add)
                        .addComponent(btn_exp_search)))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel40Layout = new javax.swing.GroupLayout(jPanel40);
        jPanel40.setLayout(jPanel40Layout);
        jPanel40Layout.setHorizontalGroup(
            jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel40Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane29)
                    .addGroup(jPanel40Layout.createSequentialGroup()
                        .addComponent(btn_exp_remove, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_exp_view, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10))
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel40Layout.setVerticalGroup(
            jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel40Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane29, javax.swing.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
                .addGap(6, 6, 6)
                .addGroup(jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_exp_view)
                    .addComponent(btn_exp_remove))
                .addGap(8, 8, 8))
        );

        jTabbedPane1.addTab("Expenses", jPanel40);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel3)
                        .addGap(0, 701, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jTabbedPane1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel3)
                .addGap(6, 6, 6)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 580, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txt_expKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_expKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_expKeyPressed

    private void btn_exp_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_exp_searchActionPerformed
        try {
        tbl_expenses_model.setRowCount(0);
        String expenses;
        try {
            expenses = txt_exp.getText().trim();
        } catch (Exception e) {
            expenses = "";
        }        
        if(expenses.equals("")){
            for (int i = 0; i < Config.config_expenses_details.size(); i++) {
                ExpensesDetails dd = Config.config_expenses_details.get(i);
                if (Config.config_expenses_details.get(i).getType().toUpperCase().startsWith(expenses.toUpperCase())){
                
                tbl_expenses_model.addRow(new Object[] {
                    Config.config_expenses_details.get(i).getExpenses_details_id(),
                    Config.config_expenses_details.get(i).getType()                  
                });
            }
           }
        } else {
            for (int i = 0; i < Config.config_expenses_details.size(); i++) {
                ExpensesDetails dd = Config.config_expenses_details.get(i);
                if (Config.config_expenses_details.get(i).getType().toUpperCase().startsWith(expenses.toUpperCase())){
                    tbl_expenses_model.addRow(new Object[] {
                    Config.config_expenses_details.get(i).getExpenses_details_id(),
                    Config.config_expenses_details.get(i).getType()
                    });
                }
            }
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_exp_searchActionPerformed

    private void btn_exp_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_exp_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_exp_searchKeyPressed

    private void btn_exp_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_exp_addActionPerformed
       Config.new_expenses.onloadReset();
       Config.new_expenses.setVisible(true);
    }//GEN-LAST:event_btn_exp_addActionPerformed

    private void btn_exp_addKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_exp_addKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_exp_addKeyPressed

    private void txt_expCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_expCaretUpdate
        btn_exp_searchActionPerformed(null);
    }//GEN-LAST:event_txt_expCaretUpdate

    private void btn_exp_removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_exp_removeActionPerformed
        try {
            int dd = JOptionPane.showConfirmDialog(this,"Are you really want to delete this entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
         if (dd == 0){
            if (Config.expenses_details_mgr.delExpensesDetails(Config.id_expenses_details.get(tbl_expenses.getSelectedRow()))){
                Config.expenses_details.onloadExpenses();
                JOptionPane.showMessageDialog(this, "Expenses Details Delete successfully", "Success", JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion ", "Error", JOptionPane.ERROR_MESSAGE);
            }
         }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_exp_removeActionPerformed

    private void btn_exp_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_exp_viewActionPerformed
        try {
            Config.view_expenses.onloadReset(Config.id_expenses_details.get(tbl_expenses.getSelectedRow()));
            Config.view_expenses.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_exp_viewActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_exp_add;
    private javax.swing.JButton btn_exp_remove;
    private javax.swing.JButton btn_exp_search;
    private javax.swing.JButton btn_exp_view;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel40;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane29;
    private javax.swing.JSeparator jSeparator23;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable tbl_expenses;
    private javax.swing.JTextField txt_exp;
    // End of variables declaration//GEN-END:variables

    public void onloadReset() {        
        onloadExpenses();
    }
    void onloadExpenses() {
        txt_exp.setText("");
        btn_exp_searchActionPerformed(null);
    }
}