package modules.admin_management.configproduct;

import data_manager.DesignationDetails;
import data_manager.configuration.Config;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class productconfiguration extends javax.swing.JPanel {
    
    public DefaultTableModel tbl_designation_model = null;
    public DefaultTableModel tbl_bike_model = null;
    public DefaultTableModel tbl_maintenance_model = null;
    
    public productconfiguration() {
        initComponents();
        tbl_designation_model = (DefaultTableModel) tbl_designation.getModel();
        tbl_bike_model = (DefaultTableModel) tbl_bike.getModel();
        tbl_maintenance_model = (DefaultTableModel) tbl_maintenance.getModel();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txt_designation = new javax.swing.JTextField();
        btn_designation_search = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        btn_designation_new = new javax.swing.JButton();
        jScrollPane9 = new javax.swing.JScrollPane();
        tbl_designation = new javax.swing.JTable();
        btn_designation_remove = new javax.swing.JButton();
        btn_designation_view = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txt_bike = new javax.swing.JTextField();
        btn_bike_search = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        btn_bike_new = new javax.swing.JButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        tbl_bike = new javax.swing.JTable();
        btn_bike_remove = new javax.swing.JButton();
        btn_bike_view = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txt_maintenance = new javax.swing.JTextField();
        btn_maintenance_search = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        btn_maintenance_new = new javax.swing.JButton();
        jScrollPane10 = new javax.swing.JScrollPane();
        tbl_maintenance = new javax.swing.JTable();
        btn_maintenance_remove = new javax.swing.JButton();
        btn_maintenance_view = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(835, 620));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Product Configuration");

        jTabbedPane3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Designation :");

        txt_designation.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_designationCaretUpdate(evt);
            }
        });

        btn_designation_search.setText("Search");
        btn_designation_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_designation_searchActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        btn_designation_new.setText("New");
        btn_designation_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_designation_newActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_designation, javax.swing.GroupLayout.DEFAULT_SIZE, 551, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_designation_search, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_designation_new, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btn_designation_new, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(txt_designation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_designation_search)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_designation_new, btn_designation_search, jLabel4, jSeparator1, txt_designation});

        tbl_designation.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "DESIGNATION NAME", "CONTACT PERSON", "CONTACT NO", "ADDRESS", "EMAIL"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_designation.getTableHeader().setReorderingAllowed(false);
        tbl_designation.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_designationMouseClicked(evt);
            }
        });
        jScrollPane9.setViewportView(tbl_designation);

        btn_designation_remove.setText("Remove");
        btn_designation_remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_designation_removeActionPerformed(evt);
            }
        });

        btn_designation_view.setText("View");
        btn_designation_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_designation_viewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 790, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btn_designation_remove, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_designation_view, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 458, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_designation_remove)
                    .addComponent(btn_designation_view))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane3.addTab("Designation Details", jPanel4);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Transport :");

        txt_bike.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_bikeCaretUpdate(evt);
            }
        });

        btn_bike_search.setText("Search");
        btn_bike_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_bike_searchActionPerformed(evt);
            }
        });

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        btn_bike_new.setText("New");
        btn_bike_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_bike_newActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_bike)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_bike_search, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_bike_new, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_bike_new, btn_bike_search});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btn_bike_new, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(txt_bike, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_bike_search)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_bike_new, btn_bike_search, jLabel5, jSeparator2, txt_bike});

        tbl_bike.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "TRANSPORT NAME", "OTHER"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_bike.getTableHeader().setReorderingAllowed(false);
        tbl_bike.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_bikeMouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(tbl_bike);
        if (tbl_bike.getColumnModel().getColumnCount() > 0) {
            tbl_bike.getColumnModel().getColumn(0).setResizable(false);
            tbl_bike.getColumnModel().getColumn(1).setResizable(false);
            tbl_bike.getColumnModel().getColumn(2).setResizable(false);
        }

        btn_bike_remove.setText("Remove");
        btn_bike_remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_bike_removeActionPerformed(evt);
            }
        });

        btn_bike_view.setText("View");
        btn_bike_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_bike_viewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 790, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_bike_remove, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_bike_view, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_bike_remove, btn_bike_view});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_bike_remove)
                    .addComponent(btn_bike_view))
                .addGap(8, 8, 8))
        );

        jTabbedPane3.addTab("Transport Details", jPanel1);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("Maintenance :");

        txt_maintenance.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_maintenanceCaretUpdate(evt);
            }
        });

        btn_maintenance_search.setText("Search");
        btn_maintenance_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_maintenance_searchActionPerformed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        btn_maintenance_new.setText("New");
        btn_maintenance_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_maintenance_newActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_maintenance, javax.swing.GroupLayout.PREFERRED_SIZE, 539, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_maintenance_search, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_maintenance_new, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btn_maintenance_new, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(txt_maintenance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_maintenance_search)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_maintenance_search, txt_maintenance});

        tbl_maintenance.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "MAINTENANCE", "OTHER"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_maintenance.getTableHeader().setReorderingAllowed(false);
        tbl_maintenance.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_maintenanceMouseClicked(evt);
            }
        });
        jScrollPane10.setViewportView(tbl_maintenance);
        if (tbl_maintenance.getColumnModel().getColumnCount() > 0) {
            tbl_maintenance.getColumnModel().getColumn(0).setResizable(false);
            tbl_maintenance.getColumnModel().getColumn(1).setResizable(false);
            tbl_maintenance.getColumnModel().getColumn(2).setResizable(false);
        }

        btn_maintenance_remove.setText("Remove");
        btn_maintenance_remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_maintenance_removeActionPerformed(evt);
            }
        });

        btn_maintenance_view.setText("View");
        btn_maintenance_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_maintenance_viewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane10)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(btn_maintenance_remove, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_maintenance_view, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_maintenance_remove)
                    .addComponent(btn_maintenance_view))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 810, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 554, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jTabbedPane3.addTab("Transport Maintenance", jPanel5);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane3)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 580, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txt_designationCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_designationCaretUpdate
        btn_designation_searchActionPerformed(null);
    }//GEN-LAST:event_txt_designationCaretUpdate

    private void btn_designation_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_designation_newActionPerformed
        Config.new_designation.onloadReset();
        Config.new_designation.setVisible(true);
    }//GEN-LAST:event_btn_designation_newActionPerformed

    private void btn_designation_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_designation_searchActionPerformed
        try {
        tbl_designation_model.setRowCount(0);
        String designation;
            try {
                designation = txt_designation.getText();
            } catch (Exception e) {
                designation = "";
            }        
        if(designation.equals("")){
            for (int i = 0; i < Config.config_designation_details.size(); i++) {
                DesignationDetails dd = Config.config_designation_details.get(i);
                 if (Config.config_designation_details.get(i).getDesignation().toUpperCase().startsWith(designation.toUpperCase())){
                     System.out.println(dd.getContact_id());
                     System.out.println(Config.id_contact_details.indexOf(dd.getContact_id()));
                tbl_designation_model.addRow(new Object[] {
                    Config.config_designation_details.get(i).getDesignation_id(),
                    Config.config_designation_details.get(i).getDesignation(),
                    Config.config_designation_details.get(i).getContact_person(),
                    Config.config_contact_details.get(Config.id_contact_details.indexOf(dd.getContact_id())).getContact_no(),
                    Config.config_address_details.get(Config.id_address_details.indexOf(dd.getAddress_id())).getAddress_1(),
                    Config.config_contact_details.get(Config.id_contact_details.indexOf(dd.getContact_id())).getEmail()
                   });
                }
            }      
        } else {
            for (int i = 0; i < Config.config_designation_details.size(); i++) {
                DesignationDetails dd = Config.config_designation_details.get(i);
                 if (Config.config_designation_details.get(i).getDesignation().toUpperCase().startsWith(designation.toUpperCase())){
                     System.out.println(dd.getContact_id());
                     System.out.println(Config.id_contact_details.indexOf(dd.getContact_id()));
                
                tbl_designation_model.addRow(new Object[] {
                    Config.config_designation_details.get(i).getDesignation_id(),
                    Config.config_designation_details.get(i).getDesignation(),
                    Config.config_designation_details.get(i).getContact_person(),
                    Config.config_contact_details.get(Config.id_contact_details.indexOf(dd.getContact_id())).getContact_no(),
                    Config.config_address_details.get(Config.id_address_details.indexOf(dd.getAddress_id())).getAddress_1(),
                    Config.config_contact_details.get(Config.id_contact_details.indexOf(dd.getContact_id())).getEmail()
                   });
                }
            }      
        } 
        } catch (Exception e) {
          e.printStackTrace();
        }
    }//GEN-LAST:event_btn_designation_searchActionPerformed

    private void tbl_bikeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_bikeMouseClicked
        if(evt.getClickCount()==2){
            btn_bike_viewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_bikeMouseClicked

    private void btn_bike_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_bike_viewActionPerformed
        try {
            Config.view_bike.onloadReset(tbl_bike.getValueAt(tbl_bike.getSelectedRow(), 0).toString());
            Config.view_bike.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,"Select Any row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_bike_viewActionPerformed

    private void btn_bike_removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_bike_removeActionPerformed
        try {
             int dd = JOptionPane.showConfirmDialog(this,"Are you really want to delete this entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
            if (dd == 0){
            if(Config.bike_details_mgr.delBikeDetails(Config.id_bike_details.get(tbl_bike.getSelectedRow()))){
                Config.product_configuration.onloadBike();
                JOptionPane.showMessageDialog(this, "Bike Details Delete successfully", "Success", JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion ", "Error", JOptionPane.ERROR_MESSAGE);
            }
         }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error" ,JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_bike_removeActionPerformed

    private void txt_bikeCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_bikeCaretUpdate
        btn_bike_searchActionPerformed(null);
    }//GEN-LAST:event_txt_bikeCaretUpdate

    private void btn_bike_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_bike_searchActionPerformed
        tbl_bike_model.setRowCount(0);
        if(!txt_bike.getText().equals("")){
            for (int i = 0; i < Config.config_bike_details.size(); i++) {
                if(Config.config_bike_details.get(i).getName().toUpperCase().startsWith(txt_bike.getText().toUpperCase())){
                    tbl_bike_model.addRow(new Object[]{
                        Config.config_bike_details.get(i).getBike_id(),
                        Config.config_bike_details.get(i).getName(),
                        Config.config_bike_details.get(i).getOther()
                    });
                }
            }
        }else{
            for (int i = 0; i < Config.config_bike_details.size(); i++) {
                tbl_bike_model.addRow(new Object[]{
                        Config.config_bike_details.get(i).getBike_id(),
                        Config.config_bike_details.get(i).getName(),
                        Config.config_bike_details.get(i).getOther()
                });
            }
        }
    }//GEN-LAST:event_btn_bike_searchActionPerformed

    private void btn_bike_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_bike_newActionPerformed
        Config.new_bike.onloadReset();
        Config.new_bike.setVisible(true);
    }//GEN-LAST:event_btn_bike_newActionPerformed

    private void tbl_designationMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_designationMouseClicked
       if(evt.getClickCount()==2){
            btn_designation_viewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_designationMouseClicked

    private void btn_designation_removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_designation_removeActionPerformed
        try {
           int dd = JOptionPane.showConfirmDialog(this,"Are you really want to delete this entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
         if (dd == 0){
            if (Config.designation_details_mgr.delDesignationDetails(Config.id_designation_details.get(tbl_designation.getSelectedRow()))){
                Config.product_configuration.onloadDesignation();
                JOptionPane.showMessageDialog(this, "Designation Details Delete successfully", "Success", JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion ", "Error", JOptionPane.ERROR_MESSAGE);
            }
         }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_designation_removeActionPerformed

    private void btn_designation_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_designation_viewActionPerformed
        try {
            Config.view_designation.onloadReset(Config.id_designation_details.get(tbl_designation.getSelectedRow()));
            Config.view_designation.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_designation_viewActionPerformed

    private void txt_maintenanceCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_maintenanceCaretUpdate
        btn_maintenance_searchActionPerformed(null);
    }//GEN-LAST:event_txt_maintenanceCaretUpdate

    private void btn_maintenance_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_maintenance_searchActionPerformed
        tbl_maintenance_model.setRowCount(0);
        if(!txt_maintenance.getText().equals("")){
            for (int i = 0; i < Config.config_maintenance_type.size(); i++) {
                if(Config.config_maintenance_type.get(i).getType().toUpperCase().startsWith(txt_maintenance.getText().toUpperCase())){
                    tbl_maintenance_model.addRow(new Object[]{
                        Config.config_maintenance_type.get(i).getMaintenance_type_id(),
                        Config.config_maintenance_type.get(i).getType(),
                        Config.config_maintenance_type.get(i).getOther()
                    });
                }
            }
        }else{
            for (int i = 0; i < Config.config_maintenance_type.size(); i++) {
                tbl_maintenance_model.addRow(new Object[]{
                        Config.config_maintenance_type.get(i).getMaintenance_type_id(),
                        Config.config_maintenance_type.get(i).getType(),
                        Config.config_maintenance_type.get(i).getOther()
                });
            }
        }
    }//GEN-LAST:event_btn_maintenance_searchActionPerformed

    private void btn_maintenance_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_maintenance_newActionPerformed
        Config.new_maintenance.onloadReset();
        Config.new_maintenance.setVisible(true);
    }//GEN-LAST:event_btn_maintenance_newActionPerformed

    private void tbl_maintenanceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_maintenanceMouseClicked
        if(evt.getClickCount()==2){
            btn_maintenance_viewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_maintenanceMouseClicked

    private void btn_maintenance_removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_maintenance_removeActionPerformed
        try {
            int dd = JOptionPane.showConfirmDialog(this,"Are you really want to delete this entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
            if (dd == 0){
            if(Config.maintenance_type_mgr.delMaintenanceType(Config.id_maintenance_type.get(tbl_maintenance.getSelectedRow()))){
                Config.product_configuration.onloadMaintenance();
                JOptionPane.showMessageDialog(this, "Bike Details Delete successfully", "Success", JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion ", "Error", JOptionPane.ERROR_MESSAGE);
            }
         }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_maintenance_removeActionPerformed

    private void btn_maintenance_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_maintenance_viewActionPerformed
        try {
            Config.view_Maintenance.onloadReset(tbl_maintenance_model.getValueAt(tbl_maintenance.getSelectedRow(), 0).toString());
            Config.view_Maintenance.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,"Select Any row", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_btn_maintenance_viewActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_bike_new;
    private javax.swing.JButton btn_bike_remove;
    private javax.swing.JButton btn_bike_search;
    private javax.swing.JButton btn_bike_view;
    private javax.swing.JButton btn_designation_new;
    private javax.swing.JButton btn_designation_remove;
    private javax.swing.JButton btn_designation_search;
    private javax.swing.JButton btn_designation_view;
    private javax.swing.JButton btn_maintenance_new;
    private javax.swing.JButton btn_maintenance_remove;
    private javax.swing.JButton btn_maintenance_search;
    private javax.swing.JButton btn_maintenance_view;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTable tbl_bike;
    private javax.swing.JTable tbl_designation;
    private javax.swing.JTable tbl_maintenance;
    private javax.swing.JTextField txt_bike;
    private javax.swing.JTextField txt_designation;
    private javax.swing.JTextField txt_maintenance;
    // End of variables declaration//GEN-END:variables

    public void onloadReset() {        
        onloadDesignation();
        onloadBike();
        onloadMaintenance();
    }    
    void onloadDesignation() {
        txt_designation.setText("");
        btn_designation_searchActionPerformed(null);
    }
    void onloadBike() {
        txt_bike.setText("");
        btn_bike_searchActionPerformed(null);
    }
    void onloadMaintenance() {
        txt_maintenance.setText("");
        btn_maintenance_searchActionPerformed(null);
    }
}
