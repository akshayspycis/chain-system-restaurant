package modules.admin_management.itemlist;

import data_manager.ItemList;
import data_manager.configuration.Config;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class itemlist extends javax.swing.JPanel {
    
    public DefaultTableModel tbl_item_list_model = null;
    SimpleDateFormat simple_date_formate = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
    public itemlist() {
        initComponents();
        tbl_item_list_model = (DefaultTableModel) tbl_itemlist.getModel();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel40 = new javax.swing.JPanel();
        jScrollPane29 = new javax.swing.JScrollPane();
        tbl_itemlist = new javax.swing.JTable();
        btn_itemlist_view = new javax.swing.JButton();
        btn_itemlist_remove = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        txt_itemlist = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        btn_itemlist_add = new javax.swing.JButton();
        btn_itemlist_search = new javax.swing.JButton();
        jSeparator23 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cb_Item_type = new javax.swing.JComboBox();
        jmc_month = new com.toedter.calendar.JMonthChooser();
        jLabel4 = new javax.swing.JLabel();
        jyc_year = new com.toedter.calendar.JYearChooser();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(835, 620));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Item Details");

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jPanel40.setBackground(new java.awt.Color(255, 255, 255));

        tbl_itemlist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "ITEM NAME", "DATE", "UNIT ", "QUANTITY", "PACK", "PRICE", "SUPPLIER NAME"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_itemlist.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_itemlistMouseClicked(evt);
            }
        });
        jScrollPane29.setViewportView(tbl_itemlist);
        if (tbl_itemlist.getColumnModel().getColumnCount() > 0) {
            tbl_itemlist.getColumnModel().getColumn(0).setResizable(false);
            tbl_itemlist.getColumnModel().getColumn(1).setResizable(false);
            tbl_itemlist.getColumnModel().getColumn(2).setResizable(false);
            tbl_itemlist.getColumnModel().getColumn(3).setResizable(false);
            tbl_itemlist.getColumnModel().getColumn(4).setResizable(false);
            tbl_itemlist.getColumnModel().getColumn(5).setResizable(false);
            tbl_itemlist.getColumnModel().getColumn(6).setResizable(false);
            tbl_itemlist.getColumnModel().getColumn(7).setResizable(false);
        }

        btn_itemlist_view.setText("View");
        btn_itemlist_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_itemlist_viewActionPerformed(evt);
            }
        });

        btn_itemlist_remove.setText("Remove");
        btn_itemlist_remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_itemlist_removeActionPerformed(evt);
            }
        });

        txt_itemlist.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_itemlistCaretUpdate(evt);
            }
        });
        txt_itemlist.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_itemlistKeyPressed(evt);
            }
        });

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel26.setText("Item :");

        btn_itemlist_add.setText("Add");
        btn_itemlist_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_itemlist_addActionPerformed(evt);
            }
        });
        btn_itemlist_add.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_itemlist_addKeyPressed(evt);
            }
        });

        btn_itemlist_search.setText("Search");
        btn_itemlist_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_itemlist_searchActionPerformed(evt);
            }
        });
        btn_itemlist_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_itemlist_searchKeyPressed(evt);
            }
        });

        jSeparator23.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Month :");

        jLabel2.setText("Item Type :");

        cb_Item_type.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- Select -", "Stock Item", "Sada flavour", "Manufacture Item" }));
        cb_Item_type.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_Item_typeActionPerformed(evt);
            }
        });

        jmc_month.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jmc_monthMouseClicked(evt);
            }
        });
        jmc_month.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jmc_monthPropertyChange(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Year :");

        jyc_year.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jyc_yearPropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_Item_type, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel26)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_itemlist, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jmc_month, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel4)
                .addGap(10, 10, 10)
                .addComponent(jyc_year, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_itemlist_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_itemlist_add, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(3, 3, 3))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator23)
                            .addComponent(jmc_month, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jyc_year, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txt_itemlist, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel26)
                                .addComponent(btn_itemlist_add)
                                .addComponent(jLabel2)
                                .addComponent(cb_Item_type, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(btn_itemlist_search)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel40Layout = new javax.swing.GroupLayout(jPanel40);
        jPanel40.setLayout(jPanel40Layout);
        jPanel40Layout.setHorizontalGroup(
            jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel40Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane29)
                    .addGroup(jPanel40Layout.createSequentialGroup()
                        .addComponent(btn_itemlist_remove, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_itemlist_view, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10))
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel40Layout.setVerticalGroup(
            jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel40Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jScrollPane29, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                .addGap(6, 6, 6)
                .addGroup(jPanel40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_itemlist_view)
                    .addComponent(btn_itemlist_remove))
                .addGap(10, 10, 10))
        );

        jTabbedPane1.addTab("Item List", jPanel40);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jTabbedPane1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addGap(6, 6, 6)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txt_itemlistKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_itemlistKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_itemlistKeyPressed

    private void btn_itemlist_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_itemlist_searchActionPerformed
        tbl_item_list_model.setRowCount(0);
        if(!txt_itemlist.getText().equals("")){
                checkSwitchCondition(cb_Item_type.getSelectedIndex(),txt_itemlist.getText().toUpperCase(),jmc_month.getMonth()+1,jyc_year.getYear());
        } else {
                checkSwitchCondition(cb_Item_type.getSelectedIndex(),jmc_month.getMonth()+1,jyc_year.getYear() );
        }
    }//GEN-LAST:event_btn_itemlist_searchActionPerformed

    private void btn_itemlist_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_itemlist_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_itemlist_searchKeyPressed

    private void btn_itemlist_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_itemlist_addActionPerformed
       Config.new_item.onloadReset();
       Config.new_item.setVisible(true);
    }//GEN-LAST:event_btn_itemlist_addActionPerformed

    private void btn_itemlist_addKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_itemlist_addKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_itemlist_addKeyPressed

    private void txt_itemlistCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_itemlistCaretUpdate
        btn_itemlist_searchActionPerformed(null);
    }//GEN-LAST:event_txt_itemlistCaretUpdate

    private void btn_itemlist_removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_itemlist_removeActionPerformed
        try {
            int dd = JOptionPane.showConfirmDialog(this,"Are you really want to delete this entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
         if (dd == 0){
            if(Config.item_list_mgr.delItemList(tbl_item_list_model.getValueAt(tbl_itemlist.getSelectedRow(), 0).toString())){
                Config.item_list.onloadResetTableData(cb_Item_type.getSelectedIndex());
                JOptionPane.showMessageDialog(this, "Item List delete successfully", "Success", JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion ", "Error", JOptionPane.ERROR_MESSAGE);
            }
         }
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_itemlist_removeActionPerformed

    private void cb_Item_typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_Item_typeActionPerformed
        txt_itemlist.setText("");
        tbl_item_list_model.setRowCount(0);
        if(cb_Item_type.getSelectedIndex()!=0){
            btn_itemlist_searchActionPerformed(null);
        }
    }//GEN-LAST:event_cb_Item_typeActionPerformed

    private void jmc_monthMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jmc_monthMouseClicked
        
    }//GEN-LAST:event_jmc_monthMouseClicked

    private void jmc_monthPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jmc_monthPropertyChange
        btn_itemlist_searchActionPerformed(null);
    }//GEN-LAST:event_jmc_monthPropertyChange

    private void jyc_yearPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jyc_yearPropertyChange
        btn_itemlist_searchActionPerformed(null);
    }//GEN-LAST:event_jyc_yearPropertyChange

    private void btn_itemlist_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_itemlist_viewActionPerformed
        try {
            Config.view_item.onloadReset(tbl_item_list_model.getValueAt(tbl_itemlist.getSelectedRow(),0).toString());
            Config.view_item.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_btn_itemlist_viewActionPerformed

    private void tbl_itemlistMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_itemlistMouseClicked
        if(evt.getClickCount()==2){
            btn_itemlist_viewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_itemlistMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_itemlist_add;
    private javax.swing.JButton btn_itemlist_remove;
    private javax.swing.JButton btn_itemlist_search;
    private javax.swing.JButton btn_itemlist_view;
    private javax.swing.JComboBox cb_Item_type;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel40;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane29;
    private javax.swing.JSeparator jSeparator23;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.toedter.calendar.JMonthChooser jmc_month;
    private com.toedter.calendar.JYearChooser jyc_year;
    private javax.swing.JTable tbl_itemlist;
    private javax.swing.JTextField txt_itemlist;
    // End of variables declaration//GEN-END:variables

    public void onloadReset() {        
       txt_itemlist.setText("");
       jmc_month.setMonth(Integer.parseInt(sdf_month.format(new Date()))-1);
       jyc_year.setYear(Integer.parseInt(sdf_year.format(new Date())));
    }  
    
    private void addTableContent(ResultSet rs) {
        try {
        while (rs.next()) {
                tbl_item_list_model.addRow(new Object[]{
                rs.getString("item_list_id"),
                Config.config_stock_item.get(Config.id_stock_item.indexOf(rs.getString("Item_id"))).getName(),
                rs.getString("_date"),
                Config.config_stock_item.get(Config.id_stock_item.indexOf(rs.getString("Item_id"))).getUnit(),
                rs.getString("quantity"),
                rs.getString("pack"),
                rs.getString("Prise"),
                Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(rs.getString("supplier_id"))).getProfile_id())).getName()
                });
        }    
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkSwitchCondition(int selectedIndex,int month ,int year) {
        String identity = "";
            switch(selectedIndex){
                case 1:
                    identity="ST";
                break;
                case 2:
                    identity="S";
                break;
                case 3:
                    identity="M";
                break;    
            }    
            try {
            Config.sql ="select a.* from item_list a,stock_item b where a.item_id=b.item_id and b.identity='"+identity+"' and a._year='"+year+"' and a._month='"+month+"';";
            Config.rs = Config.stmt.executeQuery(Config.sql);
                addTableContent(Config.rs);
            } catch (Exception ex) {
                ex.printStackTrace();
            }    
            
     }
    

    private void checkSwitchCondition(int selectedIndex, String text,int month,int year) {
    text+="%"    ;
    String identity = "";
            switch(selectedIndex){
                case 1:
                    identity="ST";
                break;
                case 2:
                    identity="S";
                break;
                case 3:
                    identity="M";
                break;    
            }    
            try {
            Config.sql ="select a.* from item_list a,stock_item b where a.item_id=b.item_id and b.identity='"+identity+"' and a._year='"+year+"' and a._month='"+month+"' and b.name like '"+text+"'";
            Config.rs = Config.stmt.executeQuery(Config.sql);
                addTableContent(Config.rs);
            } catch (Exception ex) {
                ex.printStackTrace();
            }    
            
    }

    void onloadResetTableData(int selectedIndex) {
        cb_Item_type.setSelectedIndex(selectedIndex);
    }
}
