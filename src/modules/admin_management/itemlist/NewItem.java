package modules.admin_management.itemlist;

import data_manager.ItemList;
import data_manager.ProfileDetails;
import data_manager.StockItem;
import data_manager.configuration.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class NewItem extends javax.swing.JDialog {
String item_id = null;
String supplier_id = null;

SimpleDateFormat simple_date_formate = new SimpleDateFormat("dd/MM/yyyy");
SimpleDateFormat sdf_month = new SimpleDateFormat("M");
SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
   
    public NewItem(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txt_unit = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txt_quantity = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txt_contactno = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_price = new javax.swing.JTextField();
        txt_pack = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cb_item_name = new javax.swing.JComboBox();
        cb_supplier = new javax.swing.JComboBox();
        cb_Item_type = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        jdc_date = new com.toedter.calendar.JDateChooser();
        btn_cancel = new javax.swing.JButton();
        btn_msg = new javax.swing.JButton();
        btn_save = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        jTextField1.setText("jTextField1");

        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Item Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel10.setText("Item Name");

        jLabel11.setText("Unit");

        txt_unit.setEditable(false);

        jLabel12.setText("Quantity");

        jLabel13.setText("Supplier Name");

        jLabel14.setText("Contact No.");

        txt_contactno.setEditable(false);
        txt_contactno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_contactnoActionPerformed(evt);
            }
        });

        jLabel2.setText("Date");

        jLabel3.setText("Price");

        jLabel4.setText("Pack");

        cb_item_name.setEditable(true);
        cb_item_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_item_nameActionPerformed(evt);
            }
        });

        cb_supplier.setEditable(true);
        cb_supplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_supplierActionPerformed(evt);
            }
        });

        cb_Item_type.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-Select-", "Stock Item", "Sada flavour", "Manufacture Item" }));
        cb_Item_type.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_Item_typeActionPerformed(evt);
            }
        });

        jLabel5.setText("Item Type");

        jdc_date.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel3)
                                .addGap(57, 57, 57))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txt_pack, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txt_price, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(cb_supplier, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cb_Item_type, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel10)
                                    .addComponent(cb_item_name, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel11)
                    .addComponent(txt_unit, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel14)
                    .addComponent(txt_contactno, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                    .addComponent(jdc_date, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cb_item_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_unit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_Item_type, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_contactno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_supplier, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jLabel12)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_pack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_price, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });
        btn_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_cancelKeyPressed(evt);
            }
        });

        btn_msg.setText("Reset");
        btn_msg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_msgActionPerformed(evt);
            }
        });
        btn_msg.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_msgKeyPressed(evt);
            }
        });

        btn_save.setText("Save");
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });
        btn_save.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_saveKeyPressed(evt);
            }
        });

        jPanel5.setBackground(new java.awt.Color(230, 79, 6));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("New Item");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(btn_msg, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_save, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_cancel, btn_msg, btn_save});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_save)
                    .addComponent(btn_cancel)
                    .addComponent(btn_msg))
                .addGap(11, 11, 11))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void btn_msgKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_msgKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_UP){
            jdc_date.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT){
            btn_save.requestFocus();
        }
    }//GEN-LAST:event_btn_msgKeyPressed
        
    private void btn_saveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_saveKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT){
            btn_cancel.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_LEFT){
            btn_msg.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP){
            jdc_date.requestFocus();
        }
    }//GEN-LAST:event_btn_saveKeyPressed

    private void btn_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_cancelKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_LEFT){
            btn_save.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP){
            jdc_date.requestFocus();
        }
    }//GEN-LAST:event_btn_cancelKeyPressed

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        try {
        String str = checkValidity();
         if (str.equals("ok")) {
                ItemList it = new ItemList();
                 System.out.println(item_id);
                 System.out.println(supplier_id);
                    it.setItem_id(item_id);
                    it.setSupplier_id(supplier_id);
                    it.setQuantity(txt_quantity.getText().trim());
                    it.setPrise(txt_price.getText().trim());
                    it.setPack(txt_pack.getText().trim());
                    it.setDate(simple_date_formate.format(jdc_date.getDate()).trim());
                    it.setMonth(sdf_month.format(simple_date_formate.parse(simple_date_formate.format(jdc_date.getDate()).trim())));
                    it.setYear(sdf_year.format(simple_date_formate.parse(simple_date_formate.format(jdc_date.getDate()).trim())));
                    if (Config.item_list_mgr.insItemList(it)){
                    Config.item_list.onloadResetTableData(cb_Item_type.getSelectedIndex());
                    onloadReset();
                    JOptionPane.showMessageDialog(this, "ItemList created successfully.", "Creation successful", JOptionPane.NO_OPTION);
                    }else {
                    JOptionPane.showMessageDialog(this, "Error in Itemlist creation.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }else{
                JOptionPane.showMessageDialog(this, "Check '"+str+"' field.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception e) {
        }
    }//GEN-LAST:event_btn_saveActionPerformed

    private void cb_item_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_item_nameActionPerformed
       showDetails();
    }//GEN-LAST:event_cb_item_nameActionPerformed

    private void txt_contactnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_contactnoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_contactnoActionPerformed

    private void cb_supplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_supplierActionPerformed
      showSupplierDetails();
    }//GEN-LAST:event_cb_supplierActionPerformed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
       dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void cb_Item_typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_Item_typeActionPerformed
        txt_unit.setText("");
        cb_item_name.removeAllItems();
        if(cb_Item_type.getSelectedIndex()!=0){
        switch(cb_Item_type.getSelectedIndex()){
            case 1:
                try{
                for (int i = 0;  i < Config.config_stock_item.size();i++) {
                    if(Config.config_stock_item.get(i).getIdentity().equalsIgnoreCase("ST")){
                        cb_item_name.addItem(Config.config_stock_item.get(i).getName());
                    }
                }
                }catch(Exception e){
                    e.printStackTrace();
                }
             break;  
            case 2:
                try{
                for (int i = 0;  i < Config.config_stock_item.size();i++) {
                    if(Config.config_stock_item.get(i).getIdentity().equalsIgnoreCase("S")){
                        cb_item_name.addItem(Config.config_stock_item.get(i).getName());
                    }
                }
                }catch(Exception e){
                    e.printStackTrace();
                }
             break;   
            case 3:
                try{
                for (int i = 0;  i < Config.config_stock_item.size();i++) {
                    if(Config.config_stock_item.get(i).getIdentity().equalsIgnoreCase("M")){
                        cb_item_name.addItem(Config.config_stock_item.get(i).getName());
                    }
                }
             }catch(Exception e){
                    e.printStackTrace();
                }
             break;
        }    
        }
    }//GEN-LAST:event_cb_Item_typeActionPerformed

    private void btn_msgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_msgActionPerformed
        onloadReset();
    }//GEN-LAST:event_btn_msgActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_msg;
    private javax.swing.JButton btn_save;
    private javax.swing.JComboBox cb_Item_type;
    private javax.swing.JComboBox cb_item_name;
    private javax.swing.JComboBox cb_supplier;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JTextField jTextField1;
    private com.toedter.calendar.JDateChooser jdc_date;
    private javax.swing.JTextField txt_contactno;
    private javax.swing.JTextField txt_pack;
    private javax.swing.JTextField txt_price;
    private javax.swing.JTextField txt_quantity;
    private javax.swing.JTextField txt_unit;
    // End of variables declaration//GEN-END:variables

     public void onloadReset() {
        cb_Item_type.setSelectedIndex(0);
        cb_item_name.setSelectedItem("");
        cb_supplier.setSelectedIndex(-1);
        jdc_date.setDate(Calendar.getInstance().getTime());
        txt_unit.setText("");
        txt_quantity.setText("");
        txt_contactno.setText("");
        txt_price.setText("");
        txt_pack.setText("");
        onloadSupplier();
        txt_contactno.setText("");
        }
     
     public void onloadSupplier(){
         cb_supplier.removeAllItems();
         for (int i = 0; i < Config.config_supplier_details.size(); i++) {
             ProfileDetails pd =Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_supplier_details.get(i).getProfile_id()));
             cb_supplier.addItem(pd.getName());
         }
         cb_supplier.setSelectedIndex(-1);
     }
     
    private String checkValidity() {
        if (cb_item_name.getSelectedIndex()== -1) {
            return "Name";
        }
        else if (txt_unit.getText().equals("")) {
            return "Unit";
        }
        else if (cb_supplier.getSelectedIndex()==-1) {
            return "Supplier Name";
        } 
        else if (txt_price.getText().equals("")) {
            return "Price";
        }
        else if (txt_pack.getText().equals("")) {
            return "pack";
        }
        else if (jdc_date.getDate().equals("")) {
            return "Date";
        }
        else {
            return "ok";
        }
    }
    
    void showDetails(){
        String name;
        try {            
            name = cb_item_name.getSelectedItem().toString();
            switch(cb_Item_type.getSelectedIndex()){
                case 1:
                for (int i = 0; i < Config.config_stock_item.size(); i++) {
                StockItem st = Config.config_stock_item.get(i);
                if (st.getName().equals(name) && st.getIdentity().equals("ST")) {
                    txt_unit.setText(st.getUnit());
                    item_id = st.getItem_id();
                    break;
                    }
                }  
                break;    
                case 2:
                for (int i = 0; i < Config.config_stock_item.size(); i++) {
                StockItem st = Config.config_stock_item.get(i);
                if (st.getName().equals(name)&&st.getIdentity().equals("S")) {
                    txt_unit.setText(st.getUnit());
                    item_id = st.getItem_id();
                    break;
                    }
                }  
                break;    
                case 3:    
                for (int i = 0; i < Config.config_stock_item.size(); i++) {
                StockItem st = Config.config_stock_item.get(i);
                if (st.getName().equals(name)&&st.getIdentity().equals("M")) {
                    txt_unit.setText(st.getUnit());
                    item_id = st.getItem_id();
                    break;
                    }
                }  
                break;    
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    void showSupplierDetails(){
        try {
        supplier_id=Config.config_supplier_details.get(cb_supplier.getSelectedIndex()).getSupplier_id();    
        txt_contactno.setText(Config.config_contact_details.get(Config.id_contact_details.indexOf(Config.config_supplier_details.get(cb_supplier.getSelectedIndex()).getContact_id())).getContact_no());
        } catch (Exception e) {
            e.printStackTrace();
         }
      }
    }