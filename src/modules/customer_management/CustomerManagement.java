package modules.customer_management;

import data_manager.CustomerDetails;
import data_manager.configuration.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

public class CustomerManagement extends javax.swing.JDialog {

    DefaultTableModel tbl_cmmodel;
    public CustomerManagement(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
         tbl_cmmodel = (DefaultTableModel) tbl_customer.getModel();   
       
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        pnl_employee = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txt_customer_search = new javax.swing.JTextField();
        btn_customer_search = new javax.swing.JButton();
        btn_customer_new = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        txt_customer_customername = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        txt_customer_address = new javax.swing.JTextField();
        txt_customer_city = new javax.swing.JTextField();
        txt_customer_pincode = new javax.swing.JTextField();
        txt_customer_state = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txt_customer_payment = new javax.swing.JTable();
        txt_customer_dob = new javax.swing.JTextField();
        jPanel14 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txt_customer_message = new javax.swing.JTextPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_customer = new javax.swing.JTable();
        btn_customer_cancel = new javax.swing.JButton();
        btn_customer_print = new javax.swing.JButton();
        txt_customer_gender = new javax.swing.JTextField();
        btn_customer_delete = new javax.swing.JButton();
        btn_customer_view = new javax.swing.JButton();
        jPanel22 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane2.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        pnl_employee.setBackground(new java.awt.Color(255, 255, 255));
        pnl_employee.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pnl_employeeKeyPressed(evt);
            }
        });

        jLabel5.setText("Customer Name");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Search By :");

        txt_customer_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_customer_searchCaretUpdate(evt);
            }
        });
        txt_customer_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_customer_searchKeyPressed(evt);
            }
        });

        btn_customer_search.setText("Search");
        btn_customer_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_customer_searchActionPerformed(evt);
            }
        });
        btn_customer_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_customer_searchKeyPressed(evt);
            }
        });

        btn_customer_new.setText("New Customer");
        btn_customer_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_customer_newActionPerformed(evt);
            }
        });
        btn_customer_new.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_customer_newKeyPressed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_customer_search, javax.swing.GroupLayout.PREFERRED_SIZE, 464, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_customer_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_customer_new, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(btn_customer_new)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_customer_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_customer_search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator3))
                .addContainerGap())
        );

        txt_customer_customername.setEditable(false);
        txt_customer_customername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_customer_customernameActionPerformed(evt);
            }
        });

        jLabel12.setText("Gender");

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Address", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_customer_address.setEditable(false);
        txt_customer_address.setBackground(new java.awt.Color(255, 255, 255));
        txt_customer_address.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_customer_address.setBorder(null);
        txt_customer_address.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_customer_addressActionPerformed(evt);
            }
        });

        txt_customer_city.setEditable(false);
        txt_customer_city.setBackground(new java.awt.Color(255, 255, 255));
        txt_customer_city.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_customer_city.setBorder(null);
        txt_customer_city.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_customer_cityActionPerformed(evt);
            }
        });

        txt_customer_pincode.setEditable(false);
        txt_customer_pincode.setBackground(new java.awt.Color(255, 255, 255));
        txt_customer_pincode.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_customer_pincode.setBorder(null);
        txt_customer_pincode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_customer_pincodeActionPerformed(evt);
            }
        });

        txt_customer_state.setEditable(false);
        txt_customer_state.setBackground(new java.awt.Color(255, 255, 255));
        txt_customer_state.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_customer_state.setBorder(null);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_customer_pincode)
                    .addComponent(txt_customer_state)
                    .addComponent(txt_customer_city)
                    .addComponent(txt_customer_address))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(txt_customer_address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_customer_city, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_customer_pincode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_customer_state, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel13.setText("Date of Birth");

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Payment Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jScrollPane6.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane6.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        txt_customer_payment.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txt_customer_payment.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null}
            },
            new String [] {
                "PAYABLE AMT", "PAID AMT ", "BALANCE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        txt_customer_payment.setAutoscrolls(false);
        txt_customer_payment.setRowHeight(25);
        txt_customer_payment.getTableHeader().setReorderingAllowed(false);
        jScrollPane6.setViewportView(txt_customer_payment);
        if (txt_customer_payment.getColumnModel().getColumnCount() > 0) {
            txt_customer_payment.getColumnModel().getColumn(0).setResizable(false);
            txt_customer_payment.getColumnModel().getColumn(1).setResizable(false);
            txt_customer_payment.getColumnModel().getColumn(2).setResizable(false);
        }

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        txt_customer_dob.setEditable(false);

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_customer_message.setEditable(false);
        jScrollPane5.setViewportView(txt_customer_message);

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tbl_customer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "CUSTOMER NAME", "CONTACT NO.", "ORGANIZATION", "EMIAL", "IDENTITY TYPE", "IDENTITY NO.", "ACCOUNT NO.", "BANK NAME"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_customer.getTableHeader().setReorderingAllowed(false);
        tbl_customer.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tbl_customerFocusGained(evt);
            }
        });
        tbl_customer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_customerMouseClicked(evt);
            }
        });
        tbl_customer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_customerKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbl_customerKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_customer);
        if (tbl_customer.getColumnModel().getColumnCount() > 0) {
            tbl_customer.getColumnModel().getColumn(0).setResizable(false);
            tbl_customer.getColumnModel().getColumn(0).setPreferredWidth(50);
            tbl_customer.getColumnModel().getColumn(1).setResizable(false);
            tbl_customer.getColumnModel().getColumn(1).setPreferredWidth(150);
            tbl_customer.getColumnModel().getColumn(2).setResizable(false);
            tbl_customer.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_customer.getColumnModel().getColumn(3).setResizable(false);
            tbl_customer.getColumnModel().getColumn(3).setPreferredWidth(110);
            tbl_customer.getColumnModel().getColumn(4).setResizable(false);
            tbl_customer.getColumnModel().getColumn(4).setPreferredWidth(200);
            tbl_customer.getColumnModel().getColumn(5).setResizable(false);
            tbl_customer.getColumnModel().getColumn(5).setPreferredWidth(100);
            tbl_customer.getColumnModel().getColumn(6).setResizable(false);
            tbl_customer.getColumnModel().getColumn(6).setPreferredWidth(100);
            tbl_customer.getColumnModel().getColumn(7).setResizable(false);
            tbl_customer.getColumnModel().getColumn(7).setPreferredWidth(100);
            tbl_customer.getColumnModel().getColumn(8).setResizable(false);
            tbl_customer.getColumnModel().getColumn(8).setPreferredWidth(100);
        }

        btn_customer_cancel.setText("Cancel");
        btn_customer_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_customer_cancelActionPerformed(evt);
            }
        });
        btn_customer_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_customer_cancelKeyPressed(evt);
            }
        });

        btn_customer_print.setText("Print");
        btn_customer_print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_customer_printKeyPressed(evt);
            }
        });

        txt_customer_gender.setEditable(false);

        btn_customer_delete.setText("Delete");
        btn_customer_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_customer_deleteActionPerformed(evt);
            }
        });
        btn_customer_delete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_customer_deleteKeyPressed(evt);
            }
        });

        btn_customer_view.setText("View");
        btn_customer_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_customer_viewActionPerformed(evt);
            }
        });
        btn_customer_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_customer_viewKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout pnl_employeeLayout = new javax.swing.GroupLayout(pnl_employee);
        pnl_employee.setLayout(pnl_employeeLayout);
        pnl_employeeLayout.setHorizontalGroup(
            pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_employeeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnl_employeeLayout.createSequentialGroup()
                                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(txt_customer_customername, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(txt_customer_gender, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13)
                                    .addComponent(txt_customer_dob, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_employeeLayout.createSequentialGroup()
                        .addComponent(btn_customer_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_customer_delete, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_customer_view)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_customer_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pnl_employeeLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_customer_cancel, btn_customer_delete, btn_customer_print, btn_customer_view});

        pnl_employeeLayout.setVerticalGroup(
            pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_employeeLayout.createSequentialGroup()
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_customer_customername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_customer_dob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_customer_gender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_customer_cancel)
                    .addComponent(btn_customer_print)
                    .addComponent(btn_customer_delete)
                    .addComponent(btn_customer_view))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Customer Information", pnl_employee);

        jPanel22.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        jTabbedPane2.addTab("Reports", jPanel22);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );

        jPanel2.setBackground(new java.awt.Color(230, 79, 6));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Customer Management");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void pnl_employeeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pnl_employeeKeyPressed
        System.out.println("sajjjj");
    }//GEN-LAST:event_pnl_employeeKeyPressed

    private void btn_customer_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_customer_printKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_customer_delete.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            tbl_customer.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_customer.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_customer_delete.requestFocus();
        }
    }//GEN-LAST:event_btn_customer_printKeyPressed

    private void btn_customer_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_customer_cancelKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_customer_view.requestFocus();
        }
//      if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
  //            .requestFocus()
//      }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
//            tbl_supplier.requestFocus();
//        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            tbl_employee.requestFocus();
            //           }
    }//GEN-LAST:event_btn_customer_cancelKeyPressed

    private void tbl_customerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_customerKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_customer_print.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_customer_new.requestFocus();
        }
    }//GEN-LAST:event_tbl_customerKeyPressed

    private void txt_customer_pincodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_customer_pincodeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_customer_pincodeActionPerformed

    private void txt_customer_cityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_customer_cityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_customer_cityActionPerformed

    private void txt_customer_customernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_customer_customernameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_customer_customernameActionPerformed

    private void btn_customer_newKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_customer_newKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            tbl_customer.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_customer_search.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_customer.requestFocus();
        }
    }//GEN-LAST:event_btn_customer_newKeyPressed

    private void btn_customer_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_customer_newActionPerformed
        Config.new_customer.onloadReset();
        Config.new_customer.setVisible(true);
    }//GEN-LAST:event_btn_customer_newActionPerformed

    private void btn_customer_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_customer_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_customer_new.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            txt_customer_search.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_customer.requestFocus();
        }
    }//GEN-LAST:event_btn_customer_searchKeyPressed

    private void txt_customer_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_customer_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_customer_search.requestFocus();
        }
//        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
//            pnl_adv_report.requestFocus();
//        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_customer.requestFocus();
        }

    }//GEN-LAST:event_txt_customer_searchKeyPressed

    private void btn_customer_deleteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_customer_deleteKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_customer_view.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_customer_print.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_customer_view.requestFocus();
        }
    }//GEN-LAST:event_btn_customer_deleteKeyPressed

    private void btn_customer_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_customer_viewKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_customer_cancel.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_customer_delete.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_customer_cancel.requestFocus();
        }
    }//GEN-LAST:event_btn_customer_viewKeyPressed

    private void btn_customer_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_customer_viewActionPerformed
        try {
            Config.view_customer.onloadReset(tbl_cmmodel.getValueAt(tbl_customer.getSelectedRow(),0).toString());  
            Config.view_customer.setVisible(true);  
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,"Select any row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_customer_viewActionPerformed

    private void btn_customer_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_customer_cancelActionPerformed
         dispose();
    }//GEN-LAST:event_btn_customer_cancelActionPerformed

    private void txt_customer_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_customer_searchCaretUpdate
        btn_customer_searchActionPerformed(null);
    }//GEN-LAST:event_txt_customer_searchCaretUpdate

    private void btn_customer_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_customer_searchActionPerformed
        try{
        tbl_cmmodel.setRowCount(0);
        if(!txt_customer_search.getText().equals("")){
            for (int i = 0; i < Config.config_customer_details.size(); i++) {
            CustomerDetails cd = Config.config_customer_details.get(i);
            if(Config.config_profile_details.get(Config.id_profile_details.indexOf(cd.getProfile_id())).getName().toUpperCase().startsWith(txt_customer_search.getText().trim().toUpperCase())  ){
            tbl_cmmodel.addRow(new Object[]{
            cd.getCustomer_id(),    
            Config.config_profile_details.get(Config.id_profile_details.indexOf(cd.getProfile_id())).getName(),
            Config.config_contact_details.get(Config.id_contact_details.indexOf(cd.getContact_id())).getContact_no(),    
            Config.config_contact_details.get(Config.id_contact_details.indexOf(cd.getContact_id())).getOther(),
            Config.config_contact_details.get(Config.id_contact_details.indexOf(cd.getContact_id())).getEmail(),
            Config.config_identity_details.get(Config.id_identity_details.indexOf(cd.getIdentity_id())).getIdentity_type(),
            Config.config_identity_details.get(Config.id_identity_details.indexOf(cd.getIdentity_id())).getIdentity_no(),
            Config.config_customer_details.get(i).getAccount_no(),
            Config.config_customer_details.get(i).getBank_name()
            });
            }
            }
        }else{
           
            for (int i = 0; i < Config.config_customer_details.size(); i++) {
              CustomerDetails cd = Config.config_customer_details.get(i);
              if(Config.config_profile_details.get(Config.id_profile_details.indexOf(cd.getProfile_id())).getName().toUpperCase().startsWith(txt_customer_search.getText().trim().toUpperCase())  ){
                System.out.println(cd.getProfile_id());
                System.out.println(Config.id_profile_details.indexOf(cd.getProfile_id()));
            tbl_cmmodel.addRow(new Object[]{
            cd.getCustomer_id(),    
            Config.config_profile_details.get(Config.id_profile_details.indexOf(cd.getProfile_id())).getName(),
            Config.config_contact_details.get(Config.id_contact_details.indexOf(cd.getContact_id())).getContact_no(),    
            Config.config_contact_details.get(Config.id_contact_details.indexOf(cd.getContact_id())).getOther(),
            Config.config_contact_details.get(Config.id_contact_details.indexOf(cd.getContact_id())).getEmail(),
            Config.config_identity_details.get(Config.id_identity_details.indexOf(cd.getIdentity_id())).getIdentity_type(),
            Config.config_identity_details.get(Config.id_identity_details.indexOf(cd.getIdentity_id())).getIdentity_no(),
            Config.config_customer_details.get(i).getAccount_no(),
            Config.config_customer_details.get(i).getBank_name()
            });
            }
        }
        }
    } catch (Exception e) {
      e.printStackTrace();
    }
    }//GEN-LAST:event_btn_customer_searchActionPerformed

    private void txt_customer_addressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_customer_addressActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_customer_addressActionPerformed

    private void tbl_customerFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tbl_customerFocusGained
       setData();
    }//GEN-LAST:event_tbl_customerFocusGained

    private void tbl_customerKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_customerKeyReleased
        setData();  
    }//GEN-LAST:event_tbl_customerKeyReleased

    private void tbl_customerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_customerMouseClicked
         try {
            if(evt.getClickCount()==1){
            setData();
        }
        } catch (Exception e) {
            setData(); 
        }
        if(evt.getClickCount()==2){
            btn_customer_viewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_customerMouseClicked

    private void btn_customer_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_customer_deleteActionPerformed
    try {
           int dd = JOptionPane.showConfirmDialog(this,"Are you really want to delete this entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
         if (dd == 0){
            if (Config.customer_details_mgr.delCustomerDetails(Config.id_customer_details.get(tbl_customer.getSelectedRow()))){
                Config.customer_management.onloadCustomer();
                JOptionPane.showMessageDialog(this, "Customer Profile Delete successfully", "Success", JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion ", "Error", JOptionPane.ERROR_MESSAGE);
            }
         }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_customer_deleteActionPerformed
           
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_customer_cancel;
    private javax.swing.JButton btn_customer_delete;
    private javax.swing.JButton btn_customer_new;
    private javax.swing.JButton btn_customer_print;
    private javax.swing.JButton btn_customer_search;
    private javax.swing.JButton btn_customer_view;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JPanel pnl_employee;
    private javax.swing.JTable tbl_customer;
    private javax.swing.JTextField txt_customer_address;
    private javax.swing.JTextField txt_customer_city;
    private javax.swing.JTextField txt_customer_customername;
    private javax.swing.JTextField txt_customer_dob;
    private javax.swing.JTextField txt_customer_gender;
    private javax.swing.JTextPane txt_customer_message;
    private javax.swing.JTable txt_customer_payment;
    private javax.swing.JTextField txt_customer_pincode;
    private javax.swing.JTextField txt_customer_search;
    private javax.swing.JTextField txt_customer_state;
    // End of variables declaration//GEN-END:variables
  
     public void onloadReset(){
       
         onloadCustomer();
    }
    public void onloadCustomer(){
        btn_customer_searchActionPerformed(null);
      }
    
    public void setData(){
        try {
           
     CustomerDetails cus = Config.config_customer_details.get(Config.id_customer_details.indexOf(tbl_cmmodel.getValueAt(tbl_customer.getSelectedRow(), 0).toString()));
        txt_customer_customername.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(cus.getProfile_id())).getName());
        txt_customer_gender.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(cus.getProfile_id())).getGender());
        txt_customer_dob.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(cus.getProfile_id())).getDob());
        txt_customer_address.setText(Config.config_address_details.get(Config.id_address_details.indexOf(cus.getAddress_id())).getAddress_1());
        txt_customer_city.setText(Config.config_address_details.get(Config.id_address_details.indexOf(cus.getAddress_id())).getCity());
        txt_customer_pincode.setText(Config.config_address_details.get(Config.id_address_details.indexOf(cus.getAddress_id())).getPincode());
        txt_customer_state.setText(Config.config_address_details.get(Config.id_address_details.indexOf(cus.getAddress_id())).getState());
        
        } catch (Exception e) {
        }
        }
    
}
