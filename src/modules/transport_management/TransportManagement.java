package modules.transport_management;

import data_manager.configuration.Config;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

public class TransportManagement extends javax.swing.JDialog {
    boolean transport_from = true;
    boolean transport_to = true;
    boolean maintenance_from = true;
    boolean maintenance_to = true;
    SimpleDateFormat simple_date_formate = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
  SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
  SimpleDateFormat sdf_day = new SimpleDateFormat("dd");
    
    DefaultTableModel tbl_transport_model=null;
    DefaultTableModel tbl_maintenance_model=null;
     DefaultTableModel tbl_transport_report_model=null;

    public TransportManagement(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        tbl_expenses_report1.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        tbl_transport_model=(DefaultTableModel) tbl_transport.getModel();
        tbl_maintenance_model=(DefaultTableModel) tbl_maintenance.getModel();
        tbl_transport_report_model=(DefaultTableModel) tbl_expenses_report1.getModel();
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        pnl_employee = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txt_transport_search = new javax.swing.JTextField();
        btn_transport_search = new javax.swing.JButton();
        btn_transport_add = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        transport_check_to = new javax.swing.JCheckBox();
        transport_check_from = new javax.swing.JCheckBox();
        jLabel15 = new javax.swing.JLabel();
        jdc_transport_to = new com.toedter.calendar.JDateChooser();
        jdc_transport_from = new com.toedter.calendar.JDateChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_transport = new javax.swing.JTable();
        btn_expenses_cancel = new javax.swing.JButton();
        btn_expenses_view = new javax.swing.JButton();
        btn_expenses_print = new javax.swing.JButton();
        jPanel16 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        txt_transport_message = new javax.swing.JTextPane();
        jPanel3 = new javax.swing.JPanel();
        pnl_employee3 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txt_maintenance_search = new javax.swing.JTextField();
        btn_maintenance_search = new javax.swing.JButton();
        btn_maintenance_add = new javax.swing.JButton();
        jSeparator7 = new javax.swing.JSeparator();
        maintenance_check_to = new javax.swing.JCheckBox();
        maintenance_check_from = new javax.swing.JCheckBox();
        jLabel16 = new javax.swing.JLabel();
        jdc_main_to = new com.toedter.calendar.JDateChooser();
        jdc_main_from = new com.toedter.calendar.JDateChooser();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbl_maintenance = new javax.swing.JTable();
        btn_maintenance_cancel = new javax.swing.JButton();
        btn_maintenance_view = new javax.swing.JButton();
        btn_maintenance_print = new javax.swing.JButton();
        jPanel20 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        txt_maintenance_message = new javax.swing.JTextPane();
        jPanel4 = new javax.swing.JPanel();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        pnl_adv_report1 = new javax.swing.JPanel();
        jPanel24 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        btn_expenses_report_search1 = new javax.swing.JButton();
        jmc_month_report = new com.toedter.calendar.JMonthChooser();
        jLabel24 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        jdc_date_report = new com.toedter.calendar.JDateChooser();
        jyc_year_report = new com.toedter.calendar.JYearChooser();
        jLabel5 = new javax.swing.JLabel();
        check_date_report = new javax.swing.JCheckBox();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane13 = new javax.swing.JScrollPane();
        tbl_expenses_report1 = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane2.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        pnl_employee.setBackground(new java.awt.Color(255, 255, 255));
        pnl_employee.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pnl_employeeKeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Search By :");

        txt_transport_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_transport_searchCaretUpdate(evt);
            }
        });
        txt_transport_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_transport_searchKeyPressed(evt);
            }
        });

        btn_transport_search.setText("Search");
        btn_transport_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_transport_searchActionPerformed(evt);
            }
        });
        btn_transport_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_transport_searchKeyPressed(evt);
            }
        });

        btn_transport_add.setText("Add");
        btn_transport_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_transport_addActionPerformed(evt);
            }
        });
        btn_transport_add.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_transport_addKeyPressed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        transport_check_to.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        transport_check_to.setText("To");
        transport_check_to.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                transport_check_toActionPerformed(evt);
            }
        });

        transport_check_from.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        transport_check_from.setText("From");
        transport_check_from.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                transport_check_fromActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel15.setText("Date :");

        jdc_transport_to.setDateFormatString("dd/MM/yyyy");

        jdc_transport_from.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_transport_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(transport_check_from)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_transport_from, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(transport_check_to)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_transport_to, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_transport_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_transport_add)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jdc_transport_from, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jdc_transport_to, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_transport_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_transport_search)
                        .addComponent(btn_transport_add)
                        .addComponent(transport_check_to)
                        .addComponent(transport_check_from)
                        .addComponent(jLabel15))
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator3))
                .addContainerGap())
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_transport_add, btn_transport_search, jLabel15, jLabel2, jdc_transport_from, jdc_transport_to, transport_check_from, transport_check_to, txt_transport_search});

        tbl_transport.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "TRANSPORT NAME ", "DATE", "STARTING READING", "ENDING READING", "RUN", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_transport.getTableHeader().setReorderingAllowed(false);
        tbl_transport.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_transportMouseClicked(evt);
            }
        });
        tbl_transport.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_transportKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbl_transportKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_transport);
        if (tbl_transport.getColumnModel().getColumnCount() > 0) {
            tbl_transport.getColumnModel().getColumn(0).setResizable(false);
            tbl_transport.getColumnModel().getColumn(0).setPreferredWidth(100);
            tbl_transport.getColumnModel().getColumn(1).setResizable(false);
            tbl_transport.getColumnModel().getColumn(1).setPreferredWidth(250);
            tbl_transport.getColumnModel().getColumn(2).setResizable(false);
            tbl_transport.getColumnModel().getColumn(2).setPreferredWidth(150);
            tbl_transport.getColumnModel().getColumn(3).setResizable(false);
            tbl_transport.getColumnModel().getColumn(3).setPreferredWidth(150);
            tbl_transport.getColumnModel().getColumn(4).setResizable(false);
            tbl_transport.getColumnModel().getColumn(4).setPreferredWidth(150);
            tbl_transport.getColumnModel().getColumn(5).setResizable(false);
            tbl_transport.getColumnModel().getColumn(5).setPreferredWidth(150);
            tbl_transport.getColumnModel().getColumn(6).setMinWidth(0);
            tbl_transport.getColumnModel().getColumn(6).setPreferredWidth(0);
            tbl_transport.getColumnModel().getColumn(6).setMaxWidth(0);
        }

        btn_expenses_cancel.setText("Cancel");
        btn_expenses_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_expenses_cancelActionPerformed(evt);
            }
        });
        btn_expenses_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_expenses_cancelKeyPressed(evt);
            }
        });

        btn_expenses_view.setText("View");
        btn_expenses_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_expenses_viewActionPerformed(evt);
            }
        });
        btn_expenses_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_expenses_viewKeyPressed(evt);
            }
        });

        btn_expenses_print.setText("Print");
        btn_expenses_print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_expenses_printKeyPressed(evt);
            }
        });

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_transport_message.setEditable(false);
        jScrollPane7.setViewportView(txt_transport_message);

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7)
                .addContainerGap())
        );

        javax.swing.GroupLayout pnl_employeeLayout = new javax.swing.GroupLayout(pnl_employee);
        pnl_employee.setLayout(pnl_employeeLayout);
        pnl_employeeLayout.setHorizontalGroup(
            pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnl_employeeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 752, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addComponent(btn_expenses_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_expenses_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_expenses_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnl_employeeLayout.setVerticalGroup(
            pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_employeeLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE)
                    .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_expenses_cancel)
                    .addComponent(btn_expenses_view)
                    .addComponent(btn_expenses_print))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Tarnsport Details", pnl_employee);

        pnl_employee3.setBackground(new java.awt.Color(255, 255, 255));
        pnl_employee3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pnl_employee3KeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Search By :");

        txt_maintenance_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_maintenance_searchCaretUpdate(evt);
            }
        });
        txt_maintenance_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_maintenance_searchKeyPressed(evt);
            }
        });

        btn_maintenance_search.setText("Search");
        btn_maintenance_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_maintenance_searchActionPerformed(evt);
            }
        });
        btn_maintenance_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_maintenance_searchKeyPressed(evt);
            }
        });

        btn_maintenance_add.setText("Add");
        btn_maintenance_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_maintenance_addActionPerformed(evt);
            }
        });
        btn_maintenance_add.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_maintenance_addKeyPressed(evt);
            }
        });

        jSeparator7.setOrientation(javax.swing.SwingConstants.VERTICAL);

        maintenance_check_to.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        maintenance_check_to.setText("To");
        maintenance_check_to.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maintenance_check_toActionPerformed(evt);
            }
        });

        maintenance_check_from.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        maintenance_check_from.setText("From");
        maintenance_check_from.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maintenance_check_fromActionPerformed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel16.setText("Date :");

        jdc_main_to.setDateFormatString("dd/MM/yyyy");

        jdc_main_from.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_maintenance_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(maintenance_check_from)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_main_from, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(maintenance_check_to)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_main_to, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_maintenance_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_maintenance_add)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jdc_main_from, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jdc_main_to, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_maintenance_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_maintenance_search)
                        .addComponent(btn_maintenance_add)
                        .addComponent(maintenance_check_to)
                        .addComponent(maintenance_check_from)
                        .addComponent(jLabel16))
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator7))
                .addContainerGap())
        );

        jPanel11Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_maintenance_add, btn_maintenance_search, jLabel16, jLabel6, jdc_main_from, jdc_main_to, maintenance_check_from, maintenance_check_to, txt_maintenance_search});

        tbl_maintenance.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "TRANSPORT NAME ", "MAINTENANCE TYPE ", "DATE", "AMOUNT", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_maintenance.getTableHeader().setReorderingAllowed(false);
        tbl_maintenance.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_maintenanceMouseClicked(evt);
            }
        });
        tbl_maintenance.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_maintenanceKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbl_maintenanceKeyReleased(evt);
            }
        });
        jScrollPane5.setViewportView(tbl_maintenance);
        if (tbl_maintenance.getColumnModel().getColumnCount() > 0) {
            tbl_maintenance.getColumnModel().getColumn(0).setResizable(false);
            tbl_maintenance.getColumnModel().getColumn(1).setResizable(false);
            tbl_maintenance.getColumnModel().getColumn(2).setResizable(false);
            tbl_maintenance.getColumnModel().getColumn(3).setResizable(false);
            tbl_maintenance.getColumnModel().getColumn(4).setResizable(false);
            tbl_maintenance.getColumnModel().getColumn(5).setMinWidth(0);
            tbl_maintenance.getColumnModel().getColumn(5).setPreferredWidth(0);
            tbl_maintenance.getColumnModel().getColumn(5).setMaxWidth(0);
        }

        btn_maintenance_cancel.setText("Cancel");
        btn_maintenance_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_maintenance_cancelActionPerformed(evt);
            }
        });
        btn_maintenance_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_maintenance_cancelKeyPressed(evt);
            }
        });

        btn_maintenance_view.setText("View");
        btn_maintenance_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_maintenance_viewActionPerformed(evt);
            }
        });
        btn_maintenance_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_maintenance_viewKeyPressed(evt);
            }
        });

        btn_maintenance_print.setText("Print");
        btn_maintenance_print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_maintenance_printKeyPressed(evt);
            }
        });

        jPanel20.setBackground(new java.awt.Color(255, 255, 255));
        jPanel20.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_maintenance_message.setEditable(false);
        jScrollPane8.setViewportView(txt_maintenance_message);

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane8)
                .addContainerGap())
        );

        javax.swing.GroupLayout pnl_employee3Layout = new javax.swing.GroupLayout(pnl_employee3);
        pnl_employee3.setLayout(pnl_employee3Layout);
        pnl_employee3Layout.setHorizontalGroup(
            pnl_employee3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnl_employee3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_employee3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_employee3Layout.createSequentialGroup()
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 752, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnl_employee3Layout.createSequentialGroup()
                        .addComponent(btn_maintenance_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_maintenance_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_maintenance_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnl_employee3Layout.setVerticalGroup(
            pnl_employee3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_employee3Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_employee3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 463, Short.MAX_VALUE)
                    .addComponent(jPanel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_employee3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_maintenance_cancel)
                    .addComponent(btn_maintenance_view)
                    .addComponent(btn_maintenance_print))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1062, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnl_employee3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 570, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnl_employee3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Maintenance Details", jPanel3);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane3.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        pnl_adv_report1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel23.setText("Search By :");

        btn_expenses_report_search1.setText("Search");
        btn_expenses_report_search1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_expenses_report_search1ActionPerformed(evt);
            }
        });
        btn_expenses_report_search1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_expenses_report_search1KeyPressed(evt);
            }
        });

        jmc_month_report.setVerifyInputWhenFocusTarget(false);
        jmc_month_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_month_reportKeyPressed(evt);
            }
        });

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel24.setText("Month :");

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Date :");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Year :");

        jButton1.setText("Print");

        jButton2.setText("Cancel");

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(check_date_report)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_date_report, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jmc_month_report, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(jyc_year_report, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_expenses_report_search1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 281, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addContainerGap())
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel24Layout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator5)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btn_expenses_report_search1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(check_date_report, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jdc_date_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jButton1)
                                            .addComponent(jButton2))
                                        .addComponent(jyc_year_report, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jmc_month_report, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addContainerGap())
        );

        jPanel24Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_expenses_report_search1, check_date_report, jLabel23, jLabel24, jLabel4, jLabel5, jdc_date_report, jmc_month_report, jyc_year_report});

        jScrollPane13.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tbl_expenses_report1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbl_expenses_report1.getTableHeader().setReorderingAllowed(false);
        tbl_expenses_report1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_expenses_report1KeyPressed(evt);
            }
        });
        jScrollPane13.setViewportView(tbl_expenses_report1);

        javax.swing.GroupLayout pnl_adv_report1Layout = new javax.swing.GroupLayout(pnl_adv_report1);
        pnl_adv_report1.setLayout(pnl_adv_report1Layout);
        pnl_adv_report1Layout.setHorizontalGroup(
            pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnl_adv_report1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane13)
                .addContainerGap())
        );
        pnl_adv_report1Layout.setVerticalGroup(
            pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report1Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane13, javax.swing.GroupLayout.DEFAULT_SIZE, 467, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane3.addTab("Month wise Expenses", pnl_adv_report1);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane3, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane3, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        jTabbedPane2.addTab("Report", jPanel4);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        jPanel2.setBackground(new java.awt.Color(230, 79, 6));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Transport Management");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispatchEvent(evt);
    }//GEN-LAST:event_closeDialog

    private void txt_transport_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_transport_searchCaretUpdate
        btn_transport_searchActionPerformed(null);
    }//GEN-LAST:event_txt_transport_searchCaretUpdate

    private void txt_transport_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_transport_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jdc_transport_from.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            //            pnl_adv_report.requestFocustbl_transport    //        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_transport.requestFocus();
        }
    }//GEN-LAST:event_txt_transport_searchKeyPressed

    private void btn_transport_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_transport_searchActionPerformed
        String sql = "";
        String text=txt_transport_search.getText().trim().toUpperCase()+"%";
        if(transport_from == true && transport_to==true ){
            if (!txt_transport_search.getText().equals("")) {
                sql = "select ed.* from bike_details e,transport_details ed where e.bike_id=ed.bike_id and STR_TO_DATE(ed._date,'%d/%m/%Y') >STR_TO_DATE('"+simple_date_formate.format(jdc_transport_from.getDate())+"','%d/%m/%Y') and STR_TO_DATE(ed._date,'%d/%m/%Y')<=STR_TO_DATE('"+simple_date_formate.format(jdc_transport_to.getDate())+"','%d/%m/%Y') and e.name like '"+text+"' " ;
            } else {
                sql = "select ed.* from bike_details e,transport_details ed where e.bike_id=ed.bike_id and STR_TO_DATE(ed._date,'%d/%m/%Y') >STR_TO_DATE('"+simple_date_formate.format(jdc_transport_from.getDate())+"','%d/%m/%Y') and STR_TO_DATE(ed._date,'%d/%m/%Y')<=STR_TO_DATE('"+simple_date_formate.format(jdc_transport_to.getDate())+"','%d/%m/%Y') " ;
            }
        }else if(transport_from == true && transport_to==false ){
            if (!txt_transport_search.getText().equals("")) {
                sql = "select ed.* from bike_details e,transport_details ed where e.bike_id=ed.bike_id and STR_TO_DATE(ed._date,'%d/%m/%Y')  =STR_TO_DATE('"+simple_date_formate.format(jdc_transport_from.getDate())+"','%d/%m/%Y') and e.name like '"+text+"'" ;
            } else {
                sql = "select ed.* from bike_details e,transport_details ed where e.bike_id=ed.bike_id and STR_TO_DATE(ed._date,'%d/%m/%Y')  =STR_TO_DATE('"+simple_date_formate.format(jdc_transport_from.getDate())+"','%d/%m/%Y')" ;
            }
        }else{
            if (!txt_transport_search.getText().equals("")) {
                sql = "select ed.* from bike_details e,transport_details ed where e.bike_id=ed.bike_id and e.name like '"+text+"'" ;
            } else {
                sql = "select ed.* from bike_details e,transport_details ed where e.bike_id=ed.bike_id ";
            }
        }
//        System.out.println(sql);
        loadTransportDetails(sql);
    }//GEN-LAST:event_btn_transport_searchActionPerformed

    private void btn_transport_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_transport_searchKeyPressed
//        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
//            btn_transport_add.requestFocus();
//        }
//        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
//            jfmt_transport_ttbl_transportus();
//        }
//        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
//            //            .requestFocus();
//            //           }
//        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
//            tbl_transport.requestFocus();
//        }
    }//GEN-LAST:event_btn_transport_searchKeyPressed

    private void btn_transport_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_transport_addActionPerformed
        Config.new_transport.onloadReset();
        Config.new_transport.setVisible(true);
    }//GEN-LAST:event_btn_transport_addActionPerformed

    private void btn_transport_addKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_transport_addKeyPressed
//        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
//            tbl_transport.requestFocus();
//        }
//        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
//            btn_transport_searctbl_transportus();
//        }
//        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
//            //            .requestFocus();
//            //           }
//        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
//            tbl_transport.requestFocus();
//        }
    }//GEN-LAST:event_btn_transport_addKeyPressed

    private void transport_check_toActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_transport_check_toActionPerformed
        if(transport_check_to.isSelected()){
            transport_to=true;
            jdc_transport_to.setEnabled(true);
        }else{
            transport_to=false;
            jdc_transport_to.setEnabled(false);
        }
    }//GEN-LAST:event_transport_check_toActionPerformed

    private void transport_check_fromActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_transport_check_fromActionPerformed
        if(transport_check_from.isSelected()){
            transport_from =true;
            transport_to=false;
            jdc_transport_from.setEnabled(true);
            jdc_transport_to.setEnabled(false);
            transport_check_to.setSelected(false);
            transport_check_to.setEnabled(true);
        }else{
            transport_from =false;
            transport_to=false;
            jdc_transport_from.setEnabled(false);
            jdc_transport_to.setEnabled(false);
            transport_check_to.setSelected(false);
            transport_check_to.setEnabled(false);
        }
    }//GEN-LAST:event_transport_check_fromActionPerformed

    private void tbl_transportMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_transportMouseClicked
        try {
            if(evt.getClickCount()==1){
                onloadTransportMessage(tbl_transport_model.getValueAt(tbl_transport.getSelectedRow(), 6).toString());
            }
            if(evt.getClickCount()==2){
                btn_expenses_viewActionPerformed(null);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_tbl_transportMouseClicked

    private void tbl_transportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_transportKeyPressed
//        onloadExpensesMessage(tbl_transport_model.getValueAt(tbl_transport.getSelectedRow(), 4).toString());
    }//GEN-LAST:event_tbl_transportKeyPressed

    private void btn_expenses_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_expenses_cancelKeyPressed
        //        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            //    tbl_transportuestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_expenses_view.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_transport.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            tbl_employee.requestFocus();
            //           }
    }//GEN-LAST:event_btn_expenses_cancelKeyPressed

    private void btn_expenses_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_expenses_viewActionPerformed
        try{
            Config.view_transport.onloadReset(tbl_transport_model.getValueAt(tbl_transport.getSelectedRow(), 0).toString());
            Config.view_transport.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_expenses_viewActionPerformed

    private void btn_expenses_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_expenses_viewKeyPressed
//        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
//            tbl_transportenses_cancel.requestFocus();
//        }
//        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
//            btn_transport_print.requestFocus();
//        }
//        if(evt.getKeyCode() == KeyEvent.VK_UP) {
//            tbl_transport.requestFocus();
//        }
//        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
//            btn_transport_cancel.requestFocus();
//        }
    }//GEN-LAST:event_btn_expenses_viewKeyPressed

    private void btn_expenses_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_expenses_printKeyPressed
//        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {tbl_transport btn_transport_view.requestFocus();
//        }
//        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
//            tbl_transport.requestFocus();
//        }
//        if(evt.getKeyCode() == KeyEvent.VK_UP) {
//            tbl_transport.requestFocus();
//        }
//        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
//            btn_transport_view.requestFocus();
//        }
    }//GEN-LAST:event_btn_expenses_printKeyPressed

    private void pnl_employeeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pnl_employeeKeyPressed
//        System.out.println("sajjjj");
    }//GEN-LAST:event_pnl_employeeKeyPressed

    private void txt_maintenance_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_maintenance_searchCaretUpdate
        btn_maintenance_searchActionPerformed(null);
    }//GEN-LAST:event_txt_maintenance_searchCaretUpdate

    private void txt_maintenance_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_maintenance_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_maintenance_searchKeyPressed

    private void btn_maintenance_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_maintenance_searchActionPerformed
        String sql = "";
        String text=txt_maintenance_search.getText().trim().toUpperCase()+"%";
        if(maintenance_from == true && maintenance_to==true ){
            if (!txt_maintenance_search.getText().equals("")) {
                sql = "select ed.* from maintenance_type e,maintenance_details ed where e.maintenance_type_id=ed.maintenance_type_id and STR_TO_DATE(ed._date,'%d/%m/%Y') >STR_TO_DATE('"+simple_date_formate.format(jdc_main_from.getDate())+"','%d/%m/%Y') and STR_TO_DATE(ed._date,'%d/%m/%Y')<=STR_TO_DATE('"+simple_date_formate.format(jdc_main_to.getDate())+"','%d/%m/%Y') and e.type like '"+text+"' " ;
            } else {
                sql = "select ed.* from maintenance_type e,maintenance_details ed where e.maintenance_type_id=ed.maintenance_type_id and STR_TO_DATE(ed._date,'%d/%m/%Y') >STR_TO_DATE('"+simple_date_formate.format(jdc_main_from.getDate())+"','%d/%m/%Y') and STR_TO_DATE(ed._date,'%d/%m/%Y')<=STR_TO_DATE('"+simple_date_formate.format(jdc_main_to.getDate())+"','%d/%m/%Y') " ;
            }
        }else if(maintenance_from == true && maintenance_to==false ){
            if (!txt_maintenance_search.getText().equals("")) {
                sql = "select ed.* from maintenance_type e,maintenance_details ed where e.maintenance_type_id=ed.maintenance_type_id and STR_TO_DATE(ed._date,'%d/%m/%Y')  =STR_TO_DATE('"+simple_date_formate.format(jdc_main_from.getDate())+"','%d/%m/%Y') and e.type like '"+text+"'" ;
            } else {
                sql = "select ed.* from maintenance_type e,maintenance_details ed where e.maintenance_type_id=ed.maintenance_type_id and STR_TO_DATE(ed._date,'%d/%m/%Y')  =STR_TO_DATE('"+simple_date_formate.format(jdc_main_from.getDate())+"','%d/%m/%Y')" ;
            }
        }else{
            if (!txt_maintenance_search.getText().equals("")) {
                sql = "select ed.* from maintenance_type e,maintenance_details ed where e.maintenance_type_id=ed.maintenance_type_id and e.type like '"+text+"'" ;
            } else {
                sql = "select ed.* from maintenance_type e,maintenance_details ed where e.maintenance_type_id=ed.maintenance_type_id ";
            }
        }
//        System.out.println(sql);
        loadMaintenanceDetails(sql);
    }//GEN-LAST:event_btn_maintenance_searchActionPerformed

    private void btn_maintenance_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_maintenance_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_maintenance_searchKeyPressed

    private void btn_maintenance_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_maintenance_addActionPerformed
        Config.add_maintenance.onloadReset();
        Config.add_maintenance.setVisible(true);
    }//GEN-LAST:event_btn_maintenance_addActionPerformed

    private void btn_maintenance_addKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_maintenance_addKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_maintenance_addKeyPressed

    private void maintenance_check_toActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maintenance_check_toActionPerformed
        if(maintenance_check_to.isSelected()){
           maintenance_to=true;
           jdc_transport_to.setEnabled(true);
        }else{
           maintenance_to=false;
           jdc_main_to.setEnabled(false);
        }
    }//GEN-LAST:event_maintenance_check_toActionPerformed

    private void maintenance_check_fromActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maintenance_check_fromActionPerformed
        if(maintenance_check_from.isSelected()){
           maintenance_from =true;
           maintenance_to=false;
           jdc_main_from.setEnabled(true);
           jdc_main_to.setEnabled(false);
           maintenance_check_to.setSelected(false);
           maintenance_check_to.setEnabled(true);
        }else{
           maintenance_from =false;
           maintenance_to=false;
           jdc_main_from.setEnabled(false);
           jdc_main_to.setEnabled(false);
           maintenance_check_to.setSelected(false);
           maintenance_check_to.setEnabled(false);
        }
    }//GEN-LAST:event_maintenance_check_fromActionPerformed

    private void tbl_maintenanceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_maintenanceMouseClicked
        try {
            if(evt.getClickCount()==1){
                onloadMaintenanceMessage(tbl_maintenance_model.getValueAt(tbl_maintenance.getSelectedRow(), 5).toString());
            }
            if(evt.getClickCount()==2){
                btn_maintenance_viewActionPerformed(null);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_tbl_maintenanceMouseClicked

    private void tbl_maintenanceKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_maintenanceKeyPressed
        try {
                onloadMaintenanceMessage(tbl_transport_model.getValueAt(tbl_transport.getSelectedRow(), 5).toString());
        } catch (Exception e) {
        }
         
    }//GEN-LAST:event_tbl_maintenanceKeyPressed

    private void btn_maintenance_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_maintenance_cancelKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_maintenance_cancelKeyPressed

    private void btn_maintenance_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_maintenance_viewActionPerformed
        try{
                   Config.view_add_maintenance.onloadReset(tbl_maintenance_model.getValueAt(tbl_maintenance.getSelectedRow(), 0).toString());
                   Config.view_add_maintenance.setVisible(true);
               } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_maintenance_viewActionPerformed

    private void btn_maintenance_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_maintenance_viewKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_maintenance_viewKeyPressed

    private void btn_maintenance_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_maintenance_printKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_maintenance_printKeyPressed

    private void pnl_employee3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pnl_employee3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pnl_employee3KeyPressed

    private void btn_expenses_report_search1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_expenses_report_search1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_expenses_report_search1KeyPressed

    private void jmc_month_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_month_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmc_month_reportKeyPressed

    private void tbl_expenses_report1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_expenses_report1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_expenses_report1KeyPressed

    private void tbl_transportKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_transportKeyReleased
        try {
            onloadTransportMessage(tbl_transport_model.getValueAt(tbl_transport.getSelectedRow(), 6).toString());
        } catch (Exception e) {
        }
    }//GEN-LAST:event_tbl_transportKeyReleased

    private void btn_expenses_report_search1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_expenses_report_search1ActionPerformed
        String sql="";
        if(!check_date_report.isSelected()){
            sql=" SELECT a._date,a.bike_id ,a.maintenance_type_id,a.amount FROM maintenance_details a inner join bike_details b on a.bike_id=b.bike_id inner join maintenance_details c on c.maintenance_details_id=a.maintenance_details_id where a._month='"+(jmc_month_report.getMonth()+1)+"' and a._year='"+jyc_year_report.getYear()+"' order by STR_TO_DATE(a._date,'%d/%m/%Y'),a.bike_id,a.maintenance_type_id";
        }else{
            sql=" SELECT a._date,a.bike_id ,a.maintenance_type_id,a.amount FROM maintenance_details a inner join bike_details b on a.bike_id=b.bike_id inner join maintenance_details c on c.maintenance_details_id=a.maintenance_details_id where a._month='"+(jmc_month_report.getMonth()+1)+"' and a._year='"+jyc_year_report.getYear()+"' and STR_TO_DATE(a._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_date_report.getDate())+"','%d/%m/%Y') order by STR_TO_DATE(a._date,'%d/%m/%Y'),a.bike_id,a.maintenance_type_id";
        }
        loadReport(sql);
    }//GEN-LAST:event_btn_expenses_report_search1ActionPerformed

    private void btn_maintenance_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_maintenance_cancelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_maintenance_cancelActionPerformed

    private void tbl_maintenanceKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_maintenanceKeyReleased
        onloadMaintenanceMessage(tbl_maintenance_model.getValueAt(tbl_maintenance.getSelectedRow(), 5).toString());
            
    }//GEN-LAST:event_tbl_maintenanceKeyReleased

    private void btn_expenses_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_expenses_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_expenses_cancelActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_expenses_cancel;
    private javax.swing.JButton btn_expenses_print;
    private javax.swing.JButton btn_expenses_report_search1;
    private javax.swing.JButton btn_expenses_view;
    private javax.swing.JButton btn_maintenance_add;
    private javax.swing.JButton btn_maintenance_cancel;
    private javax.swing.JButton btn_maintenance_print;
    private javax.swing.JButton btn_maintenance_search;
    private javax.swing.JButton btn_maintenance_view;
    private javax.swing.JButton btn_transport_add;
    private javax.swing.JButton btn_transport_search;
    private javax.swing.JCheckBox check_date_report;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTabbedPane jTabbedPane3;
    private com.toedter.calendar.JDateChooser jdc_date_report;
    private com.toedter.calendar.JDateChooser jdc_main_from;
    private com.toedter.calendar.JDateChooser jdc_main_to;
    private com.toedter.calendar.JDateChooser jdc_transport_from;
    private com.toedter.calendar.JDateChooser jdc_transport_to;
    private com.toedter.calendar.JMonthChooser jmc_month_report;
    private com.toedter.calendar.JYearChooser jyc_year_report;
    private javax.swing.JCheckBox maintenance_check_from;
    private javax.swing.JCheckBox maintenance_check_to;
    private javax.swing.JPanel pnl_adv_report1;
    private javax.swing.JPanel pnl_employee;
    private javax.swing.JPanel pnl_employee3;
    private javax.swing.JTable tbl_expenses_report1;
    private javax.swing.JTable tbl_maintenance;
    private javax.swing.JTable tbl_transport;
    private javax.swing.JCheckBox transport_check_from;
    private javax.swing.JCheckBox transport_check_to;
    private javax.swing.JTextPane txt_maintenance_message;
    private javax.swing.JTextField txt_maintenance_search;
    private javax.swing.JTextPane txt_transport_message;
    private javax.swing.JTextField txt_transport_search;
    // End of variables declaration//GEN-END:variables

    public void onloadReset() {
        transport_check_from.setSelected(true);
        transport_check_to.setSelected(true);
        jdc_transport_to.setDate(Calendar.getInstance().getTime());
        maintenance_check_from.setSelected(true);
        maintenance_check_to.setSelected(true);
        jdc_main_to.setDate(Calendar.getInstance().getTime());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -5);
        Date date=cal.getTime();
        jdc_transport_from.setDate(date);
        jdc_main_from.setDate(date);
        onloadTransportDetails();
        onloadMaintenanceDetails();
    }
    public void onloadTransportDetails(){
        btn_transport_searchActionPerformed(null);
    }
    public void loadTransportDetails(String sql) {
            try {
                tbl_transport_model.setRowCount(0);
                Config.sql =sql;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                while (Config.rs.next()) {
                tbl_transport_model.addRow(new Object[]{
                Config.rs.getString("transport_details_id"),
                Config.config_bike_details.get(Config.id_bike_details.indexOf(Config.rs.getString("bike_id"))).getName(),
                Config.rs.getString("_date"),
                Config.rs.getString("str_reading"),
                Config.rs.getString("end_reading"),
                Config.rs.getString("diff_reading"),
                Config.rs.getString("message_id")
                });
                }
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }
    public void loadMaintenanceDetails(String sql) {
            try {
                tbl_maintenance_model.setRowCount(0);
                Config.sql =sql;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                while (Config.rs.next()) {
                tbl_maintenance_model.addRow(new Object[]{
                Config.rs.getString("maintenance_details_id"),    
                Config.config_bike_details.get(Config.id_bike_details.indexOf(Config.rs.getString("bike_id"))).getName(),
                Config.config_maintenance_type.get(Config.id_maintenance_type.indexOf(Config.rs.getString("maintenance_type_id"))).getType(),
                Config.rs.getString("_date"),
                Config.rs.getString("amount"),
                Config.rs.getString("message_id")
                });
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void onloadTransportMessage(String message_id) {
       try {
            if (!message_id.equals("")) {
                Config.sql ="select message from message_details where message_id="+message_id;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                if(Config.rs.next()) {
                    txt_transport_message.setText(Config.rs.getString("message"));
                }
            } else {
                txt_transport_message.setText("");
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    private void onloadMaintenanceMessage(String message_id) {
       try {
            if (!message_id.equals("")) {
                Config.sql ="select message from message_details where message_id="+message_id;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                if(Config.rs.next()) {
                    txt_maintenance_message.setText(Config.rs.getString("message"));
                }
            } else {
                txt_maintenance_message.setText("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void onloadMaintenanceDetails() {
        btn_maintenance_searchActionPerformed(null);
    }
int col=0;
private void loadReport(String sql) {
        int col_height=0;  
        int row=1;  
        int year = jyc_year_report.getYear();
        int month = jmc_month_report.getMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        int total = calendar.getActualMaximum(Calendar.DATE);
        int size= (Config.config_maintenance_type.size()*Config.config_bike_details.size())+2;
//        System.out.println(size);
        
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = new boolean [size];
        String []col = new String[size];
        Object [][]obj = new Object[total][size];
        
        
        //...................................................................SET FIRST COLUMN ON DATE.....................................................
        for (int i = 0; i < total; i++) {
         if(i<9){
             if(month<9){
                 obj[i][0]="0"+(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]="0"+(i+1)+"/"+(month+1)+"/"+year;
             }
         }else{
             if(month<9){
                 obj[i][0]=(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]=(i+1)+"/"+(month+1)+"/"+year;
             }
         }
        }
        //...................................................................SET COLUMN NAME.....................................................
        can[0]=false;
        col[0]="  DATE";
          
        for (int j = 0; j < Config.config_bike_details.size(); j++) {
            for (int i = 0; i < Config.config_maintenance_type.size(); i++) {
            can[row]=false;
            col[row]="  "+Config.config_bike_details.get(j).getName()+"  ("+Config.config_maintenance_type.get(i).getType()+")";
            if(col_height<col[row].length()){
                col_height=col[row].length();
            }
            row++;
            }
        }
        can[row]=false;
        col[row]="  TOTAL";
        
        //...................................................................SET DATE ON CELL.....................................................
        try {
            Config.sql =sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()) {
                int c=1;
            for (int j = 0; j < Config.config_bike_details.size(); j++) {
                for (int i = 0; i < Config.config_maintenance_type.size(); i++) {
                    if(Config.config_bike_details.get(j).getBike_id().equals(Config.rs.getString("a.bike_id"))&& Config.config_maintenance_type.get(i).getMaintenance_type_id().equals(Config.rs.getString("a.maintenance_type_id"))){
                        Double advance=0.0;
                            try {
                            if(obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("a._date"))))-1][c]==null){
                                advance=Double.parseDouble(Config.rs.getString("a.amount").toString());
                                obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("a._date"))))-1][c]=advance;;
                            }else{
                                advance= Double.parseDouble(obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("a._date"))))-1][c].toString());
                                advance=advance+Double.parseDouble(Config.rs.getString("a.amount"));
                                obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("a._date").toString())))-1][c]=advance;;
                            }
                            } catch (Exception e) {
//                                e.printStackTrace();
                            }
                  }
                 c++;
                }
             }
            }
        }catch (Exception e) {
        }
                
        //...................................................................SET TOTAL DATE WISE.....................................................
 for (int i = 0; i < total; i++) {
    Double amount=0.0;
    int j=0;
    for ( j = 1; j < size-1; j++) {
        try {
            amount=amount+Double.parseDouble(obj[i][j].toString());
        } catch (Exception e) {
        }
    }
    obj[i][j]=amount;
}
 
        //...................................................................SET TABLE..................................................... 
        tbl_expenses_report1.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return can [columnIndex];
        }
    });
    
tbl_expenses_report1.getTableHeader().getColumnModel().getColumn(0).setHeaderRenderer(new RotatedTableCellRenderer(270));
tbl_expenses_report1.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
tbl_expenses_report1.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(150);
tbl_expenses_report1.getTableHeader().setPreferredSize(new Dimension(1000000,col_height*8));
for (int j = 1; j < size-1; j++) {
  tbl_expenses_report1.getTableHeader().getColumnModel().getColumn(j).setHeaderRenderer(new RotatedTableCellRenderer(270));
  tbl_expenses_report1.getTableHeader().getColumnModel().getColumn(j).setResizable(false);
  tbl_expenses_report1.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(80);
}    
tbl_expenses_report1.getTableHeader().getColumnModel().getColumn(row).setHeaderRenderer(new RotatedTableCellRenderer(270));
tbl_expenses_report1.getTableHeader().getColumnModel().getColumn(row).setResizable(false);
tbl_expenses_report1.getTableHeader().getColumnModel().getColumn(row).setPreferredWidth(200);
        //...................................................................SET DATE ON CELL.....................................................    
}
}
