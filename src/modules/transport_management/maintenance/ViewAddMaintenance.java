package modules.transport_management.maintenance;

import data_manager.MaintenanceDetails;
import data_manager.configuration.Config;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class ViewAddMaintenance extends javax.swing.JDialog {

    String bike_id="";
    String maintenance_type_id="";
    String message_id="";
    
    SimpleDateFormat simple_date_formate = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
    
    public ViewAddMaintenance(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_maintenance = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_tansport = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_amount = new javax.swing.JTextField();
        lbl_id = new javax.swing.JLabel();
        jdc_date = new com.toedter.calendar.JDateChooser();
        btn_update = new javax.swing.JButton();
        btn_message = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();

        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Expenses", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Maintenance Name");

        txt_maintenance.setEditable(false);
        txt_maintenance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_maintenanceActionPerformed(evt);
            }
        });
        txt_maintenance.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_maintenanceKeyPressed(evt);
            }
        });

        jLabel2.setText("Date ");

        jLabel4.setText("Transport Name");

        txt_tansport.setEditable(false);
        txt_tansport.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_tansportCaretUpdate(evt);
            }
        });

        jLabel5.setText("Amount");

        lbl_id.setForeground(new java.awt.Color(255, 255, 255));
        lbl_id.setText("jLabel3");

        jdc_date.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbl_id))
                    .addComponent(txt_maintenance)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(txt_tansport))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lbl_id))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_maintenance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_tansport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btn_update.setText("Update");
        btn_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateActionPerformed(evt);
            }
        });
        btn_update.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_updateKeyPressed(evt);
            }
        });

        btn_message.setText("Massege");
        btn_message.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_messageActionPerformed(evt);
            }
        });
        btn_message.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_messageKeyPressed(evt);
            }
        });

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });
        btn_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_cancelKeyPressed(evt);
            }
        });

        jPanel13.setBackground(new java.awt.Color(230, 79, 6));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("View Maintenance Details");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_message)
                        .addGap(54, 54, 54)
                        .addComponent(btn_update)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancel)))
                .addGap(0, 5, Short.MAX_VALUE))
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_cancel)
                    .addComponent(btn_update)
                    .addComponent(btn_message))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void txt_maintenanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_maintenanceActionPerformed
        Config.transport_search.onloadReset(1);
        Config.transport_search.setVisible(true);
    }//GEN-LAST:event_txt_maintenanceActionPerformed

    private void txt_maintenanceKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_maintenanceKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            jdc_date.requestFocus();
            jdc_date.setBackground(new Color(240,240,240));
            txt_maintenance.setBackground(Color.WHITE);
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            jdc_date.requestFocus();
            jdc_date.setBackground(new Color(240,240,240));
            txt_maintenance.setBackground(Color.WHITE);
        }
    }//GEN-LAST:event_txt_maintenanceKeyPressed

    private void txt_tansportCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_tansportCaretUpdate

    }//GEN-LAST:event_txt_tansportCaretUpdate

    private void btn_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateActionPerformed
        try {
            String str = checkValidity();
            if(str.equals("ok")){
                MaintenanceDetails ed = new MaintenanceDetails();
                ed.setMaintenance_details_id(lbl_id.getText());
                ed.setMaintenance_type_id(maintenance_type_id);
                ed.setBike_id(bike_id);
                 ed.setDate(simple_date_formate.format(jdc_date.getDate()));
                ed.setMonth(sdf_month.format(simple_date_formate.parse(simple_date_formate.format(jdc_date.getDate()))));
                ed.setYear(sdf_year.format(simple_date_formate.parse(simple_date_formate.format(jdc_date.getDate()))));
                ed.setAmount(txt_amount.getText().trim());
                ed.setMessage_id(message_id);
                if(Config.maintenance_details_mgr.updMaintenanceDetails(ed)){
                    Config.transport_management.onloadMaintenanceDetails();
                    JOptionPane.showMessageDialog(this, "Transport Details Insertion successfully", "Success", JOptionPane.NO_OPTION);
                    dispose();
                }else{
                    JOptionPane.showMessageDialog(this, "Error in insertion ", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }else{
                JOptionPane.showMessageDialog(this, str +"should not be blank", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_updateActionPerformed

    private void btn_updateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_updateKeyPressed
        //        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            //            btn_cancel.requestFocus();
            //        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            //            btn_cancel.requestFocus();
            //        }
        //        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
            //            btn_message.requestFocus();
            //        }
        //        if (evt.getKeyCode() == KeyEvent.VK_UP) {
            //            btn_message.requestFocus();
            //        }
    }//GEN-LAST:event_btn_updateKeyPressed

    private void btn_messageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_messageActionPerformed
        if(message_id.equals("")){
            Config.new_message.onloadReset(19);
            Config.new_message.setVisible(true);
        }else{
            Config.view_message.onloadReset(19,message_id);
            Config.view_message.setVisible(true);
        }
    }//GEN-LAST:event_btn_messageActionPerformed

    private void btn_messageKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_messageKeyPressed
        //        if(evt.getKeyCode() == KeyEvent.VK_RIGHT){
            //            btn_save.requestFocus();
            //        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN){
            //            btn_save.requestFocus();
            //        }
        //        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            //            txt_end_reading.requestFocus();
            //            txt_end_reading.setBackground(new Color(240,240,240));
            //        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            txt_end_reading.requestFocus();
            //            txt_end_reading.setBackground(new Color(240,240,240));
            //        }
    }//GEN-LAST:event_btn_messageKeyPressed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void btn_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_cancelKeyPressed
//        if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
//            btn_update.requestFocus();
//        }
//        if (evt.getKeyCode() == KeyEvent.VK_UP) {
//            btn_update.requestFocus();
//        }
    }//GEN-LAST:event_btn_cancelKeyPressed
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_message;
    private javax.swing.JButton btn_update;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private com.toedter.calendar.JDateChooser jdc_date;
    private javax.swing.JLabel lbl_id;
    private javax.swing.JTextField txt_amount;
    private javax.swing.JTextField txt_maintenance;
    private javax.swing.JTextField txt_tansport;
    // End of variables declaration//GEN-END:variables

public void onloadReset(String id){
        lbl_id.setText(id);
        try {
            Config.sql = "Select * from maintenance_details where maintenance_details_id = '"+id+"'";
            Config.rs = Config.stmt.executeQuery(Config.sql);            
            if (Config.rs.next()) {
              lbl_id.setText(Config.rs.getString("maintenance_details_id"));
              maintenance_type_id=Config.rs.getString("maintenance_type_id");
              txt_maintenance.setText(Config.config_maintenance_type.get(Config.id_maintenance_type.indexOf(Config.rs.getString("maintenance_type_id"))).getType());
              bike_id=Config.rs.getString("bike_id");
              txt_tansport.setText(Config.config_bike_details.get(Config.id_bike_details.indexOf(Config.rs.getString("bike_id"))).getName());
              jdc_date.setDate(simple_date_formate.parse(Config.rs.getString("_date")));
              txt_amount.setText(Config.rs.getString("amount"));
              message_id=Config.rs.getString("message_id");
            }    
        } catch (Exception e) {
        }
    }
    private String checkValidity() {
        if(txt_amount.equals("")){
            return "Amount";
        }else{
            return "ok";
        }
    }

    public void setMessage(String message_id) {
        this.message_id = message_id;
    }
}
