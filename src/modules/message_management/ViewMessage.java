package modules.message_management;

import data_manager.MessageDetails;
import data_manager.configuration.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class ViewMessage extends javax.swing.JDialog {
    int index=0;
    int module = 0;
    public ViewMessage(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        lbl_msg_id = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txt_msg = new javax.swing.JTextArea();
        btn_delete = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        btn_update = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setToolTipText("");

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jPanel13.setBackground(new java.awt.Color(230, 79, 6));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("View Message");

        lbl_msg_id.setForeground(new java.awt.Color(230, 79, 6));
        lbl_msg_id.setText("jLabel1");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbl_msg_id)
                .addGap(36, 36, 36))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                .addComponent(lbl_msg_id))
        );

        txt_msg.setColumns(20);
        txt_msg.setRows(5);
        jScrollPane1.setViewportView(txt_msg);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btn_delete.setText("Delete");
        btn_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteActionPerformed(evt);
            }
        });
        btn_delete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_deleteKeyPressed(evt);
            }
        });

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });
        btn_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_cancelKeyPressed(evt);
            }
        });

        btn_update.setText("Update");
        btn_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateActionPerformed(evt);
            }
        });
        btn_update.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_updateKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_delete, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addComponent(btn_update)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_delete)
                    .addComponent(btn_cancel)
                    .addComponent(btn_update))
                .addGap(0, 6, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
                                                      
    private void closeDialog(java.awt.event.WindowEvent evt) {                             
        
    } 
    private void btn_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_cancelKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
           btn_update.requestFocus();
        }
    }//GEN-LAST:event_btn_cancelKeyPressed

    private void btn_deleteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_deleteKeyPressed
       if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_update.requestFocus();
        } 
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_update.requestFocus();
        }                 
    }//GEN-LAST:event_btn_deleteKeyPressed

    private void btn_updateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_updateKeyPressed
       if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_cancel.requestFocus();
        } 
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
           btn_delete.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_cancel.requestFocus();
        }  
    }//GEN-LAST:event_btn_updateKeyPressed

    private void btn_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateActionPerformed
        try {
            String str = checkValidity();
        if(str.equals("ok")){
                MessageDetails md = new MessageDetails();
                md.setMessage_id(lbl_msg_id.getText());
                md.setMessage(txt_msg.getText().trim().toUpperCase());
                if(Config.message_details_mgr.updMessageDetails(md)){
                    JOptionPane.showMessageDialog(this, "Message Updation successfully", "Success", JOptionPane.NO_OPTION);
                    set();
                }else{
                    JOptionPane.showMessageDialog(this, "Error in updation ", "Error", JOptionPane.ERROR_MESSAGE);   
                }
        }else{
                    JOptionPane.showMessageDialog(this, str +"should not be blank", "Error", JOptionPane.ERROR_MESSAGE);
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_updateActionPerformed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void btn_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteActionPerformed
        if(Config.message_details_mgr.delMessageDetails(lbl_msg_id.getText())){
            lbl_msg_id.setText("");
            JOptionPane.showMessageDialog(this, "Message deletion successfully", "Success", JOptionPane.NO_OPTION);
            set();
            dispose();
        }else{
            JOptionPane.showMessageDialog(this, "Error in deletion ", "Error", JOptionPane.ERROR_MESSAGE);   
        }
    }//GEN-LAST:event_btn_deleteActionPerformed
     
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_delete;
    private javax.swing.JButton btn_update;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbl_msg_id;
    private javax.swing.JTextArea txt_msg;
    // End of variables declaration//GEN-END:variables

    public void onloadReset(int module,String message_id) {
        this.module = module;
        lbl_msg_id.setText(message_id);
        setData();
    }
    public void setData(){
        try {
            Config.sql ="select message from message_details where message_id='"+lbl_msg_id.getText()+"'";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if(Config.rs.next()) {
            txt_msg.setText(Config.rs.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private String checkValidity() {
        if(txt_msg.getText().equals("")){
         return "Message";
        }else{
         return "ok";
        }
    }
        
    public void set(){
       switch(module){
           case 0:
           Config.new_stock.setMessage(lbl_msg_id.getText());    
           break;
           case 1:
           Config.view_stock.setMessage(lbl_msg_id.getText());        
           break;    
           case 2:
           Config.new_demage_item.setMessage(lbl_msg_id.getText());        
           break;        
           case 3:
           Config.view_demage_Item.setMessage(lbl_msg_id.getText());        
           break;        
            case 4:
           Config.new_return_item.setMessage(lbl_msg_id.getText());        
           break;    
           case 5:
           Config.view_return_Item.setMessage(lbl_msg_id.getText());        
           break;    
           case 6:
           Config.add_expenses.setMessage(lbl_msg_id.getText());        
           break; 
           case 7:
           Config.view_add_expenses.setMessage(lbl_msg_id.getText());        
           break;         
           case 8:
           Config.new_employee.setMessage(lbl_msg_id.getText());        
           break;
           case 9:
           Config.view_employee.setMessage(lbl_msg_id.getText());        
           break;         
           case 10:
           Config.add_salary_info.setMessage(lbl_msg_id.getText());        
           break;
           case 11:
           Config.view_salary_info.setMessage(lbl_msg_id.getText());        
           break;
           case 12:
           Config.add_advance.setMessage(lbl_msg_id.getText());    
           break;
           case 13:
           Config.view_advance.setMessage(lbl_msg_id.getText());    
           break;    
           case 14:
           Config.attendence.setMessage(lbl_msg_id.getText(),index);    
           break;        
           case 15:
           Config.view_attendence.setMessage(lbl_msg_id.getText());    
           break;        
            case 16:
           Config.new_transport.setMessage(lbl_msg_id.getText());    
           break;  
           case 17:
           Config.view_transport.setMessage(lbl_msg_id.getText());    
           break;        
           case 18:
           Config.add_maintenance.setMessage(lbl_msg_id.getText());    
           break;        
           case 19:
           Config.view_add_maintenance.setMessage(lbl_msg_id.getText());    
           break;        
           case 20:
           Config.new_distribution.setMessage(lbl_msg_id.getText());    
           break;        
           case 21:
           Config.view_distribution_item.setMessage(lbl_msg_id.getText());    
           break;        
       }
       dispose();
    }
public void onloadResetAttendance(int module,String message_id,int index) {
        this.module = module;
        this.index = index;
        lbl_msg_id.setText(message_id);
        setData();
        
    }   
}
