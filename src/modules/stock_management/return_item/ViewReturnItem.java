package modules.stock_management.return_item;

import data_manager.BillType;
import data_manager.ReturnStock;
import data_manager.configuration.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

public class ViewReturnItem extends javax.swing.JDialog {
    
    String item_id = "";
    String supplier_id = "";
    String item_list_id = "";
    String message_id = "";
    String bill_type_id ="";
    DefaultTableModel tbl_stock_model = null;
    
    SimpleDateFormat simple_date_formate = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");

    public ViewReturnItem(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        tbl_stock_model=(DefaultTableModel) tbl_stock.getModel();
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel6 = new javax.swing.JPanel();
        btn_update = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        panal = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txt_name = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_suppliername = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_price = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_unit = new javax.swing.JTextField();
        txt_quantity = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        cb_Item_type = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jfmt_date = new javax.swing.JFormattedTextField();
        btn_add = new javax.swing.JButton();
        btn_s_reset = new javax.swing.JButton();
        lbl_receive_stock_id = new javax.swing.JLabel();
        lbl_item_id = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txt_challannno = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        txt_loading = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txt_other = new javax.swing.JTextField();
        txt_invoiceno = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_stock = new javax.swing.JTable();
        btn_message = new javax.swing.JButton();
        lbl_bill_type_id = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        btn_update.setText("Update");
        btn_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateActionPerformed(evt);
            }
        });
        btn_update.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_updateKeyPressed(evt);
            }
        });

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });
        btn_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_cancelKeyPressed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        panal.setBackground(new java.awt.Color(255, 255, 255));
        panal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Stock Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(51, 51, 51))); // NOI18N
        panal.setForeground(new java.awt.Color(255, 255, 255));

        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Producte Name");

        txt_name.setEditable(false);
        txt_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nameActionPerformed(evt);
            }
        });
        txt_name.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_nameKeyPressed(evt);
            }
        });

        jLabel8.setForeground(new java.awt.Color(51, 51, 51));
        jLabel8.setText("Supplier Name");

        txt_suppliername.setEditable(false);
        txt_suppliername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_suppliernameActionPerformed(evt);
            }
        });
        txt_suppliername.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_suppliernameKeyPressed(evt);
            }
        });

        jLabel10.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setText("Date");

        txt_price.setEditable(false);
        txt_price.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_priceActionPerformed(evt);
            }
        });

        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Unit");

        txt_unit.setEditable(false);
        txt_unit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_unitKeyPressed(evt);
            }
        });

        jLabel14.setText("Price");

        jLabel24.setText("Quantity");

        cb_Item_type.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-Select-", "Stock Item", "Sada flavour", "Manufacture Item" }));
        cb_Item_type.setEnabled(false);
        cb_Item_type.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_Item_typeActionPerformed(evt);
            }
        });

        jLabel7.setText("Item Type");

        jfmt_date.setEditable(false);
        try {
            jfmt_date.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        btn_add.setText("Update");
        btn_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addActionPerformed(evt);
            }
        });

        btn_s_reset.setText("Reset");
        btn_s_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_s_resetActionPerformed(evt);
            }
        });

        lbl_receive_stock_id.setForeground(new java.awt.Color(255, 255, 255));

        lbl_item_id.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout panalLayout = new javax.swing.GroupLayout(panal);
        panal.setLayout(panalLayout);
        panalLayout.setHorizontalGroup(
            panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_unit, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_price)
                            .addGroup(panalLayout.createSequentialGroup()
                                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(txt_suppliername)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cb_Item_type, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_name)
                            .addGroup(panalLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(panalLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(panalLayout.createSequentialGroup()
                                .addComponent(btn_s_reset)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lbl_receive_stock_id, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lbl_item_id, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel10)
                                .addComponent(jfmt_date, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panalLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_quantity)
                                    .addGroup(panalLayout.createSequentialGroup()
                                        .addComponent(jLabel24)
                                        .addGap(0, 0, Short.MAX_VALUE))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panalLayout.createSequentialGroup()
                                .addGap(78, 78, 78)
                                .addComponent(btn_add)))))
                .addContainerGap())
        );
        panalLayout.setVerticalGroup(
            panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panalLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cb_Item_type, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(8, 8, 8)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_suppliername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_price, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_unit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jfmt_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_receive_stock_id, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbl_item_id, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_add)
                        .addComponent(btn_s_reset)))
                .addContainerGap())
        );

        jLabel17.setForeground(new java.awt.Color(51, 51, 51));
        jLabel17.setText("Invoice No.");

        jLabel16.setForeground(new java.awt.Color(51, 51, 51));
        jLabel16.setText("Challan No.");

        txt_challannno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_challannnoKeyPressed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setText("Other Details");

        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Loading Charges");

        txt_loading.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_loadingCaretUpdate(evt);
            }
        });

        jLabel18.setForeground(new java.awt.Color(51, 51, 51));
        jLabel18.setText("Other Charges");

        txt_other.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_otherCaretUpdate(evt);
            }
        });
        txt_other.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_otherKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_invoiceno)
                    .addComponent(txt_loading)
                    .addComponent(txt_other)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel21)
                            .addComponent(jLabel5)
                            .addComponent(jLabel18))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addComponent(jLabel16)
                            .addComponent(txt_challannno, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_invoiceno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_challannno, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_loading, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_other, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(98, 98, 98))
        );

        jPanel5.setBackground(new java.awt.Color(230, 79, 6));

        jLabel20.setBackground(new java.awt.Color(255, 255, 255));
        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("View Return Item");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        tbl_stock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PRODUCT NAME", "DATE", "PRICE", "QUANTITY", "TOTAL", "", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_stock.getTableHeader().setReorderingAllowed(false);
        tbl_stock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_stockMouseClicked(evt);
            }
        });
        tbl_stock.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_stockKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tbl_stock);
        if (tbl_stock.getColumnModel().getColumnCount() > 0) {
            tbl_stock.getColumnModel().getColumn(0).setResizable(false);
            tbl_stock.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_stock.getColumnModel().getColumn(1).setResizable(false);
            tbl_stock.getColumnModel().getColumn(1).setPreferredWidth(150);
            tbl_stock.getColumnModel().getColumn(2).setResizable(false);
            tbl_stock.getColumnModel().getColumn(2).setPreferredWidth(150);
            tbl_stock.getColumnModel().getColumn(3).setResizable(false);
            tbl_stock.getColumnModel().getColumn(3).setPreferredWidth(150);
            tbl_stock.getColumnModel().getColumn(4).setResizable(false);
            tbl_stock.getColumnModel().getColumn(4).setPreferredWidth(100);
            tbl_stock.getColumnModel().getColumn(5).setMinWidth(0);
            tbl_stock.getColumnModel().getColumn(5).setPreferredWidth(0);
            tbl_stock.getColumnModel().getColumn(5).setMaxWidth(0);
            tbl_stock.getColumnModel().getColumn(6).setMinWidth(0);
            tbl_stock.getColumnModel().getColumn(6).setPreferredWidth(0);
            tbl_stock.getColumnModel().getColumn(6).setMaxWidth(0);
            tbl_stock.getColumnModel().getColumn(7).setMinWidth(0);
            tbl_stock.getColumnModel().getColumn(7).setPreferredWidth(0);
            tbl_stock.getColumnModel().getColumn(7).setMaxWidth(0);
        }

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(panal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btn_message.setText("Message");
        btn_message.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_messageActionPerformed(evt);
            }
        });

        lbl_bill_type_id.setForeground(new java.awt.Color(240, 240, 240));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_message)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_bill_type_id, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_update, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_cancel)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_cancel)
                            .addComponent(btn_update))
                        .addComponent(lbl_bill_type_id, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(btn_message))
                .addContainerGap())
        );

        getRootPane().setDefaultButton(btn_update);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void btn_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateActionPerformed
        try {
            BillType bt = new BillType();
            bt.setBill_type_id(lbl_bill_type_id.getText());
            bt.setBill_no(txt_invoiceno.getText());
            bt.setChalan_no(txt_challannno.getText());
            bt.setLoading_charges(txt_loading.getText());
            bt.setOther_charges(txt_other.getText());
            bt.setDate(jfmt_date.getText());
            bt.setMonth(sdf_month.format(simple_date_formate.parse(jfmt_date.getText())));
            bt.setYear(sdf_year.format(simple_date_formate.parse(jfmt_date.getText())));
            bt.setMessage_id(message_id);
            if(Config.bill_type_mgr.updBillType(bt)){
                Config.stock_management.onloadReset();
                JOptionPane.showMessageDialog(this, "Stock List created successfully.", "Creation successful", JOptionPane.NO_OPTION);
                dispose();
            }else{
                JOptionPane.showMessageDialog(this, "Error in Stock List creation", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btn_updateActionPerformed

    private void btn_updateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_updateKeyPressed

    }//GEN-LAST:event_btn_updateKeyPressed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void btn_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_cancelKeyPressed

    }//GEN-LAST:event_btn_cancelKeyPressed

    private void txt_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nameActionPerformed
        if(cb_Item_type.getSelectedIndex()!=0){
            Config.item_search.onloadReset(1, cb_Item_type.getSelectedIndex());
            Config.item_search.setVisible(true);
        }else{
            JOptionPane.showMessageDialog(this, "Select Item Type ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_nameActionPerformed

    private void txt_nameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nameKeyPressed

    }//GEN-LAST:event_txt_nameKeyPressed

    private void txt_suppliernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_suppliernameActionPerformed

    }//GEN-LAST:event_txt_suppliernameActionPerformed

    private void txt_suppliernameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_suppliernameKeyPressed

    }//GEN-LAST:event_txt_suppliernameKeyPressed

    private void txt_priceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_priceActionPerformed

    }//GEN-LAST:event_txt_priceActionPerformed

    private void txt_unitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_unitKeyPressed

    }//GEN-LAST:event_txt_unitKeyPressed

    private void cb_Item_typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_Item_typeActionPerformed
        txt_unit.setText("");
        if(cb_Item_type.getSelectedIndex()!=0){
            switch(cb_Item_type.getSelectedIndex()){
                case 1:

                break;
                case 2:
                break;
                case 3:
                break;
            }
        }
    }//GEN-LAST:event_cb_Item_typeActionPerformed

    private void btn_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addActionPerformed
        try {
            String str = checkValidity();
            if (str.equals("ok")) {
                ReturnStock rs = new ReturnStock();
                rs.setReturn_id(lbl_receive_stock_id.getText());
                rs.setItem_id(lbl_item_id.getText());
                rs.setQuantity(txt_quantity.getText());
                double quantity=0;
                quantity=Double.parseDouble(txt_quantity.getText())-Double.parseDouble(tbl_stock_model.getValueAt(tbl_stock.getSelectedRow(),3).toString());
                System.out.println(quantity);
                if (Config.return_stock_mgr.updReturnStock(rs,quantity)) {
                    tbl_stock.setValueAt(txt_quantity.getText(), tbl_stock.getSelectedRow(),3);
                    Config.stock_management.onloadReset();
                    JOptionPane.showMessageDialog(this, "ItemList created successfully.", "Creation successful", JOptionPane.NO_OPTION);
                } else {
                    JOptionPane.showMessageDialog(this, "Error in Itemlist creation.", "Error", JOptionPane.ERROR_MESSAGE);
                }

            }else{
                JOptionPane.showMessageDialog(this, "Check '"+str+"' field.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btn_addActionPerformed

    private void btn_s_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_s_resetActionPerformed
        //onloadReset_StockDetails();
    }//GEN-LAST:event_btn_s_resetActionPerformed

    private void txt_challannnoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_challannnoKeyPressed

    }//GEN-LAST:event_txt_challannnoKeyPressed

    private void txt_otherKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_otherKeyPressed

    }//GEN-LAST:event_txt_otherKeyPressed

    private void tbl_stockMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_stockMouseClicked
        setData();
    }//GEN-LAST:event_tbl_stockMouseClicked

    private void tbl_stockKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_stockKeyPressed
        setData();
    }//GEN-LAST:event_tbl_stockKeyPressed

    private void btn_messageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_messageActionPerformed
        if(message_id.equals("")){
            Config.new_message.onloadReset(5);
            Config.new_message.setVisible(true);
        }else{
            Config.view_message.onloadReset(5,message_id);
            Config.view_message.setVisible(true);
        }
    }//GEN-LAST:event_btn_messageActionPerformed

    private void txt_loadingCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_loadingCaretUpdate
    }//GEN-LAST:event_txt_loadingCaretUpdate

    private void txt_otherCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_otherCaretUpdate
        
    }//GEN-LAST:event_txt_otherCaretUpdate
         
        
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_add;
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_message;
    private javax.swing.JButton btn_s_reset;
    private javax.swing.JButton btn_update;
    private javax.swing.JComboBox cb_Item_type;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JFormattedTextField jfmt_date;
    private javax.swing.JLabel lbl_bill_type_id;
    private javax.swing.JLabel lbl_item_id;
    private javax.swing.JLabel lbl_receive_stock_id;
    private javax.swing.JPanel panal;
    private javax.swing.JTable tbl_stock;
    private javax.swing.JTextField txt_challannno;
    private javax.swing.JTextField txt_invoiceno;
    private javax.swing.JTextField txt_loading;
    private javax.swing.JTextField txt_name;
    private javax.swing.JTextField txt_other;
    private javax.swing.JTextField txt_price;
    private javax.swing.JTextField txt_quantity;
    private javax.swing.JTextField txt_suppliername;
    private javax.swing.JTextField txt_unit;
    // End of variables declaration//GEN-END:variables

     public void onloadReset(String bill_type_id) {        
        lbl_bill_type_id.setText(bill_type_id);
        tbl_stock_model.setRowCount(0);
        try {
           
            Config.sql ="select *,(select prise from item_list i where i.item_list_id=rs.item_list_id) as price from return_stock rs where bill_type_id='"+bill_type_id+"'";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                tbl_stock_model.addRow(new Object[]{
                    Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.rs.getString("item_id"))).getName(),
                    Config.rs.getString("_date"),
                    Config.rs.getString("price"),
                    Config.rs.getString("quantity"),
                    Double.parseDouble(Config.rs.getString("price"))*Double.parseDouble(Config.rs.getString("quantity")),
                    Config.rs.getString("item_id"),
                    Config.rs.getString("item_list_id"),
                    Config.rs.getString("return_id")
                    });
            }
            
            tbl_stock.getSelectionModel().setSelectionInterval(0,0);
            setData();
            
            Config.sql ="select * from bill_type where bill_type_id="+bill_type_id;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if(Config.rs.next()) {
              message_id=Config.rs.getString("message_id");
              txt_invoiceno.setText(Config.rs.getString("bill_no"));
              txt_challannno.setText(Config.rs.getString("chalan_no"));
              txt_loading.setText(Config.rs.getString("loading_charges"));
              txt_other.setText(Config.rs.getString("other_charges"));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
}

    public void setData() {
        lbl_item_id.setText(tbl_stock_model.getValueAt(tbl_stock.getSelectedRow(), 5).toString());
        lbl_receive_stock_id.setText(tbl_stock_model.getValueAt(tbl_stock.getSelectedRow(),7).toString());
                
        switch(Config.config_stock_item.get(Config.id_stock_item.indexOf(tbl_stock_model.getValueAt(tbl_stock.getSelectedRow(), 5).toString())).getIdentity()){
            case "ST":
                cb_Item_type.setSelectedIndex(1);
            break;    
            case "S":
                cb_Item_type.setSelectedIndex(2);
            break;
            case "M":
                cb_Item_type.setSelectedIndex(3);
            break;
        }
        txt_name.setText(Config.config_stock_item.get(Config.id_stock_item.indexOf(tbl_stock_model.getValueAt(tbl_stock.getSelectedRow(), 5).toString())).getName());
        try {
            Config.sql="select  * from item_list where item_list_id="+tbl_stock_model.getValueAt(tbl_stock.getSelectedRow(), 6).toString();
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if (Config.rs.next()) {
            txt_price.setText(Config.rs.getString("prise"));    
            txt_suppliername.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(Config.rs.getString("supplier_id"))).getProfile_id())).getName());
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        txt_unit.setText(Config.config_stock_item.get(Config.id_stock_item.indexOf(tbl_stock_model.getValueAt(tbl_stock.getSelectedRow(), 5).toString())).getUnit());
        jfmt_date.setText(tbl_stock_model.getValueAt(tbl_stock.getSelectedRow(), 1).toString());
        txt_quantity.setText(tbl_stock_model.getValueAt(tbl_stock.getSelectedRow(), 3).toString());
        
    }

    public String checkValidity() {
        if(txt_quantity.getText().equals("")){
            return "Quantity";
        }else{
            return "ok";
        }
    }

    public void setMessage(String message_id) {
        this.message_id=message_id;
    }

}