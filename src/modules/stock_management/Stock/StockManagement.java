package modules.stock_management.Stock;

import data_manager.configuration.Config;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

public class StockManagement extends javax.swing.JDialog {
    boolean stock_receive_from = true;
    boolean stock_receive_to = true;
    boolean stock_demage_from = true;
    boolean stock_demage_to = true;
    boolean stock_return_from = true;
    boolean stock_return_to = true;
    
    SimpleDateFormat simple_date_formate = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_day = new SimpleDateFormat("dd");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
    
    DefaultTableModel tbl_sm_stock_item_model =null;
    DefaultTableModel tbl_sm_soda_model =null;
    DefaultTableModel tbl_sm_manufacture_item_model =null;
    DefaultTableModel tbl_receive_item_model =null;
    DefaultTableModel tbl_demage_item_model =null;
    DefaultTableModel tbl_return_item_model =null;
    DefaultTableModel tbl_receive_report_model =null;
    DefaultTableModel tbl_other_model =null;
    
    public StockManagement(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        tbl_sm_stock_item_model = (DefaultTableModel) tbl_sm_stock_item.getModel();
        tbl_sm_soda_model = (DefaultTableModel) tbl_sm_soda.getModel();
        tbl_sm_manufacture_item_model = (DefaultTableModel) tbl_sm_manufacture_item.getModel();
        tbl_receive_item_model = (DefaultTableModel) tbl_stock_recevie.getModel();
        tbl_demage_item_model = (DefaultTableModel) tbl_demage_item.getModel();
        tbl_return_item_model=(DefaultTableModel) tbl_return_item.getModel();
        tbl_receive_report_model=(DefaultTableModel) tbl_receive_report.getModel();
        tbl_receive_report.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        tbl_return_report.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        tbl_total_report.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
        tbl_other_model=(DefaultTableModel) tbl_other.getModel();
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cb_Item_type = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        pnl_employee = new javax.swing.JPanel();
        btn_stock_mnt_cancel = new javax.swing.JButton();
        btn_stock_mnt_print = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txt_stock_mnt_search = new javax.swing.JTextField();
        btn_stock_mnt_search = new javax.swing.JButton();
        btn_stock_mnt_new = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        cb_sm_Item_type = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_sm_soda = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbl_sm_manufacture_item = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_sm_stock_item = new javax.swing.JTable();
        jPanel7 = new javax.swing.JPanel();
        pnl_salary_info = new javax.swing.JPanel();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        pnl_employee2 = new javax.swing.JPanel();
        btn_stock_recevie_cancel = new javax.swing.JButton();
        btn_stock_recevie_view = new javax.swing.JButton();
        btn_stock_recevie_print = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        txt_stock_recevie_search = new javax.swing.JTextField();
        btn_stock_recevie_search = new javax.swing.JButton();
        btn_stock_recevie_new = new javax.swing.JButton();
        jSeparator10 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        stock_receive_check_to = new javax.swing.JCheckBox();
        stock_receive_check_from = new javax.swing.JCheckBox();
        jdc_date_rec_from = new com.toedter.calendar.JDateChooser();
        jdc_date_rec_to = new com.toedter.calendar.JDateChooser();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_stock_recevie = new javax.swing.JTable();
        jPanel16 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        txt_stock_recevie_message = new javax.swing.JTextPane();
        jPanel6 = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        txt_stock_recevie_invoiceno = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txt_stock_recevie_challanno = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        txt_stock_recevie_loadingcharges = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        txt_stock_recevie_othercharges = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        pnl_employee3 = new javax.swing.JPanel();
        btn_demage_cancel = new javax.swing.JButton();
        btn_demage_view = new javax.swing.JButton();
        btn_demage_print = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        txt_demage_search = new javax.swing.JTextField();
        btn_demage_search = new javax.swing.JButton();
        btn_demage_new = new javax.swing.JButton();
        jSeparator11 = new javax.swing.JSeparator();
        jLabel44 = new javax.swing.JLabel();
        stock_demage_check_to = new javax.swing.JCheckBox();
        stock_demage_check_from = new javax.swing.JCheckBox();
        jdc_date_dem_from = new com.toedter.calendar.JDateChooser();
        jdc_date_dem_to = new com.toedter.calendar.JDateChooser();
        jPanel18 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        txt_demage_message = new javax.swing.JTextPane();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbl_demage_item = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        pnl_employee4 = new javax.swing.JPanel();
        btn_return_cancel = new javax.swing.JButton();
        btn_return_view = new javax.swing.JButton();
        btn_return_print = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel55 = new javax.swing.JLabel();
        txt_stock_return_search = new javax.swing.JTextField();
        btn_return_search = new javax.swing.JButton();
        btn_return_new = new javax.swing.JButton();
        jSeparator12 = new javax.swing.JSeparator();
        jLabel56 = new javax.swing.JLabel();
        stock_return_check_to = new javax.swing.JCheckBox();
        stock_return_check_from = new javax.swing.JCheckBox();
        jdc_date_return_from = new com.toedter.calendar.JDateChooser();
        jdc_date_return_to = new com.toedter.calendar.JDateChooser();
        jPanel19 = new javax.swing.JPanel();
        jScrollPane16 = new javax.swing.JScrollPane();
        txt_stock_return_message = new javax.swing.JTextPane();
        jPanel20 = new javax.swing.JPanel();
        jLabel57 = new javax.swing.JLabel();
        txt_stock_return_invoiceno = new javax.swing.JTextField();
        jLabel58 = new javax.swing.JLabel();
        txt_stock_return_challanno = new javax.swing.JTextField();
        jLabel59 = new javax.swing.JLabel();
        txt_stock_return_loadingcharges = new javax.swing.JTextField();
        jLabel60 = new javax.swing.JLabel();
        txt_stock_return_othercharges = new javax.swing.JTextField();
        jScrollPane6 = new javax.swing.JScrollPane();
        tbl_return_item = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();
        jPanel22 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        pnl_adv_report = new javax.swing.JPanel();
        pnl_adv_report1 = new javax.swing.JPanel();
        jPanel24 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        btn_receive_report_search = new javax.swing.JButton();
        jmc_receive_report = new com.toedter.calendar.JMonthChooser();
        jLabel26 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jdc_receive_report = new com.toedter.calendar.JDateChooser();
        jyc_receive_report = new com.toedter.calendar.JYearChooser();
        jLabel7 = new javax.swing.JLabel();
        check_receive_report = new javax.swing.JCheckBox();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jScrollPane13 = new javax.swing.JScrollPane();
        tbl_receive_report = new javax.swing.JTable();
        btn_att_report_reset2 = new javax.swing.JButton();
        btn_att_report_reset3 = new javax.swing.JButton();
        jLabel39 = new javax.swing.JLabel();
        txt_receive_date = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        txt_receive_amount = new javax.swing.JTextField();
        btn_att_report_view1 = new javax.swing.JButton();
        btn_att_report_cancel1 = new javax.swing.JButton();
        pnl_attendence_report = new javax.swing.JPanel();
        pnl_adv_report2 = new javax.swing.JPanel();
        pnl_adv_report3 = new javax.swing.JPanel();
        jPanel25 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        btn_return_report_search1 = new javax.swing.JButton();
        jmc_return_report = new com.toedter.calendar.JMonthChooser();
        jLabel27 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel10 = new javax.swing.JLabel();
        jdc_return_report = new com.toedter.calendar.JDateChooser();
        jyc_return_report = new com.toedter.calendar.JYearChooser();
        jLabel11 = new javax.swing.JLabel();
        check_return_report = new javax.swing.JCheckBox();
        btn_return_report_print = new javax.swing.JButton();
        btn_return_report_cancel = new javax.swing.JButton();
        jScrollPane14 = new javax.swing.JScrollPane();
        tbl_return_report = new javax.swing.JTable();
        btn_att_report_reset4 = new javax.swing.JButton();
        btn_att_report_reset5 = new javax.swing.JButton();
        jLabel40 = new javax.swing.JLabel();
        txt_return_date = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        txt_return_amount = new javax.swing.JTextField();
        btn_att_report_view2 = new javax.swing.JButton();
        btn_att_report_cancel2 = new javax.swing.JButton();
        pnl_salary_report = new javax.swing.JPanel();
        pnl_adv_report4 = new javax.swing.JPanel();
        pnl_adv_report5 = new javax.swing.JPanel();
        jPanel26 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        btn_total_report_search = new javax.swing.JButton();
        jmc_total_report = new com.toedter.calendar.JMonthChooser();
        jLabel28 = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        jLabel14 = new javax.swing.JLabel();
        jdc_total_report = new com.toedter.calendar.JDateChooser();
        jyc_total_report = new com.toedter.calendar.JYearChooser();
        jLabel15 = new javax.swing.JLabel();
        check_total_report = new javax.swing.JCheckBox();
        btn_total_report_print = new javax.swing.JButton();
        btn_total_report_cancel = new javax.swing.JButton();
        jScrollPane15 = new javax.swing.JScrollPane();
        tbl_total_report = new javax.swing.JTable();
        btn_att_report_reset6 = new javax.swing.JButton();
        btn_att_report_reset7 = new javax.swing.JButton();
        jLabel41 = new javax.swing.JLabel();
        txt_total_report_date = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        txt_total_report_amount = new javax.swing.JTextField();
        btn_att_report_view3 = new javax.swing.JButton();
        btn_att_report_cancel3 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        pnl_adv_report6 = new javax.swing.JPanel();
        pnl_adv_report7 = new javax.swing.JPanel();
        jPanel27 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        btn_other_search = new javax.swing.JButton();
        jmc_other = new com.toedter.calendar.JMonthChooser();
        jLabel31 = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel16 = new javax.swing.JLabel();
        jdc_other = new com.toedter.calendar.JDateChooser();
        jyc_other = new com.toedter.calendar.JYearChooser();
        jLabel17 = new javax.swing.JLabel();
        check_other = new javax.swing.JCheckBox();
        btn_other_print1 = new javax.swing.JButton();
        btn_other_cancel = new javax.swing.JButton();
        jScrollPane17 = new javax.swing.JScrollPane();
        tbl_other = new javax.swing.JTable();
        btn_att_report_reset8 = new javax.swing.JButton();
        btn_att_report_reset9 = new javax.swing.JButton();
        jLabel45 = new javax.swing.JLabel();
        txt_total_report_date1 = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        txt_total_report_amount1 = new javax.swing.JTextField();
        btn_att_report_view4 = new javax.swing.JButton();
        btn_att_report_cancel4 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        cb_Item_type.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-Select-", "Stock Item", "Sada flavour", "Manufacture Item" }));
        cb_Item_type.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_Item_typeActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Item Type");

        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane2.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        pnl_employee.setBackground(new java.awt.Color(255, 255, 255));
        pnl_employee.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pnl_employeeKeyPressed(evt);
            }
        });

        btn_stock_mnt_cancel.setText("Cancel");
        btn_stock_mnt_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stock_mnt_cancelActionPerformed(evt);
            }
        });
        btn_stock_mnt_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_stock_mnt_cancelKeyPressed(evt);
            }
        });

        btn_stock_mnt_print.setText("Print");
        btn_stock_mnt_print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_stock_mnt_printKeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Search By :");

        txt_stock_mnt_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_stock_mnt_searchCaretUpdate(evt);
            }
        });
        txt_stock_mnt_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_stock_mnt_searchKeyPressed(evt);
            }
        });

        btn_stock_mnt_search.setText("Search");
        btn_stock_mnt_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stock_mnt_searchActionPerformed(evt);
            }
        });
        btn_stock_mnt_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_stock_mnt_searchKeyPressed(evt);
            }
        });

        btn_stock_mnt_new.setText("New Item");
        btn_stock_mnt_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stock_mnt_newActionPerformed(evt);
            }
        });
        btn_stock_mnt_new.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_stock_mnt_newKeyPressed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        cb_sm_Item_type.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-Select-", "Stock Item", "Sada flavour", "Manufacture Item" }));
        cb_sm_Item_type.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_sm_Item_typeActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Item Type :");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cb_sm_Item_type, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_stock_mnt_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_stock_mnt_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_stock_mnt_new)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_stock_mnt_new, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
                    .addComponent(btn_stock_mnt_search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cb_sm_Item_type, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2))
                    .addComponent(txt_stock_mnt_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator3))
                .addContainerGap())
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cb_sm_Item_type, jLabel2, jLabel6, txt_stock_mnt_search});

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Soda Flavour", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tbl_sm_soda.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "PRODUCT NAME", "UNIT", "QUANTITY"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_sm_soda.getTableHeader().setReorderingAllowed(false);
        tbl_sm_soda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_sm_sodaKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_sm_soda);

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Manufacture Item", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tbl_sm_manufacture_item.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "PRODUCT NAME", "UNIT", "QUANTITY"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_sm_manufacture_item.getTableHeader().setReorderingAllowed(false);
        tbl_sm_manufacture_item.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_sm_manufacture_itemKeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(tbl_sm_manufacture_item);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Stock Item", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tbl_sm_stock_item.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "PRODUCT NAME", "UNIT", "QUANTITY"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_sm_stock_item.getTableHeader().setReorderingAllowed(false);
        tbl_sm_stock_item.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_sm_stock_itemKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tbl_sm_stock_item);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 459, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout pnl_employeeLayout = new javax.swing.GroupLayout(pnl_employee);
        pnl_employee.setLayout(pnl_employeeLayout);
        pnl_employeeLayout.setHorizontalGroup(
            pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnl_employeeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addComponent(btn_stock_mnt_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_stock_mnt_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        pnl_employeeLayout.setVerticalGroup(
            pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_employeeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_employeeLayout.createSequentialGroup()
                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(11, 11, 11)
                .addGroup(pnl_employeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_stock_mnt_cancel)
                    .addComponent(btn_stock_mnt_print))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Stock management", pnl_employee);

        pnl_salary_info.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        pnl_employee2.setBackground(new java.awt.Color(255, 255, 255));
        pnl_employee2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pnl_employee2KeyPressed(evt);
            }
        });

        btn_stock_recevie_cancel.setText("Cancel");
        btn_stock_recevie_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stock_recevie_cancelActionPerformed(evt);
            }
        });
        btn_stock_recevie_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_stock_recevie_cancelKeyPressed(evt);
            }
        });

        btn_stock_recevie_view.setText("View");
        btn_stock_recevie_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stock_recevie_viewActionPerformed(evt);
            }
        });
        btn_stock_recevie_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_stock_recevie_viewKeyPressed(evt);
            }
        });

        btn_stock_recevie_print.setText("Print");
        btn_stock_recevie_print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_stock_recevie_printKeyPressed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Search By :");

        txt_stock_recevie_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_stock_recevie_searchCaretUpdate(evt);
            }
        });
        txt_stock_recevie_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_stock_recevie_searchActionPerformed(evt);
            }
        });
        txt_stock_recevie_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_stock_recevie_searchKeyPressed(evt);
            }
        });

        btn_stock_recevie_search.setText("Search");
        btn_stock_recevie_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stock_recevie_searchActionPerformed(evt);
            }
        });
        btn_stock_recevie_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_stock_recevie_searchKeyPressed(evt);
            }
        });

        btn_stock_recevie_new.setText("New Item");
        btn_stock_recevie_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_stock_recevie_newActionPerformed(evt);
            }
        });

        jSeparator10.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Date :");

        stock_receive_check_to.setText(" To");
        stock_receive_check_to.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stock_receive_check_toActionPerformed(evt);
            }
        });

        stock_receive_check_from.setText("From");
        stock_receive_check_from.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stock_receive_check_fromActionPerformed(evt);
            }
        });

        jdc_date_rec_from.setDateFormatString("dd/MM/yyyy");

        jdc_date_rec_to.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_stock_recevie_search, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(stock_receive_check_from)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdc_date_rec_from, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(stock_receive_check_to)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdc_date_rec_to, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_stock_recevie_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_stock_recevie_new)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_stock_recevie_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_stock_recevie_search)
                        .addComponent(btn_stock_recevie_new)
                        .addComponent(jLabel9)
                        .addComponent(stock_receive_check_from))
                    .addComponent(jSeparator10, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(stock_receive_check_to)
                            .addComponent(jdc_date_rec_to, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jdc_date_rec_from, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        jPanel10Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_stock_recevie_new, btn_stock_recevie_search, jLabel8, jLabel9, jdc_date_rec_from, jdc_date_rec_to, stock_receive_check_from, stock_receive_check_to, txt_stock_recevie_search});

        tbl_stock_recevie.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "PRODUCT NAME", "DATE", "UNIT", "QUANTITY", "PRICE", "TOTAL", "SUPPLIER NAME", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_stock_recevie.getTableHeader().setReorderingAllowed(false);
        tbl_stock_recevie.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_stock_recevieMouseClicked(evt);
            }
        });
        tbl_stock_recevie.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_stock_recevieKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbl_stock_recevieKeyReleased(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_stock_recevie);
        if (tbl_stock_recevie.getColumnModel().getColumnCount() > 0) {
            tbl_stock_recevie.getColumnModel().getColumn(0).setResizable(false);
            tbl_stock_recevie.getColumnModel().getColumn(0).setPreferredWidth(100);
            tbl_stock_recevie.getColumnModel().getColumn(1).setResizable(false);
            tbl_stock_recevie.getColumnModel().getColumn(1).setPreferredWidth(200);
            tbl_stock_recevie.getColumnModel().getColumn(2).setResizable(false);
            tbl_stock_recevie.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_stock_recevie.getColumnModel().getColumn(3).setResizable(false);
            tbl_stock_recevie.getColumnModel().getColumn(3).setPreferredWidth(100);
            tbl_stock_recevie.getColumnModel().getColumn(4).setResizable(false);
            tbl_stock_recevie.getColumnModel().getColumn(4).setPreferredWidth(100);
            tbl_stock_recevie.getColumnModel().getColumn(5).setResizable(false);
            tbl_stock_recevie.getColumnModel().getColumn(5).setPreferredWidth(100);
            tbl_stock_recevie.getColumnModel().getColumn(6).setResizable(false);
            tbl_stock_recevie.getColumnModel().getColumn(6).setPreferredWidth(100);
            tbl_stock_recevie.getColumnModel().getColumn(7).setResizable(false);
            tbl_stock_recevie.getColumnModel().getColumn(7).setPreferredWidth(200);
            tbl_stock_recevie.getColumnModel().getColumn(8).setMinWidth(0);
            tbl_stock_recevie.getColumnModel().getColumn(8).setPreferredWidth(0);
            tbl_stock_recevie.getColumnModel().getColumn(8).setMaxWidth(0);
            tbl_stock_recevie.getColumnModel().getColumn(8).setHeaderValue("");
        }

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_stock_recevie_message.setEditable(false);
        jScrollPane7.setViewportView(txt_stock_recevie_message);

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7)
                .addContainerGap())
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Other Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel30.setText("Invoice no. :");

        jLabel12.setText("Challan no. :");

        jLabel42.setText("Loading Charges :");

        jLabel43.setText("Other Charges  :");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel42)
                            .addComponent(jLabel30)
                            .addComponent(jLabel12))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_stock_recevie_invoiceno)
                            .addComponent(txt_stock_recevie_challanno)
                            .addComponent(txt_stock_recevie_loadingcharges)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel43)
                        .addGap(18, 18, 18)
                        .addComponent(txt_stock_recevie_othercharges)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(txt_stock_recevie_invoiceno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txt_stock_recevie_challanno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel42)
                    .addComponent(txt_stock_recevie_loadingcharges, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_stock_recevie_othercharges, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel43))
                .addContainerGap())
        );

        jButton1.setText("Delete");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnl_employee2Layout = new javax.swing.GroupLayout(pnl_employee2);
        pnl_employee2.setLayout(pnl_employee2Layout);
        pnl_employee2Layout.setHorizontalGroup(
            pnl_employee2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnl_employee2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_employee2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(pnl_employee2Layout.createSequentialGroup()
                        .addComponent(btn_stock_recevie_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_employee2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_employee2Layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_stock_recevie_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_stock_recevie_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnl_employee2Layout.setVerticalGroup(
            pnl_employee2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_employee2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_employee2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_employee2Layout.createSequentialGroup()
                        .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE))
                .addGap(11, 11, 11)
                .addGroup(pnl_employee2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_stock_recevie_cancel)
                    .addComponent(btn_stock_recevie_view)
                    .addComponent(btn_stock_recevie_print)
                    .addComponent(jButton1))
                .addContainerGap())
        );

        jTabbedPane3.addTab("Receive Item", pnl_employee2);

        pnl_employee3.setBackground(new java.awt.Color(255, 255, 255));
        pnl_employee3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pnl_employee3KeyPressed(evt);
            }
        });

        btn_demage_cancel.setText("Cancel");
        btn_demage_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_demage_cancelKeyPressed(evt);
            }
        });

        btn_demage_view.setText("View");
        btn_demage_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_demage_viewActionPerformed(evt);
            }
        });
        btn_demage_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_demage_viewKeyPressed(evt);
            }
        });

        btn_demage_print.setText("Print");
        btn_demage_print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_demage_printKeyPressed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setText("Search By :");

        txt_demage_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_demage_searchCaretUpdate(evt);
            }
        });
        txt_demage_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_demage_searchActionPerformed(evt);
            }
        });
        txt_demage_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_demage_searchKeyPressed(evt);
            }
        });

        btn_demage_search.setText("Search");
        btn_demage_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_demage_searchActionPerformed(evt);
            }
        });
        btn_demage_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_demage_searchKeyPressed(evt);
            }
        });

        btn_demage_new.setText("Demage ");
        btn_demage_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_demage_newActionPerformed(evt);
            }
        });
        btn_demage_new.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_demage_newKeyPressed(evt);
            }
        });

        jSeparator11.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel44.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel44.setText("Date :");

        stock_demage_check_to.setText(" To");
        stock_demage_check_to.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stock_demage_check_toActionPerformed(evt);
            }
        });

        stock_demage_check_from.setText("From");
        stock_demage_check_from.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stock_demage_check_fromActionPerformed(evt);
            }
        });

        jdc_date_dem_from.setDateFormatString("dd/MM/yyyy");

        jdc_date_dem_to.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_demage_search, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel44)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(stock_demage_check_from)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdc_date_dem_from, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(stock_demage_check_to)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdc_date_dem_to, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btn_demage_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator11, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_demage_new, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_demage_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_demage_search)
                            .addComponent(btn_demage_new)
                            .addComponent(jLabel44)
                            .addComponent(stock_demage_check_from))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jSeparator11)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(stock_demage_check_to, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jdc_date_dem_to, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jdc_date_dem_from, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        jPanel11Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_demage_new, btn_demage_search, jLabel13, jLabel44, jdc_date_dem_from, jdc_date_dem_to, stock_demage_check_from, stock_demage_check_to, txt_demage_search});

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));
        jPanel18.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_demage_message.setEditable(false);
        jScrollPane10.setViewportView(txt_demage_message);

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane10)
                .addContainerGap())
        );

        tbl_demage_item.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "PRODUCT NAME", "DATE", "UNIT", "QUANTITY", "PRICE", "TOTAL", "SUPPLIER NAME", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_demage_item.getTableHeader().setReorderingAllowed(false);
        tbl_demage_item.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tbl_demage_itemFocusGained(evt);
            }
        });
        tbl_demage_item.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_demage_itemMouseClicked(evt);
            }
        });
        tbl_demage_item.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_demage_itemKeyPressed(evt);
            }
        });
        jScrollPane5.setViewportView(tbl_demage_item);
        if (tbl_demage_item.getColumnModel().getColumnCount() > 0) {
            tbl_demage_item.getColumnModel().getColumn(0).setResizable(false);
            tbl_demage_item.getColumnModel().getColumn(0).setPreferredWidth(100);
            tbl_demage_item.getColumnModel().getColumn(1).setResizable(false);
            tbl_demage_item.getColumnModel().getColumn(1).setPreferredWidth(200);
            tbl_demage_item.getColumnModel().getColumn(2).setResizable(false);
            tbl_demage_item.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_demage_item.getColumnModel().getColumn(3).setResizable(false);
            tbl_demage_item.getColumnModel().getColumn(3).setPreferredWidth(100);
            tbl_demage_item.getColumnModel().getColumn(4).setResizable(false);
            tbl_demage_item.getColumnModel().getColumn(4).setPreferredWidth(100);
            tbl_demage_item.getColumnModel().getColumn(5).setResizable(false);
            tbl_demage_item.getColumnModel().getColumn(5).setPreferredWidth(100);
            tbl_demage_item.getColumnModel().getColumn(6).setResizable(false);
            tbl_demage_item.getColumnModel().getColumn(6).setPreferredWidth(100);
            tbl_demage_item.getColumnModel().getColumn(7).setResizable(false);
            tbl_demage_item.getColumnModel().getColumn(7).setPreferredWidth(200);
            tbl_demage_item.getColumnModel().getColumn(8).setMinWidth(0);
            tbl_demage_item.getColumnModel().getColumn(8).setPreferredWidth(0);
            tbl_demage_item.getColumnModel().getColumn(8).setMaxWidth(0);
        }

        jButton2.setText("Delete");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnl_employee3Layout = new javax.swing.GroupLayout(pnl_employee3);
        pnl_employee3.setLayout(pnl_employee3Layout);
        pnl_employee3Layout.setHorizontalGroup(
            pnl_employee3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnl_employee3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_employee3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 743, Short.MAX_VALUE)
                    .addGroup(pnl_employee3Layout.createSequentialGroup()
                        .addComponent(btn_demage_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_employee3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_employee3Layout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_demage_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_demage_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        pnl_employee3Layout.setVerticalGroup(
            pnl_employee3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_employee3Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(pnl_employee3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
                    .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(11, 11, 11)
                .addGroup(pnl_employee3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_demage_cancel)
                    .addComponent(btn_demage_view)
                    .addComponent(btn_demage_print)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        jTabbedPane3.addTab("Demage Item", pnl_employee3);

        pnl_employee4.setBackground(new java.awt.Color(255, 255, 255));
        pnl_employee4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pnl_employee4KeyPressed(evt);
            }
        });

        btn_return_cancel.setText("Cancel");
        btn_return_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_return_cancelKeyPressed(evt);
            }
        });

        btn_return_view.setText("View");
        btn_return_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_return_viewActionPerformed(evt);
            }
        });
        btn_return_view.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_return_viewKeyPressed(evt);
            }
        });

        btn_return_print.setText("Print");
        btn_return_print.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_return_printKeyPressed(evt);
            }
        });

        jLabel55.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel55.setText("Search By :");

        txt_stock_return_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_stock_return_searchCaretUpdate(evt);
            }
        });
        txt_stock_return_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_stock_return_searchKeyPressed(evt);
            }
        });

        btn_return_search.setText("Search");
        btn_return_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_return_searchActionPerformed(evt);
            }
        });
        btn_return_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_return_searchKeyPressed(evt);
            }
        });

        btn_return_new.setText("New Item");
        btn_return_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_return_newActionPerformed(evt);
            }
        });
        btn_return_new.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_return_newKeyPressed(evt);
            }
        });

        jSeparator12.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel56.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel56.setText("Date :");

        stock_return_check_to.setText(" To");
        stock_return_check_to.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stock_return_check_toActionPerformed(evt);
            }
        });

        stock_return_check_from.setText("From");
        stock_return_check_from.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stock_return_check_fromActionPerformed(evt);
            }
        });

        jdc_date_return_from.setDateFormatString("dd/MM/yyyy");

        jdc_date_return_to.setDateFormatString("dd/MM/yyyy");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel55)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_stock_return_search, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel56)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(stock_return_check_from)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdc_date_return_from, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(stock_return_check_to)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdc_date_return_to, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_return_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator12, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_return_new)
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator12, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel55, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jdc_date_return_to, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(btn_return_search)
                                    .addComponent(btn_return_new)
                                    .addComponent(jLabel56)
                                    .addComponent(stock_return_check_from))
                                .addComponent(jdc_date_return_from, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(stock_return_check_to))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel13Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(txt_stock_return_search, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel13Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_return_new, btn_return_search, jLabel56, jdc_date_return_from, jdc_date_return_to, stock_return_check_from, stock_return_check_to, txt_stock_return_search});

        jPanel19.setBackground(new java.awt.Color(255, 255, 255));
        jPanel19.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Message", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_stock_return_message.setEditable(false);
        jScrollPane16.setViewportView(txt_stock_return_message);

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane16, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane16, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel20.setBackground(new java.awt.Color(255, 255, 255));
        jPanel20.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Other Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel57.setText("Invoice no. :");

        jLabel58.setText("Challan no. :");

        jLabel59.setText("Loading Charges :");

        jLabel60.setText("Other Charges  :");

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel20Layout.createSequentialGroup()
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel59)
                            .addComponent(jLabel57)
                            .addComponent(jLabel58))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_stock_return_invoiceno)
                            .addComponent(txt_stock_return_challanno)
                            .addComponent(txt_stock_return_loadingcharges)))
                    .addGroup(jPanel20Layout.createSequentialGroup()
                        .addComponent(jLabel60)
                        .addGap(18, 18, 18)
                        .addComponent(txt_stock_return_othercharges)))
                .addContainerGap())
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel20Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel57)
                    .addComponent(txt_stock_return_invoiceno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel58)
                    .addComponent(txt_stock_return_challanno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel59)
                    .addComponent(txt_stock_return_loadingcharges, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_stock_return_othercharges, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel60))
                .addContainerGap())
        );

        tbl_return_item.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "PRODUCT NAME", "DATE", "UNIT", "QUANTITY", "PRICE", "TOTAL", "SUPPLIER NAME", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_return_item.getTableHeader().setReorderingAllowed(false);
        tbl_return_item.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_return_itemMouseClicked(evt);
            }
        });
        tbl_return_item.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_return_itemKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbl_return_itemKeyReleased(evt);
            }
        });
        jScrollPane6.setViewportView(tbl_return_item);
        if (tbl_return_item.getColumnModel().getColumnCount() > 0) {
            tbl_return_item.getColumnModel().getColumn(0).setResizable(false);
            tbl_return_item.getColumnModel().getColumn(0).setPreferredWidth(100);
            tbl_return_item.getColumnModel().getColumn(1).setResizable(false);
            tbl_return_item.getColumnModel().getColumn(1).setPreferredWidth(200);
            tbl_return_item.getColumnModel().getColumn(2).setResizable(false);
            tbl_return_item.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_return_item.getColumnModel().getColumn(3).setResizable(false);
            tbl_return_item.getColumnModel().getColumn(3).setPreferredWidth(100);
            tbl_return_item.getColumnModel().getColumn(4).setResizable(false);
            tbl_return_item.getColumnModel().getColumn(4).setPreferredWidth(100);
            tbl_return_item.getColumnModel().getColumn(5).setResizable(false);
            tbl_return_item.getColumnModel().getColumn(5).setPreferredWidth(100);
            tbl_return_item.getColumnModel().getColumn(6).setResizable(false);
            tbl_return_item.getColumnModel().getColumn(6).setPreferredWidth(100);
            tbl_return_item.getColumnModel().getColumn(7).setResizable(false);
            tbl_return_item.getColumnModel().getColumn(7).setPreferredWidth(200);
            tbl_return_item.getColumnModel().getColumn(8).setMinWidth(0);
            tbl_return_item.getColumnModel().getColumn(8).setPreferredWidth(0);
            tbl_return_item.getColumnModel().getColumn(8).setMaxWidth(0);
            tbl_return_item.getColumnModel().getColumn(8).setHeaderValue("");
        }

        jButton3.setText("Delete");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnl_employee4Layout = new javax.swing.GroupLayout(pnl_employee4);
        pnl_employee4.setLayout(pnl_employee4Layout);
        pnl_employee4Layout.setHorizontalGroup(
            pnl_employee4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnl_employee4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_employee4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_employee4Layout.createSequentialGroup()
                        .addComponent(jScrollPane6)
                        .addGap(10, 10, 10))
                    .addGroup(pnl_employee4Layout.createSequentialGroup()
                        .addComponent(btn_return_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(pnl_employee4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_employee4Layout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_return_view, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_return_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl_employee4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnl_employee4Layout.setVerticalGroup(
            pnl_employee4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_employee4Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnl_employee4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_employee4Layout.createSequentialGroup()
                        .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane6))
                .addGap(11, 11, 11)
                .addGroup(pnl_employee4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_return_cancel)
                    .addComponent(btn_return_view)
                    .addComponent(btn_return_print)
                    .addComponent(jButton3))
                .addContainerGap())
        );

        jTabbedPane3.addTab("Return Item", pnl_employee4);

        javax.swing.GroupLayout pnl_salary_infoLayout = new javax.swing.GroupLayout(pnl_salary_info);
        pnl_salary_info.setLayout(pnl_salary_infoLayout);
        pnl_salary_infoLayout.setHorizontalGroup(
            pnl_salary_infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane3)
        );
        pnl_salary_infoLayout.setVerticalGroup(
            pnl_salary_infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane3)
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnl_salary_info, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnl_salary_info, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("Stock", jPanel7);

        jPanel22.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        pnl_adv_report.setBackground(new java.awt.Color(255, 255, 255));

        pnl_adv_report1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel23.setText("Search By :");

        btn_receive_report_search.setText("Search");
        btn_receive_report_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_receive_report_searchActionPerformed(evt);
            }
        });
        btn_receive_report_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_receive_report_searchKeyPressed(evt);
            }
        });

        jmc_receive_report.setVerifyInputWhenFocusTarget(false);
        jmc_receive_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_receive_reportKeyPressed(evt);
            }
        });

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel26.setText("Month :");

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Date :");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Year :");

        jButton4.setText("Print");

        jButton5.setText("Cancel");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(check_receive_report)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_receive_report, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jmc_receive_report, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(jyc_receive_report, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_receive_report_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 240, Short.MAX_VALUE)
                .addComponent(jButton5)
                .addContainerGap())
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel24Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator5)
                    .addComponent(btn_receive_report_search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(check_receive_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jdc_receive_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton4)
                        .addComponent(jButton5))
                    .addComponent(jyc_receive_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jmc_receive_report, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jScrollPane13.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tbl_receive_report.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbl_receive_report.getTableHeader().setReorderingAllowed(false);
        tbl_receive_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_receive_reportKeyPressed(evt);
            }
        });
        jScrollPane13.setViewportView(tbl_receive_report);

        btn_att_report_reset2.setText("Reset");
        btn_att_report_reset2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_reset2KeyPressed(evt);
            }
        });

        btn_att_report_reset3.setText("Print");
        btn_att_report_reset3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_reset3KeyPressed(evt);
            }
        });

        jLabel39.setText("Date :");

        txt_receive_date.setEditable(false);

        jLabel32.setText("Total Amount :");

        txt_receive_amount.setEditable(false);

        btn_att_report_view1.setText("View");
        btn_att_report_view1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_view1KeyPressed(evt);
            }
        });

        btn_att_report_cancel1.setText("Cancel");
        btn_att_report_cancel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_cancel1KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout pnl_adv_report1Layout = new javax.swing.GroupLayout(pnl_adv_report1);
        pnl_adv_report1.setLayout(pnl_adv_report1Layout);
        pnl_adv_report1Layout.setHorizontalGroup(
            pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_adv_report1Layout.createSequentialGroup()
                        .addComponent(btn_att_report_reset2, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_att_report_reset3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel39)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_receive_date, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel32)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_receive_amount, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_att_report_view1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_att_report_cancel1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane13))
                .addGap(11, 11, 11))
        );
        pnl_adv_report1Layout.setVerticalGroup(
            pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report1Layout.createSequentialGroup()
                .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 477, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(pnl_adv_report1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_att_report_cancel1)
                    .addComponent(btn_att_report_view1)
                    .addComponent(txt_receive_amount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32)
                    .addComponent(txt_receive_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39)
                    .addComponent(btn_att_report_reset2)
                    .addComponent(btn_att_report_reset3)))
        );

        javax.swing.GroupLayout pnl_adv_reportLayout = new javax.swing.GroupLayout(pnl_adv_report);
        pnl_adv_report.setLayout(pnl_adv_reportLayout);
        pnl_adv_reportLayout.setHorizontalGroup(
            pnl_adv_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnl_adv_report1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnl_adv_reportLayout.setVerticalGroup(
            pnl_adv_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_reportLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(pnl_adv_report1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Receive Report Day wise", pnl_adv_report);

        pnl_attendence_report.setBackground(new java.awt.Color(255, 255, 255));

        pnl_adv_report2.setBackground(new java.awt.Color(255, 255, 255));

        pnl_adv_report3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel24.setText("Search By :");

        btn_return_report_search1.setText("Search");
        btn_return_report_search1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_return_report_search1ActionPerformed(evt);
            }
        });
        btn_return_report_search1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_return_report_search1KeyPressed(evt);
            }
        });

        jmc_return_report.setVerifyInputWhenFocusTarget(false);
        jmc_return_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_return_reportKeyPressed(evt);
            }
        });

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel27.setText("Month :");

        jSeparator6.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel10.setText("Date :");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("Year :");

        btn_return_report_print.setText("Print");

        btn_return_report_cancel.setText("Cancel");
        btn_return_report_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_return_report_cancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(check_return_report)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_return_report, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jmc_return_report, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(jyc_return_report, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_return_report_search1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_return_report_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 240, Short.MAX_VALUE)
                .addComponent(btn_return_report_cancel)
                .addContainerGap())
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel25Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator6)
                    .addComponent(btn_return_report_search1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(check_return_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jdc_return_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_return_report_print)
                        .addComponent(btn_return_report_cancel))
                    .addComponent(jyc_return_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jmc_return_report, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jScrollPane14.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tbl_return_report.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbl_return_report.getTableHeader().setReorderingAllowed(false);
        tbl_return_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_return_reportKeyPressed(evt);
            }
        });
        jScrollPane14.setViewportView(tbl_return_report);

        btn_att_report_reset4.setText("Reset");
        btn_att_report_reset4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_reset4KeyPressed(evt);
            }
        });

        btn_att_report_reset5.setText("Print");
        btn_att_report_reset5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_reset5KeyPressed(evt);
            }
        });

        jLabel40.setText("Date :");

        txt_return_date.setEditable(false);

        jLabel33.setText("Total Amount :");

        txt_return_amount.setEditable(false);

        btn_att_report_view2.setText("View");
        btn_att_report_view2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_view2KeyPressed(evt);
            }
        });

        btn_att_report_cancel2.setText("Cancel");
        btn_att_report_cancel2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_cancel2KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout pnl_adv_report3Layout = new javax.swing.GroupLayout(pnl_adv_report3);
        pnl_adv_report3.setLayout(pnl_adv_report3Layout);
        pnl_adv_report3Layout.setHorizontalGroup(
            pnl_adv_report3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane14, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(pnl_adv_report3Layout.createSequentialGroup()
                .addComponent(btn_att_report_reset4, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_att_report_reset5, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel40)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_return_date, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel33)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_return_amount, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(423, 423, 423)
                .addComponent(btn_att_report_view2, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_att_report_cancel2, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnl_adv_report3Layout.setVerticalGroup(
            pnl_adv_report3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report3Layout.createSequentialGroup()
                .addComponent(jPanel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 477, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(pnl_adv_report3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_att_report_cancel2)
                    .addComponent(btn_att_report_view2)
                    .addComponent(txt_return_amount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33)
                    .addComponent(txt_return_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40)
                    .addComponent(btn_att_report_reset4)
                    .addComponent(btn_att_report_reset5)))
        );

        javax.swing.GroupLayout pnl_adv_report2Layout = new javax.swing.GroupLayout(pnl_adv_report2);
        pnl_adv_report2.setLayout(pnl_adv_report2Layout);
        pnl_adv_report2Layout.setHorizontalGroup(
            pnl_adv_report2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnl_adv_report3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(11, 11, 11))
        );
        pnl_adv_report2Layout.setVerticalGroup(
            pnl_adv_report2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report2Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(pnl_adv_report3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout pnl_attendence_reportLayout = new javax.swing.GroupLayout(pnl_attendence_report);
        pnl_attendence_report.setLayout(pnl_attendence_reportLayout);
        pnl_attendence_reportLayout.setHorizontalGroup(
            pnl_attendence_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1037, Short.MAX_VALUE)
            .addGroup(pnl_attendence_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnl_attendence_reportLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(pnl_adv_report2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        pnl_attendence_reportLayout.setVerticalGroup(
            pnl_attendence_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 573, Short.MAX_VALUE)
            .addGroup(pnl_attendence_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnl_attendence_reportLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(pnl_adv_report2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("Return Report Day wise", pnl_attendence_report);

        pnl_salary_report.setBackground(new java.awt.Color(255, 255, 255));

        pnl_adv_report4.setBackground(new java.awt.Color(255, 255, 255));

        pnl_adv_report5.setBackground(new java.awt.Color(255, 255, 255));

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel25.setText("Search By :");

        btn_total_report_search.setText("Search");
        btn_total_report_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_total_report_searchActionPerformed(evt);
            }
        });
        btn_total_report_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_total_report_searchKeyPressed(evt);
            }
        });

        jmc_total_report.setVerifyInputWhenFocusTarget(false);
        jmc_total_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_total_reportKeyPressed(evt);
            }
        });

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel28.setText("Month :");

        jSeparator7.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel14.setText("Date :");

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Year :");

        btn_total_report_print.setText("Print");

        btn_total_report_cancel.setText("Cancel");
        btn_total_report_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_total_report_cancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel26Layout = new javax.swing.GroupLayout(jPanel26);
        jPanel26.setLayout(jPanel26Layout);
        jPanel26Layout.setHorizontalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel26Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(check_total_report)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_total_report, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jmc_total_report, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(jyc_total_report, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_total_report_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_total_report_print, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_total_report_cancel)
                .addContainerGap())
        );
        jPanel26Layout.setVerticalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel26Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator7)
                    .addComponent(btn_total_report_search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(check_total_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jdc_total_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_total_report_print)
                        .addComponent(btn_total_report_cancel))
                    .addComponent(jyc_total_report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jmc_total_report, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel25, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jScrollPane15.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tbl_total_report.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbl_total_report.getTableHeader().setReorderingAllowed(false);
        tbl_total_report.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_total_reportKeyPressed(evt);
            }
        });
        jScrollPane15.setViewportView(tbl_total_report);

        btn_att_report_reset6.setText("Reset");
        btn_att_report_reset6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_reset6KeyPressed(evt);
            }
        });

        btn_att_report_reset7.setText("Print");
        btn_att_report_reset7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_reset7KeyPressed(evt);
            }
        });

        jLabel41.setText("Date :");

        txt_total_report_date.setEditable(false);

        jLabel34.setText("Total Amount :");

        txt_total_report_amount.setEditable(false);

        btn_att_report_view3.setText("View");
        btn_att_report_view3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_att_report_view3ActionPerformed(evt);
            }
        });
        btn_att_report_view3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_view3KeyPressed(evt);
            }
        });

        btn_att_report_cancel3.setText("Cancel");
        btn_att_report_cancel3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_cancel3KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout pnl_adv_report5Layout = new javax.swing.GroupLayout(pnl_adv_report5);
        pnl_adv_report5.setLayout(pnl_adv_report5Layout);
        pnl_adv_report5Layout.setHorizontalGroup(
            pnl_adv_report5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report5Layout.createSequentialGroup()
                .addGroup(pnl_adv_report5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_adv_report5Layout.createSequentialGroup()
                        .addComponent(btn_att_report_reset6, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_att_report_reset7, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel41)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_total_report_date, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel34)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_total_report_amount, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_att_report_view3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_att_report_cancel3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl_adv_report5Layout.createSequentialGroup()
                        .addGroup(pnl_adv_report5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane15, javax.swing.GroupLayout.DEFAULT_SIZE, 1016, Short.MAX_VALUE)
                            .addComponent(jPanel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnl_adv_report5Layout.setVerticalGroup(
            pnl_adv_report5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report5Layout.createSequentialGroup()
                .addComponent(jPanel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 477, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(pnl_adv_report5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_att_report_cancel3)
                    .addComponent(btn_att_report_view3)
                    .addComponent(txt_total_report_amount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel34)
                    .addComponent(txt_total_report_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel41)
                    .addComponent(btn_att_report_reset6)
                    .addComponent(btn_att_report_reset7)))
        );

        javax.swing.GroupLayout pnl_adv_report4Layout = new javax.swing.GroupLayout(pnl_adv_report4);
        pnl_adv_report4.setLayout(pnl_adv_report4Layout);
        pnl_adv_report4Layout.setHorizontalGroup(
            pnl_adv_report4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnl_adv_report5, javax.swing.GroupLayout.PREFERRED_SIZE, 1017, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnl_adv_report4Layout.setVerticalGroup(
            pnl_adv_report4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report4Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(pnl_adv_report5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout pnl_salary_reportLayout = new javax.swing.GroupLayout(pnl_salary_report);
        pnl_salary_report.setLayout(pnl_salary_reportLayout);
        pnl_salary_reportLayout.setHorizontalGroup(
            pnl_salary_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1037, Short.MAX_VALUE)
            .addGroup(pnl_salary_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnl_salary_reportLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(pnl_adv_report4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        pnl_salary_reportLayout.setVerticalGroup(
            pnl_salary_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 573, Short.MAX_VALUE)
            .addGroup(pnl_salary_reportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnl_salary_reportLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(pnl_adv_report4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("Total Stock Report Day wise", pnl_salary_report);

        pnl_adv_report6.setBackground(new java.awt.Color(255, 255, 255));

        pnl_adv_report7.setBackground(new java.awt.Color(255, 255, 255));

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel29.setText("Search By :");

        btn_other_search.setText("Search");
        btn_other_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_other_searchActionPerformed(evt);
            }
        });
        btn_other_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_other_searchKeyPressed(evt);
            }
        });

        jmc_other.setVerifyInputWhenFocusTarget(false);
        jmc_other.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jmc_otherKeyPressed(evt);
            }
        });

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel31.setText("Month :");

        jSeparator8.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel16.setText("Date :");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setText("Year :");

        btn_other_print1.setText("Print");

        btn_other_cancel.setText("Cancel");
        btn_other_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_other_cancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel27Layout = new javax.swing.GroupLayout(jPanel27);
        jPanel27.setLayout(jPanel27Layout);
        jPanel27Layout.setHorizontalGroup(
            jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel27Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(check_other)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_other, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jmc_other, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(jyc_other, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_other_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_other_print1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_other_cancel)
                .addContainerGap())
        );
        jPanel27Layout.setVerticalGroup(
            jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel27Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator8)
                    .addComponent(btn_other_search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(check_other, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jdc_other, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel31, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_other_print1)
                        .addComponent(btn_other_cancel))
                    .addComponent(jyc_other, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jmc_other, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jScrollPane17.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tbl_other.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbl_other.getTableHeader().setReorderingAllowed(false);
        tbl_other.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_otherKeyPressed(evt);
            }
        });
        jScrollPane17.setViewportView(tbl_other);

        btn_att_report_reset8.setText("Reset");
        btn_att_report_reset8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_reset8KeyPressed(evt);
            }
        });

        btn_att_report_reset9.setText("Print");
        btn_att_report_reset9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_reset9KeyPressed(evt);
            }
        });

        jLabel45.setText("Date :");

        txt_total_report_date1.setEditable(false);

        jLabel35.setText("Total Amount :");

        txt_total_report_amount1.setEditable(false);

        btn_att_report_view4.setText("View");
        btn_att_report_view4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_view4KeyPressed(evt);
            }
        });

        btn_att_report_cancel4.setText("Cancel");
        btn_att_report_cancel4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_att_report_cancel4KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout pnl_adv_report7Layout = new javax.swing.GroupLayout(pnl_adv_report7);
        pnl_adv_report7.setLayout(pnl_adv_report7Layout);
        pnl_adv_report7Layout.setHorizontalGroup(
            pnl_adv_report7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report7Layout.createSequentialGroup()
                .addGroup(pnl_adv_report7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_adv_report7Layout.createSequentialGroup()
                        .addComponent(btn_att_report_reset8, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_att_report_reset9, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel45)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_total_report_date1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel35)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_total_report_amount1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_att_report_view4, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_att_report_cancel4))
                    .addGroup(pnl_adv_report7Layout.createSequentialGroup()
                        .addGroup(pnl_adv_report7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane17, javax.swing.GroupLayout.DEFAULT_SIZE, 1016, Short.MAX_VALUE)
                            .addComponent(jPanel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnl_adv_report7Layout.setVerticalGroup(
            pnl_adv_report7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report7Layout.createSequentialGroup()
                .addComponent(jPanel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane17, javax.swing.GroupLayout.PREFERRED_SIZE, 477, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(pnl_adv_report7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_att_report_cancel4)
                    .addComponent(btn_att_report_view4)
                    .addComponent(txt_total_report_amount1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35)
                    .addComponent(txt_total_report_date1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel45)
                    .addComponent(btn_att_report_reset8)
                    .addComponent(btn_att_report_reset9)))
        );

        javax.swing.GroupLayout pnl_adv_report6Layout = new javax.swing.GroupLayout(pnl_adv_report6);
        pnl_adv_report6.setLayout(pnl_adv_report6Layout);
        pnl_adv_report6Layout.setHorizontalGroup(
            pnl_adv_report6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnl_adv_report7, javax.swing.GroupLayout.PREFERRED_SIZE, 1017, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnl_adv_report6Layout.setVerticalGroup(
            pnl_adv_report6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_adv_report6Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(pnl_adv_report7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1037, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(pnl_adv_report6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 573, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(pnl_adv_report6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("Other Charges Report", jPanel5);

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        jTabbedPane2.addTab("Reports", jPanel22);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );

        jPanel2.setBackground(new java.awt.Color(230, 79, 6));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Stock Management");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
   
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void pnl_employee4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pnl_employee4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pnl_employee4KeyPressed

    private void btn_return_newKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_return_newKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_return_newKeyPressed

    private void btn_return_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_return_newActionPerformed
        Config.new_return_item.onloadReset();
        Config.new_return_item.setVisible(true);
    }//GEN-LAST:event_btn_return_newActionPerformed

    private void btn_return_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_return_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_return_searchKeyPressed

    private void txt_stock_return_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_stock_return_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_stock_return_searchKeyPressed

    private void btn_return_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_return_printKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_return_printKeyPressed

    private void btn_return_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_return_viewKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_return_viewKeyPressed

    private void btn_return_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_return_viewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_return_viewActionPerformed

    private void btn_return_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_return_cancelKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_return_cancelKeyPressed

    private void pnl_employee3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pnl_employee3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pnl_employee3KeyPressed

    private void btn_demage_newKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_demage_newKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_demage_newKeyPressed

    private void btn_demage_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_demage_newActionPerformed
        Config.new_demage_item.onloadReset();
        Config.new_demage_item.setVisible(true);
    }//GEN-LAST:event_btn_demage_newActionPerformed

    private void btn_demage_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_demage_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_demage_searchKeyPressed

    private void txt_demage_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_demage_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_demage_searchKeyPressed

    private void btn_demage_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_demage_printKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_demage_printKeyPressed

    private void btn_demage_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_demage_viewKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_demage_viewKeyPressed

    private void btn_demage_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_demage_viewActionPerformed
        try {
            Config.view_demage_Item.onloadReset(tbl_demage_item_model.getValueAt(tbl_demage_item.getSelectedRow(), 0).toString());
            Config.view_demage_Item.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_demage_viewActionPerformed

    private void btn_demage_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_demage_cancelKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_demage_cancelKeyPressed

    private void pnl_employee2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pnl_employee2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_pnl_employee2KeyPressed

    private void tbl_stock_recevieKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_stock_recevieKeyPressed
        try {
            onloadReceiveMessage(tbl_receive_item_model.getValueAt(tbl_stock_recevie.getSelectedRow(), 8).toString());
        } catch (Exception e) {
            onloadReceiveMessage("");
        }
    }//GEN-LAST:event_tbl_stock_recevieKeyPressed

    private void btn_stock_recevie_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stock_recevie_newActionPerformed
        Config.new_stock.onloadReset();
        Config.new_stock.setVisible(true);
    }//GEN-LAST:event_btn_stock_recevie_newActionPerformed

    private void btn_stock_recevie_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_stock_recevie_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_stock_recevie_searchKeyPressed

    private void txt_stock_recevie_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_stock_recevie_searchKeyPressed
       if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            jdc_date_rec_from.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
//            jyc_adv_report.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_stock_recevie.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            tbl_attendence.requestFocus();
            //           }
    }//GEN-LAST:event_txt_stock_recevie_searchKeyPressed

    private void btn_stock_recevie_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_stock_recevie_printKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_stock_recevie_printKeyPressed

    private void btn_stock_recevie_viewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_stock_recevie_viewKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_stock_recevie_viewKeyPressed

    private void btn_stock_recevie_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stock_recevie_viewActionPerformed
        try {
            Config.view_stock.onloadReset(tbl_receive_item_model.getValueAt(tbl_stock_recevie.getSelectedRow(), 8).toString());
            Config.view_stock.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_stock_recevie_viewActionPerformed

    private void btn_stock_recevie_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_stock_recevie_cancelKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_stock_recevie_cancelKeyPressed

    private void pnl_employeeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pnl_employeeKeyPressed
       
    }//GEN-LAST:event_pnl_employeeKeyPressed

    private void tbl_sm_sodaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_sm_sodaKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_stock_mnt_print.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_stock_mnt_new.requestFocus();
        }
    }//GEN-LAST:event_tbl_sm_sodaKeyPressed

    private void btn_stock_mnt_newKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_stock_mnt_newKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            tbl_sm_soda.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_stock_mnt_search.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_sm_soda.requestFocus();
        }
    }//GEN-LAST:event_btn_stock_mnt_newKeyPressed

    private void btn_stock_mnt_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stock_mnt_newActionPerformed
        Config.new_stock.onloadReset();
        Config.new_stock.setVisible(true);
    }//GEN-LAST:event_btn_stock_mnt_newActionPerformed

    private void btn_stock_mnt_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_stock_mnt_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_stock_mnt_new.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            //jfmt_stock_mnt_to.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_sm_soda.requestFocus();
        }
    }//GEN-LAST:event_btn_stock_mnt_searchKeyPressed

    private void txt_stock_mnt_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_stock_mnt_searchKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
           // jfmt_stock_mnt_from.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
//            .requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            tbl_sm_soda.requestFocus();
        }
    }//GEN-LAST:event_txt_stock_mnt_searchKeyPressed

    private void btn_stock_mnt_printKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_stock_mnt_printKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            btn_stock_mnt_cancel.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            tbl_sm_soda.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_sm_soda.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            btn_stock_mnt_cancel.requestFocus();
        }
    }//GEN-LAST:event_btn_stock_mnt_printKeyPressed

    private void btn_stock_mnt_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_stock_mnt_cancelKeyPressed
        //        if(evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            //            .requestFocus();
            //           }
        if(evt.getKeyCode() == KeyEvent.VK_LEFT) {
            btn_stock_mnt_cancel.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_UP) {
            tbl_sm_soda.requestFocus();
        }
        //        if(evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //            tbl_employee.requestFocus();
            //           }
    }//GEN-LAST:event_btn_stock_mnt_cancelKeyPressed

    private void btn_stock_mnt_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stock_mnt_searchActionPerformed
    try {
        if(!txt_stock_mnt_search.getText().trim().equals("")){
            switch(cb_sm_Item_type.getSelectedIndex()){
            case 1:
            tbl_sm_stock_item_model.setRowCount(0);
                for (int i = 0; i < Config.config_stock_details.size(); i++) {
                if(Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getIdentity().equals("ST") && Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getName().toUpperCase().startsWith(txt_stock_mnt_search.getText().trim().toUpperCase())){
                    tbl_sm_stock_item_model.addRow(new Object[]{
                    Config.config_stock_details.get(i).getItem_id(),
                    Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getName(),  
                    Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getUnit(),  
                    Config.config_stock_details.get(i).getTotal()
                    });
                    }
                    }
                break;
                case 2:
                    tbl_sm_soda_model.setRowCount(0);
                    for (int i = 0; i < Config.config_stock_details.size(); i++) {
                        if(Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getIdentity().equals("S")&&Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getName().toUpperCase().startsWith(txt_stock_mnt_search.getText().trim().toUpperCase()) ){
                           tbl_sm_soda_model.addRow(new Object[]{
                             Config.config_stock_details.get(i).getItem_id(),
                             Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getName(),  
                             Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getUnit(),  
                             Config.config_stock_details.get(i).getTotal()
                           });
                        }
                    }

                 break;
                case 3:
                    tbl_sm_manufacture_item_model.setRowCount(0);
                    for (int i = 0; i < Config.config_stock_details.size(); i++) {
                        if(Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getIdentity().equals("M")&&Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getName().toUpperCase().startsWith(txt_stock_mnt_search.getText().trim().toUpperCase()) ){
                           tbl_sm_manufacture_item_model.addRow(new Object[]{
                             Config.config_stock_details.get(i).getItem_id(),
                             Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getName(),  
                             Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getUnit(),  
                             Config.config_stock_details.get(i).getTotal()
                           });
                        }
                    }
                     break;   
            }
            }else{
                    tbl_sm_stock_item_model.setRowCount(0);
                    tbl_sm_soda_model.setRowCount(0);
                    tbl_sm_manufacture_item_model.setRowCount(0);
                    for (int i = 0; i < Config.config_stock_details.size(); i++) {
                        if(Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getIdentity().equals("ST")){
                           tbl_sm_stock_item_model.addRow(new Object[]{
                             Config.config_stock_details.get(i).getItem_id(),
                             Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getName(),  
                             Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getUnit(),  
                             Config.config_stock_details.get(i).getTotal()
                           });
                        }
                        if(Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getIdentity().equals("S")){
                           tbl_sm_soda_model.addRow(new Object[]{
                             Config.config_stock_details.get(i).getItem_id(),
                             Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getName(),  
                             Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getUnit(),  
                             Config.config_stock_details.get(i).getTotal()
                           });
                        }
                            if(Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getIdentity().equals("M")){
                           tbl_sm_manufacture_item_model.addRow(new Object[]{
                             Config.config_stock_details.get(i).getItem_id(),
                             Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getName(),  
                             Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.config_stock_details.get(i).getItem_id())).getUnit(),  
                             Config.config_stock_details.get(i).getTotal()
                           });
                        }
                    }
                 }
        } catch (Exception e) {
            e.printStackTrace();
        }        
    }//GEN-LAST:event_btn_stock_mnt_searchActionPerformed

    private void stock_receive_check_fromActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stock_receive_check_fromActionPerformed
        if(stock_receive_check_from.isSelected()){
           stock_receive_from =true;
           stock_receive_to=false;
           jdc_date_rec_from.setEnabled(true);
           jdc_date_rec_to.setEnabled(false);
           stock_receive_check_to.setSelected(false);
           stock_receive_check_to.setEnabled(true);
        }else{
           stock_receive_from =false;
           stock_receive_to=false;
           jdc_date_rec_from.setEnabled(false);
           jdc_date_rec_to.setEnabled(false);
           stock_receive_check_to.setSelected(false);
           stock_receive_check_to.setEnabled(false);
        }
    }//GEN-LAST:event_stock_receive_check_fromActionPerformed

    private void cb_Item_typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_Item_typeActionPerformed
//        btn_report_month_searchActionPerformed(null);
    }//GEN-LAST:event_cb_Item_typeActionPerformed

    private void cb_sm_Item_typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_sm_Item_typeActionPerformed
    //    btn_report_month_searchActionPerformed(null);
    }//GEN-LAST:event_cb_sm_Item_typeActionPerformed

    private void tbl_sm_stock_itemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_sm_stock_itemKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_sm_stock_itemKeyPressed

    private void tbl_sm_manufacture_itemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_sm_manufacture_itemKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_sm_manufacture_itemKeyPressed

    private void btn_stock_recevie_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stock_recevie_searchActionPerformed
            String sql = "";
            String text=txt_stock_recevie_search.getText().trim().toUpperCase()+"%";
            if(stock_receive_from == true && stock_receive_to==true ){
                if (!txt_stock_recevie_search.getText().equals("")) {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from receive_stock a,stock_item b where b.item_id=a.item_id and STR_TO_DATE(a._date,'%d/%m/%Y') >STR_TO_DATE('"+simple_date_formate.format(jdc_date_rec_from.getDate())+"','%d/%m/%Y')and STR_TO_DATE(a._date,'%d/%m/%Y')<=STR_TO_DATE('"+simple_date_formate.format(jdc_date_rec_to.getDate())+"','%d/%m/%Y') and b.name like '"+text+"' order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                    
                } else {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from receive_stock a,stock_item b where b.item_id=a.item_id and STR_TO_DATE(a._date,'%d/%m/%Y') >STR_TO_DATE('"+simple_date_formate.format(jdc_date_rec_from.getDate())+"','%d/%m/%Y') and STR_TO_DATE(a._date,'%d/%m/%Y')<=STR_TO_DATE('"+simple_date_formate.format(jdc_date_rec_to.getDate())+"','%d/%m/%Y') order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                }
            }else if(stock_receive_from == true && stock_receive_to==false ){
                if (!txt_stock_recevie_search.getText().equals("")) {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from receive_stock a,stock_item b where b.item_id=a.item_id and STR_TO_DATE(a._date,'%d/%m/%Y') =STR_TO_DATE('"+simple_date_formate.format(jdc_date_rec_from.getDate())+"','%d/%m/%Y') and b.name like '"+text+"' order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                } else {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from receive_stock a,stock_item b where b.item_id=a.item_id and STR_TO_DATE(a._date,'%d/%m/%Y') =STR_TO_DATE('"+simple_date_formate.format(jdc_date_rec_from.getDate())+"','%d/%m/%Y') order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                }
            }else{
                if (!txt_stock_recevie_search.getText().equals("")) {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from receive_stock a,stock_item b where b.item_id=a.item_id and b.name like '"+text+"' order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                } else {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from receive_stock a,stock_item b where b.item_id=a.item_id order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                }
            }
            view(sql);
    }//GEN-LAST:event_btn_stock_recevie_searchActionPerformed

    private void txt_stock_mnt_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_stock_mnt_searchCaretUpdate
        btn_stock_mnt_searchActionPerformed(null);
    }//GEN-LAST:event_txt_stock_mnt_searchCaretUpdate

    private void stock_receive_check_toActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stock_receive_check_toActionPerformed
        if(stock_receive_check_to.isSelected()){
           stock_receive_to=true;
           jdc_date_rec_to.setEnabled(true);
        }else{
           stock_receive_to=false;
           jdc_date_rec_to.setEnabled(false);
        }
    }//GEN-LAST:event_stock_receive_check_toActionPerformed

    private void txt_stock_recevie_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_stock_recevie_searchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_stock_recevie_searchActionPerformed

    private void txt_stock_recevie_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_stock_recevie_searchCaretUpdate
        btn_stock_recevie_searchActionPerformed(null);
    }//GEN-LAST:event_txt_stock_recevie_searchCaretUpdate

    private void tbl_stock_recevieMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_stock_recevieMouseClicked
        try {
            if(evt.getClickCount()==1){
        try {
            onloadReceiveMessage(tbl_receive_item_model.getValueAt(tbl_stock_recevie.getSelectedRow(), 8).toString());
        } catch (Exception e) {
            onloadReceiveMessage("");
        }
            }
            if(evt.getClickCount()==2){
               Config.view_stock.onloadReset(tbl_receive_item_model.getValueAt(tbl_stock_recevie.getSelectedRow(), 8).toString());
               Config.view_stock.setVisible(true);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_tbl_stock_recevieMouseClicked

    private void btn_demage_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_demage_searchActionPerformed
         String sql = "";
            String text=txt_demage_search.getText().trim().toUpperCase()+"%";
            if(stock_demage_from == true && stock_demage_to==true ){
                if (!txt_demage_search.getText().equals("")) {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from demage_stock a,stock_item b where b.item_id=a.item_id and STR_TO_DATE(a._date,'%d/%m/%Y') >STR_TO_DATE('"+simple_date_formate.format(jdc_date_dem_from.getDate())+"','%d/%m/%Y') and STR_TO_DATE(a._date,'%d/%m/%Y') <=STR_TO_DATE('"+simple_date_formate.format(jdc_date_dem_to.getDate())+"','%d/%m/%Y') and b.name like '"+text+"' order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                    
                } else {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from demage_stock a,stock_item b where b.item_id=a.item_id and STR_TO_DATE(a._date,'%d/%m/%Y') >STR_TO_DATE('"+simple_date_formate.format(jdc_date_dem_from.getDate())+"','%d/%m/%Y') and STR_TO_DATE(a._date,'%d/%m/%Y')<=STR_TO_DATE('"+simple_date_formate.format(jdc_date_dem_to.getDate())+"','%d/%m/%Y') order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                }
            }else if(stock_demage_from == true && stock_demage_to==false ){
                if (!txt_demage_search.getText().equals("")) {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from demage_stock a,stock_item b where b.item_id=a.item_id and STR_TO_DATE(a._date,'%d/%m/%Y') =STR_TO_DATE('"+simple_date_formate.format(jdc_date_dem_from.getDate())+"','%d/%m/%Y') and b.name like '"+text+"' order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                } else {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from demage_stock a,stock_item b where b.item_id=a.item_id and STR_TO_DATE(a._date,'%d/%m/%Y') =STR_TO_DATE('"+simple_date_formate.format(jdc_date_dem_from.getDate())+"','%d/%m/%Y') order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                }
            }else{
                if (!txt_demage_search.getText().equals("")) {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from demage_stock a,stock_item b where b.item_id=a.item_id and b.name like '"+text+"' order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                } else {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from demage_stock a,stock_item b where b.item_id=a.item_id order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                }
            }
            setDemageTable(sql);
    }//GEN-LAST:event_btn_demage_searchActionPerformed

    private void tbl_demage_itemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_demage_itemMouseClicked
        try {
            if(evt.getClickCount()==1){
                try {
                    onloadDemageMessage(tbl_demage_item.getValueAt(tbl_demage_item.getSelectedRow(), 8).toString());
                } catch (Exception e) {
                    onloadDemageMessage("");
                }
            }
            if(evt.getClickCount()==2){
               Config.view_demage_Item.onloadReset(tbl_demage_item.getValueAt(tbl_demage_item.getSelectedRow(), 0).toString());
               Config.view_demage_Item.setVisible(true);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_tbl_demage_itemMouseClicked

    private void tbl_demage_itemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_demage_itemKeyPressed

    }//GEN-LAST:event_tbl_demage_itemKeyPressed

    private void stock_demage_check_fromActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stock_demage_check_fromActionPerformed
        if(stock_demage_check_from.isSelected()){
           stock_demage_from =true;
           stock_demage_to=false;
           jdc_date_dem_from.setEnabled(true);
           jdc_date_dem_to.setEnabled(false);
           stock_demage_check_to.setSelected(false);
           stock_demage_check_to.setEnabled(true);
        }else{
           stock_demage_from =false;
           stock_demage_to=false;
           jdc_date_dem_from.setEnabled(false);
           jdc_date_dem_to.setEnabled(false);
           stock_demage_check_to.setSelected(false);
           stock_demage_check_to.setEnabled(false);
        }
    }//GEN-LAST:event_stock_demage_check_fromActionPerformed

    private void stock_demage_check_toActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stock_demage_check_toActionPerformed
        if(stock_demage_check_to.isSelected()){
           stock_demage_to=true;
           jdc_date_dem_from.setEnabled(true);
        }else{
           stock_demage_to=false;
           jdc_date_dem_from.setEnabled(false);
        }
    }//GEN-LAST:event_stock_demage_check_toActionPerformed

    private void tbl_return_itemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_return_itemMouseClicked
        try {
            if(evt.getClickCount()==1){
                onloadReturnMessage(tbl_return_item_model.getValueAt(tbl_return_item.getSelectedRow(), 8).toString());
            }
            if(evt.getClickCount()==2){
               Config.view_return_Item.onloadReset(tbl_return_item_model.getValueAt(tbl_return_item.getSelectedRow(), 8).toString());
               Config.view_return_Item.setVisible(true);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Row ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_tbl_return_itemMouseClicked

    private void tbl_return_itemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_return_itemKeyPressed
        
    }//GEN-LAST:event_tbl_return_itemKeyPressed

    private void btn_return_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_return_searchActionPerformed
        String sql = "";
            String text=txt_stock_return_search.getText().trim().toUpperCase()+"%";
            if(stock_return_from == true && stock_return_to==true ){
                if (!txt_stock_return_search.getText().equals("")) {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from return_stock a,stock_item b where b.item_id=a.item_id and STR_TO_DATE(a._date,'%d/%m/%Y') >STR_TO_DATE('"+simple_date_formate.format(jdc_date_return_from.getDate())+"','%d/%m/%Y') and STR_TO_DATE(a._date,'%d/%m/%Y')<=STR_TO_DATE('"+simple_date_formate.format(jdc_date_return_to.getDate())+"','%d/%m/%Y') and b.name like '"+text+"' order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                } else {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from return_stock a,stock_item b where b.item_id=a.item_id and STR_TO_DATE(a._date,'%d/%m/%Y') >STR_TO_DATE('"+simple_date_formate.format(jdc_date_return_from.getDate())+"','%d/%m/%Y') and STR_TO_DATE(a._date,'%d/%m/%Y')<=STR_TO_DATE('"+simple_date_formate.format(jdc_date_return_to.getDate())+"','%d/%m/%Y') order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                }
            }else if(stock_return_from == true && stock_return_to==false ){
                if (!txt_stock_recevie_search.getText().equals("")) {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from return_stock a,stock_item b where b.item_id=a.item_id and STR_TO_DATE(a._date,'%d/%m/%Y') =STR_TO_DATE('"+simple_date_formate.format(jdc_date_return_from.getDate())+"','%d/%m/%Y') and b.name like '"+text+"' order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                } else {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from return_stock a,stock_item b where b.item_id=a.item_id and STR_TO_DATE(a._date,'%d/%m/%Y') =STR_TO_DATE('"+simple_date_formate.format(jdc_date_return_from.getDate())+"','%d/%m/%Y') order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                }
            }else{
                if (!txt_stock_return_search.getText().equals("")) {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from return_stock a,stock_item b where b.item_id=a.item_id and b.name like '"+text+"' order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                } else {
                    sql = "select a.*,(select prise from item_list where item_list_id=a.item_list_id) as price,(select supplier_id from item_list where item_list_id=a.item_list_id) as supplier_id from return_stock a,stock_item b where b.item_id=a.item_id order by  STR_TO_DATE(a._date,'%d/%m/%Y')" ;
                }
            }
            setReturnTable(sql);
    }//GEN-LAST:event_btn_return_searchActionPerformed

    private void stock_return_check_fromActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stock_return_check_fromActionPerformed
        if(stock_return_check_from.isSelected()){
           stock_return_from =true;
           stock_return_to=false;
           jdc_date_return_from.setEnabled(true);
           jdc_date_return_to.setEnabled(false);
           stock_return_check_to.setSelected(false);
           stock_return_check_to.setEnabled(true);
        }else{
           stock_return_from =false;
           stock_return_to=false;
           jdc_date_return_from.setEnabled(false);
           jdc_date_return_to.setEnabled(false);
           stock_return_check_to.setSelected(false);
           stock_return_check_to.setEnabled(false);
        }
    }//GEN-LAST:event_stock_return_check_fromActionPerformed

    private void stock_return_check_toActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stock_return_check_toActionPerformed
        if(stock_return_check_to.isSelected()){
           stock_return_to=true;
           jdc_date_return_to.setEnabled(true);
        }else{
           stock_return_to=false;
           jdc_date_return_to.setEnabled(false);
        }
    }//GEN-LAST:event_stock_return_check_toActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            int sd = JOptionPane.showConfirmDialog(this,"Are you really want to delete this entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
         if (sd == 0){
            if(Config.receive_stock_mgr.delReceiveStock(tbl_receive_item_model.getValueAt(tbl_stock_recevie.getSelectedRow(), 0).toString())){
                onloadReset();
                JOptionPane.showMessageDialog(this, "ItemList deleted successfully.", "Creation successful", JOptionPane.NO_OPTION);
            } else {
                JOptionPane.showMessageDialog(this, "Error in Itemlist deletion creation.", "Error", JOptionPane.ERROR_MESSAGE);
            }
         }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any row.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            if(Config.demage_stock_mgr.delReceiveStock(tbl_demage_item_model.getValueAt(tbl_demage_item.getSelectedRow(), 0).toString())){
                onloadReset();
                JOptionPane.showMessageDialog(this, "Demage item deleted successfully.", "Creation successful", JOptionPane.NO_OPTION);
                } else {
                JOptionPane.showMessageDialog(this, "Error in Demage item deletion creation.", "Error", JOptionPane.ERROR_MESSAGE);
                }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any row.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try {
            if(Config.return_stock_mgr.delReturnStock(tbl_return_item_model.getValueAt(tbl_return_item.getSelectedRow(), 0).toString())){
                onloadReset();
                JOptionPane.showMessageDialog(this, "Demage item deleted successfully.", "Creation successful", JOptionPane.NO_OPTION);
            } else {
                JOptionPane.showMessageDialog(this, "Error in Demage item deletion creation.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any row.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void txt_stock_return_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_stock_return_searchCaretUpdate
        btn_return_searchActionPerformed(null);
    }//GEN-LAST:event_txt_stock_return_searchCaretUpdate

    private void txt_demage_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_demage_searchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_demage_searchActionPerformed

    private void txt_demage_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_demage_searchCaretUpdate
        btn_demage_searchActionPerformed(null);
    }//GEN-LAST:event_txt_demage_searchCaretUpdate

    private void tbl_demage_itemFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tbl_demage_itemFocusGained
//        try {
//             onloadDemageMessage(tbl_demage_item.getValueAt(tbl_demage_item.getSelectedRow(), 0).toString());
//        } catch (Exception e) {
//             onloadDemageMessage("");
//        }
    }//GEN-LAST:event_tbl_demage_itemFocusGained

    private void btn_receive_report_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_receive_report_searchActionPerformed
        String sql="";
        if(!check_receive_report.isSelected()){
            sql="select r._date,r.item_id,si.identity,sum(r.quantity*il.prise) as amount,r._month,r._year from receive_stock r\n" +
                "inner join item_list il on il.item_list_id=r.item_list_id\n" +
                "inner join stock_item si on si.item_id=r.item_id\n" +
                "group by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id\n" +
                "having  r._month='"+(jmc_receive_report.getMonth()+1)+"'"
                + " and r._year='"+jyc_receive_report.getYear()+"' "
                + "and si.identity!='M'" +
                "order by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id";
            txt_receive_date.setText("\tALL");
        }else{
            sql="select r._date,r.item_id,si.identity,sum(r.quantity*il.prise) as amount,r._month,r._year from receive_stock r\n" +
                "inner join item_list il on il.item_list_id=r.item_list_id\n" +
                "inner join stock_item si on si.item_id=r.item_id\n" +
                "group by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id\n" +
                "having  r._month='"+(jmc_receive_report.getMonth()+1)+"'"
                + " and r._year='"+jyc_receive_report.getYear()+"' "
                + "and STR_TO_DATE(r._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_receive_report.getDate())+"','%d/%m/%Y') "
                + "and si.identity!='M'" +
                "order by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id";
            txt_receive_date.setText(simple_date_formate.format(jdc_receive_report.getDate()));
        }
        loadReceiveReport(sql);
    }//GEN-LAST:event_btn_receive_report_searchActionPerformed

    private void btn_receive_report_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_receive_report_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_receive_report_searchKeyPressed

    private void jmc_receive_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_receive_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmc_receive_reportKeyPressed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void tbl_receive_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_receive_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_receive_reportKeyPressed

    private void btn_att_report_reset2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_reset2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_reset2KeyPressed

    private void btn_att_report_reset3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_reset3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_reset3KeyPressed

    private void btn_att_report_view1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_view1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_view1KeyPressed

    private void btn_att_report_cancel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_cancel1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_cancel1KeyPressed

    private void btn_return_report_search1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_return_report_search1ActionPerformed
        String sql="";
        if(!check_return_report.isSelected()){
        sql="select r._date,r.item_id,si.identity,sum(r.quantity*il.prise) as amount,r._month,r._year from return_stock r\n" +
                "inner join item_list il on il.item_list_id=r.item_list_id\n" +
                "inner join stock_item si on si.item_id=r.item_id\n" +
                "group by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id\n" +
                "having  r._month='"+(jmc_return_report.getMonth()+1)+"'"
                + " and r._year='"+jyc_return_report.getYear()+"' "
                + "and si.identity!='M'" +
                "order by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id";
        txt_return_date.setText("\tALL");
        }else{
            sql="select r._date,r.item_id,si.identity,sum(r.quantity*il.prise) as amount,r._month,r._year from return_stock r\n" +
                "inner join item_list il on il.item_list_id=r.item_list_id\n" +
                "inner join stock_item si on si.item_id=r.item_id\n" +
                "group by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id\n" +
                "having  r._month='"+(jmc_return_report.getMonth()+1)+"'"
                + " and r._year='"+jyc_return_report.getYear()+"' "
                + "and STR_TO_DATE(r._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_return_report.getDate())+"','%d/%m/%Y') "
                + "and si.identity!='M'" +
                "order by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id";
            txt_return_date.setText(simple_date_formate.format(jdc_return_report.getDate()));
        }
        loadReturnReport(sql);
    }//GEN-LAST:event_btn_return_report_search1ActionPerformed

    private void btn_return_report_search1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_return_report_search1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_return_report_search1KeyPressed

    private void jmc_return_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_return_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmc_return_reportKeyPressed

    private void btn_return_report_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_return_report_cancelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_return_report_cancelActionPerformed

    private void tbl_return_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_return_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_return_reportKeyPressed

    private void btn_att_report_reset4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_reset4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_reset4KeyPressed

    private void btn_att_report_reset5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_reset5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_reset5KeyPressed

    private void btn_att_report_view2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_view2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_view2KeyPressed

    private void btn_att_report_cancel2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_cancel2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_cancel2KeyPressed

    private void btn_total_report_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_total_report_searchActionPerformed
               loadTotalStockReport();
    }//GEN-LAST:event_btn_total_report_searchActionPerformed

    private void btn_total_report_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_total_report_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_total_report_searchKeyPressed

    private void jmc_total_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_total_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmc_total_reportKeyPressed

    private void btn_total_report_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_total_report_cancelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_total_report_cancelActionPerformed

    private void tbl_total_reportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_total_reportKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_total_reportKeyPressed

    private void btn_att_report_reset6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_reset6KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_reset6KeyPressed

    private void btn_att_report_reset7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_reset7KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_reset7KeyPressed

    private void btn_att_report_view3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_view3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_view3KeyPressed

    private void btn_att_report_cancel3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_cancel3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_cancel3KeyPressed

    private void btn_other_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_other_searchActionPerformed
        loadOtherStockReport();
    }//GEN-LAST:event_btn_other_searchActionPerformed

    private void btn_other_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_other_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_other_searchKeyPressed

    private void jmc_otherKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jmc_otherKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jmc_otherKeyPressed

    private void btn_other_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_other_cancelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_other_cancelActionPerformed

    private void tbl_otherKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_otherKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_otherKeyPressed

    private void btn_att_report_reset8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_reset8KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_reset8KeyPressed

    private void btn_att_report_reset9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_reset9KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_reset9KeyPressed

    private void btn_att_report_view4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_view4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_view4KeyPressed

    private void btn_att_report_cancel4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_att_report_cancel4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_cancel4KeyPressed

    private void btn_stock_recevie_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stock_recevie_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_stock_recevie_cancelActionPerformed

    private void btn_att_report_view3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_att_report_view3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_att_report_view3ActionPerformed

    private void btn_stock_mnt_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_stock_mnt_cancelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_stock_mnt_cancelActionPerformed

    private void tbl_stock_recevieKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_stock_recevieKeyReleased
        try {
            onloadReceiveMessage(tbl_receive_item_model.getValueAt(tbl_stock_recevie.getSelectedRow(), 8).toString());
        } catch (Exception e) {
            onloadReceiveMessage("");
        }
    }//GEN-LAST:event_tbl_stock_recevieKeyReleased

    private void tbl_return_itemKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_return_itemKeyReleased
        try {
             onloadReturnMessage(tbl_return_item_model.getValueAt(tbl_return_item.getSelectedRow(), 8).toString());
        } catch (Exception e) {
            onloadReturnMessage("");
        }
    }//GEN-LAST:event_tbl_return_itemKeyReleased
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_att_report_cancel1;
    private javax.swing.JButton btn_att_report_cancel2;
    private javax.swing.JButton btn_att_report_cancel3;
    private javax.swing.JButton btn_att_report_cancel4;
    private javax.swing.JButton btn_att_report_reset2;
    private javax.swing.JButton btn_att_report_reset3;
    private javax.swing.JButton btn_att_report_reset4;
    private javax.swing.JButton btn_att_report_reset5;
    private javax.swing.JButton btn_att_report_reset6;
    private javax.swing.JButton btn_att_report_reset7;
    private javax.swing.JButton btn_att_report_reset8;
    private javax.swing.JButton btn_att_report_reset9;
    private javax.swing.JButton btn_att_report_view1;
    private javax.swing.JButton btn_att_report_view2;
    private javax.swing.JButton btn_att_report_view3;
    private javax.swing.JButton btn_att_report_view4;
    private javax.swing.JButton btn_demage_cancel;
    private javax.swing.JButton btn_demage_new;
    private javax.swing.JButton btn_demage_print;
    private javax.swing.JButton btn_demage_search;
    private javax.swing.JButton btn_demage_view;
    private javax.swing.JButton btn_other_cancel;
    private javax.swing.JButton btn_other_print1;
    private javax.swing.JButton btn_other_search;
    private javax.swing.JButton btn_receive_report_search;
    private javax.swing.JButton btn_return_cancel;
    private javax.swing.JButton btn_return_new;
    private javax.swing.JButton btn_return_print;
    private javax.swing.JButton btn_return_report_cancel;
    private javax.swing.JButton btn_return_report_print;
    private javax.swing.JButton btn_return_report_search1;
    private javax.swing.JButton btn_return_search;
    private javax.swing.JButton btn_return_view;
    private javax.swing.JButton btn_stock_mnt_cancel;
    private javax.swing.JButton btn_stock_mnt_new;
    private javax.swing.JButton btn_stock_mnt_print;
    private javax.swing.JButton btn_stock_mnt_search;
    private javax.swing.JButton btn_stock_recevie_cancel;
    private javax.swing.JButton btn_stock_recevie_new;
    private javax.swing.JButton btn_stock_recevie_print;
    private javax.swing.JButton btn_stock_recevie_search;
    private javax.swing.JButton btn_stock_recevie_view;
    private javax.swing.JButton btn_total_report_cancel;
    private javax.swing.JButton btn_total_report_print;
    private javax.swing.JButton btn_total_report_search;
    private javax.swing.JComboBox cb_Item_type;
    private javax.swing.JComboBox cb_sm_Item_type;
    private javax.swing.JCheckBox check_other;
    private javax.swing.JCheckBox check_receive_report;
    private javax.swing.JCheckBox check_return_report;
    private javax.swing.JCheckBox check_total_report;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane17;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTabbedPane jTabbedPane3;
    private com.toedter.calendar.JDateChooser jdc_date_dem_from;
    private com.toedter.calendar.JDateChooser jdc_date_dem_to;
    private com.toedter.calendar.JDateChooser jdc_date_rec_from;
    private com.toedter.calendar.JDateChooser jdc_date_rec_to;
    private com.toedter.calendar.JDateChooser jdc_date_return_from;
    private com.toedter.calendar.JDateChooser jdc_date_return_to;
    private com.toedter.calendar.JDateChooser jdc_other;
    private com.toedter.calendar.JDateChooser jdc_receive_report;
    private com.toedter.calendar.JDateChooser jdc_return_report;
    private com.toedter.calendar.JDateChooser jdc_total_report;
    private com.toedter.calendar.JMonthChooser jmc_other;
    private com.toedter.calendar.JMonthChooser jmc_receive_report;
    private com.toedter.calendar.JMonthChooser jmc_return_report;
    private com.toedter.calendar.JMonthChooser jmc_total_report;
    private com.toedter.calendar.JYearChooser jyc_other;
    private com.toedter.calendar.JYearChooser jyc_receive_report;
    private com.toedter.calendar.JYearChooser jyc_return_report;
    private com.toedter.calendar.JYearChooser jyc_total_report;
    private javax.swing.JPanel pnl_adv_report;
    private javax.swing.JPanel pnl_adv_report1;
    private javax.swing.JPanel pnl_adv_report2;
    private javax.swing.JPanel pnl_adv_report3;
    private javax.swing.JPanel pnl_adv_report4;
    private javax.swing.JPanel pnl_adv_report5;
    private javax.swing.JPanel pnl_adv_report6;
    private javax.swing.JPanel pnl_adv_report7;
    private javax.swing.JPanel pnl_attendence_report;
    private javax.swing.JPanel pnl_employee;
    private javax.swing.JPanel pnl_employee2;
    private javax.swing.JPanel pnl_employee3;
    private javax.swing.JPanel pnl_employee4;
    private javax.swing.JPanel pnl_salary_info;
    private javax.swing.JPanel pnl_salary_report;
    private javax.swing.JCheckBox stock_demage_check_from;
    private javax.swing.JCheckBox stock_demage_check_to;
    private javax.swing.JCheckBox stock_receive_check_from;
    private javax.swing.JCheckBox stock_receive_check_to;
    private javax.swing.JCheckBox stock_return_check_from;
    private javax.swing.JCheckBox stock_return_check_to;
    private javax.swing.JTable tbl_demage_item;
    private javax.swing.JTable tbl_other;
    private javax.swing.JTable tbl_receive_report;
    private javax.swing.JTable tbl_return_item;
    private javax.swing.JTable tbl_return_report;
    private javax.swing.JTable tbl_sm_manufacture_item;
    private javax.swing.JTable tbl_sm_soda;
    private javax.swing.JTable tbl_sm_stock_item;
    private javax.swing.JTable tbl_stock_recevie;
    private javax.swing.JTable tbl_total_report;
    private javax.swing.JTextPane txt_demage_message;
    private javax.swing.JTextField txt_demage_search;
    private javax.swing.JTextField txt_receive_amount;
    private javax.swing.JTextField txt_receive_date;
    private javax.swing.JTextField txt_return_amount;
    private javax.swing.JTextField txt_return_date;
    private javax.swing.JTextField txt_stock_mnt_search;
    private javax.swing.JTextField txt_stock_recevie_challanno;
    private javax.swing.JTextField txt_stock_recevie_invoiceno;
    private javax.swing.JTextField txt_stock_recevie_loadingcharges;
    private javax.swing.JTextPane txt_stock_recevie_message;
    private javax.swing.JTextField txt_stock_recevie_othercharges;
    private javax.swing.JTextField txt_stock_recevie_search;
    private javax.swing.JTextField txt_stock_return_challanno;
    private javax.swing.JTextField txt_stock_return_invoiceno;
    private javax.swing.JTextField txt_stock_return_loadingcharges;
    private javax.swing.JTextPane txt_stock_return_message;
    private javax.swing.JTextField txt_stock_return_othercharges;
    private javax.swing.JTextField txt_stock_return_search;
    private javax.swing.JTextField txt_total_report_amount;
    private javax.swing.JTextField txt_total_report_amount1;
    private javax.swing.JTextField txt_total_report_date;
    private javax.swing.JTextField txt_total_report_date1;
    // End of variables declaration//GEN-END:variables
    public void onloadReset() {
        btn_stock_mnt_searchActionPerformed(null);
        stock_receive_check_from.setSelected(true);
        stock_receive_check_to.setSelected(true);
        stock_demage_check_from.setSelected(true);
        stock_demage_check_to.setSelected(true);
        stock_return_check_from.setSelected(true);
        stock_return_check_to.setSelected(true);
        
        jdc_date_rec_to.setDate(Calendar.getInstance().getTime()); 
        jdc_date_dem_to.setDate(Calendar.getInstance().getTime()); 
        jdc_date_return_to.setDate(Calendar.getInstance().getTime()); 
        
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -5);
        Date date=cal.getTime();
        jdc_date_rec_from.setDate(date); 
        jdc_date_dem_from.setDate(date); 
        jdc_date_return_from.setDate(date); 
        
        btn_stock_recevie_searchActionPerformed(null);
        btn_demage_searchActionPerformed(null);
        btn_return_searchActionPerformed(null);
    }

    public void view(String sql) {
            try {
                tbl_receive_item_model.setRowCount(0);
                Config.sql =sql;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                while (Config.rs.next()) {
                tbl_receive_item_model.addRow(new Object[]{
                Config.rs.getString("receive_stock_id"),
                Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.rs.getString("Item_id"))).getName(),
                Config.rs.getString("_date"),
                Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.rs.getString("Item_id"))).getUnit(),
                Config.rs.getString("quantity"),
                Config.rs.getString("Price"),
                Double.parseDouble(Config.rs.getString("quantity"))*Double.parseDouble(Config.rs.getString("Price")),
                Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(Config.rs.getString("supplier_id"))).getProfile_id())).getName(),
                Config.rs.getString("bill_type_id")
                });
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void onloadReceiveMessage(String bill_type_id){
        try {
                Config.sql ="SELECT *,(select message from message_details where message_id=b.message_id) as message FROM bill_type b where bill_type_id="+bill_type_id;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                if(Config.rs.next()) {
                    txt_stock_recevie_message.setText(Config.rs.getString("message"));
                    txt_stock_recevie_invoiceno.setText(Config.rs.getString("bill_no"));
                    txt_stock_recevie_challanno.setText(Config.rs.getString("chalan_no"));
                    txt_stock_recevie_loadingcharges.setText(Config.rs.getString("loading_charges"));
                    txt_stock_recevie_othercharges.setText(Config.rs.getString("other_charges"));
                }else{
                    txt_stock_recevie_message.setText("");
                    txt_stock_recevie_invoiceno.setText("");
                    txt_stock_recevie_challanno.setText("");
                    txt_stock_recevie_loadingcharges.setText("");
                    txt_stock_recevie_othercharges.setText("");
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onloadDemageMessage(String message_id){
        try {
            if (!message_id.equals("")) {
                System.out.println(Config.sql ="select * from message_details where message_id="+message_id);
                Config.sql ="select * from message_details where message_id="+message_id;
                Config.rs = Config.stmt.executeQuery(Config.sql);
            }
            if(Config.rs.next()) {
                txt_demage_message.setText(Config.rs.getString("message"));
            } else {
                txt_demage_message.setText("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void setDemageTable(String sql) {
        try {
                tbl_demage_item_model.setRowCount(0);
                Config.sql =sql;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                while (Config.rs.next()) {
                tbl_demage_item_model.addRow(new Object[]{
                Config.rs.getString("demage_stock_id"),
                Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.rs.getString("Item_id"))).getName(),
                Config.rs.getString("_date"),
                Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.rs.getString("Item_id"))).getUnit(),
                Config.rs.getString("quantity"),
                Config.rs.getString("Price"),
                Double.parseDouble(Config.rs.getString("quantity"))*Double.parseDouble(Config.rs.getString("Price")),
                Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(Config.rs.getString("supplier_id"))).getProfile_id())).getName(),
                Config.rs.getString("message_id")
                });
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void setReturnTable(String sql) {
                 try {
                tbl_return_item_model.setRowCount(0);
                Config.sql =sql;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                while (Config.rs.next()) {
                tbl_return_item_model.addRow(new Object[]{
                Config.rs.getString("return_id"),
                Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.rs.getString("Item_id"))).getName(),
                Config.rs.getString("_date"),
                Config.config_stock_item.get(Config.id_stock_item.indexOf(Config.rs.getString("Item_id"))).getUnit(),
                Config.rs.getString("quantity"),
                Config.rs.getString("Price"),
                Double.parseDouble(Config.rs.getString("quantity"))*Double.parseDouble(Config.rs.getString("Price")),
                Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(Config.rs.getString("supplier_id"))).getProfile_id())).getName(),
                Config.rs.getString("bill_type_id")
                });
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onloadReturnMessage(String bill_type_id) {
        try {
                Config.sql ="SELECT *,(select message from message_details where message_id=b.message_id) as message FROM bill_type b where bill_type_id="+bill_type_id;
                Config.rs = Config.stmt.executeQuery(Config.sql);
                if(Config.rs.next()) {
                    txt_stock_return_message.setText(Config.rs.getString("message"));
                    txt_stock_return_invoiceno.setText(Config.rs.getString("bill_no"));
                    txt_stock_return_challanno.setText(Config.rs.getString("chalan_no"));
                    txt_stock_return_loadingcharges.setText(Config.rs.getString("loading_charges"));
                    txt_stock_return_othercharges.setText(Config.rs.getString("other_charges"));
                }else{
                    txt_stock_return_message.setText("");
                    txt_stock_return_invoiceno.setText("");
                    txt_stock_return_challanno.setText("");
                    txt_stock_return_loadingcharges.setText("");
                    txt_stock_return_othercharges.setText("");
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadReceiveReport(String sql) {
    //.........................................................................................................................................
        int col_height=0;  
        int row=1;  
        int year = jyc_receive_report.getYear();
        int month = jmc_receive_report.getMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        int total = calendar.getActualMaximum(Calendar.DATE);
        int t_col = 0;
        for(int i = 0; i < Config.config_stock_item.size(); i++) {
        if(!Config.config_stock_item.get(i).getIdentity().equals("M")){
            t_col++;
        }
        }
        int size= t_col+2;
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = new boolean [size];
        String []col = new String[size];
        Object [][]obj = new Object[total][size];
        //...................................................................SET FIRST COLUMN ON DATE.....................................................
    for (int i = 0; i < total; i++) {
         if(i<9){
             if(month<9){
                 obj[i][0]="0"+(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]="0"+(i+1)+"/"+(month+1)+"/"+year;
             }
         }else{
             if(month<9){
                 obj[i][0]=(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]=(i+1)+"/"+(month+1)+"/"+year;
             }
         }
            for (int j = 1; j < size-3; j++) {
                obj[i][j]=0.0;
            }
        }
        //...................................................................SET COLUMN NAME.....................................................
        can[0]=false;
        col[0]="  DATE";
         for (int i = 0; i < Config.config_stock_item.size(); i++) {
            if(!Config.config_stock_item.get(i).getIdentity().equals("M")){    
            can[row]=false;
            col[row]="  "+Config.config_stock_item.get(i).getName();
            if(col_height<col[row].length()){
                col_height=col[row].length();
            }
            row++;
            }
         }
        can[row]=false;
        col[row]="  TOTAL";
        //...................................................................SET DATE ON CELL.....................................................
        try {
            Config.sql =sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()) {
                int c=1;
                for (int i = 0; i < Config.config_stock_item.size(); i++) {
                if(!Config.config_stock_item.get(i).getIdentity().equals("M")){
                if(Config.config_stock_item.get(i).getItem_id().equals(Config.rs.getString("r.item_id"))){
                obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("r._date"))))-1][c]=Double.parseDouble(Config.rs.getString("amount").toString());
                }
                c++;
                }
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
                
        //...................................................................SET TOTAL DATE WISE.....................................................
    Double am=0.0;    
     for (int i = 0; i < total; i++) {
        Double amount=0.0;
        int j=0;
        for ( j = 1; j < size-1; j++) {
            try {
                amount=amount+Double.parseDouble(obj[i][j].toString());
            } catch (Exception e) {
            }
        }
        am=am+amount;
        obj[i][j]=amount;
    }
    txt_receive_amount.setText(String.valueOf(am));

//...................................................................SET TABLE..................................................... 
    tbl_receive_report.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return can [columnIndex];
        }
    });
    
    tbl_receive_report.getTableHeader().getColumnModel().getColumn(0).setHeaderRenderer(new RotatedTableCellRenderer(270));
    tbl_receive_report.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
    tbl_receive_report.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(100);
    tbl_receive_report.getTableHeader().setPreferredSize(new Dimension(100000,col_height*8));
    for (int j = 1; j < size-1; j++) {
      tbl_receive_report.getTableHeader().getColumnModel().getColumn(j).setHeaderRenderer(new RotatedTableCellRenderer(270));
      tbl_receive_report.getTableHeader().getColumnModel().getColumn(j).setResizable(false);
      tbl_receive_report.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(50);
    }    
    tbl_receive_report.getTableHeader().getColumnModel().getColumn(row).setHeaderRenderer(new RotatedTableCellRenderer(270));
    tbl_receive_report.getTableHeader().getColumnModel().getColumn(row).setResizable(false);
    tbl_receive_report.getTableHeader().getColumnModel().getColumn(row).setPreferredWidth(100);
            //...................................................................SET DATE ON CELL.....................................................    
    }
    
private void loadReturnReport(String sql) {
    //.........................................................................................................................................
        int col_height=0;  
        int row=1;  
        int year = jyc_return_report.getYear();
        int month = jmc_return_report.getMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        int total = calendar.getActualMaximum(Calendar.DATE);
        int t_col = 0;
        for (int i = 0; i < Config.config_stock_item.size(); i++) {
        if(!Config.config_stock_item.get(i).getIdentity().equals("M")){
            t_col++;
        }
        }
        int size= t_col+2;

        
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = new boolean [size];
        String []col = new String[size];
        Object [][]obj = new Object[total][size];
        
        //...................................................................SET FIRST COLUMN ON DATE.....................................................
for (int i = 0; i < total; i++) {
         if(i<9){
             if(month<9){
                 obj[i][0]="0"+(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]="0"+(i+1)+"/"+(month+1)+"/"+year;
             }
         }else{
             if(month<9){
                 obj[i][0]=(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]=(i+1)+"/"+(month+1)+"/"+year;
             }
         }

            for (int j = 1; j < size-3; j++) {
                obj[i][j]=0.0;
            }
        }
        //...................................................................SET COLUMN NAME.....................................................
        can[0]=false;
        col[0]="  DATE";
        
            for (int i = 0; i < Config.config_stock_item.size(); i++) {
            if(!Config.config_stock_item.get(i).getIdentity().equals("M")){    
            can[row]=false;
            col[row]="  "+Config.config_stock_item.get(i).getName();
            if(col_height<col[row].length()){
                col_height=col[row].length();
            }
            row++;
            }
            }
        can[row]=false;
        col[row]="  TOTAL";
        
        //...................................................................SET DATE ON CELL.....................................................
        try {
            Config.sql =sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()) {
                int c=1;
                for (int i = 0; i < Config.config_stock_item.size(); i++) {
                if(!Config.config_stock_item.get(i).getIdentity().equals("M")){
                if(Config.config_stock_item.get(i).getItem_id().equals(Config.rs.getString("r.item_id"))){
                obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("r._date"))))-1][c]=Double.parseDouble(Config.rs.getString("amount").toString());
                }
                c++;
                }
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        //...................................................................SET TOTAL DATE WISE.....................................................
    Double am=0.0;    
     for (int i = 0; i < total; i++) {
        Double amount=0.0;
        int j=0;
        for ( j = 1; j < size-1; j++) {
            try {
                amount=amount+Double.parseDouble(obj[i][j].toString());
            } catch (Exception e) {
            }
        }
        am=am+amount;
        obj[i][j]=amount;
    }
    txt_return_amount.setText(String.valueOf(am));

            //...................................................................SET TABLE..................................................... 
        tbl_return_report.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return can [columnIndex];
            }
        });

    tbl_return_report.getTableHeader().getColumnModel().getColumn(0).setHeaderRenderer(new RotatedTableCellRenderer(270));
    tbl_return_report.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
    tbl_return_report.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(100);
    tbl_return_report.getTableHeader().setPreferredSize(new Dimension(100000,col_height*8));
    for (int j = 1; j < size-1; j++) {
      tbl_return_report.getTableHeader().getColumnModel().getColumn(j).setHeaderRenderer(new RotatedTableCellRenderer(270));
      tbl_return_report.getTableHeader().getColumnModel().getColumn(j).setResizable(false);
      tbl_return_report.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(50);
    }    
    tbl_return_report.getTableHeader().getColumnModel().getColumn(row).setHeaderRenderer(new RotatedTableCellRenderer(270));
    tbl_return_report.getTableHeader().getColumnModel().getColumn(row).setResizable(false);
    tbl_return_report.getTableHeader().getColumnModel().getColumn(row).setPreferredWidth(100);
            //...................................................................SET DATE ON CELL.....................................................    

}   

public void loadTotalStockReport() {
    //.........................................................................................................................................
        String sql ="";
        int col_height=0;  
        int row=1;  
        int year = jyc_total_report.getYear();
        int month = jmc_total_report.getMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        int total = calendar.getActualMaximum(Calendar.DATE);
        int t_col = 0;
        for (int i = 0; i < Config.config_stock_item.size(); i++) {
        if(!Config.config_stock_item.get(i).getIdentity().equals("M")){
            t_col++;
        }
        }
        int size= t_col+2;
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = new boolean [size];
        String []col = new String[size];
        Object [][]obj = new Object[total][size];
        //...................................................................SET FIRST COLUMN ON DATE.....................................................
        for (int i = 0; i < total; i++) {
         if(i<9){
             if(month<9){
                 obj[i][0]="0"+(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]="0"+(i+1)+"/"+(month+1)+"/"+year;
             }
         }else{
             if(month<9){
                 obj[i][0]=(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]=(i+1)+"/"+(month+1)+"/"+year;
             }
         }
         

            for (int j = 1; j < size-3; j++) {
                obj[i][j]=0.0;
            }
        }
        //...................................................................SET COLUMN NAME.....................................................
        can[0]=false;
        col[0]="  DATE";
            for (int i = 0; i < Config.config_stock_item.size(); i++) {
            if(!Config.config_stock_item.get(i).getIdentity().equals("M")){    
            can[row]=false;
            col[row]="  "+Config.config_stock_item.get(i).getName();
            if(col_height<col[row].length()){
                col_height=col[row].length();
            }
            row++;
            }
            }
        can[row]=false;
        col[row]="  TOTAL";
        
        //...................................................................SET DATE ON CELL.....................................................
        if(!check_total_report.isSelected()){
            sql="select r._date,r.item_id,si.identity,sum(r.quantity*il.prise) as amount,r._month,r._year from receive_stock r\n" +
                "inner join item_list il on il.item_list_id=r.item_list_id\n" +
                "inner join stock_item si on si.item_id=r.item_id\n" +
                "group by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id\n" +
                "having  r._month='"+(jmc_total_report.getMonth()+1)+"'"
                +" and r._year='"+jyc_total_report.getYear()+"' "
                +" and si.identity!='M'" +
                "order by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id";
            txt_total_report_date.setText("\tALL");
        }else{
            sql="select r._date,r.item_id,si.identity,sum(r.quantity*il.prise) as amount,r._month,r._year from receive_stock r\n" +
                "inner join item_list il on il.item_list_id=r.item_list_id\n" +
                "inner join stock_item si on si.item_id=r.item_id\n" +
                "group by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id\n" +
                "having  r._month='"+(jmc_total_report.getMonth()+1)+"'"
                +" and r._year='"+jyc_total_report.getYear()+"' "
                + "and STR_TO_DATE(r._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_total_report.getDate())+"','%d/%m/%Y') "
                + "and si.identity!='M'" +
                "order by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id";
            txt_total_report_date.setText(simple_date_formate.format(jdc_total_report.getDate()));
        }

 try {
      Config.sql =sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()) {
                int c=1;
                for (int i = 0; i < Config.config_stock_item.size(); i++) {
                if(!Config.config_stock_item.get(i).getIdentity().equals("M")){
                if(Config.config_stock_item.get(i).getItem_id().equals(Config.rs.getString("r.item_id"))){
                obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("r._date"))))-1][c]=Double.parseDouble(Config.rs.getString("amount").toString());
                }
                c++;
                }
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
                   //...................................................................SET TOTAL DATE WISE.....................................................
        if(!check_total_report.isSelected()){
            sql="select r._date,r.item_id,si.identity,sum(r.quantity*il.prise) as amount,r._month,r._year from return_stock r\n" +
                "inner join item_list il on il.item_list_id=r.item_list_id\n" +
                "inner join stock_item si on si.item_id=r.item_id\n" +
                "group by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id\n" +
                "having  r._month='"+(jmc_total_report.getMonth()+1)+"'"
                +" and r._year='"+jyc_total_report.getYear()+"' "
                +" and si.identity!='M'" +
                "order by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id";
        }else{
            sql="select r._date,r.item_id,si.identity,sum(r.quantity*il.prise) as amount,r._month,r._year from return_stock r\n" +
                "inner join item_list il on il.item_list_id=r.item_list_id\n" +
                "inner join stock_item si on si.item_id=r.item_id\n" +
                "group by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id\n" +
                "having  r._month='"+(jmc_total_report.getMonth()+1)+"'"
                +" and r._year='"+jyc_total_report.getYear()+"' "
                + "and STR_TO_DATE(r._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_total_report.getDate())+"','%d/%m/%Y') "
                + "and si.identity!='M'" +
                "order by STR_TO_DATE(r._date,'%d/%m/%Y'),item_id";
        }
        try {
            Config.sql =sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()) {
                int c=1;
        for (int i = 0; i < Config.config_stock_item.size(); i++) {
                if(!Config.config_stock_item.get(i).getIdentity().equals("M")){
                if(Config.config_stock_item.get(i).getItem_id().equals(Config.rs.getString("r.item_id"))){
                        Double amount=0.0;
                        try {
                            if(obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("r._date"))))-1][c]==null){
                                amount=Double.parseDouble(Config.rs.getString("amount").toString());
                                obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("r._date"))))-1][c]=-amount;;
                            }else{
                                amount= Double.parseDouble(obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("r._date"))))-1][c].toString());
                                amount=amount-Double.parseDouble(Config.rs.getString("amount"));
                                obj[Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("r._date").toString())))-1][c]=amount;;
                            }
                        }catch (Exception e) {
                                e.printStackTrace();
                        }
                      }
                    c++;
                }
            }
          }
        }catch (Exception e) {
        }
        
        //...................................................................SET TOTAL DATE WISE.....................................................
Double am=0.0;    
 for (int i = 0; i < total; i++) {
    Double amount=0.0;
    int j=0;
    for ( j = 1; j < size-1; j++) {
        try {
            amount=amount+Double.parseDouble(obj[i][j].toString());
        } catch (Exception e) {
        }
    }
    am=am+amount;
    obj[i][j]=amount;
}
txt_total_report_amount.setText(String.valueOf(am));
 //...................................................................SET TABLE..................................................... 
    tbl_total_report.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return can [columnIndex];
        }
    });
    
    tbl_total_report.getTableHeader().getColumnModel().getColumn(0).setHeaderRenderer(new RotatedTableCellRenderer(270));
    tbl_total_report.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
    tbl_total_report.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(100);
    tbl_total_report.getTableHeader().setPreferredSize(new Dimension(100000,col_height*8));
    for (int j = 1; j < size-1; j++) {
      tbl_total_report.getTableHeader().getColumnModel().getColumn(j).setHeaderRenderer(new RotatedTableCellRenderer(270));
      tbl_total_report.getTableHeader().getColumnModel().getColumn(j).setResizable(false);
      tbl_total_report.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(50);
    }    
    tbl_total_report.getTableHeader().getColumnModel().getColumn(row).setHeaderRenderer(new RotatedTableCellRenderer(270));
    tbl_total_report.getTableHeader().getColumnModel().getColumn(row).setResizable(false);
    tbl_total_report.getTableHeader().getColumnModel().getColumn(row).setPreferredWidth(100);
        //...................................................................SET DATE ON CELL.....................................................    
}   

public void loadOtherStockReport() {
    //.........................................................................................................................................
        String sql ="";
        int col_height=0;  
        int row=1;  
        int year = jyc_total_report.getYear();
        int month = jmc_total_report.getMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        int total = calendar.getActualMaximum(Calendar.DATE);
        int size= 4;
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = new boolean [size];
        String []col = new String[size];
        Object [][]obj = new Object[total][size];
        //...................................................................SET FIRST COLUMN ON DATE.....................................................
        for (int i = 0; i < total; i++) {
         if(i<9){
             if(month<9){
                 obj[i][0]="0"+(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]="0"+(i+1)+"/"+(month+1)+"/"+year;
             }
         }else{
             if(month<9){
                 obj[i][0]=(i+1)+"/0"+(month+1)+"/"+year;
             }else{
                 obj[i][0]=(i+1)+"/"+(month+1)+"/"+year;
             }
         }

        }
        //...................................................................SET COLUMN NAME.....................................................
        can[0]=false;
        col[0]="DATE";
        can[1]=false;
        col[1]="LOADING CHARGES";
        can[2]=false;
        col[2]="OTHER CHARGES";
        can[3]=false;
        col[3]="TOTAL";
        
        //...................................................................SET DATE ON CELL.....................................................
        
        if(!check_other.isSelected()){
            sql="SELECT * FROM bill_type r "
                + " where r._year='"+jyc_other.getYear()+"'"
                + " and r._month='"+(jmc_other.getMonth()+1)+"'";
        }else{
            sql="SELECT * FROM bill_type r "
                + " where r._year='"+jyc_other.getYear()+"'"
                + " and r._month='"+(jmc_other.getMonth()+1)+"'"
                + " and STR_TO_DATE(r._date,'%d/%m/%Y')=STR_TO_DATE('"+simple_date_formate.format(jdc_other.getDate())+"','%d/%m/%Y')";
        }
        try {
            Config.sql =sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()) {
            int c=1;
            for (int i = 0; i < total; i++) {
                if(Integer.parseInt(sdf_day.format(simple_date_formate.parse(Config.rs.getString("r._date"))))==(i+1)){
                    //.......................................................
                    //.......................................................
                    try {
                        Double amount=0.0;
                        if(obj[i][1]==null){
                            obj[i][1]=Config.rs.getString("loading_charges");
                        }else{
                            amount=Double.parseDouble(obj[i][1].toString());
                            amount=amount+Double.parseDouble(Config.rs.getString("loading_charges"));
                            obj[i][1]=amount;
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        Double amount=0.0;
                        if(obj[i][2]==null){
                            obj[i][2]=Config.rs.getString("other_charges");
                        }else{
                            amount=Double.parseDouble(obj[i][2].toString());
                            amount=amount+Double.parseDouble(Config.rs.getString("other_charges"));
                            obj[i][2]=amount;
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        Double a=0.0;
                        try {
                            a=Double.parseDouble(obj[i][1].toString());
                        } catch (Exception e) {
                            a=0.0;
                        }
                        try {
                            a=a+Double.parseDouble(obj[i][2].toString());
                        } catch (Exception e) {
                        }
                            obj[i][3]=a;
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    

                }
            }
          }
        }catch (Exception e) {
            e.printStackTrace();
        }
        //...................................................................SET TOTAL DATE WISE.....................................................
        
        //...................................................................SET TOTAL DATE WISE.....................................................
//Double am=0.0;    
// for (int i = 0; i < total; i++) {
//    Double amount=0.0;
//    int j=0;
//    for ( j = 1; j < size-1; j++) {
//        try {
//            amount=amount+Double.parseDouble(obj[i][j].toString());
//        } catch (Exception e) {
//        }
//    }
//    am=am+amount;
//    obj[i][j]=amount;
//}
//txt_total_report_amount.setText(String.valueOf(am));
 //...................................................................SET TABLE..................................................... 
tbl_other.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return can [columnIndex];
        }
    });
    
tbl_other.getTableHeader().getColumnModel().getColumn(0).setResizable(false);
tbl_other.getTableHeader().getColumnModel().getColumn(0).setPreferredWidth(100);

tbl_other.getTableHeader().getColumnModel().getColumn(1).setResizable(false);
tbl_other.getTableHeader().getColumnModel().getColumn(1).setPreferredWidth(100);

tbl_other.getTableHeader().getColumnModel().getColumn(2).setResizable(false);
tbl_other.getTableHeader().getColumnModel().getColumn(2).setPreferredWidth(100);

tbl_other.getTableHeader().getColumnModel().getColumn(3).setResizable(false);
tbl_other.getTableHeader().getColumnModel().getColumn(3).setPreferredWidth(100);

tbl_other.getTableHeader().getColumnModel().getColumn(4).setResizable(false);
tbl_other.getTableHeader().getColumnModel().getColumn(4).setPreferredWidth(100);

tbl_other.getTableHeader().getColumnModel().getColumn(5).setResizable(false);
tbl_other.getTableHeader().getColumnModel().getColumn(5).setPreferredWidth(100);

        //...................................................................SET DATE ON CELL.....................................................    
}   
}
//class CustomTableCellRenderer extends DefaultTableCellRenderer {
//
//    @Override
//        public Component getTableCellRendererComponent (JTable table, Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
//            Component cell = super.getTableCellRendererComponent(table, obj, isSelected, hasFocus, row, column);            
//            try {
//                if (isSelected) {
//                    cell.setBackground(Color.BLACK);                    
//                } else {
//               for (int i = 0; i < table.getRowCount(); i++) {
//                Config.sql ="SELECT * FROM bill_type b where bill_type_id="+table.getValueAt(i, table.getSelectedRow()).toString();
//                Config.rs = Config.stmt.executeQuery(Config.sql);
//                if(Config.rs.next()) {
//                    if(!Config.rs.getString("message_id").equals("")){
//                        if(!Config.rs.getString("bill_no").equals("") ){
//                            cell.setBackground(Color.BLUE);
//                        }else{
//                            cell.setBackground(Color.BLUE);
//                        }
//                    }else{
//                        if(!Config.rs.getString("bill_no").equals("") ){
//                            cell.setBackground(Color.BLUE);
//                        }else{
//                         cell.setBackground(Color.BLUE);   
//                        }
//                    }
//                }
//               }
//                    
////                    if (Integer.parseInt(table.getValueAt(row, 0).toString()) == 0) {
////                        cell.setBackground(Color.BLUE);
////                    } else if (Integer.parseInt(table.getValueAt(row, 7). toString()) != 0 && Float.parseFloat(table.getValueAt(row, 10). toString()) == 0) {
////                        cell.setBackground(new java.awt.Color(255,245,158));
////                    } else if (Integer.parseInt(table.getValueAt(row, 7). toString()) == 0 && Float.parseFloat(table.getValueAt(row, 10). toString()) != 0) {
////                        cell.setBackground(Color.CYAN);                    
////                    } else {
////                        cell.setBackground(Color.WHITE);
////                    }
//                }
//            } catch (Exception e) {
//                cell.setBackground(Color.WHITE);
//            }
//            return cell;
//        }
//
//         
//    }
