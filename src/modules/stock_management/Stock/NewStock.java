package modules.stock_management.Stock;

import data_manager.BillType;
import data_manager.ReceiveStock;
import data_manager.configuration.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

public class NewStock extends javax.swing.JDialog {
    String item_id = "";
    String supplier_id = "";
    String item_list_id = "";
    String message_id = "";
    String bill_type_id ="";
    DefaultTableModel tbl_stock_model = null;
    
    SimpleDateFormat simple_date_formate = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf_month = new SimpleDateFormat("M");
    SimpleDateFormat sdf_year = new SimpleDateFormat("Y");
    
    public NewStock(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        tbl_stock_model = (DefaultTableModel) tbl_stock.getModel();
       // setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icon.png")));

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        btn_reset = new javax.swing.JButton();
        btn_save = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        panal = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txt_name = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_suppliername = new javax.swing.JTextField();
        txt_price = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_unit = new javax.swing.JTextField();
        txt_quantity = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        cb_Item_type = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        btn_s_reset = new javax.swing.JButton();
        btn_add = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txt_challannno = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        txt_loading = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txt_other = new javax.swing.JTextField();
        txt_invoiceno = new javax.swing.JTextField();
        btn_o_reset = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_stock = new javax.swing.JTable();
        btn_remove = new javax.swing.JButton();
        btn_tbl_reset = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jdc_date_stock = new com.toedter.calendar.JDateChooser();
        btn_message = new javax.swing.JButton();

        jPanel3.setBackground(new java.awt.Color(230, 79, 6));

        jLabel22.setBackground(new java.awt.Color(255, 255, 255));
        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Employee Management");

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel22)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel3Layout.createSequentialGroup()
                .add(6, 6, 6)
                .add(jLabel22, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTextField2.setText("jTextField2");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Payroll Management - New Profile");
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        btn_reset.setText("Reset");
        btn_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_resetActionPerformed(evt);
            }
        });

        btn_save.setText("Save");
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });
        btn_save.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_saveKeyPressed(evt);
            }
        });

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });
        btn_cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_cancelKeyPressed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        panal.setBackground(new java.awt.Color(255, 255, 255));
        panal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Stock Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(51, 51, 51))); // NOI18N

        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Producte Name");

        txt_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nameActionPerformed(evt);
            }
        });
        txt_name.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_nameKeyPressed(evt);
            }
        });

        jLabel8.setForeground(new java.awt.Color(51, 51, 51));
        jLabel8.setText("Supplier Name");

        txt_suppliername.setEditable(false);
        txt_suppliername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_suppliernameActionPerformed(evt);
            }
        });
        txt_suppliername.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_suppliernameKeyPressed(evt);
            }
        });

        txt_price.setEditable(false);
        txt_price.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_priceActionPerformed(evt);
            }
        });

        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Unit");

        txt_unit.setEditable(false);
        txt_unit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_unitKeyPressed(evt);
            }
        });

        txt_quantity.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_quantityCaretUpdate(evt);
            }
        });
        txt_quantity.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_quantityFocusLost(evt);
            }
        });

        jLabel14.setText("Price");

        jLabel24.setText("Quantity");

        cb_Item_type.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-Select-", "Stock Item", "Sada flavour", "Manufacture Item" }));
        cb_Item_type.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_Item_typeActionPerformed(evt);
            }
        });

        jLabel7.setText("Item Type");

        btn_s_reset.setText("Reset");
        btn_s_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_s_resetActionPerformed(evt);
            }
        });

        btn_add.setText("Add");
        btn_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout panalLayout = new org.jdesktop.layout.GroupLayout(panal);
        panal.setLayout(panalLayout);
        panalLayout.setHorizontalGroup(
            panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panalLayout.createSequentialGroup()
                .addContainerGap()
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, panalLayout.createSequentialGroup()
                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(cb_Item_type, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 104, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabel7))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(txt_name)
                            .add(panalLayout.createSequentialGroup()
                                .add(jLabel6)
                                .add(0, 93, Short.MAX_VALUE))))
                    .add(panalLayout.createSequentialGroup()
                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel8)
                            .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, txt_suppliername)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, txt_unit, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel4)))
                        .add(10, 10, 10)
                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(txt_quantity)
                            .add(txt_price)
                            .add(panalLayout.createSequentialGroup()
                                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(jLabel24)
                                    .add(jLabel14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 37, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .add(0, 0, Short.MAX_VALUE))))
                    .add(panalLayout.createSequentialGroup()
                        .add(btn_s_reset)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(btn_add)))
                .addContainerGap())
        );

        panalLayout.linkSize(new java.awt.Component[] {btn_add, btn_s_reset}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        panalLayout.setVerticalGroup(
            panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panalLayout.createSequentialGroup()
                .add(9, 9, 9)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(panalLayout.createSequentialGroup()
                        .add(20, 20, 20)
                        .add(txt_name, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(panalLayout.createSequentialGroup()
                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel7)
                            .add(jLabel6))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(cb_Item_type, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(8, 8, 8)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel8)
                    .add(jLabel14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(3, 3, 3)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, panalLayout.createSequentialGroup()
                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(txt_suppliername, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(txt_price, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jLabel4)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(txt_unit, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(panalLayout.createSequentialGroup()
                        .add(34, 34, 34)
                        .add(jLabel24)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(txt_quantity, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 22, Short.MAX_VALUE)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btn_s_reset)
                    .add(btn_add))
                .addContainerGap())
        );

        jLabel17.setForeground(new java.awt.Color(51, 51, 51));
        jLabel17.setText("Invoice No.");

        jLabel16.setForeground(new java.awt.Color(51, 51, 51));
        jLabel16.setText("Challan No.");

        txt_challannno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_challannnoKeyPressed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setText("Other Details");

        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Loading Charges");

        txt_loading.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_loadingCaretUpdate(evt);
            }
        });
        txt_loading.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_loadingFocusLost(evt);
            }
        });

        jLabel18.setForeground(new java.awt.Color(51, 51, 51));
        jLabel18.setText("Other Charges");

        txt_other.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_otherCaretUpdate(evt);
            }
        });
        txt_other.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_otherFocusLost(evt);
            }
        });
        txt_other.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_otherKeyPressed(evt);
            }
        });

        btn_o_reset.setText("Reset");
        btn_o_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_o_resetActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jSeparator1)
            .add(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(txt_invoiceno)
                    .add(txt_loading)
                    .add(txt_other)
                    .add(jPanel4Layout.createSequentialGroup()
                        .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel21)
                            .add(jLabel5)
                            .add(jLabel18))
                        .add(0, 0, Short.MAX_VALUE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel4Layout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(jLabel17)
                                .add(jLabel16)
                                .add(txt_challannno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 135, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, btn_o_reset))))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel21)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(1, 1, 1)
                .add(jLabel17)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(txt_invoiceno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(txt_challannno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel5)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(txt_loading, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jLabel18)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(txt_other, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(btn_o_reset)
                .add(57, 57, 57))
        );

        jPanel5.setBackground(new java.awt.Color(230, 79, 6));

        jLabel20.setBackground(new java.awt.Color(255, 255, 255));
        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("New Stock");

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 120, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jLabel20, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        tbl_stock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PRODUCT NAME", "DATE", "PRICE", "QUANTITY", "TOTAL", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_stock.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tbl_stock);
        if (tbl_stock.getColumnModel().getColumnCount() > 0) {
            tbl_stock.getColumnModel().getColumn(0).setResizable(false);
            tbl_stock.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_stock.getColumnModel().getColumn(1).setResizable(false);
            tbl_stock.getColumnModel().getColumn(1).setPreferredWidth(150);
            tbl_stock.getColumnModel().getColumn(2).setResizable(false);
            tbl_stock.getColumnModel().getColumn(2).setPreferredWidth(150);
            tbl_stock.getColumnModel().getColumn(3).setResizable(false);
            tbl_stock.getColumnModel().getColumn(3).setPreferredWidth(150);
            tbl_stock.getColumnModel().getColumn(4).setResizable(false);
            tbl_stock.getColumnModel().getColumn(4).setPreferredWidth(100);
            tbl_stock.getColumnModel().getColumn(5).setMinWidth(0);
            tbl_stock.getColumnModel().getColumn(5).setPreferredWidth(0);
            tbl_stock.getColumnModel().getColumn(5).setMaxWidth(0);
            tbl_stock.getColumnModel().getColumn(6).setMinWidth(0);
            tbl_stock.getColumnModel().getColumn(6).setPreferredWidth(0);
            tbl_stock.getColumnModel().getColumn(6).setMaxWidth(0);
        }

        btn_remove.setText("Remove");
        btn_remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_removeActionPerformed(evt);
            }
        });

        btn_tbl_reset.setText("Reset");
        btn_tbl_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_tbl_resetActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jScrollPane1)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(btn_remove)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 321, Short.MAX_VALUE)
                        .add(btn_tbl_reset)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btn_remove)
                    .add(btn_tbl_reset))
                .addContainerGap())
        );

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setText("Date :");

        jdc_date_stock.setDateFormatString("dd/MM/yyyy");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(panal, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(10, 10, 10)
                                .add(jLabel10)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jdc_date_stock, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 113, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                .add(jPanel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 265, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(jdc_date_stock, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jLabel10, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(panal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btn_message.setText("Message");
        btn_message.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_messageActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel6Layout = new org.jdesktop.layout.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .add(btn_reset)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(btn_message)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btn_save, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 67, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btn_cancel)
                .addContainerGap())
            .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btn_cancel)
                    .add(btn_save)
                    .add(btn_reset)
                    .add(btn_message))
                .addContainerGap())
        );

        getRootPane().setDefaultButton(btn_save);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
        
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void txt_nameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nameKeyPressed
       
    }//GEN-LAST:event_txt_nameKeyPressed

    private void txt_suppliernameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_suppliernameKeyPressed
        
    }//GEN-LAST:event_txt_suppliernameKeyPressed

    private void txt_challannnoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_challannnoKeyPressed
       
    }//GEN-LAST:event_txt_challannnoKeyPressed

    private void txt_otherKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_otherKeyPressed
        
    }//GEN-LAST:event_txt_otherKeyPressed

    private void btn_saveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_saveKeyPressed
        
    }//GEN-LAST:event_btn_saveKeyPressed

    private void btn_cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_cancelKeyPressed
       
    }//GEN-LAST:event_btn_cancelKeyPressed

    private void txt_unitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_unitKeyPressed

    }//GEN-LAST:event_txt_unitKeyPressed

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
          try {
                String str = checkValidityTable();
                if(str.equals("ok")){
                    BillType bt = new BillType();
                    bt.setBill_no(txt_invoiceno.getText());
                    bt.setChalan_no(txt_challannno.getText());
                    bt.setLoading_charges(txt_loading.getText());
                    bt.setOther_charges(txt_other.getText());
                    bt.setDate(simple_date_formate.format(jdc_date_stock.getDate()));
                    bt.setMonth(sdf_month.format(simple_date_formate.parse(simple_date_formate.format(jdc_date_stock.getDate()))));
                    bt.setYear(sdf_year.format(simple_date_formate.parse(simple_date_formate.format(jdc_date_stock.getDate()))));
                    bt.setMessage_id(message_id);
                    if(Config.bill_type_mgr.insBillType(bt)){
                        try {
                        Config.sql ="select max(bill_type_id) from bill_type";
                        Config.rs = Config.stmt.executeQuery(Config.sql);
                        if(Config.rs.next()) {
                        bill_type_id = Config.rs.getString(1);
                        }
                        } catch (Exception e) {
                        e.printStackTrace();
                        }
                            ArrayList<ReceiveStock> arList_rs = new ArrayList<>();
                            for (int i = 0; i < tbl_stock_model.getRowCount(); i++) {
                            ReceiveStock rs = new ReceiveStock();
                            rs.setItem_id(tbl_stock_model.getValueAt(i, 5).toString());
                            rs.setItem_list_id(tbl_stock_model.getValueAt(i, 6).toString());
                            rs.setDate(tbl_stock_model.getValueAt(i, 1).toString());
                            rs.setMonth(sdf_month.format(simple_date_formate.parse(tbl_stock_model.getValueAt(i, 1).toString())));
                            rs.setYear(sdf_year.format(simple_date_formate.parse(tbl_stock_model.getValueAt(i, 1).toString())));
                            rs.setQuantity(tbl_stock_model.getValueAt(i, 3).toString());
                            rs.setBill_type_id(bill_type_id);
                            arList_rs.add(rs);
                            }
                        if(Config.receive_stock_mgr.insReceiveStock(arList_rs)){
                            onloadReset();
                            Config.stock_management.onloadReset();
                            JOptionPane.showMessageDialog(this, "Stock List created successfully.", "Creation successful", JOptionPane.NO_OPTION);
                        }else{
                            JOptionPane.showMessageDialog(this, "Error in Stock List creation", "Error", JOptionPane.ERROR_MESSAGE);
                        }  
                    }else{
                    JOptionPane.showMessageDialog(this, "Error in Stock List ", "Error", JOptionPane.ERROR_MESSAGE);
                    } 
               }else{
                JOptionPane.showMessageDialog(this, "Check '"+str+"' field.", "Error", JOptionPane.ERROR_MESSAGE);
               }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_saveActionPerformed

    private void cb_Item_typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_Item_typeActionPerformed
        txt_unit.setText("");
        if(cb_Item_type.getSelectedIndex()!=0){
            switch(cb_Item_type.getSelectedIndex()){
                case 1:
                break;
                case 2:
                break;
                case 3:
                break;
            }
        }
    }//GEN-LAST:event_cb_Item_typeActionPerformed

    private void txt_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nameActionPerformed
     if(cb_Item_type.getSelectedIndex()!=0){
         Config.item_search.onloadReset(1, cb_Item_type.getSelectedIndex());
         Config.item_search.setVisible(true);
     }else{
         JOptionPane.showMessageDialog(this, "Select Item Type ", "Error", JOptionPane.ERROR_MESSAGE);   
     }
    }//GEN-LAST:event_txt_nameActionPerformed

    private void txt_suppliernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_suppliernameActionPerformed
        
    }//GEN-LAST:event_txt_suppliernameActionPerformed
    private void btn_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addActionPerformed
        try {
        String str = checkValidity();
         if (str.equals("ok")) {
                    tbl_stock_model.addRow(new Object[]{
                    txt_name.getText(),
                    simple_date_formate.format(jdc_date_stock.getDate()),
                    txt_price.getText(),
                    txt_quantity.getText(),
                    Double.parseDouble(txt_price.getText())*Double.parseDouble(txt_quantity.getText()),
                    item_id,
                    item_list_id
                    });
                    JOptionPane.showMessageDialog(this, "Stock List created successfully.", "Creation successful", JOptionPane.NO_OPTION);
                    onloadReset_StockDetails();
                }else{
                JOptionPane.showMessageDialog(this, "Check '"+str+"' field.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception e) {
                e.printStackTrace();
        }
    }//GEN-LAST:event_btn_addActionPerformed

    private void btn_s_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_s_resetActionPerformed
        onloadReset_StockDetails();
    }//GEN-LAST:event_btn_s_resetActionPerformed

    private void btn_o_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_o_resetActionPerformed
        onloadReset_OtherDetails();
    }//GEN-LAST:event_btn_o_resetActionPerformed

    private void btn_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_resetActionPerformed
        onloadReset();
    }//GEN-LAST:event_btn_resetActionPerformed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void btn_tbl_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_tbl_resetActionPerformed
        onloadReset();
    }//GEN-LAST:event_btn_tbl_resetActionPerformed

    private void btn_removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_removeActionPerformed
        try {
            tbl_stock_model.removeRow(tbl_stock.getSelectedRow());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select any Row ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_removeActionPerformed

    private void btn_messageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_messageActionPerformed
      if(message_id.equals("")){
           Config.new_message.onloadReset(0);
           Config.new_message.setVisible(true);
       }else{
           Config.view_message.onloadReset(0,message_id);
           Config.view_message.setVisible(true);
       }
    }//GEN-LAST:event_btn_messageActionPerformed

    private void txt_priceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_priceActionPerformed
     if(!item_id.equals("")){
         Config.rate_search.onloadReset(1, item_id);
         Config.rate_search.setVisible(true);
     }else{
         JOptionPane.showMessageDialog(this, "Select Item Categories ", "Error", JOptionPane.ERROR_MESSAGE);   
     }
    }//GEN-LAST:event_txt_priceActionPerformed

    private void txt_quantityCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_quantityCaretUpdate
        
    }//GEN-LAST:event_txt_quantityCaretUpdate

    private void txt_loadingCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_loadingCaretUpdate
        
    }//GEN-LAST:event_txt_loadingCaretUpdate

    private void txt_otherCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_otherCaretUpdate
        
    }//GEN-LAST:event_txt_otherCaretUpdate

    private void txt_quantityFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_quantityFocusLost
        if(!txt_quantity.getText().trim().equals("")){
        try {
        Double.parseDouble(txt_quantity.getText().trim());
        }catch (Exception e) {
        JOptionPane.showMessageDialog(this, "Only No.Value Required ", "Error", JOptionPane.ERROR_MESSAGE);   
        txt_quantity.setText("");
        txt_quantity.requestFocus();
        }
        }
    }//GEN-LAST:event_txt_quantityFocusLost

    private void txt_loadingFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_loadingFocusLost
if(!txt_loading.getText().trim().equals("")){
        try {
        Double.parseDouble(txt_loading.getText().trim());
        }catch (Exception e) {
        JOptionPane.showMessageDialog(this, "Only No.Value Required ", "Error", JOptionPane.ERROR_MESSAGE);   
        txt_loading.setText("");
        txt_loading.requestFocus();
        }
        }                

    }//GEN-LAST:event_txt_loadingFocusLost

    private void txt_otherFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_otherFocusLost
        if(!txt_other.getText().trim().equals("")){
        try {
        Double.parseDouble(txt_loading.getText().trim());
        }catch (Exception e) {
        JOptionPane.showMessageDialog(this, "Only No.Value Required ", "Error", JOptionPane.ERROR_MESSAGE);   
        txt_other.setText("");
        txt_other.requestFocus();
        }
        }                
    }//GEN-LAST:event_txt_otherFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_add;
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_message;
    private javax.swing.JButton btn_o_reset;
    private javax.swing.JButton btn_remove;
    private javax.swing.JButton btn_reset;
    private javax.swing.JButton btn_s_reset;
    private javax.swing.JButton btn_save;
    private javax.swing.JButton btn_tbl_reset;
    private javax.swing.JComboBox cb_Item_type;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jTextField2;
    private com.toedter.calendar.JDateChooser jdc_date_stock;
    private javax.swing.JPanel panal;
    private javax.swing.JTable tbl_stock;
    private javax.swing.JTextField txt_challannno;
    private javax.swing.JTextField txt_invoiceno;
    private javax.swing.JTextField txt_loading;
    private javax.swing.JTextField txt_name;
    private javax.swing.JTextField txt_other;
    private javax.swing.JTextField txt_price;
    private javax.swing.JTextField txt_quantity;
    private javax.swing.JTextField txt_suppliername;
    private javax.swing.JTextField txt_unit;
    // End of variables declaration//GEN-END:variables
    
    public void onloadReset() {
        onloadReset_StockDetails();
        onloadReset_OtherDetails();
        tbl_stock_model.setRowCount(0);
        jdc_date_stock.setDate(Calendar.getInstance().getTime());
    }
    public void onloadReset_StockDetails(){
        cb_Item_type.setSelectedIndex(0);
        txt_name.setText("");
        txt_suppliername.setText("");
        txt_price.setText("");
        txt_unit.setText("");
        txt_quantity.setText("");
    }
    public void onloadReset_OtherDetails(){
        txt_invoiceno.setText("");
        txt_challannno.setText("");
        txt_loading.setText("");
        txt_other.setText("");
    }
    private String checkValidity(){
        if(cb_Item_type.getSelectedIndex()==0){
        return "Item Type";    
        }else if(txt_name.getText().equals("")){
        return "Item Name";    
        }else if(jdc_date_stock.getDate().equals("")){
        return "Date ";    
        }else if (Double.parseDouble(txt_quantity.getText())==0.0) {
            return "Quantity";
        }else{
        return "ok";    
        }
    }
    private String checkValidityInvoiceDetails(){
        if(cb_Item_type.getSelectedIndex()==1){
        if(txt_invoiceno.getText().equals("") &&  txt_challannno.getText().equals("")){
            return "Invoice No.";    
        }
        return "ok";
        }
        else{
        return "ok";    
        }
    }
    
    public void setData(String item_id) {
        this.item_id = item_id;
        txt_name.setText(Config.config_stock_item.get(Config.id_stock_item.indexOf(item_id)).getName());
        int i=0;
        for ( i = 0; i < Config.config_item_list.size(); i++) {
            if(Config.config_item_list.get(i).getItem_id().equals(item_id)){
            item_list_id=Config.config_item_list.get(i).getItem_list_id();
             break;   
            }
        }
       if(i!=Config.config_item_list.size()){ 
       try{
       txt_suppliername.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(Config.config_item_list.get(i).getSupplier_id())).getProfile_id())).getName());}catch(Exception e){e.printStackTrace();}
       txt_unit.setText(Config.config_stock_item.get(Config.id_stock_item.indexOf(item_id)).getUnit());
       txt_price.setText(Config.config_item_list.get(i).getPrise());
       }else{
           txt_suppliername.setText("");
           txt_price.setText("");
           txt_unit.setText("");
           JOptionPane.showMessageDialog(this, "Select Item Price is not configure", "Error", JOptionPane.ERROR_MESSAGE);   
       }
     }
     public void setMessage(String message_id){
         this.message_id=message_id;
     }

    private String checkValidityTable() {
        if(tbl_stock_model.getRowCount()==0){
            return "Table";
        }else{
            return "ok";
        }
    }

    public void setItemListId(String item_list_id,String price) {
        this.item_list_id=item_list_id;
        txt_price.setText(price);
        try {
            Config.sql="select  supplier_id from item_list where item_list_id="+item_list_id;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if (Config.rs.next()) {
            txt_suppliername.setText(Config.config_profile_details.get(Config.id_profile_details.indexOf(Config.config_supplier_details.get(Config.id_supplier_details.indexOf(Config.rs.getString("supplier_id"))).getProfile_id())).getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     
}
