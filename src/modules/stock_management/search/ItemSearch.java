package modules.stock_management.search;

import data_manager.StockItem;
import data_manager.configuration.Config;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;


public class ItemSearch extends javax.swing.JDialog {
    int module = 0;
    int index = 0;
    DefaultTableModel search_model = null;
    boolean b =true;        
    String c="";
    public ItemSearch(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        search_model = (DefaultTableModel) tbl_search.getModel();
        tbl_search.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Enter");
        KeyboardFocusManager.getCurrentKeyboardFocusManager()
        .addKeyEventDispatcher(new KeyEventDispatcher() {
        public boolean dispatchKeyEvent(KeyEvent evt) {
        if(!(evt.getKeyCode() ==KeyEvent.VK_ENTER ||evt.getKeyCode() ==KeyEvent.VK_DOWN || evt.getKeyCode() ==KeyEvent.VK_UP )){
        if (b){
                c = String.valueOf(evt.getKeyChar());
                    txt_findname.setText(c);
                b =false;
              }
         txt_findname.requestFocus();
        }else{
            tbl_search.requestFocus();
            b =true;
            }
            return false;}});
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_findname = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_search = new javax.swing.JTable();

        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Find Name  ? >>    [");

        txt_findname.setBackground(new java.awt.Color(240, 240, 240));
        txt_findname.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txt_findname.setBorder(null);
        txt_findname.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_findnameCaretUpdate(evt);
            }
        });
        txt_findname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_findnameActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("]");

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        tbl_search.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_search.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "ITEM NAME"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_search.setRowHeight(20);
        tbl_search.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_search.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_searchMouseClicked(evt);
            }
        });
        tbl_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_searchKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_search);
        if (tbl_search.getColumnModel().getColumnCount() > 0) {
            tbl_search.getColumnModel().getColumn(0).setResizable(false);
            tbl_search.getColumnModel().getColumn(0).setPreferredWidth(100);
            tbl_search.getColumnModel().getColumn(1).setResizable(false);
            tbl_search.getColumnModel().getColumn(1).setPreferredWidth(300);
        }

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_findname, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addContainerGap())
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(txt_findname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void txt_findnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_findnameActionPerformed
       
    }//GEN-LAST:event_txt_findnameActionPerformed

    private void txt_findnameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_findnameCaretUpdate
        search();
    }//GEN-LAST:event_txt_findnameCaretUpdate

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
    }//GEN-LAST:event_formKeyPressed

    private void tbl_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_searchKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            switch(module){
                case 1:
                Config.new_stock.setData(search_model.getValueAt(tbl_search.getSelectedRow(), 0).toString());    
                break;    
                case 2:
                Config.new_demage_item.setData(search_model.getValueAt(tbl_search.getSelectedRow(), 0).toString());        
                break;    
                case 3:
                Config.new_return_item.setData(search_model.getValueAt(tbl_search.getSelectedRow(), 0).toString());        
                break; 
                case 4:
                Config.new_distribution.setData(search_model.getValueAt(tbl_search.getSelectedRow(), 0).toString());        
                break; 
                case 5:
                Config.view_distribution_item.setData(search_model.getValueAt(tbl_search.getSelectedRow(), 0).toString());        
                break; 
                case 6:
                Config.distribution_management.setData(search_model.getValueAt(tbl_search.getSelectedRow(), 0).toString());        
                break; 
                case 7:
                Config.sale_management.setSaleReportDesignationWiseItem(search_model.getValueAt(tbl_search.getSelectedRow(), 0).toString());        
                break; 
             }
            dispose();
        }
    }//GEN-LAST:event_tbl_searchKeyPressed

    private void tbl_searchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_searchMouseClicked
        
    }//GEN-LAST:event_tbl_searchMouseClicked
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbl_search;
    private javax.swing.JTextField txt_findname;
    // End of variables declaration//GEN-END:variables

    public void onloadReset(int module,int index){
        this.module=module;
        this.index =index;
        txt_findname.setText("");
    }

    private void search() {
        try {
            search_model.setRowCount(0);
        if(!txt_findname.getText().trim().equals("")){
        for (int i = 0; i < Config.config_stock_item.size(); i++) {
            StockItem cp =Config.config_stock_item.get(i);
            switch(index){
            case 1:
            if(cp.getIdentity().equals("ST")){    
            if(cp.getName().toLowerCase().startsWith(txt_findname.getText().trim().toLowerCase()) ){
            search_model.addRow(new Object []{
                cp.getItem_id(),
                cp.getName()
            });    
            }
            }
                break;
            case 2:
            if(cp.getIdentity().equals("S")){        
                if(cp.getName().toLowerCase().startsWith(txt_findname.getText().trim().toLowerCase()) ){
            search_model.addRow(new Object []{
                cp.getItem_id(),
                cp.getName()
            });    
            }
            }
                break;    
            case 3:
            if(cp.getIdentity().equals("M")){        
                if(cp.getName().toLowerCase().startsWith(txt_findname.getText().trim().toLowerCase()) ){
            search_model.addRow(new Object []{
                cp.getItem_id(),
                cp.getName()
            });    
            }
            }    
            break;    
        }
            
        }
        }else{
        for (int i = 0; i < Config.config_stock_item.size(); i++) {
            StockItem cp =Config.config_stock_item.get(i);
            System.out.println(index);
            switch(index){
            case 1:
            if(cp.getIdentity().equals("ST")){    
            search_model.addRow(new Object [] {
               cp.getItem_id(),
               cp.getName()
            });
            }
                break;
            case 2:
            if(cp.getIdentity().equals("S")){        
            search_model.addRow(new Object [] {
               cp.getItem_id(),
               cp.getName()
            });
            }
                break;    
            case 3:
            if(cp.getIdentity().equals("M")){        
                System.out.println(cp.getItem_id());    
            search_model.addRow(new Object [] {
               cp.getItem_id(),
               cp.getName()
            });
            }    
                break;    
        }
        }    
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
