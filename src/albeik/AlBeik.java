package albeik;

import class_manager.configuration.ConfigMgr;
import data_manager.configuration.Config;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Akshay
 */
public class AlBeik {
    
    banner banner;
    
    public AlBeik() {
        banner = new banner();
        Config.config_mgr = new ConfigMgr();
    }    
    private void showbanner() {        
        banner.setVisible(true);
        banner.setBannerLabel("Checking system configuration...");
    }
    
    private void connserver() {
        banner.setBannerLabel("Connecting to server...");        
    }
    
    private void conndatabase() {
        banner.setBannerLabel("Checking databse connection...");
        if (!Config.config_mgr.loadDatabase()) {
            JOptionPane.showMessageDialog(banner,"Database connectivity problem, product can not be launch.","Error : 10068.",JOptionPane.ERROR_MESSAGE);            
            System.exit(0);
        }
    }
    
    private void initcomponent() {
        banner.setBannerLabel("Initialising components...");        
        int x=0;
        if (Config.config_mgr.loadStockItem()){x++;} else {System.out.println("Error : 1, Stock Item loading problem.");}
        if (Config.config_mgr.loadBikeDetails()){x++;} else {System.out.println("Error : 2, Bike loading problem.");}
        if (Config.config_mgr.loadDesignationDetails()){x++;} else {System.out.println("Error : 3, Designation loading problem.");}
        if (Config.config_mgr.loadSupplierDetails()){x++;} else {System.out.println("Error : 4, Supplier loading problem.");}
        if (Config.config_mgr.loadCustomerDetails()){x++;} else {System.out.println("Error : 5, Cutomer loading problem.");}
        if (Config.config_mgr.loadProfileDetails()){x++;} else {System.out.println("Error : 6, Profile loading problem.");}
        if (Config.config_mgr.loadAddressDetails()){x++;} else {System.out.println("Error : 7, Address loading problem.");}
        if (Config.config_mgr.loadContactDetails()){x++;} else {System.out.println("Error : 8, Contact loading problem.");}
        if (Config.config_mgr.loadIdentityDetails()){x++;} else {System.out.println("Error : 9, Identity loading problem.");}
        if (Config.config_mgr.loadEmployeeDetails()){x++;} else {System.out.println("Error : 10, Employee loading problem.");}
        if (Config.config_mgr.loadExpensesDetails()){x++;} else {System.out.println("Error : 11, Expenses loading problem.");}
        if (Config.config_mgr.loadItemList()){x++;} else {System.out.println("Error : 12, ItemList loading problem.");}
        if (Config.config_mgr.loadSalaryDetails()){x++;} else {System.out.println("Error : 13, Salary Details loading problem.");}
        if (Config.config_mgr.loadStockDetails()){x++;} else {System.out.println("Error : 14, Stock Details loading problem.");}
        if (Config.config_mgr.loadMaintenanceType()){x++;} else {System.out.println("Error : 15, Maintenance Details loading problem.");}
        if (Config.config_mgr.loadClassManager()){x++;} else {System.out.println("Error : 16, ClassManager loading problem.");}
        if (Config.config_mgr.loadForms()){x++;} else {System.out.println("Error : 17, Forms loading problem.");}
                    
        if (x!=17) {
            JOptionPane.showMessageDialog(banner,"Product initialisation problem, product can not be launch.","Error.",JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }
    
    private void hidebanner() {
        banner.dispose();
        Config.home.onloadReset();
        Config.home.setVisible(true);
        
    }
    
    public static void main(String[] args) {
        try {        
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AlBeik.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(AlBeik.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(AlBeik.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(AlBeik.class.getName()).log(Level.SEVERE, null, ex);
        }               
        try {            
            AlBeik obj = new AlBeik();
            obj.showbanner();        
            Thread.sleep(1000);
            obj.connserver();
            Thread.sleep(1000);
            obj.conndatabase();
            Thread.sleep(1000);
            obj.initcomponent();
            Thread.sleep(1000);
            obj.hidebanner();            
        } catch (InterruptedException ex) {
            Logger.getLogger(AlBeik.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
