package albeik;

import java.awt.Toolkit;

/**
 *
 * @author AMS
 */
public class banner extends javax.swing.JFrame {
    /**
     * Creates new form banner
     */
    public banner() {
        initComponents();        
        this.setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icon.png")));
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblShow = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);

        lblShow.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblShow.setForeground(new java.awt.Color(255, 255, 255));
        lblShow.setText("...");
        getContentPane().add(lblShow);
        lblShow.setBounds(420, 220, 170, 30);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/banner.PNG"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 590, 250);

        setSize(new java.awt.Dimension(590, 251));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblShow;
    // End of variables declaration//GEN-END:variables
    
    void setBannerLabel(String lable) {
        lblShow.setText(lable);
    }
}
